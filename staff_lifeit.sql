-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 27-09-2016 a las 16:28:04
-- Versión del servidor: 5.6.27-0ubuntu0.15.04.1
-- Versión de PHP: 5.6.4-4ubuntu6.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `staff_lifeit`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
`id` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contenido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagen_fondo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_boton1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto_boton2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_boton1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_boton2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `banner`
--

INSERT INTO `banner` (`id`, `titulo`, `contenido`, `imagen`, `imagen_fondo`, `texto_boton1`, `texto_boton2`, `url_boton1`, `url_boton2`) VALUES
(1, '*Show time*TV headphones', '¡Deja de pelear por el volumen de la televisión de una vez por todas!', 'uploads/banner/b1-img.png', 'uploads/banner/b1-bg.jpg', 'Descubre más', 'Comprar ahora', 'http://localhost:9001/categorias/', 'http://localhost:9001/categorias/masajes/sub1/cojin-masajero');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bolsa_trabajo`
--

CREATE TABLE IF NOT EXISTS `bolsa_trabajo` (
`id` int(11) NOT NULL,
  `nombres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `documento` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` longtext COLLATE utf8_unicode_ci NOT NULL,
  `cv` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ruta_cv` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `provincia_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `bolsa_trabajo`
--

INSERT INTO `bolsa_trabajo` (`id`, `nombres`, `documento`, `email`, `telefono`, `cv`, `ruta_cv`, `fecha`, `provincia_id`) VALUES
(13, 'Juan Arturo', '71056925', 'jbravoaguinaga@gmail.com', '979814339', '1473347556.docx', 'uploads/1473347556.docx', '2016-09-08 10:12:36', 107),
(14, 'Marcell Solis', '77208443', 'elio.mstp@gmail.com', '3646956', '1474930997.pdf', 'uploads/1474930997.pdf', '2016-09-26 18:03:17', 147);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito`
--

CREATE TABLE IF NOT EXISTS `carrito` (
`id` int(11) NOT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `precio_real` double NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_unitario` double NOT NULL,
  `pedido_id` int(11) DEFAULT NULL,
  `precio_real_con_cupon` double DEFAULT NULL,
  `cupon_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `carrito`
--

INSERT INTO `carrito` (`id`, `cliente_id`, `producto_id`, `fecha`, `precio_real`, `estado`, `cantidad`, `precio_unitario`, `pedido_id`, `precio_real_con_cupon`, `cupon_id`) VALUES
(19, 29, 2, '2016-09-05 18:13:36', 148, 'P', 1, 148, NULL, 0, NULL),
(20, 31, 1, '2016-09-12 09:04:20', 400, 'C', 4, 100, 29, 0, NULL),
(21, 31, 2, '2016-09-12 09:05:03', 740, 'C', 5, 148, 29, 0, NULL),
(22, 31, 4, '2016-09-12 18:58:40', 500, 'C', 5, 100, 31, 0, NULL),
(23, 31, 1, '2016-09-13 16:07:37', 300, 'C', 3, 100, 37, NULL, NULL),
(24, 31, 3, '2016-09-13 16:09:21', 270, 'C', 2, 135, 37, NULL, NULL),
(25, 31, 2, '2016-09-13 16:19:39', 148, 'C', 1, 148, 37, NULL, NULL),
(26, 31, 4, '2016-09-13 16:19:50', 100, 'C', 1, 100, 37, NULL, NULL),
(27, 31, 1, '2016-09-13 16:26:58', 100, 'C', 1, 100, 39, NULL, NULL),
(28, 31, 4, '2016-09-13 16:36:58', 100, 'C', 1, 100, 40, NULL, NULL),
(32, 29, 5, '2016-09-14 10:04:33', 330, 'P', 3, 110, NULL, NULL, NULL),
(33, 29, 1, '2016-09-14 10:26:23', 100, 'P', 1, 100, NULL, NULL, NULL),
(34, 31, 11, '2016-09-14 15:24:36', 220, 'C', 2, 110, 41, NULL, NULL),
(35, 31, 10, '2016-09-14 15:25:27', 500, 'C', 2, 250, 41, NULL, NULL),
(36, 31, 1, '2016-09-15 08:50:05', 100, 'C', 1, 100, 42, NULL, NULL),
(46, 31, 7, '2016-09-15 09:18:49', 200, 'C', 1, 200, 43, NULL, NULL),
(47, 31, 10, '2016-09-15 09:24:16', 250, 'C', 1, 250, 43, NULL, NULL),
(48, 31, 11, '2016-09-15 10:12:19', 110, 'P', 1, 110, NULL, NULL, NULL),
(49, 31, 6, '2016-09-15 10:12:36', 120, 'P', 1, 120, NULL, NULL, NULL),
(67, 32, 8, '2016-09-20 15:55:18', 150, 'C', 1, 150, 44, NULL, NULL),
(70, 32, 7, '2016-09-21 12:32:35', 200, 'C', 1, 200, 44, NULL, NULL),
(71, 32, 10, '2016-09-21 13:56:43', 250, 'C', 1, 250, 44, NULL, NULL),
(72, 32, 9, '2016-09-22 09:31:18', 200, 'C', 1, 200, 44, NULL, NULL),
(73, 32, 7, '2016-09-22 11:22:38', 400, 'C', 2, 200, 47, NULL, 3),
(75, 32, 10, '2016-09-22 11:39:02', 250, 'C', 1, 250, 47, NULL, 3),
(76, 32, 9, '2016-09-22 11:50:06', 200, 'C', 1, 200, 47, NULL, 3),
(77, 32, 6, '2016-09-22 12:24:55', 120, 'C', 1, 120, 47, NULL, 3),
(78, 32, 8, '2016-09-22 13:53:24', 150, 'C', 1, 150, 47, NULL, 3),
(86, 33, 7, '2016-09-26 10:39:30', 800, 'C', 4, 200, 48, NULL, NULL),
(87, 33, 8, '2016-09-26 10:39:31', 150, 'C', 1, 150, 48, NULL, NULL),
(88, 33, 6, '2016-09-26 15:53:01', 120, 'C', 1, 120, 48, NULL, NULL),
(89, 33, 10, '2016-09-26 18:25:03', 500, 'C', 2, 250, 49, NULL, NULL),
(90, 33, 7, '2016-09-26 18:28:34', 200, 'C', 1, 200, 50, NULL, NULL),
(91, 33, 8, '2016-09-26 18:31:45', 150, 'C', 1, 150, 51, NULL, NULL),
(92, 33, 9, '2016-09-26 18:35:54', 200, 'C', 1, 200, 52, NULL, NULL),
(93, 33, 14, '2016-09-26 18:41:38', 499, 'C', 1, 499, 53, NULL, NULL),
(94, 33, 11, '2016-09-26 19:03:34', 110, 'C', 1, 110, 54, NULL, NULL),
(98, 33, 8, '2016-09-26 19:30:14', 150, 'C', 1, 150, 55, NULL, 3),
(99, 33, 9, '2016-09-26 19:36:31', 200, 'C', 1, 200, 56, NULL, 3),
(101, 33, 7, '2016-09-27 08:58:38', 200, 'C', 1, 200, 68, NULL, 3),
(102, 33, 10, '2016-09-27 12:09:40', 500, 'C', 2, 250, 69, NULL, 3),
(103, 33, 11, '2016-09-27 12:09:51', 110, 'C', 1, 110, 69, NULL, 3),
(104, 33, 7, '2016-09-27 12:58:29', 600, 'C', 3, 200, 71, NULL, NULL),
(105, 33, 14, '2016-09-27 13:17:43', 499, 'C', 1, 499, 72, NULL, NULL),
(106, 33, 15, '2016-09-27 13:29:14', 500, 'C', 1, 500, 73, NULL, NULL),
(107, 33, 7, '2016-09-27 14:07:25', 400, 'C', 2, 200, 74, NULL, 3),
(108, 33, 12, '2016-09-27 14:22:15', 100, 'C', 1, 100, 74, NULL, 3),
(109, 33, 8, '2016-09-27 15:00:33', 150, 'C', 1, 150, 74, NULL, 3),
(110, 33, 7, '2016-09-27 15:04:59', 200, 'C', 1, 200, 75, NULL, NULL),
(111, 33, 7, '2016-09-27 15:07:53', 200, 'C', 1, 200, 76, NULL, NULL),
(112, 33, 8, '2016-09-27 16:14:55', 150, 'P', 1, 150, 78, NULL, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
`id` int(11) NOT NULL,
  `nombre_categoria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion_categoria` longtext COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cupon_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre_categoria`, `descripcion_categoria`, `slug`, `imagen`, `cupon_id`) VALUES
(1, 'Masajes', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.', 'masajes', 'uploads/categoria/almohadas-verde-big.jpg', 2),
(2, 'Fitness', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.', 'fitness', 'uploads/categoria/almohadas-verde-big.jpg', 2),
(3, 'Almohadas', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.', 'almohadas', 'uploads/categoria/almohadas-verde-big.jpg', 2),
(4, 'Otros', 'Más productos', 'otros', 'uploads/categoria/almohadas-verde-big.jpg', 2),
(5, 'Laptops', 'Categoria para laptops', 'laptops', 'uploads/categoria/hp255laptop.jpg', 2),
(6, 'Camaras', 'Categoria para las camaras', 'camaras', 'uploads/categoria/camara.png', 3),
(7, 'Ropa', 'Categoria de ropa', 'ropa', 'uploads/1473277307.jpeg', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
`id` int(11) NOT NULL,
  `nombres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dni` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `distrito_id` int(11) DEFAULT NULL,
  `fecha_nacimiento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `nombres`, `email`, `telefono`, `dni`, `password`, `distrito_id`, `fecha_nacimiento`, `direccion`) VALUES
(29, 'Juan Arturo Bravo Aguinaga', 'jbravoaguinaga@gmail.com', '979814339', '71056925', '$2y$10$K9IfQoVSgwyK/MXVO2t2iOxUXY/K4ESDjiXv9mAb1t1WlqC.f3edG', NULL, '0000-00-00', NULL),
(31, 'Marcell Solis a', 'elio.gs@gmail.com', '3646956', '77205555', '$2y$10$t64G2pzZF3wNOQO./fDiO.U2NSIMCM4Y93zkLndqZfIBe1pPTQYmK', 1090, '0000-00-00', NULL),
(32, 'Marcell Solis', 'elio.mstp@gmail.com', '3646956', '77208443', '$2y$10$QZHIK.rUCQT.oLNcHKq4gOk4Gw2KwboAc6Zel4Xv8PE2Q5HPJ6kom', 1072, '0000-00-00', NULL),
(33, 'Marcell G Solis', 'elio@staffcreativa.com', '947331054', '772084432', '$2y$10$ZI0noQUTKGltbyzaTpTb9O9abZJPDv7DSYAl2CHoLJRjfQSsqPbHu', 1090, '05/05/1995', 'Av. Primavera 850 Chacarilla Santiago de Surcossss');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE IF NOT EXISTS `contacto` (
`id` int(11) NOT NULL,
  `nombres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensaje` longtext COLLATE utf8_unicode_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `provincia_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `contacto`
--

INSERT INTO `contacto` (`id`, `nombres`, `email`, `telefono`, `mensaje`, `fecha`, `provincia_id`) VALUES
(10, 'Juan Arturo Bravo Aguinaga', 'jbravoaguinaga@gmail.com', '979814339', 'Mensaje de prueba!', '2016-09-07 18:42:35', 107),
(11, 'Juan Arturo Bravo Aguinaga', 'jbravoaguinaga@gmail.com', '979814339', 'Mensaje de prueba!', '2016-09-07 18:44:47', 108),
(12, 'Juan Arturo Bravo Aguinaga', 'jbravoaguinaga@gmail.com', '979814339', 'Mensaje de prueba!', '2016-09-07 18:44:57', 108);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cupon`
--

CREATE TABLE IF NOT EXISTS `cupon` (
`id` int(11) NOT NULL,
  `codigo` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descuento` double NOT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `descuento_en_porcentaje` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cupon`
--

INSERT INTO `cupon` (`id`, `codigo`, `descripcion`, `descuento`, `activo`, `descuento_en_porcentaje`) VALUES
(2, '000000', 'Ninguno', 0, 1, NULL),
(3, 'ABC123', 'Cupón de descuento 1', 10, 1, NULL),
(4, 'ABC122', 'DESC PROCENTAJE', 10, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE IF NOT EXISTS `departamento` (
  `id` int(11) NOT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`id`, `codigo`, `nombre`) VALUES
(1, '3', 'APURIMAC'),
(2, '4', 'AREQUIPA'),
(3, '6', 'CAJAMARCA'),
(4, '7', 'CUSCO'),
(5, '8', 'HUANCAVELICA'),
(6, '9', 'HUANUCO'),
(7, '10', 'ICA'),
(8, '16', 'MADRE DE DIOS'),
(9, '18', 'PASCO'),
(10, '19', 'PIURA'),
(11, '22', 'TACNA'),
(12, '23', 'TUMBES'),
(13, '11', 'JUNIN'),
(14, '12', 'LA LIBERTAD'),
(15, '13', 'LAMBAYEQUE'),
(16, '14', 'LIMA'),
(17, '24', 'CALLAO'),
(18, '25', 'UCAYALI'),
(19, '5', 'AYACUCHO'),
(20, '1', 'AMAZONAS'),
(21, '2', 'ANCASH'),
(22, '15', 'LORETO'),
(23, '17', 'MOQUEGUA'),
(24, '20', 'PUNO'),
(25, '21', 'SAN MARTIN');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `distrito`
--

CREATE TABLE IF NOT EXISTS `distrito` (
  `id` int(11) NOT NULL,
  `provincia_id` int(11) DEFAULT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `distrito`
--

INSERT INTO `distrito` (`id`, `provincia_id`, `codigo`, `nombre`) VALUES
(1, 1, '1', 'TAMBOBAMBA\r\n'),
(2, 1, '2', 'COYLLURQUI\r\n'),
(3, 1, '4', 'HAQUIRA\r\n'),
(4, 1, '5', 'MARA\r\n'),
(5, 1, '3', 'COTABAMBAS\r\n'),
(6, 1, '6', 'CHALLHUAHUACHO\r\n'),
(7, 2, '1', 'CHALHUANCA\r\n'),
(8, 2, '3', 'CARAYBAMBA\r\n'),
(9, 2, '4', 'COLCABAMBA\r\n'),
(10, 2, '5', 'COTARUSE\r\n'),
(11, 2, '6', 'CHAPIMARCA\r\n'),
(12, 2, '7', 'IHUAYLLO\r\n'),
(13, 2, '8', 'LUCRE\r\n'),
(14, 2, '10', 'SAÑAYCA\r\n'),
(15, 2, '11', 'SORAYA\r\n'),
(16, 2, '12', 'TAPAIRIHUA\r\n'),
(17, 2, '13', 'TINTAY\r\n'),
(18, 2, '14', 'TORAYA\r\n'),
(19, 2, '15', 'YANACA\r\n'),
(20, 2, '16', 'SAN JUAN DE CHACÑA\r\n'),
(21, 2, '2', 'CAPAYA\r\n'),
(22, 2, '9', 'POCOHUANCA\r\n'),
(23, 2, '17', 'JUSTO APU SAHUARAURA\r\n'),
(24, 3, '1', 'CHINCHEROS\r\n'),
(25, 3, '2', 'ONGOY\r\n'),
(26, 3, '3', 'OCOBAMBA\r\n'),
(27, 3, '4', 'COCHARCAS\r\n'),
(28, 3, '5', 'ANCO HUALLO\r\n'),
(29, 3, '6', 'HUACCANA\r\n'),
(30, 3, '8', 'RANRACANCHA\r\n'),
(31, 3, '7', 'URANMARCA\r\n'),
(32, 4, '1', 'CHUQUIBAMBILLA\r\n'),
(33, 4, '2', 'CURPAHUASI\r\n'),
(34, 4, '3', 'HUAYLLATI\r\n'),
(35, 4, '4', 'MAMARA\r\n'),
(36, 4, '6', 'MICAELA BASTIDAS\r\n'),
(37, 4, '7', 'PROGRESO\r\n'),
(38, 4, '8', 'PATAYPAMPA\r\n'),
(39, 4, '9', 'SAN ANTONIO\r\n'),
(40, 4, '10', 'TURPAY\r\n'),
(41, 4, '11', 'VILCABAMBA\r\n'),
(42, 4, '12', 'VIRUNDO\r\n'),
(43, 4, '13', 'SANTA ROSA\r\n'),
(44, 4, '5', 'MARISCAL GAMARRA\r\n'),
(45, 4, '14', 'CURASCO\r\n'),
(46, 5, '1', 'ANTABAMBA\r\n'),
(47, 5, '2', 'EL ORO\r\n'),
(48, 5, '3', 'HUAQUIRCA\r\n'),
(49, 5, '4', 'JUAN ESPINOZA MEDRANO\r\n'),
(50, 5, '5', 'OROPESA\r\n'),
(51, 5, '6', 'PACHACONAS\r\n'),
(52, 5, '7', 'SABAINO\r\n'),
(53, 6, '1', 'ANDAHUAYLAS\r\n'),
(54, 6, '2', 'ANDARAPA\r\n'),
(55, 6, '3', 'CHIARA\r\n'),
(56, 6, '4', 'HUANCARAMA\r\n'),
(57, 6, '5', 'HUANCARAY\r\n'),
(58, 6, '6', 'KISHUARA\r\n'),
(59, 6, '7', 'PACOBAMBA\r\n'),
(60, 6, '8', 'PAMPACHIRI\r\n'),
(61, 6, '10', 'SAN JERONIMO\r\n'),
(62, 6, '11', 'TALAVERA\r\n'),
(63, 6, '12', 'TURPO\r\n'),
(64, 6, '13', 'PACUCHA\r\n'),
(65, 6, '14', 'POMACOCHA\r\n'),
(66, 6, '15', 'SANTA MARIA DE CHICMO\r\n'),
(67, 6, '16', 'TUMAY HUARACA\r\n'),
(68, 6, '17', 'HUAYANA\r\n'),
(69, 6, '19', 'KAQUIABAMBA\r\n'),
(70, 6, '9', 'SAN ANTONIO DE CACHI\r\n'),
(71, 6, '18', 'SAN MIGUEL DE CHACCRAMPA\r\n'),
(72, 7, '1', 'ABANCAY\r\n'),
(73, 7, '2', 'CIRCA\r\n'),
(74, 7, '3', 'CURAHUASI\r\n'),
(75, 7, '5', 'HUANIPACA\r\n'),
(76, 7, '6', 'LAMBRAMA\r\n'),
(77, 7, '7', 'PICHIRHUA\r\n'),
(78, 7, '8', 'SAN PEDRO DE CACHORA\r\n'),
(79, 7, '9', 'TAMBURCO\r\n'),
(80, 7, '4', 'CHACOCHE\r\n'),
(81, 8, '1', 'MOLLENDO\r\n'),
(82, 8, '3', 'DEAN VALDIVIA\r\n'),
(83, 8, '4', 'ISLAY\r\n'),
(84, 8, '5', 'MEJIA\r\n'),
(85, 8, '6', 'PUNTA DE BOMBON\r\n'),
(86, 8, '2', 'COCACHACRA\r\n'),
(87, 9, '1', 'AREQUIPA\r\n'),
(88, 9, '2', 'CAYMA\r\n'),
(89, 9, '3', 'CERRO COLORADO\r\n'),
(90, 9, '4', 'CHARACATO\r\n'),
(91, 9, '5', 'CHIGUATA\r\n'),
(92, 9, '7', 'MIRAFLORES\r\n'),
(93, 9, '8', 'MOLLEBAYA\r\n'),
(94, 9, '9', 'PAUCARPATA\r\n'),
(95, 9, '10', 'POCSI\r\n'),
(96, 9, '11', 'POLOBAYA\r\n'),
(97, 9, '12', 'QUEQUEÑA\r\n'),
(98, 9, '14', 'SACHACA\r\n'),
(99, 9, '15', 'SAN JUAN DE SIGUAS\r\n'),
(100, 9, '16', 'SAN JUAN DE TARUCANI\r\n'),
(101, 9, '17', 'SANTA ISABEL DE SIGUAS\r\n'),
(102, 9, '18', 'SANTA RITA DE SIHUAS\r\n'),
(103, 9, '19', 'SOCABAYA\r\n'),
(104, 9, '21', 'UCHUMAYO\r\n'),
(105, 9, '22', 'VITOR\r\n'),
(106, 9, '23', 'YANAHUARA\r\n'),
(107, 9, '24', 'YARABAMBA\r\n'),
(108, 9, '25', 'YURA\r\n'),
(109, 9, '26', 'MARIANO MELGAR\r\n'),
(110, 9, '28', 'ALTO SELVA ALEGRE\r\n'),
(111, 9, '29', 'JOSE LUIS BUSTAMANTE Y RIVERO\r\n'),
(112, 9, '6', 'LA JOYA\r\n'),
(113, 9, '13', 'SABANDIA\r\n'),
(114, 9, '20', 'TIABAYA\r\n'),
(115, 9, '27', 'JACOBO HUNTER\r\n'),
(116, 10, '1', 'COTAHUASI\r\n'),
(117, 10, '2', 'ALCA\r\n'),
(118, 10, '3', 'CHARCANA\r\n'),
(119, 10, '5', 'PAMPAMARCA\r\n'),
(120, 10, '6', 'PUYCA\r\n'),
(121, 10, '7', 'QUECHUALLA\r\n'),
(122, 10, '8', 'SAYLA\r\n'),
(123, 10, '9', 'TAURIA\r\n'),
(124, 10, '10', 'TOMEPAMPA\r\n'),
(125, 10, '4', 'HUAYNACOTAS\r\n'),
(126, 10, '11', 'TORO\r\n'),
(127, 11, '1', 'CHUQUIBAMBA\r\n'),
(128, 11, '2', 'ANDARAY\r\n'),
(129, 11, '4', 'CHICHAS\r\n'),
(130, 11, '5', 'IRAY\r\n'),
(131, 11, '6', 'SALAMANCA\r\n'),
(132, 11, '7', 'YANAQUIHUA\r\n'),
(133, 11, '8', 'RIO GRANDE\r\n'),
(134, 11, '3', 'CAYARANI\r\n'),
(135, 12, '1', 'APLAO\r\n'),
(136, 12, '2', 'ANDAGUA\r\n'),
(137, 12, '4', 'CHACHAS\r\n'),
(138, 12, '5', 'CHILCAYMARCA\r\n'),
(139, 12, '6', 'CHOCO\r\n'),
(140, 12, '7', 'HUANCARQUI\r\n'),
(141, 12, '8', 'MACHAGUAY\r\n'),
(142, 12, '9', 'ORCOPAMPA\r\n'),
(143, 12, '11', 'TIPAN\r\n'),
(144, 12, '12', 'URACA\r\n'),
(145, 12, '13', 'UÑON\r\n'),
(146, 12, '14', 'VIRACO\r\n'),
(147, 12, '3', 'AYO\r\n'),
(148, 12, '10', 'PAMPACOLCA\r\n'),
(149, 13, '1', 'CARAVELI\r\n'),
(150, 13, '2', 'ACARI\r\n'),
(151, 13, '4', 'ATIQUIPA\r\n'),
(152, 13, '5', 'BELLA UNION\r\n'),
(153, 13, '6', 'CAHUACHO\r\n'),
(154, 13, '7', 'CHALA\r\n'),
(155, 13, '8', 'CHAPARRA\r\n'),
(156, 13, '9', 'HUANUHUANU\r\n'),
(157, 13, '11', 'LOMAS\r\n'),
(158, 13, '12', 'QUICACHA\r\n'),
(159, 13, '13', 'YAUCA\r\n'),
(160, 13, '3', 'ATICO\r\n'),
(161, 13, '10', 'JAQUI\r\n'),
(162, 14, '1', 'CAMANA\r\n'),
(163, 14, '4', 'MARISCAL CACERES\r\n'),
(164, 14, '5', 'NICOLAS DE PIEROLA\r\n'),
(165, 14, '6', 'OCOÑA\r\n'),
(166, 14, '7', 'QUILCA\r\n'),
(167, 14, '8', 'SAMUEL PASTOR\r\n'),
(168, 14, '2', 'JOSE MARIA QUIMPER\r\n'),
(169, 14, '3', 'MARIANO NICOLAS VALCARCEL\r\n'),
(170, 15, '1', 'CHIVAY\r\n'),
(171, 15, '2', 'ACHOMA\r\n'),
(172, 15, '3', 'CABANACONDE\r\n'),
(173, 15, '4', 'CAYLLOMA\r\n'),
(174, 15, '5', 'CALLALLI\r\n'),
(175, 15, '7', 'HUAMBO\r\n'),
(176, 15, '8', 'HUANCA\r\n'),
(177, 15, '9', 'ICHUPAMPA\r\n'),
(178, 15, '10', 'LARI\r\n'),
(179, 15, '11', 'LLUTA\r\n'),
(180, 15, '12', 'MACA\r\n'),
(181, 15, '14', 'SAN ANTONIO DE CHUCA\r\n'),
(182, 15, '15', 'SIBAYO\r\n'),
(183, 15, '16', 'TAPAY\r\n'),
(184, 15, '18', 'TUTI\r\n'),
(185, 15, '19', 'YANQUE\r\n'),
(186, 15, '20', 'MAJES\r\n'),
(187, 15, '6', 'COPORAQUE\r\n'),
(188, 15, '13', 'MADRIGAL\r\n'),
(189, 15, '17', 'TISCO\r\n'),
(190, 16, '2', 'SAN BERNARDINO\r\n'),
(191, 16, '3', 'SAN LUIS\r\n'),
(192, 16, '4', 'TUMBADEN\r\n'),
(193, 16, '1', 'SAN PABLO\r\n'),
(194, 17, '1', 'SAN MIGUEL\r\n'),
(195, 17, '2', 'CALQUIS\r\n'),
(196, 17, '3', 'LA FLORIDA\r\n'),
(197, 17, '4', 'LLAPA\r\n'),
(198, 17, '6', 'NIEPOS\r\n'),
(199, 17, '7', 'SAN GREGORIO\r\n'),
(200, 17, '8', 'SAN SILVESTRE DE COCHAN\r\n'),
(201, 17, '9', 'EL PRADO\r\n'),
(202, 17, '10', 'UNION AGUA BLANCA\r\n'),
(203, 17, '11', 'TONGOD\r\n'),
(204, 17, '13', 'BOLIVAR\r\n'),
(205, 17, '5', 'NANCHOC\r\n'),
(206, 17, '12', 'CATILLUC\r\n'),
(207, 18, '1', 'CONTUMAZA\r\n'),
(208, 18, '3', 'CHILETE\r\n'),
(209, 18, '4', 'GUZMANGO\r\n'),
(210, 18, '5', 'SAN BENITO\r\n'),
(211, 18, '7', 'TANTARICA\r\n'),
(212, 18, '8', 'YONAN\r\n'),
(213, 18, '9', 'SANTA CRUZ DE TOLED\r\n'),
(214, 18, '6', 'CUPISNIQUE\r\n'),
(215, 19, '1', 'CAJAMARCA\r\n'),
(216, 19, '2', 'ASUNCION\r\n'),
(217, 19, '3', 'COSPAN\r\n'),
(218, 19, '5', 'ENCAÑADA\r\n'),
(219, 19, '6', 'JESUS\r\n'),
(220, 19, '7', 'LOS BAÑOS DEL INCA\r\n'),
(221, 19, '8', 'LLACANORA\r\n'),
(222, 19, '9', 'MAGDALENA\r\n'),
(223, 19, '10', 'MATARA\r\n'),
(224, 19, '12', 'SAN JUAN\r\n'),
(225, 19, '4', 'CHETILLA\r\n'),
(226, 19, '11', 'NAMORA\r\n'),
(227, 20, '1', 'PEDRO GALVEZ\r\n'),
(228, 20, '2', 'ICHOCAN\r\n'),
(229, 20, '3', 'GREGORIO PITA\r\n'),
(230, 20, '4', 'JOSE MANUEL QUIROZ\r\n'),
(231, 20, '5', 'EDUARDO VILLANUEVA\r\n'),
(232, 20, '6', 'JOSE SABOGAL\r\n'),
(233, 20, '7', 'CHANCAY\r\n'),
(234, 21, '1', 'SAN IGNACIO\r\n'),
(235, 21, '2', 'CHIRINOS\r\n'),
(236, 21, '3', 'HUARANGO\r\n'),
(237, 21, '4', 'NAMBALLE\r\n'),
(238, 21, '5', 'LA COIPA\r\n'),
(239, 21, '7', 'TABACONAS\r\n'),
(240, 21, '6', 'SAN JOSE DE LOURDES\r\n'),
(241, 22, '2', 'CATACHE\r\n'),
(242, 22, '3', 'CHANCAYBAÑOS\r\n'),
(243, 22, '4', 'LA ESPERANZA\r\n'),
(244, 22, '5', 'NINABAMBA\r\n'),
(245, 22, '6', 'PULAN\r\n'),
(246, 22, '7', 'SEXI\r\n'),
(247, 22, '9', 'YAUYUCAN\r\n'),
(248, 22, '10', 'ANDABAMBA\r\n'),
(249, 22, '11', 'SAUCEPAMPA\r\n'),
(250, 22, '1', 'SANTA CRUZ\r\n'),
(251, 22, '8', 'UTICYACU\r\n'),
(252, 23, '1', 'JAEN\r\n'),
(253, 23, '2', 'BELLAVISTA\r\n'),
(254, 23, '3', 'COLASAY\r\n'),
(255, 23, '4', 'CHONTALI\r\n'),
(256, 23, '5', 'POMAHUACA\r\n'),
(257, 23, '7', 'SALLIQUE\r\n'),
(258, 23, '8', 'SAN FELIPE\r\n'),
(259, 23, '9', 'SAN JOSE DEL ALTO\r\n'),
(260, 23, '10', 'SANTA ROSA\r\n'),
(261, 23, '11', 'LAS PIRIAS\r\n'),
(262, 23, '12', 'HUABAL\r\n'),
(263, 23, '6', 'PUCARA\r\n'),
(264, 24, '2', 'CHUGUR\r\n'),
(265, 24, '3', 'HUALGAYOC\r\n'),
(266, 24, '1', 'BAMBAMARCA\r\n'),
(267, 25, '1', 'CHOTA\r\n'),
(268, 25, '2', 'ANGUIA\r\n'),
(269, 25, '3', 'COCHABAMBA\r\n'),
(270, 25, '5', 'CHADIN\r\n'),
(271, 25, '6', 'CHIGUIRIP\r\n'),
(272, 25, '7', 'CHIMBAN\r\n'),
(273, 25, '8', 'HUAMBOS\r\n'),
(274, 25, '10', 'LLAMA\r\n'),
(275, 25, '11', 'MIRACOSTA\r\n'),
(276, 25, '13', 'PION\r\n'),
(277, 25, '14', 'QUEROCOTO\r\n'),
(278, 25, '16', 'TOCMOCHE\r\n'),
(279, 25, '17', 'SAN JUAN DE LICUPIS\r\n'),
(280, 25, '18', 'CHOROPAMPA\r\n'),
(281, 25, '19', 'CHALAMARCA\r\n'),
(282, 25, '4', 'CONCHAN\r\n'),
(283, 25, '12', 'PACCHA\r\n'),
(284, 25, '9', 'LAJAS\r\n'),
(285, 25, '15', 'TACABAMBA\r\n'),
(286, 26, '1', 'CUTERVO\r\n'),
(287, 26, '2', 'CALLAYUC\r\n'),
(288, 26, '3', 'CUJILLO\r\n'),
(289, 26, '5', 'LA RAMADA\r\n'),
(290, 26, '6', 'PIMPINGOS\r\n'),
(291, 26, '7', 'QUEROCOTILLO\r\n'),
(292, 26, '8', 'SAN ANDRES DE CUTERVO\r\n'),
(293, 26, '9', 'SAN JUAN DE CUTERVO\r\n'),
(294, 26, '10', 'SAN LUIS DE LUCMA\r\n'),
(295, 26, '12', 'SANTO DOMINGO DE LA CAPILLA\r\n'),
(296, 26, '13', 'SANTO TOMAS\r\n'),
(297, 26, '14', 'SOCOTA\r\n'),
(298, 26, '15', 'TORIBIO CASANOVA\r\n'),
(299, 26, '4', 'CHOROS\r\n'),
(300, 26, '11', 'SANTA CRUZ\r\n'),
(301, 27, '1', 'CELENDIN\r\n'),
(302, 27, '3', 'CHUMUCH\r\n'),
(303, 27, '4', 'HUASMIN\r\n'),
(304, 27, '5', 'JORGE CHAVEZ\r\n'),
(305, 27, '6', 'JOSE GALVEZ\r\n'),
(306, 27, '7', 'MIGUEL IGLESIAS\r\n'),
(307, 27, '8', 'OXAMARCA\r\n'),
(308, 27, '10', 'SUCRE\r\n'),
(309, 27, '11', 'UTCO\r\n'),
(310, 27, '12', 'LA LIBERTAD DE PALLAN\r\n'),
(311, 27, '2', 'CORTEGANA\r\n'),
(312, 27, '9', 'SOROCHUCO\r\n'),
(313, 28, '1', 'CAJABAMBA\r\n'),
(314, 28, '2', 'CACHACHI\r\n'),
(315, 28, '3', 'CONDEBAMBA\r\n'),
(316, 28, '5', 'SITACOCHA\r\n'),
(317, 29, '1', 'URUBAMBA\r\n'),
(318, 29, '2', 'CHINCHERO\r\n'),
(319, 29, '3', 'HUAYLLABAMBA\r\n'),
(320, 29, '5', 'MARAS\r\n'),
(321, 29, '6', 'OLLANTAYTAMBO\r\n'),
(322, 29, '7', 'YUCAY\r\n'),
(323, 29, '4', 'MACHUPICCHU\r\n'),
(324, 30, '2', 'ANDAHUAYLILLAS\r\n'),
(325, 30, '4', 'CCARHUAYO\r\n'),
(326, 30, '5', 'CCATCA\r\n'),
(327, 30, '6', 'CUSIPATA\r\n'),
(328, 30, '7', 'HUARO\r\n'),
(329, 30, '8', 'LUCRE\r\n'),
(330, 30, '10', 'OCONGATE\r\n'),
(331, 30, '11', 'OROPESA\r\n'),
(332, 30, '12', 'QUIQUIJANA\r\n'),
(333, 30, '3', 'CAMANTI\r\n'),
(334, 30, '9', 'MARCAPATA\r\n'),
(335, 30, '1', 'URCOS\r\n'),
(336, 31, '2', 'ACCHA\r\n'),
(337, 31, '3', 'CCAPI\r\n'),
(338, 31, '4', 'COLCHA\r\n'),
(339, 31, '5', 'HUANOQUITE\r\n'),
(340, 31, '6', 'OMACHA\r\n'),
(341, 31, '7', 'YAURISQUE\r\n'),
(342, 31, '1', 'PARURO\r\n'),
(343, 31, '8', 'PACCARITAMBO\r\n'),
(344, 31, '9', 'PILLPINTO\r\n'),
(345, 32, '1', 'SANTO TOMAS\r\n'),
(346, 32, '2', 'CAPACMARCA\r\n'),
(347, 32, '3', 'COLQUEMARCA\r\n'),
(348, 32, '4', 'CHAMACA\r\n'),
(349, 32, '6', 'LLUSCO\r\n'),
(350, 32, '7', 'QUIÑOTA\r\n'),
(351, 32, '8', 'VELILLE\r\n'),
(352, 32, '5', 'LIVITACA\r\n'),
(353, 33, '1', 'ACOMAYO\r\n'),
(354, 33, '2', 'ACOPIA\r\n'),
(355, 33, '3', 'ACOS\r\n'),
(356, 33, '4', 'POMACANCHI\r\n'),
(357, 33, '7', 'MOSOC LLACTA\r\n'),
(358, 33, '6', 'SANGARARA\r\n'),
(359, 33, '5', 'RONDOCAN\r\n'),
(360, 34, '1', 'PAUCARTAMBO\r\n'),
(361, 34, '2', 'CAICAY\r\n'),
(362, 34, '3', 'COLQUEPATA\r\n'),
(363, 34, '4', 'CHALLABAMBA\r\n'),
(364, 34, '5', 'KOSÑIPATA\r\n'),
(365, 34, '6', 'HUANCARANI\r\n'),
(366, 35, '1', 'SANTA ANA\r\n'),
(367, 35, '2', 'ECHARATI\r\n'),
(368, 35, '3', 'HUAYOPATA\r\n'),
(369, 35, '5', 'OCOBAMBA\r\n'),
(370, 35, '6', 'SANTA TERESA\r\n'),
(371, 35, '7', 'VILCABAMBA\r\n'),
(372, 35, '8', 'QUELLOUNO\r\n'),
(373, 35, '9', 'KIMBIRI\r\n'),
(374, 35, '10', 'PICHARI\r\n'),
(375, 35, '4', 'MARANURA\r\n'),
(376, 36, '1', 'ESPINAR\r\n'),
(377, 36, '2', 'CONDOROMA\r\n'),
(378, 36, '3', 'COPORAQUE\r\n'),
(379, 36, '4', 'OCORURO\r\n'),
(380, 36, '6', 'PICHIGUA\r\n'),
(381, 36, '7', 'SUYCKUTAMBO\r\n'),
(382, 36, '8', 'ALTO PICHIGUA\r\n'),
(383, 36, '5', 'PALLPATA\r\n'),
(384, 37, '1', 'SICUANI\r\n'),
(385, 37, '2', 'COMBAPATA\r\n'),
(386, 37, '3', 'CHECACUPE\r\n'),
(387, 37, '4', 'MARANGANI\r\n'),
(388, 37, '5', 'PITUMARCA\r\n'),
(389, 37, '7', 'SAN PEDRO\r\n'),
(390, 37, '8', 'TINTA\r\n'),
(391, 37, '6', 'SAN PABLO\r\n'),
(392, 38, '1', 'YANAOCA\r\n'),
(393, 38, '2', 'CHECCA\r\n'),
(394, 38, '3', 'KUNTURKANKI\r\n'),
(395, 38, '4', 'LANGUI\r\n'),
(396, 38, '5', 'LAYO\r\n'),
(397, 38, '6', 'PAMPAMARCA\r\n'),
(398, 38, '8', 'TUPAC AMARU\r\n'),
(399, 38, '7', 'QUEHUE\r\n'),
(400, 39, '2', 'COYA\r\n'),
(401, 39, '3', 'LAMAY\r\n'),
(402, 39, '4', 'LARES\r\n'),
(403, 39, '5', 'PISAC\r\n'),
(404, 39, '6', 'SAN SALVADOR\r\n'),
(405, 39, '7', 'TARAY\r\n'),
(406, 39, '1', 'CALCA\r\n'),
(407, 39, '8', 'YANATILE\r\n'),
(408, 40, '1', 'ANTA\r\n'),
(409, 40, '2', 'CHINCHAYPUJIO\r\n'),
(410, 40, '4', 'LIMATAMBO\r\n'),
(411, 40, '5', 'MOLLEPATA\r\n'),
(412, 40, '6', 'PUCYURA\r\n'),
(413, 40, '7', 'ZURITE\r\n'),
(414, 40, '8', 'CACHIMAYO\r\n'),
(415, 40, '9', 'ANCAHUASI\r\n'),
(416, 40, '3', 'HUAROCONDO\r\n'),
(417, 41, '1', 'CUSCO\r\n'),
(418, 41, '2', 'CCORCA\r\n'),
(419, 41, '3', 'POROY\r\n'),
(420, 41, '5', 'SAN SEBASTIAN\r\n'),
(421, 41, '6', 'SANTIAGO\r\n'),
(422, 41, '8', 'WANCHAQ\r\n'),
(423, 41, '4', 'SAN JERONIMO\r\n'),
(424, 41, '7', 'SAYLLA\r\n'),
(425, 42, '1', 'CASTROVIRREYNA\r\n'),
(426, 42, '3', 'AURAHUA\r\n'),
(427, 42, '5', 'CAPILLAS\r\n'),
(428, 42, '6', 'COCAS\r\n'),
(429, 42, '8', 'CHUPAMARCA\r\n'),
(430, 42, '9', 'HUACHOS\r\n'),
(431, 42, '10', 'HUAMATAMBO\r\n'),
(432, 42, '22', 'SAN JUAN\r\n'),
(433, 42, '27', 'TANTARA\r\n'),
(434, 42, '28', 'TICRAPO\r\n'),
(435, 42, '29', 'SANTA ANA\r\n'),
(436, 42, '2', 'ARMA\r\n'),
(437, 42, '14', 'MOLLEPAMPA\r\n'),
(438, 43, '1', 'ACOBAMBA\r\n'),
(439, 43, '2', 'ANTA\r\n'),
(440, 43, '3', 'ANDABAMBA\r\n'),
(441, 43, '4', 'CAJA\r\n'),
(442, 43, '5', 'MARCAS\r\n'),
(443, 43, '6', 'PAUCARA\r\n'),
(444, 43, '8', 'ROSARIO\r\n'),
(445, 43, '7', 'POMACOCHA\r\n'),
(446, 44, '1', 'CHURCAMPA\r\n'),
(447, 44, '2', 'ANCO\r\n'),
(448, 44, '3', 'CHINCHIHUASI\r\n'),
(449, 44, '4', 'EL CARMEN\r\n'),
(450, 44, '5', 'LA MERCED\r\n'),
(451, 44, '7', 'PAUCARBAMBA\r\n'),
(452, 44, '8', 'SAN MIGUEL DE MAYOCC\r\n'),
(453, 44, '9', 'SAN PEDRO DE CORIS\r\n'),
(454, 44, '10', 'PACHAMARCA\r\n'),
(455, 44, '6', 'LOCROJA\r\n'),
(456, 45, '1', 'AYAVI\r\n'),
(457, 45, '2', 'CORDOVA\r\n'),
(458, 45, '3', 'HUAYACUNDO ARMA\r\n'),
(459, 45, '4', 'HUAYTARA\r\n'),
(460, 45, '5', 'LARAMARCA\r\n'),
(461, 45, '7', 'PILPICHACA\r\n'),
(462, 45, '8', 'QUERCO\r\n'),
(463, 45, '9', 'QUITO ARMA\r\n'),
(464, 45, '10', 'SAN ANTONIO DE CUSICANCHA\r\n'),
(465, 45, '11', 'SAN FRANCISCO DE SANGAYAICO\r\n'),
(466, 45, '12', 'SAN ISIDRO\r\n'),
(467, 45, '14', 'SANTIAGO DE QUIRAHUARA\r\n'),
(468, 45, '15', 'SANTO DOMINGO DE CAPILLAS\r\n'),
(469, 45, '16', 'TAMBO\r\n'),
(470, 45, '6', 'OCOYO\r\n'),
(471, 45, '13', 'SANTIAGO DE CHOCORVOS\r\n'),
(472, 46, '1', 'PAMPAS\r\n'),
(473, 46, '2', 'ACOSTAMBO\r\n'),
(474, 46, '6', 'COLCABAMBA\r\n'),
(475, 46, '9', 'DANIEL HERNANDEZ\r\n'),
(476, 46, '11', 'HUACHOCOLPA\r\n'),
(477, 46, '12', 'HUARIBAMBA\r\n'),
(478, 46, '15', 'ÑAHUIMPUQUIO\r\n'),
(479, 46, '18', 'QUISHUAR\r\n'),
(480, 46, '19', 'SALCABAMBA\r\n'),
(481, 46, '23', 'SURCABAMBA\r\n'),
(482, 46, '25', 'TINTAY PUNCU\r\n'),
(483, 46, '26', 'SALCAHUASI\r\n'),
(484, 46, '3', 'ACRAQUIA\r\n'),
(485, 46, '20', 'SAN MARCOS DE ROCCHAC\r\n'),
(486, 46, '4', 'AHUAYCHA\r\n'),
(487, 46, '17', 'PAZOS\r\n'),
(488, 47, '1', 'LIRCAY\r\n'),
(489, 47, '2', 'ANCHONGA\r\n'),
(490, 47, '3', 'CALLANMARCA\r\n'),
(491, 47, '4', 'CONGALLA\r\n'),
(492, 47, '5', 'CHINCHO\r\n'),
(493, 47, '7', 'HUANCA-HUANCA\r\n'),
(494, 47, '8', 'JULCAMARCA\r\n'),
(495, 47, '9', 'SAN ANTONIO DE ANTAPARCO\r\n'),
(496, 47, '10', 'SANTO TOMAS DE PATA\r\n'),
(497, 47, '11', 'SECCLLA\r\n'),
(498, 47, '12', 'CCOCHACCASA\r\n'),
(499, 47, '6', 'HUALLAY-GRANDE\r\n'),
(500, 48, '1', 'HUANCAVELICA\r\n'),
(501, 48, '2', 'ACOBAMBILLA\r\n'),
(502, 48, '3', 'ACORIA\r\n'),
(503, 48, '4', 'CONAYCA\r\n'),
(504, 48, '6', 'HUACHOCOLPA\r\n'),
(505, 48, '8', 'HUAYLLAHUARA\r\n'),
(506, 48, '9', 'IZCUCHACA\r\n'),
(507, 48, '10', 'LARIA\r\n'),
(508, 48, '11', 'MANTA\r\n'),
(509, 48, '12', 'MARISCAL CACERES\r\n'),
(510, 48, '14', 'NUEVO OCCORO\r\n'),
(511, 48, '15', 'PALCA\r\n'),
(512, 48, '16', 'PILCHACA\r\n'),
(513, 48, '17', 'VILCA\r\n'),
(514, 48, '18', 'YAULI\r\n'),
(515, 48, '20', 'HUANDO\r\n'),
(516, 48, '5', 'CUENCA\r\n'),
(517, 48, '13', 'MOYA\r\n'),
(518, 48, '19', 'ASCENSION\r\n'),
(519, 49, '1', 'JESUS\r\n'),
(520, 49, '2', 'BAÑOS\r\n'),
(521, 49, '4', 'QUEROPALCA\r\n'),
(522, 49, '5', 'SAN MIGUEL DE CAURI\r\n'),
(523, 49, '6', 'RONDOS\r\n'),
(524, 49, '7', 'JIVIA\r\n'),
(525, 49, '3', 'SAN FRANCISCO DE ASIS\r\n'),
(526, 50, '1', 'LLATA\r\n'),
(527, 50, '2', 'ARANCAY\r\n'),
(528, 50, '3', 'CHAVIN DE PARIARCA\r\n'),
(529, 50, '4', 'JACAS GRANDE\r\n'),
(530, 50, '5', 'JIRCAN\r\n'),
(531, 50, '7', 'MONZON\r\n'),
(532, 50, '8', 'PUNCHAO\r\n'),
(533, 50, '9', 'PUÑOS\r\n'),
(534, 50, '10', 'SINGA\r\n'),
(535, 50, '6', 'MIRAFLORES\r\n'),
(536, 50, '11', 'TANTAMAYO\r\n'),
(537, 51, '1', 'AMBO\r\n'),
(538, 51, '2', 'CAYNA\r\n'),
(539, 51, '3', 'COLPAS\r\n'),
(540, 51, '4', 'CONCHAMARCA\r\n'),
(541, 51, '5', 'HUACAR\r\n'),
(542, 51, '6', 'SAN FRANCISCO\r\n'),
(543, 51, '7', 'SAN RAFAEL\r\n'),
(544, 51, '8', 'TOMAY-KICHWA\r\n'),
(545, 52, '1', 'CHAVINILLO\r\n'),
(546, 52, '2', 'APARICIO POMARES\r\n'),
(547, 52, '3', 'CAHUAC\r\n'),
(548, 52, '4', 'CHACABAMBA\r\n'),
(549, 52, '6', 'OBAS\r\n'),
(550, 52, '7', 'PAMPAMARCA\r\n'),
(551, 52, '8', 'CHORAS\r\n'),
(552, 52, '5', 'JACAS CHICO\r\n'),
(553, 53, '1', 'HUACAYBAMBA\r\n'),
(554, 53, '2', 'PINRA\r\n'),
(555, 53, '3', 'CANCHABAMBA\r\n'),
(556, 53, '4', 'COCHABAMBA\r\n'),
(557, 54, '1', 'HONORIA\r\n'),
(558, 54, '2', 'PUERTO INCA\r\n'),
(559, 54, '3', 'CODO DEL POZUZO\r\n'),
(560, 54, '4', 'TOURNAVISTA\r\n'),
(561, 54, '5', 'YUYAPICHIS\r\n'),
(562, 55, '1', 'PANAO\r\n'),
(563, 55, '2', 'CHAGLLA\r\n'),
(564, 55, '4', 'MOLINO\r\n'),
(565, 55, '6', 'UMARI\r\n'),
(566, 56, '1', 'RUPA-RUPA\r\n'),
(567, 56, '2', 'DANIEL ALOMIA ROBLES\r\n'),
(568, 56, '3', 'HERMILIO VALDIZAN\r\n'),
(569, 56, '4', 'LUYANDO\r\n'),
(570, 56, '6', 'JOSE CRESPO Y CASTILLO\r\n'),
(571, 56, '5', 'MARIANO DAMASO BERAUN\r\n'),
(572, 57, '1', 'HUACRACHUCO\r\n'),
(573, 57, '2', 'CHOLON\r\n'),
(574, 57, '5', 'SAN BUENAVENTURA\r\n'),
(575, 58, '1', 'LA UNION\r\n'),
(576, 58, '7', 'CHUQUIS\r\n'),
(577, 58, '12', 'MARIAS\r\n'),
(578, 58, '14', 'PACHAS\r\n'),
(579, 58, '16', 'QUIVILLA\r\n'),
(580, 58, '17', 'RIPAN\r\n'),
(581, 58, '21', 'SHUNQUI\r\n'),
(582, 58, '23', 'YANAS\r\n'),
(583, 58, '22', 'SILLAPATA\r\n'),
(584, 59, '1', 'HUANUCO\r\n'),
(585, 59, '2', 'CHINCHAO\r\n'),
(586, 59, '4', 'MARGOS\r\n'),
(587, 59, '5', 'QUISQUI\r\n'),
(588, 59, '6', 'SAN FRANCISCO DE CAYRAN\r\n'),
(589, 59, '7', 'SAN PEDRO DE CHAULAN\r\n'),
(590, 59, '8', 'SANTA MARIA DEL VALLE\r\n'),
(591, 59, '9', 'YARUMAYO\r\n'),
(592, 59, '10', 'AMARILIS\r\n'),
(593, 59, '3', 'CHURUBAMBA\r\n'),
(594, 59, '11', 'PILLCO MARCA\r\n'),
(595, 60, '2', 'LLIPATA\r\n'),
(596, 60, '1', 'PALPA\r\n'),
(597, 60, '4', 'SANTA CRUZ\r\n'),
(598, 60, '5', 'TIBILLO\r\n'),
(599, 60, '3', 'RIO GRANDE\r\n'),
(600, 61, '1', 'NAZCA\r\n'),
(601, 61, '2', 'CHANGUILLO\r\n'),
(602, 61, '3', 'EL INGENIO\r\n'),
(603, 61, '4', 'MARCONA\r\n'),
(604, 61, '5', 'VISTA ALEGRE\r\n'),
(605, 62, '1', 'ICA\r\n'),
(606, 62, '2', 'LA TINGUIÑA\r\n'),
(607, 62, '3', 'LOS AQUIJES\r\n'),
(608, 62, '4', 'PARCONA\r\n'),
(609, 62, '6', 'SALAS\r\n'),
(610, 62, '7', 'SAN JOSE DE LOS MOLINOS\r\n'),
(611, 62, '8', 'SAN JUAN BAUTISTA\r\n'),
(612, 62, '9', 'SANTIAGO\r\n'),
(613, 62, '10', 'SUBTANJALLA\r\n'),
(614, 62, '11', 'YAUCA DEL ROSARIO\r\n'),
(615, 62, '12', 'TATE\r\n'),
(616, 62, '14', 'OCUCAJE\r\n'),
(617, 62, '5', 'PUEBLO NUEVO\r\n'),
(618, 62, '13', 'PACHACUTEC\r\n'),
(619, 63, '1', 'PISCO\r\n'),
(620, 63, '2', 'HUANCANO\r\n'),
(621, 63, '3', 'HUMAY\r\n'),
(622, 63, '4', 'INDEPENDENCIA\r\n'),
(623, 63, '5', 'PARACAS\r\n'),
(624, 63, '6', 'SAN ANDRES\r\n'),
(625, 63, '8', 'TUPAC AMARU INCA\r\n'),
(626, 63, '7', 'SAN CLEMENTE\r\n'),
(627, 64, '1', 'CHINCHA ALTA\r\n'),
(628, 64, '2', 'CHAVIN\r\n'),
(629, 64, '3', 'CHINCHA BAJA\r\n'),
(630, 64, '4', 'EL CARMEN\r\n'),
(631, 64, '5', 'GROCIO PRADO\r\n'),
(632, 64, '7', 'SUNAMPE\r\n'),
(633, 64, '8', 'TAMBO DE MORA\r\n'),
(634, 64, '9', 'ALTO LARAN\r\n'),
(635, 64, '10', 'PUEBLO NUEVO\r\n'),
(636, 64, '11', 'SAN JUAN DE YANAC\r\n'),
(637, 64, '6', 'SAN PEDRO DE HUACARPANA\r\n'),
(638, 65, '1', 'TAMBOPATA\r\n'),
(639, 65, '2', 'INAMBARI\r\n'),
(640, 65, '4', 'LABERINTO\r\n'),
(641, 65, '3', 'LAS PIEDRAS\r\n'),
(642, 66, '1', 'IÑAPARI\r\n'),
(643, 66, '3', 'TAHUAMANU\r\n'),
(644, 66, '2', 'IBERIA\r\n'),
(645, 67, '1', 'MANU\r\n'),
(646, 67, '2', 'FITZCARRALD\r\n'),
(647, 67, '3', 'MADRE DE DIOS\r\n'),
(648, 67, '4', 'HUEPETUHE\r\n'),
(649, 68, '1', 'OXAPAMPA\r\n'),
(650, 68, '3', 'HUANCABAMBA\r\n'),
(651, 68, '4', 'PUERTO BERMUDEZ\r\n'),
(652, 68, '5', 'VILLA RICA\r\n'),
(653, 68, '6', 'POZUZO\r\n'),
(654, 68, '7', 'PALCAZU\r\n'),
(655, 68, '2', 'CHONTABAMBA\r\n'),
(656, 69, '3', 'HUACHON\r\n'),
(657, 69, '4', 'HUARIACA\r\n'),
(658, 69, '5', 'HUAYLLAY\r\n'),
(659, 69, '6', 'NINACACA\r\n'),
(660, 69, '7', 'PALLANCHACRA\r\n'),
(661, 69, '8', 'PAUCARTAMBO\r\n'),
(662, 69, '9', 'SAN FRANCISCO DE ASIS DE YARUSYACAN\r\n'),
(663, 69, '11', 'TICLACAYAN\r\n'),
(664, 69, '12', 'TINYAHUARCO\r\n'),
(665, 69, '13', 'VICCO\r\n'),
(666, 69, '14', 'YANACANCHA\r\n'),
(667, 69, '1', 'CHAUPIMARCA\r\n'),
(668, 69, '10', 'SIMON BOLIVAR\r\n'),
(669, 70, '1', 'YANAHUANCA\r\n'),
(670, 70, '2', 'CHACAYAN\r\n'),
(671, 70, '3', 'GOYLLARISQUIZGA\r\n'),
(672, 70, '5', 'SAN PEDRO DE PILLAO\r\n'),
(673, 70, '6', 'SANTA ANA DE TUSI\r\n'),
(674, 70, '7', 'TAPUC\r\n'),
(675, 70, '8', 'VILCABAMBA\r\n'),
(676, 70, '4', 'PAUCAR\r\n'),
(677, 71, '1', 'SULLANA\r\n'),
(678, 71, '3', 'LANCONES\r\n'),
(679, 71, '4', 'MARCAVELICA\r\n'),
(680, 71, '5', 'MIGUEL CHECA\r\n'),
(681, 71, '6', 'QUERECOTILLO\r\n'),
(682, 71, '7', 'SALITRAL\r\n'),
(683, 71, '8', 'IGNACIO ESCUDERO\r\n'),
(684, 71, '2', 'BELLAVISTA\r\n'),
(685, 72, '1', 'HUANCABAMBA\r\n'),
(686, 72, '2', 'CANCHAQUE\r\n'),
(687, 72, '3', 'HUARMACA\r\n'),
(688, 72, '4', 'SONDOR\r\n'),
(689, 72, '6', 'EL CARMEN DE LA FRONTERA\r\n'),
(690, 72, '7', 'SAN MIGUEL DE EL FAIQUE\r\n'),
(691, 72, '8', 'LALAQUIZ\r\n'),
(692, 72, '5', 'SONDORILLO\r\n'),
(693, 73, '1', 'SECHURA\r\n'),
(694, 73, '2', 'VICE\r\n'),
(695, 73, '4', 'BELLAVISTA DE LA UNION\r\n'),
(696, 73, '5', 'CRISTO NOS VALGA\r\n'),
(697, 73, '6', 'RINCONADA-LLICUAR\r\n'),
(698, 73, '3', 'BERNAL\r\n'),
(699, 74, '1', 'PARIÑAS\r\n'),
(700, 74, '3', 'LA BREA\r\n'),
(701, 74, '4', 'LOBITOS\r\n'),
(702, 74, '5', 'MANCORA\r\n'),
(703, 74, '6', 'LOS ORGANOS\r\n'),
(704, 74, '2', 'EL ALTO\r\n'),
(705, 75, '1', 'PAITA\r\n'),
(706, 75, '3', 'ARENAL\r\n'),
(707, 75, '4', 'LA HUACA\r\n'),
(708, 75, '5', 'COLAN\r\n'),
(709, 75, '6', 'TAMARINDO\r\n'),
(710, 75, '7', 'VICHAYAL\r\n'),
(711, 75, '2', 'AMOTAPE\r\n'),
(712, 76, '1', 'CHULUCANAS\r\n'),
(713, 76, '2', 'BUENOS AIRES\r\n'),
(714, 76, '3', 'CHALACO\r\n'),
(715, 76, '4', 'MORROPON\r\n'),
(716, 76, '6', 'SANTA CATALINA DE MOSSA\r\n'),
(717, 76, '7', 'SANTO DOMINGO\r\n'),
(718, 76, '8', 'LA MATANZA\r\n'),
(719, 76, '9', 'YAMANGO\r\n'),
(720, 76, '10', 'SAN JUAN DE BIGOTE\r\n'),
(721, 76, '5', 'SALITRAL\r\n'),
(722, 77, '2', 'FRIAS\r\n'),
(723, 77, '3', 'LAGUNAS\r\n'),
(724, 77, '4', 'MONTERO\r\n'),
(725, 77, '5', 'PACAIPAMPA\r\n'),
(726, 77, '6', 'SAPILLICA\r\n'),
(727, 77, '7', 'SICCHEZ\r\n'),
(728, 77, '8', 'SUYO\r\n'),
(729, 77, '10', 'PAIMAS\r\n'),
(730, 77, '1', 'AYABACA\r\n'),
(731, 77, '9', 'JILILI\r\n'),
(732, 78, '1', 'PIURA\r\n'),
(733, 78, '3', 'CASTILLA\r\n'),
(734, 78, '5', 'LA ARENA\r\n'),
(735, 78, '6', 'LA UNION\r\n'),
(736, 78, '7', 'LAS LOMAS\r\n'),
(737, 78, '9', 'TAMBO GRANDE\r\n'),
(738, 78, '13', 'CURA MORI\r\n'),
(739, 78, '14', 'EL TALLAN\r\n'),
(740, 78, '4', 'CATACAOS\r\n'),
(741, 79, '1', 'LOCUMBA\r\n'),
(742, 79, '2', 'ITE\r\n'),
(743, 79, '3', 'ILABAYA\r\n'),
(744, 80, '1', 'TACNA\r\n'),
(745, 80, '2', 'CALANA\r\n'),
(746, 80, '4', 'INCLAN\r\n'),
(747, 80, '7', 'PACHIA\r\n'),
(748, 80, '9', 'POCOLLAY\r\n'),
(749, 80, '10', 'SAMA\r\n'),
(750, 80, '11', 'ALTO DE LA ALIANZA\r\n'),
(751, 80, '12', 'CIUDAD NUEVA\r\n'),
(752, 80, '13', 'CORONEL GREGORIO ALBARRACIN LANCHIPA\r\n'),
(753, 80, '8', 'PALCA\r\n'),
(754, 81, '1', 'CANDARAVE\r\n'),
(755, 81, '2', 'CAIRANI\r\n'),
(756, 81, '3', 'CURIBAYA\r\n'),
(757, 81, '5', 'QUILAHUANI\r\n'),
(758, 81, '6', 'CAMILACA\r\n'),
(759, 81, '4', 'HUANUARA\r\n'),
(760, 82, '5', 'HEROES ALBARRACIN\r\n'),
(761, 82, '6', 'ESTIQUE\r\n'),
(762, 82, '7', 'ESTIQUE PAMPA\r\n'),
(763, 82, '10', 'SITAJARA\r\n'),
(764, 82, '11', 'SUSAPAYA\r\n'),
(765, 82, '12', 'TARUCACHI\r\n'),
(766, 82, '1', 'TARATA\r\n'),
(767, 82, '13', 'TICACO\r\n'),
(768, 83, '1', 'TUMBES\r\n'),
(769, 83, '2', 'CORRALES\r\n'),
(770, 83, '3', 'LA CRUZ\r\n'),
(771, 83, '4', 'PAMPAS DE HOSPITAL\r\n'),
(772, 83, '6', 'SAN JUAN DE LA VIRGEN\r\n'),
(773, 83, '5', 'SAN JACINTO\r\n'),
(774, 84, '1', 'ZARUMILLA\r\n'),
(775, 84, '2', 'MATAPALO\r\n'),
(776, 84, '3', 'PAPAYAL\r\n'),
(777, 84, '4', 'AGUAS VERDES\r\n'),
(778, 85, '1', 'ZORRITOS\r\n'),
(779, 85, '2', 'CASITAS\r\n'),
(780, 85, '3', 'CANOAS DE PUNTA SAL\r\n'),
(781, 86, '1', 'CHANCHAMAYO\r\n'),
(782, 86, '2', 'SAN RAMON\r\n'),
(783, 86, '3', 'VITOC\r\n'),
(784, 86, '4', 'SAN LUIS DE SHUARO\r\n'),
(785, 86, '5', 'PICHANAQUI\r\n'),
(786, 86, '6', 'PERENE\r\n'),
(787, 87, '1', 'LA OROYA\r\n'),
(788, 87, '2', 'CHACAPALPA\r\n'),
(789, 87, '3', 'HUAY HUAY\r\n'),
(790, 87, '4', 'MARCAPOMACOCHA\r\n'),
(791, 87, '5', 'MOROCOCHA\r\n'),
(792, 87, '6', 'PACCHA\r\n'),
(793, 87, '8', 'SUITUCANCHA\r\n'),
(794, 87, '9', 'YAULI\r\n'),
(795, 87, '10', 'SANTA ROSA DE SACCO\r\n'),
(796, 87, '7', 'SANTA BARBARA DE CARHUACAYAN\r\n'),
(797, 88, '21', 'PACCHA\r\n'),
(798, 88, '27', 'SAN PEDRO DE CHUNAN\r\n'),
(799, 88, '1', 'JAUJA\r\n'),
(800, 88, '2', 'ACOLLA\r\n'),
(801, 88, '3', 'APATA\r\n'),
(802, 88, '4', 'ATAURA\r\n'),
(803, 88, '6', 'EL MANTARO\r\n'),
(804, 88, '7', 'HUAMALI\r\n'),
(805, 88, '8', 'HUARIPAMPA\r\n'),
(806, 88, '9', 'HUERTAS\r\n'),
(807, 88, '10', 'JANJAILLO\r\n'),
(808, 88, '11', 'JULCAN\r\n'),
(809, 88, '12', 'LEONOR ORDOÑEZ\r\n'),
(810, 88, '14', 'MARCO\r\n'),
(811, 88, '15', 'MASMA\r\n'),
(812, 88, '16', 'MOLINOS\r\n'),
(813, 88, '17', 'MONOBAMBA\r\n'),
(814, 88, '18', 'MUQUI\r\n'),
(815, 88, '19', 'MUQUIYAUYO\r\n'),
(816, 88, '22', 'PANCAN\r\n'),
(817, 88, '23', 'PARCO\r\n'),
(818, 88, '24', 'POMACANCHA\r\n'),
(819, 88, '25', 'RICRAN\r\n'),
(820, 88, '26', 'SAN LORENZO\r\n'),
(821, 88, '28', 'SINCOS\r\n'),
(822, 88, '30', 'YAULI\r\n'),
(823, 88, '31', 'CURICACA\r\n'),
(824, 88, '32', 'MASMA CHICCHE\r\n'),
(825, 88, '33', 'SAUSA\r\n'),
(826, 88, '34', 'YAUYOS\r\n'),
(827, 88, '5', 'CANCHAYLLO\r\n'),
(828, 88, '13', 'LLOCLLAPAMPA\r\n'),
(829, 88, '20', 'PACA\r\n'),
(830, 88, '29', 'TUNAN MARCA\r\n'),
(831, 89, '1', 'HUANCAYO\r\n'),
(832, 89, '3', 'CARHUACALLANGA\r\n'),
(833, 89, '4', 'COLCA\r\n'),
(834, 89, '5', 'CULLHUAS\r\n'),
(835, 89, '6', 'CHACAPAMPA\r\n'),
(836, 89, '8', 'CHILCA\r\n'),
(837, 89, '9', 'CHONGOS ALTO\r\n'),
(838, 89, '12', 'CHUPURO\r\n'),
(839, 89, '13', 'EL TAMBO\r\n'),
(840, 89, '14', 'HUACRAPUQUIO\r\n'),
(841, 89, '16', 'HUALHUAS\r\n'),
(842, 89, '19', 'HUASICANCHA\r\n'),
(843, 89, '20', 'HUAYUCACHI\r\n'),
(844, 89, '21', 'INGENIO\r\n'),
(845, 89, '22', 'PARIAHUANCA\r\n'),
(846, 89, '23', 'PILCOMAYO\r\n'),
(847, 89, '24', 'PUCARA\r\n'),
(848, 89, '26', 'QUILCAS\r\n'),
(849, 89, '27', 'SAN AGUSTIN\r\n'),
(850, 89, '28', 'SAN JERONIMO DE TUNAN\r\n'),
(851, 89, '31', 'SANTO DOMINGO DE ACOBAMBA\r\n'),
(852, 89, '32', 'SAÑO\r\n'),
(853, 89, '33', 'SAPALLANGA\r\n'),
(854, 89, '36', 'VIQUES\r\n'),
(855, 89, '7', 'CHICCHE\r\n'),
(856, 89, '18', 'HUANCAN\r\n'),
(857, 89, '25', 'QUICHUAY\r\n'),
(858, 89, '34', 'SICAYA\r\n'),
(859, 90, '2', 'AHUAC\r\n'),
(860, 90, '3', 'CHONGOS BAJO\r\n'),
(861, 90, '4', 'HUACHAC\r\n'),
(862, 90, '5', 'HUAMANCACA CHICO\r\n'),
(863, 90, '6', 'SAN JUAN DE YSCOS\r\n'),
(864, 90, '7', 'SAN JUAN DE JARPA\r\n'),
(865, 90, '9', 'YANACANCHA\r\n'),
(866, 90, '1', 'CHUPACA\r\n'),
(867, 90, '8', 'TRES DE DICIEMBRE\r\n'),
(868, 91, '1', 'SATIPO\r\n'),
(869, 91, '2', 'COVIRIALI\r\n'),
(870, 91, '3', 'LLAYLLA\r\n'),
(871, 91, '4', 'MAZAMARI\r\n'),
(872, 91, '5', 'PAMPA HERMOSA\r\n'),
(873, 91, '6', 'PANGOA\r\n'),
(874, 91, '8', 'RIO TAMBO\r\n'),
(875, 91, '7', 'RIO NEGRO\r\n'),
(876, 92, '1', 'TARMA\r\n'),
(877, 92, '3', 'HUARICOLCA\r\n'),
(878, 92, '4', 'HUASAHUASI\r\n'),
(879, 92, '5', 'LA UNION\r\n'),
(880, 92, '6', 'PALCA\r\n'),
(881, 92, '7', 'PALCAMAYO\r\n'),
(882, 92, '8', 'SAN PEDRO DE CAJAS\r\n'),
(883, 92, '2', 'ACOBAMBA\r\n'),
(884, 92, '9', 'TAPO\r\n'),
(885, 93, '1', 'JUNIN\r\n'),
(886, 93, '2', 'CARHUAMAYO\r\n'),
(887, 93, '3', 'ONDORES\r\n'),
(888, 93, '4', 'ULCUMAYO\r\n'),
(889, 94, '1', 'CONCEPCION\r\n'),
(890, 94, '2', 'ACO\r\n'),
(891, 94, '3', 'ANDAMARCA\r\n'),
(892, 94, '4', 'COMAS\r\n'),
(893, 94, '5', 'COCHAS\r\n'),
(894, 94, '7', 'HEROINAS TOLEDO\r\n'),
(895, 94, '8', 'MANZANARES\r\n'),
(896, 94, '9', 'MARISCAL CASTILLA\r\n'),
(897, 94, '10', 'MATAHUASI\r\n'),
(898, 94, '11', 'MITO\r\n'),
(899, 94, '12', 'NUEVE DE JULIO\r\n'),
(900, 94, '14', 'SANTA ROSA DE OCOPA\r\n'),
(901, 94, '15', 'SAN JOSE DE QUERO\r\n'),
(902, 94, '6', 'CHAMBARA\r\n'),
(903, 94, '13', 'ORCOTUNA\r\n'),
(904, 95, '1', 'CASCAS\r\n'),
(905, 95, '2', 'LUCMA\r\n'),
(906, 95, '3', 'MARMOT\r\n'),
(907, 95, '4', 'SAYAPULLO\r\n'),
(908, 96, '1', 'CHEPEN\r\n'),
(909, 96, '2', 'PACANGA\r\n'),
(910, 96, '3', 'PUEBLO NUEVO\r\n'),
(911, 97, '4', 'HUARANCHAL\r\n'),
(912, 97, '1', 'OTUZCO\r\n'),
(913, 97, '2', 'AGALLPAMPA\r\n'),
(914, 97, '3', 'CHARAT\r\n'),
(915, 97, '5', 'LA CUESTA\r\n'),
(916, 97, '8', 'PARANDAY\r\n'),
(917, 97, '9', 'SALPO\r\n'),
(918, 97, '10', 'SINSICAP\r\n'),
(919, 97, '11', 'USQUIL\r\n'),
(920, 97, '13', 'MACHE\r\n'),
(921, 98, '1', 'TRUJILLO\r\n'),
(922, 98, '2', 'HUANCHACO\r\n'),
(923, 98, '3', 'LAREDO\r\n'),
(924, 98, '4', 'MOCHE\r\n'),
(925, 98, '5', 'SALAVERRY\r\n'),
(926, 98, '6', 'SIMBAL\r\n'),
(927, 98, '7', 'VICTOR LARCO HERRERA\r\n'),
(928, 98, '10', 'EL PORVENIR\r\n'),
(929, 98, '11', 'LA ESPERANZA\r\n'),
(930, 98, '12', 'FLORENCIA DE MORA\r\n'),
(931, 98, '9', 'POROTO\r\n'),
(932, 99, '1', 'VIRU\r\n'),
(933, 99, '3', 'GUADALUPITO\r\n'),
(934, 99, '2', 'CHAO\r\n'),
(935, 100, '1', 'JULCAN\r\n'),
(936, 100, '2', 'CARABAMBA\r\n'),
(937, 100, '4', 'HUASO\r\n'),
(938, 100, '3', 'CALAMARCA\r\n'),
(939, 101, '1', 'ASCOPE\r\n'),
(940, 101, '2', 'CHICAMA\r\n'),
(941, 101, '3', 'CHOCOPE\r\n'),
(942, 101, '4', 'SANTIAGO DE CAO\r\n'),
(943, 101, '5', 'MAGDALENA DE CAO\r\n'),
(944, 101, '6', 'PAIJAN\r\n'),
(945, 101, '8', 'CASA GRANDE\r\n'),
(946, 101, '7', 'RAZURI\r\n'),
(947, 102, '1', 'SANTIAGO DE CHUCO\r\n'),
(948, 102, '2', 'CACHICADAN\r\n'),
(949, 102, '3', 'MOLLEBAMBA\r\n'),
(950, 102, '4', 'MOLLEPATA\r\n'),
(951, 102, '5', 'QUIRUVILCA\r\n'),
(952, 102, '7', 'SITABAMBA\r\n'),
(953, 102, '8', 'ANGASMARCA\r\n'),
(954, 102, '6', 'SANTA CRUZ DE CHUCA\r\n'),
(955, 103, '1', 'TAYABAMBA\r\n'),
(956, 103, '2', 'BULDIBUYO\r\n'),
(957, 103, '3', 'CHILLIA\r\n'),
(958, 103, '4', 'HUAYLILLAS\r\n'),
(959, 103, '6', 'HUAYO\r\n'),
(960, 103, '7', 'ONGON\r\n'),
(961, 103, '8', 'PARCOY\r\n'),
(962, 103, '9', 'PATAZ\r\n'),
(963, 103, '10', 'PIAS\r\n'),
(964, 103, '11', 'TAURIJA\r\n'),
(965, 103, '13', 'SANTIAGO DE CHALLAS\r\n'),
(966, 103, '5', 'HUANCASPATA\r\n'),
(967, 103, '12', 'URPAY\r\n'),
(968, 104, '3', 'GUADALUPE\r\n'),
(969, 104, '4', 'JEQUETEPEQUE\r\n'),
(970, 104, '6', 'PACASMAYO\r\n'),
(971, 104, '8', 'SAN JOSE\r\n'),
(972, 104, '1', 'SAN PEDRO DE LLOC\r\n'),
(973, 105, '5', 'MARCABAL\r\n'),
(974, 105, '1', 'HUAMACHUCO\r\n'),
(975, 105, '2', 'COCHORCO\r\n'),
(976, 105, '3', 'CURGOS\r\n'),
(977, 105, '4', 'CHUGAY\r\n'),
(978, 105, '7', 'SARIN\r\n'),
(979, 105, '8', 'SARTIMBAMBA\r\n'),
(980, 105, '6', 'SANAGORAN\r\n'),
(981, 106, '1', 'BOLIVAR\r\n'),
(982, 106, '2', 'BAMBAMARCA\r\n'),
(983, 106, '4', 'LONGOTEA\r\n'),
(984, 106, '5', 'UCUNCHA\r\n'),
(985, 106, '6', 'UCHUMARCA\r\n'),
(986, 106, '3', 'CONDORMARCA\r\n'),
(987, 107, '17', 'PATAPO\r\n'),
(988, 107, '1', 'CHICLAYO\r\n'),
(989, 107, '2', 'CHONGOYAPE\r\n'),
(990, 107, '3', 'ETEN\r\n'),
(991, 107, '4', 'ETEN PUERTO\r\n'),
(992, 107, '5', 'LAGUNAS\r\n'),
(993, 107, '7', 'NUEVA ARICA\r\n'),
(994, 107, '8', 'OYOTUN\r\n'),
(995, 107, '9', 'PICSI\r\n'),
(996, 107, '10', 'PIMENTEL\r\n'),
(997, 107, '11', 'REQUE\r\n'),
(998, 107, '12', 'JOSE LEONARDO ORTIZ\r\n'),
(999, 107, '14', 'SAÑA\r\n'),
(1000, 107, '15', 'LA VICTORIA\r\n'),
(1001, 107, '16', 'CAYALTI\r\n'),
(1002, 107, '18', 'POMALCA\r\n'),
(1003, 107, '19', 'PUCALA\r\n'),
(1004, 107, '20', 'TUMAN\r\n'),
(1005, 107, '6', 'MONSEFU\r\n'),
(1006, 107, '13', 'SANTA ROSA\r\n'),
(1007, 108, '1', 'LAMBAYEQUE\r\n'),
(1008, 108, '2', 'CHOCHOPE\r\n'),
(1009, 108, '3', 'ILLIMO\r\n'),
(1010, 108, '5', 'MOCHUMI\r\n'),
(1011, 108, '6', 'MORROPE\r\n'),
(1012, 108, '7', 'MOTUPE\r\n'),
(1013, 108, '8', 'OLMOS\r\n'),
(1014, 108, '9', 'PACORA\r\n'),
(1015, 108, '10', 'SALAS\r\n'),
(1016, 108, '12', 'TUCUME\r\n'),
(1017, 108, '4', 'JAYANCA\r\n'),
(1018, 108, '11', 'SAN JOSE\r\n'),
(1019, 109, '2', 'INCAHUASI\r\n'),
(1020, 109, '4', 'PITIPO\r\n'),
(1021, 109, '5', 'PUEBLO NUEVO\r\n'),
(1022, 109, '6', 'MANUEL ANTONIO MESONES MURO\r\n'),
(1023, 109, '1', 'FERREÑAFE\r\n'),
(1024, 109, '3', 'CAÑARIS\r\n'),
(1025, 110, '1', 'HUARAL\r\n'),
(1026, 110, '2', 'ATAVILLOS ALTO\r\n'),
(1027, 110, '3', 'ATAVILLOS BAJO\r\n'),
(1028, 110, '4', 'AUCALLAMA\r\n'),
(1029, 110, '5', 'CHANCAY\r\n'),
(1030, 110, '6', 'IHUARI\r\n'),
(1031, 110, '7', 'LAMPIAN\r\n'),
(1032, 110, '9', 'SAN MIGUEL DE ACOS\r\n'),
(1033, 110, '10', 'VEINTISIETE DE NOVIEMBRE\r\n'),
(1034, 110, '11', 'SANTA CRUZ DE ANDAMARCA\r\n'),
(1035, 110, '12', 'SUMBILCA\r\n'),
(1036, 110, '8', 'PACARAOS\r\n'),
(1037, 111, '6', 'SAN BUENAVENTURA\r\n'),
(1038, 111, '1', 'CANTA\r\n'),
(1039, 111, '2', 'ARAHUAY\r\n'),
(1040, 111, '3', 'HUAMANTANGA\r\n'),
(1041, 111, '4', 'HUAROS\r\n'),
(1042, 111, '7', 'SANTA ROSA DE QUIVES\r\n'),
(1043, 111, '5', 'LACHAQUI\r\n'),
(1044, 112, '8', 'MALA\r\n'),
(1045, 112, '9', 'NUEVO IMPERIAL\r\n'),
(1046, 112, '10', 'PACARAN\r\n'),
(1047, 112, '11', 'QUILMANA\r\n'),
(1048, 112, '12', 'SAN ANTONIO\r\n'),
(1049, 112, '13', 'SAN LUIS\r\n'),
(1050, 112, '15', 'ZUÑIGA\r\n'),
(1051, 112, '16', 'ASIA\r\n'),
(1052, 112, '1', 'SAN VICENTE DE CAÑETE\r\n'),
(1053, 112, '2', 'CALANGO\r\n'),
(1054, 112, '3', 'CERRO AZUL\r\n'),
(1055, 112, '4', 'COAYLLO\r\n'),
(1056, 112, '5', 'CHILCA\r\n'),
(1057, 112, '7', 'LUNAHUANA\r\n'),
(1058, 112, '14', 'SANTA CRUZ DE FLORES\r\n'),
(1059, 112, '6', 'IMPERIAL\r\n'),
(1060, 113, '1', 'CAJATAMBO\r\n'),
(1061, 113, '5', 'COPA\r\n'),
(1062, 113, '7', 'HUANCAPON\r\n'),
(1063, 113, '8', 'MANAS\r\n'),
(1064, 113, '6', 'GORGOR\r\n'),
(1065, 114, '1', 'LIMA\r\n'),
(1066, 114, '2', 'ANCON\r\n'),
(1067, 114, '3', 'ATE\r\n'),
(1068, 114, '4', 'BREÑA\r\n'),
(1069, 114, '5', 'CARABAYLLO\r\n'),
(1070, 114, '7', 'CHACLACAYO\r\n'),
(1071, 114, '8', 'CHORRILLOS\r\n'),
(1072, 114, '9', 'LA VICTORIA\r\n'),
(1073, 114, '10', 'LA MOLINA\r\n'),
(1074, 114, '11', 'LINCE\r\n'),
(1075, 114, '13', 'LURIN\r\n'),
(1076, 114, '14', 'MAGDALENA DEL MAR\r\n'),
(1077, 114, '15', 'MIRAFLORES\r\n'),
(1078, 114, '16', 'PACHACAMAC\r\n'),
(1079, 114, '17', 'PUEBLO LIBRE\r\n'),
(1080, 114, '18', 'PUCUSANA\r\n'),
(1081, 114, '19', 'PUENTE PIEDRA\r\n'),
(1082, 114, '21', 'PUNTA NEGRA\r\n'),
(1083, 114, '22', 'RIMAC\r\n'),
(1084, 114, '23', 'SAN BARTOLO\r\n'),
(1085, 114, '24', 'SAN ISIDRO\r\n'),
(1086, 114, '25', 'BARRANCO\r\n'),
(1087, 114, '26', 'SAN MARTIN DE PORRES\r\n'),
(1088, 114, '27', 'SAN MIGUEL\r\n'),
(1089, 114, '29', 'SANTA ROSA\r\n'),
(1090, 114, '30', 'SANTIAGO DE SURCO\r\n'),
(1091, 114, '31', 'SURQUILLO\r\n'),
(1092, 114, '32', 'VILLA MARIA DEL TRIUNFO\r\n'),
(1093, 114, '33', 'JESUS MARIA\r\n'),
(1094, 114, '34', 'INDEPENDENCIA\r\n'),
(1095, 114, '35', 'EL AGUSTINO\r\n'),
(1096, 114, '36', 'SAN JUAN DE MIRAFLORES\r\n'),
(1097, 114, '38', 'SAN LUIS\r\n'),
(1098, 114, '39', 'CIENEGUILLA\r\n'),
(1099, 114, '40', 'SAN BORJA\r\n'),
(1100, 114, '41', 'VILLA EL SALVADOR\r\n'),
(1101, 114, '42', 'LOS OLIVOS\r\n'),
(1102, 114, '43', 'SANTA ANITA\r\n'),
(1103, 114, '6', 'COMAS\r\n'),
(1104, 114, '12', 'LURIGANCHO\r\n'),
(1105, 114, '20', 'PUNTA HERMOSA\r\n'),
(1106, 114, '28', 'SANTA MARIA DEL MAR\r\n'),
(1107, 114, '37', 'SAN JUAN DE LURIGANCHO\r\n'),
(1108, 115, '1', 'OYON\r\n'),
(1109, 115, '2', 'NAVAN\r\n'),
(1110, 115, '3', 'CAUJUL\r\n'),
(1111, 115, '4', 'ANDAJES\r\n'),
(1112, 115, '6', 'COCHAMARCA\r\n'),
(1113, 115, '5', 'PACHANGARA\r\n'),
(1114, 116, '1', 'BARRANCA\r\n'),
(1115, 116, '2', 'PARAMONGA\r\n'),
(1116, 116, '4', 'SUPE\r\n'),
(1117, 116, '5', 'SUPE PUERTO\r\n'),
(1118, 116, '3', 'PATIVILCA\r\n'),
(1119, 117, '2', 'ALIS\r\n'),
(1120, 117, '3', 'AYAUCA\r\n'),
(1121, 117, '4', 'AYAVIRI\r\n'),
(1122, 117, '5', 'AZANGARO\r\n'),
(1123, 117, '6', 'CACRA\r\n'),
(1124, 117, '7', 'CARANIA\r\n'),
(1125, 117, '9', 'COLONIA\r\n'),
(1126, 117, '10', 'CHOCOS\r\n'),
(1127, 117, '11', 'HUAMPARA\r\n'),
(1128, 117, '12', 'HUANCAYA\r\n'),
(1129, 117, '14', 'HUANTAN\r\n'),
(1130, 117, '15', 'HUAÑEC\r\n'),
(1131, 117, '17', 'LINCHA\r\n'),
(1132, 117, '18', 'MIRAFLORES\r\n'),
(1133, 117, '19', 'OMAS\r\n'),
(1134, 117, '21', 'QUINOCAY\r\n'),
(1135, 117, '22', 'SAN JOAQUIN\r\n'),
(1136, 117, '23', 'SAN PEDRO DE PILAS\r\n'),
(1137, 117, '25', 'TAURIPAMPA\r\n'),
(1138, 117, '26', 'TUPE\r\n'),
(1139, 117, '28', 'VIÑAC\r\n'),
(1140, 117, '29', 'VITIS\r\n'),
(1141, 117, '30', 'HONGOS\r\n'),
(1142, 117, '31', 'MADEAN\r\n'),
(1143, 117, '32', 'PUTINZA\r\n'),
(1144, 117, '13', 'HUANGASCAR\r\n'),
(1145, 117, '20', 'QUINCHES\r\n'),
(1146, 117, '1', 'YAUYOS\r\n'),
(1147, 117, '8', 'COCHAS\r\n'),
(1148, 117, '16', 'LARAOS\r\n'),
(1149, 117, '24', 'TANTA\r\n'),
(1150, 117, '27', 'TOMAS\r\n'),
(1151, 117, '33', 'CATAHUASI\r\n'),
(1152, 118, '1', 'MATUCANA\r\n'),
(1153, 118, '2', 'ANTIOQUIA\r\n'),
(1154, 118, '4', 'CARAMPOMA\r\n'),
(1155, 118, '5', 'SAN PEDRO DE CASTA\r\n'),
(1156, 118, '6', 'CUENCA\r\n'),
(1157, 118, '7', 'CHICLA\r\n'),
(1158, 118, '8', 'HUANZA\r\n'),
(1159, 118, '9', 'HUAROCHIRI\r\n'),
(1160, 118, '10', 'LAHUAYTAMBO\r\n'),
(1161, 118, '12', 'MARIATANA\r\n'),
(1162, 118, '13', 'RICARDO PALMA\r\n'),
(1163, 118, '14', 'SAN ANDRES DE TUPICOCHA\r\n'),
(1164, 118, '15', 'SAN ANTONIO\r\n'),
(1165, 118, '16', 'SAN BARTOLOME\r\n'),
(1166, 118, '17', 'SAN DAMIAN\r\n'),
(1167, 118, '19', 'SAN JUAN DE TANTARANCHE\r\n'),
(1168, 118, '20', 'SAN LORENZO DE QUINTI\r\n'),
(1169, 118, '21', 'SAN MATEO\r\n'),
(1170, 118, '22', 'SAN MATEO DE OTAO\r\n'),
(1171, 118, '23', 'SAN PEDRO DE HUANCAYRE\r\n'),
(1172, 118, '24', 'SANTA CRUZ DE COCACHACRA\r\n'),
(1173, 118, '26', 'SANTIAGO DE ANCHUCAYA\r\n'),
(1174, 118, '27', 'SANTIAGO DE TUNA\r\n'),
(1175, 118, '28', 'SANTO DOMINGO DE LOS OLLEROS\r\n'),
(1176, 118, '29', 'SURCO\r\n'),
(1177, 118, '30', 'HUACHUPAMPA\r\n'),
(1178, 118, '31', 'LARAOS\r\n'),
(1179, 118, '32', 'SAN JUAN DE IRIS\r\n'),
(1180, 118, '3', 'CALLAHUANCA\r\n'),
(1181, 118, '11', 'LANGA\r\n'),
(1182, 118, '18', 'SANGALLAYA\r\n'),
(1183, 118, '25', 'SANTA EULALIA\r\n'),
(1184, 119, '1', 'HUACHO\r\n'),
(1185, 119, '2', 'AMBAR\r\n'),
(1186, 119, '4', 'CALETA DE CARQUIN\r\n'),
(1187, 119, '5', 'CHECRAS\r\n'),
(1188, 119, '6', 'HUALMAY\r\n'),
(1189, 119, '7', 'HUAURA\r\n'),
(1190, 119, '9', 'PACCHO\r\n'),
(1191, 119, '11', 'SANTA LEONOR\r\n'),
(1192, 119, '12', 'SANTA MARIA\r\n'),
(1193, 119, '13', 'SAYAN\r\n'),
(1194, 119, '16', 'VEGUETA\r\n'),
(1195, 119, '8', 'LEONCIO PRADO\r\n'),
(1196, 120, '2', 'BELLAVISTA\r\n'),
(1197, 120, '3', 'LA PUNTA\r\n'),
(1198, 120, '4', 'CARMEN DE LA LEGUA-REYNOSO\r\n'),
(1199, 120, '5', 'LA PERLA\r\n'),
(1200, 120, '6', 'VENTANILLA\r\n'),
(1201, 120, '1', 'CALLAO\r\n'),
(1202, 121, '1', 'RAIMONDI\r\n'),
(1203, 121, '2', 'TAHUANIA\r\n'),
(1204, 121, '3', 'YURUA\r\n'),
(1205, 121, '4', 'SEPAHUA\r\n'),
(1206, 122, '1', 'PURUS\r\n'),
(1207, 123, '2', 'IRAZOLA\r\n'),
(1208, 123, '3', 'CURIMANA\r\n'),
(1209, 123, '1', 'PADRE ABAD\r\n'),
(1210, 124, '1', 'CALLERIA\r\n'),
(1211, 124, '3', 'MASISEA\r\n'),
(1212, 124, '4', 'CAMPOVERDE\r\n'),
(1213, 124, '5', 'IPARIA\r\n'),
(1214, 124, '6', 'NUEVA REQUENA\r\n'),
(1215, 124, '7', 'MANANTAY\r\n'),
(1216, 124, '2', 'YARINACOCHA\r\n'),
(1217, 125, '1', 'QUEROBAMBA\r\n'),
(1218, 125, '2', 'BELEN\r\n'),
(1219, 125, '3', 'CHALCOS\r\n'),
(1220, 125, '4', 'SAN SALVADOR DE QUIJE\r\n'),
(1221, 125, '5', 'PAICO\r\n'),
(1222, 125, '7', 'SAN PEDRO DE LARCAY\r\n'),
(1223, 125, '8', 'SORAS\r\n'),
(1224, 125, '9', 'HUACAÑA\r\n'),
(1225, 125, '10', 'CHILCAYOC\r\n'),
(1226, 125, '11', 'MORCOLLA\r\n'),
(1227, 125, '6', 'SANTIAGO DE PAUCARAY\r\n'),
(1228, 126, '1', 'HUANCAPI\r\n'),
(1229, 126, '2', 'ALCAMENCA\r\n'),
(1230, 126, '4', 'CANARIA\r\n'),
(1231, 126, '6', 'CAYARA\r\n'),
(1232, 126, '7', 'COLCA\r\n'),
(1233, 126, '8', 'HUAYA\r\n'),
(1234, 126, '9', 'HUAMANQUIQUIA\r\n'),
(1235, 126, '10', 'HUANCARAYLLA\r\n'),
(1236, 126, '14', 'VILCANCHOS\r\n'),
(1237, 126, '15', 'ASQUIPATA\r\n'),
(1238, 126, '3', 'APONGO\r\n'),
(1239, 126, '13', 'SARHUA\r\n'),
(1240, 127, '1', 'HUANTA\r\n'),
(1241, 127, '3', 'HUAMANGUILLA\r\n'),
(1242, 127, '5', 'LURICOCHA\r\n'),
(1243, 127, '8', 'SIVIA\r\n'),
(1244, 127, '9', 'LLOCHEGUA\r\n'),
(1245, 127, '4', 'IGUAIN\r\n'),
(1246, 127, '7', 'SANTILLANA\r\n'),
(1247, 127, '2', 'AYAHUANCO\r\n'),
(1248, 128, '2', 'COLTA\r\n'),
(1249, 128, '3', 'CORCULLA\r\n'),
(1250, 128, '4', 'LAMPA\r\n'),
(1251, 128, '5', 'MARCABAMBA\r\n'),
(1252, 128, '6', 'OYOLO\r\n'),
(1253, 128, '8', 'SAN JAVIER DE ALPABAMBA\r\n'),
(1254, 128, '10', 'SARA SARA\r\n'),
(1255, 128, '1', 'PAUSA\r\n'),
(1256, 128, '9', 'SAN JOSE DE USHUA\r\n'),
(1257, 128, '7', 'PARARCA\r\n'),
(1258, 129, '2', 'VISCHONGO\r\n'),
(1259, 129, '3', 'ACCOMARCA\r\n'),
(1260, 129, '4', 'CARHUANCA\r\n'),
(1261, 129, '5', 'CONCEPCION\r\n'),
(1262, 129, '6', 'HUAMBALPA\r\n'),
(1263, 129, '7', 'SAURAMA\r\n'),
(1264, 129, '8', 'INDEPENDENCIA\r\n'),
(1265, 129, '1', 'VILCAS HUAMAN\r\n'),
(1266, 130, '1', 'SANCOS\r\n'),
(1267, 130, '2', 'SACSAMARCA\r\n'),
(1268, 130, '3', 'SANTIAGO DE LUCANAMARCA\r\n'),
(1269, 130, '4', 'CARAPO\r\n'),
(1270, 131, '1', 'CORACORA\r\n'),
(1271, 131, '4', 'CORONEL CASTAÑEDA\r\n'),
(1272, 131, '5', 'CHUMPI\r\n'),
(1273, 131, '8', 'PACAPAUSA\r\n'),
(1274, 131, '12', 'PUYUSCA\r\n'),
(1275, 131, '15', 'SAN FRANCISCO DE RAVACAYCO\r\n'),
(1276, 131, '16', 'UPAHUACHO\r\n'),
(1277, 131, '11', 'PULLO\r\n'),
(1278, 132, '1', 'PUQUIO\r\n'),
(1279, 132, '2', 'AUCARA\r\n'),
(1280, 132, '3', 'CABANA\r\n'),
(1281, 132, '6', 'CHAVIÑA\r\n'),
(1282, 132, '8', 'CHIPAO\r\n'),
(1283, 132, '10', 'HUAC-HUAS\r\n'),
(1284, 132, '11', 'LARAMATE\r\n'),
(1285, 132, '12', 'LEONCIO PRADO\r\n'),
(1286, 132, '13', 'LUCANAS\r\n'),
(1287, 132, '14', 'LLAUTA\r\n'),
(1288, 132, '17', 'OTOCA\r\n'),
(1289, 132, '20', 'SANCOS\r\n'),
(1290, 132, '21', 'SAN JUAN\r\n'),
(1291, 132, '22', 'SAN PEDRO\r\n'),
(1292, 132, '24', 'SANTA ANA DE HUAYCAHUACHO\r\n'),
(1293, 132, '25', 'SANTA LUCIA\r\n'),
(1294, 132, '31', 'SAN PEDRO DE PALCO\r\n'),
(1295, 132, '32', 'SAN CRISTOBAL\r\n'),
(1296, 132, '4', 'CARMEN SALCEDO\r\n'),
(1297, 132, '16', 'OCAÑA\r\n'),
(1298, 132, '29', 'SAISA\r\n'),
(1299, 133, '1', 'SAN MIGUEL\r\n'),
(1300, 133, '2', 'ANCO\r\n'),
(1301, 133, '3', 'AYNA\r\n'),
(1302, 133, '4', 'CHILCAS\r\n'),
(1303, 133, '6', 'TAMBO\r\n'),
(1304, 133, '7', 'LUIS CARRANZA\r\n'),
(1305, 133, '8', 'SANTA ROSA\r\n'),
(1306, 133, '5', 'CHUNGUI\r\n'),
(1307, 134, '1', 'CANGALLO\r\n'),
(1308, 134, '6', 'LOS MOROCHUCOS\r\n'),
(1309, 134, '7', 'PARAS\r\n'),
(1310, 134, '8', 'TOTOS\r\n'),
(1311, 134, '11', 'MARIA PARADO DE BELLIDO\r\n'),
(1312, 134, '4', 'CHUSCHI\r\n'),
(1313, 135, '1', 'AYACUCHO\r\n'),
(1314, 135, '2', 'ACOS VINCHOS\r\n'),
(1315, 135, '3', 'CARMEN ALTO\r\n'),
(1316, 135, '4', 'CHIARA\r\n'),
(1317, 135, '5', 'QUINUA\r\n'),
(1318, 135, '6', 'SAN JOSE DE TICLLAS\r\n'),
(1319, 135, '8', 'SANTIAGO DE PISCHA\r\n'),
(1320, 135, '9', 'VINCHOS\r\n'),
(1321, 135, '10', 'TAMBILLO\r\n'),
(1322, 135, '11', 'ACOCRO\r\n'),
(1323, 135, '12', 'SOCOS\r\n'),
(1324, 135, '13', 'OCROS\r\n'),
(1325, 135, '14', 'PACAYCASA\r\n'),
(1326, 135, '7', 'SAN JUAN BAUTISTA\r\n'),
(1327, 135, '15', 'JESUS NAZARENO\r\n'),
(1328, 136, '1', 'SAN NICOLAS\r\n'),
(1329, 136, '2', 'COCHAMAL\r\n'),
(1330, 136, '3', 'CHIRIMOTO\r\n'),
(1331, 136, '4', 'HUAMBO\r\n'),
(1332, 136, '6', 'LONGAR\r\n'),
(1333, 136, '8', 'MARISCAL BENAVIDES\r\n'),
(1334, 136, '9', 'OMIA\r\n'),
(1335, 136, '10', 'SANTA ROSA\r\n'),
(1336, 136, '11', 'TOTORA\r\n'),
(1337, 136, '12', 'VISTA ALEGRE\r\n'),
(1338, 136, '5', 'LIMABAMBA\r\n'),
(1339, 136, '7', 'MILPUCC\r\n'),
(1340, 137, '1', 'BAGUA GRANDE\r\n'),
(1341, 137, '2', 'CAJARURO\r\n'),
(1342, 137, '3', 'CUMBA\r\n'),
(1343, 137, '4', 'EL MILAGRO\r\n'),
(1344, 137, '5', 'JAMALCA\r\n'),
(1345, 137, '7', 'YAMON\r\n'),
(1346, 137, '6', 'LONYA GRANDE\r\n'),
(1347, 138, '3', 'EL CENEPA\r\n'),
(1348, 138, '2', 'RIO SANTIAGO\r\n'),
(1349, 138, '1', 'NIEVA\r\n'),
(1350, 139, '1', 'LAMUD\r\n'),
(1351, 139, '2', 'CAMPORREDONDO\r\n'),
(1352, 139, '3', 'COCABAMBA\r\n'),
(1353, 139, '4', 'COLCAMAR\r\n'),
(1354, 139, '5', 'CONILA\r\n'),
(1355, 139, '6', 'INGUILPATA\r\n'),
(1356, 139, '8', 'LONYA CHICO\r\n'),
(1357, 139, '9', 'LUYA\r\n'),
(1358, 139, '10', 'LUYA VIEJO\r\n'),
(1359, 139, '11', 'MARIA\r\n'),
(1360, 139, '12', 'OCALLI\r\n'),
(1361, 139, '13', 'OCUMAL\r\n'),
(1362, 139, '15', 'SAN CRISTOBAL\r\n'),
(1363, 139, '16', 'SAN FRANCISCO DE YESO\r\n'),
(1364, 139, '17', 'SAN JERONIMO\r\n'),
(1365, 139, '18', 'SAN JUAN DE LOPECANCHA\r\n'),
(1366, 139, '19', 'SANTA CATALINA\r\n'),
(1367, 139, '20', 'SANTO TOMAS\r\n'),
(1368, 139, '22', 'TRITA\r\n'),
(1369, 139, '23', 'PROVIDENCIA\r\n'),
(1370, 139, '7', 'LONGUITA\r\n'),
(1371, 139, '14', 'PISUQUIA\r\n'),
(1372, 139, '21', 'TINGO\r\n'),
(1373, 140, '1', 'JUMBILLA\r\n'),
(1374, 140, '2', 'COROSHA\r\n'),
(1375, 140, '3', 'CUISPES\r\n'),
(1376, 140, '4', 'CHISQUILLA\r\n'),
(1377, 140, '6', 'FLORIDA\r\n'),
(1378, 140, '7', 'RECTA\r\n'),
(1379, 140, '8', 'SAN CARLOS\r\n'),
(1380, 140, '9', 'SHIPASBAMBA\r\n'),
(1381, 140, '10', 'VALERA\r\n'),
(1382, 140, '11', 'YAMBRASBAMBA\r\n'),
(1383, 140, '5', 'CHURUJA\r\n'),
(1384, 140, '12', 'JAZAN\r\n'),
(1385, 141, '1', 'LA PECA\r\n'),
(1386, 141, '2', 'ARAMANGO\r\n'),
(1387, 141, '4', 'EL PARCO\r\n'),
(1388, 141, '6', 'IMAZA\r\n'),
(1389, 141, '3', 'COPALLIN\r\n'),
(1390, 142, '1', 'CHACHAPOYAS\r\n'),
(1391, 142, '2', 'ASUNCION\r\n'),
(1392, 142, '4', 'CHETO\r\n'),
(1393, 142, '5', 'CHILIQUIN\r\n'),
(1394, 142, '6', 'CHUQUIBAMBA\r\n'),
(1395, 142, '7', 'GRANADA\r\n'),
(1396, 142, '8', 'HUANCAS\r\n'),
(1397, 142, '10', 'LEYMEBAMBA\r\n'),
(1398, 142, '11', 'LEVANTO\r\n'),
(1399, 142, '12', 'MAGDALENA\r\n'),
(1400, 142, '13', 'MARISCAL CASTILLA\r\n'),
(1401, 142, '14', 'MOLINOPAMPA\r\n'),
(1402, 142, '15', 'MONTEVIDEO\r\n'),
(1403, 142, '16', 'OLLEROS\r\n'),
(1404, 142, '18', 'SAN FRANCISCO DE DAGUAS\r\n'),
(1405, 142, '19', 'SAN ISIDRO DE MAINO\r\n'),
(1406, 142, '20', 'SOLOCO\r\n'),
(1407, 142, '21', 'SONCHE\r\n'),
(1408, 142, '3', 'BALSAS\r\n'),
(1409, 142, '9', 'LA JALCA\r\n'),
(1410, 142, '17', 'QUINJALCA\r\n'),
(1411, 143, '9', 'PAUCAS\r\n'),
(1412, 143, '10', 'PONTO\r\n'),
(1413, 143, '11', 'RAHUAPAMPA\r\n'),
(1414, 143, '12', 'RAPAYAN\r\n'),
(1415, 143, '14', 'SAN PEDRO DE CHANA\r\n'),
(1416, 143, '15', 'UCO\r\n'),
(1417, 143, '16', 'ANRA\r\n'),
(1418, 143, '3', 'CHAVIN DE HUANTAR\r\n'),
(1419, 143, '4', 'HUACACHI\r\n'),
(1420, 143, '5', 'HUACHIS\r\n'),
(1421, 143, '6', 'HUACCHIS\r\n'),
(1422, 143, '8', 'MASIN\r\n'),
(1423, 143, '2', 'CAJAY\r\n'),
(1424, 143, '7', 'HUANTAR\r\n'),
(1425, 143, '13', 'SAN MARCOS\r\n'),
(1426, 143, '1', 'HUARI\r\n'),
(1427, 144, '1', 'CORONGO\r\n'),
(1428, 144, '2', 'ACO\r\n'),
(1429, 144, '3', 'BAMBAS\r\n'),
(1430, 144, '5', 'LA PAMPA\r\n'),
(1431, 144, '6', 'YANAC\r\n'),
(1432, 144, '7', 'YUPAN\r\n'),
(1433, 144, '4', 'CUSCA\r\n'),
(1434, 145, '1', 'CHIQUIAN\r\n'),
(1435, 145, '2', 'ABELARDO PARDO LEZAMETA\r\n'),
(1436, 145, '4', 'AQUIA\r\n'),
(1437, 145, '5', 'CAJACAY\r\n'),
(1438, 145, '11', 'HUASTA\r\n'),
(1439, 145, '13', 'MANGAS\r\n'),
(1440, 145, '15', 'PACLLON\r\n'),
(1441, 145, '17', 'SAN MIGUEL DE CORPANQUI\r\n'),
(1442, 145, '20', 'TICLLOS\r\n'),
(1443, 145, '21', 'ANTONIO RAIMONDI\r\n'),
(1444, 145, '22', 'CANIS\r\n'),
(1445, 145, '24', 'LA PRIMAVERA\r\n'),
(1446, 145, '25', 'HUALLANCA\r\n'),
(1447, 145, '10', 'HUAYLLACAYAN\r\n'),
(1448, 145, '23', 'COLQUIOC\r\n'),
(1449, 146, '1', 'CHACAS\r\n'),
(1450, 146, '2', 'ACOCHACA\r\n'),
(1451, 147, '1', 'LLAMELLIN\r\n'),
(1452, 147, '3', 'CHACCHO\r\n'),
(1453, 147, '4', 'CHINGAS\r\n'),
(1454, 147, '6', 'SAN JUAN DE RONTOY\r\n'),
(1455, 147, '2', 'ACZO\r\n'),
(1456, 147, '5', 'MIRGAS\r\n'),
(1457, 148, '1', 'CHIMBOTE\r\n'),
(1458, 148, '2', 'CACERES DEL PERU\r\n'),
(1459, 148, '4', 'MORO\r\n'),
(1460, 148, '5', 'NEPEÑA\r\n'),
(1461, 148, '6', 'SAMANCO\r\n'),
(1462, 148, '7', 'SANTA\r\n'),
(1463, 148, '8', 'COISHCO\r\n'),
(1464, 148, '9', 'NUEVO CHIMBOTE\r\n'),
(1465, 148, '3', 'MACATE\r\n'),
(1466, 149, '1', 'POMABAMBA\r\n'),
(1467, 149, '3', 'PAROBAMBA\r\n'),
(1468, 149, '4', 'QUINUABAMBA\r\n'),
(1469, 149, '2', 'HUAYLLAN\r\n'),
(1470, 150, '1', 'CARAZ\r\n'),
(1471, 150, '2', 'HUALLANCA\r\n'),
(1472, 150, '3', 'HUATA\r\n'),
(1473, 150, '5', 'MATO\r\n'),
(1474, 150, '6', 'PAMPAROMAS\r\n'),
(1475, 150, '7', 'PUEBLO LIBRE\r\n'),
(1476, 150, '8', 'SANTA CRUZ\r\n'),
(1477, 150, '9', 'YURACMARCA\r\n'),
(1478, 150, '10', 'SANTO TORIBIO\r\n'),
(1479, 150, '4', 'HUAYLAS\r\n'),
(1480, 151, '2', 'BUENA VISTA ALTA\r\n'),
(1481, 151, '3', 'COMANDANTE NOEL\r\n'),
(1482, 151, '5', 'YAUTAN\r\n'),
(1483, 151, '1', 'CASMA\r\n'),
(1484, 152, '1', 'CARHUAZ\r\n'),
(1485, 152, '2', 'ACOPAMPA\r\n'),
(1486, 152, '3', 'AMASHCA\r\n'),
(1487, 152, '4', 'ANTA\r\n'),
(1488, 152, '6', 'MARCARA\r\n'),
(1489, 152, '7', 'PARIAHUANCA\r\n'),
(1490, 152, '8', 'SAN MIGUEL DE ACO\r\n'),
(1491, 152, '9', 'SHILLA\r\n'),
(1492, 152, '10', 'TINCO\r\n'),
(1493, 152, '11', 'YUNGAR\r\n'),
(1494, 152, '5', 'ATAQUERO\r\n'),
(1495, 153, '1', 'AIJA\r\n'),
(1496, 153, '3', 'CORIS\r\n'),
(1497, 153, '6', 'LA MERCED\r\n'),
(1498, 153, '8', 'SUCCHA\r\n'),
(1499, 153, '5', 'HUACLLAN\r\n'),
(1500, 154, '1', 'HUARAZ\r\n'),
(1501, 154, '2', 'INDEPENDENCIA\r\n'),
(1502, 154, '3', 'COCHABAMBA\r\n'),
(1503, 154, '4', 'COLCABAMBA\r\n'),
(1504, 154, '5', 'HUANCHAY\r\n'),
(1505, 154, '6', 'JANGAS\r\n'),
(1506, 154, '8', 'OLLEROS\r\n'),
(1507, 154, '9', 'PAMPAS\r\n'),
(1508, 154, '10', 'PARIACOTO\r\n'),
(1509, 154, '11', 'PIRA\r\n'),
(1510, 154, '12', 'TARICA\r\n'),
(1511, 154, '7', 'LA LIBERTAD\r\n'),
(1512, 155, '1', 'ACAS\r\n'),
(1513, 155, '2', 'CAJAMARQUILLA\r\n'),
(1514, 155, '3', 'CARHUAPAMPA\r\n'),
(1515, 155, '4', 'COCHAS\r\n'),
(1516, 155, '5', 'CONGAS\r\n'),
(1517, 155, '6', 'LLIPA\r\n'),
(1518, 155, '8', 'SAN CRISTOBAL DE RAJAN\r\n'),
(1519, 155, '9', 'SAN PEDRO\r\n'),
(1520, 155, '10', 'SANTIAGO DE CHILCAS\r\n'),
(1521, 155, '7', 'OCROS\r\n'),
(1522, 156, '1', 'HUARMEY\r\n'),
(1523, 156, '2', 'COCHAPETI\r\n'),
(1524, 156, '3', 'HUAYAN\r\n'),
(1525, 156, '4', 'MALVAS\r\n'),
(1526, 156, '5', 'CULEBRAS\r\n'),
(1527, 157, '2', 'YAUYA\r\n'),
(1528, 157, '3', 'SAN NICOLAS\r\n'),
(1529, 157, '1', 'SAN LUIS\r\n'),
(1530, 158, '1', 'YUNGAY\r\n'),
(1531, 158, '2', 'CASCAPARA\r\n'),
(1532, 158, '3', 'MANCOS\r\n'),
(1533, 158, '4', 'MATACOTO\r\n'),
(1534, 158, '6', 'RANRAHIRCA\r\n'),
(1535, 158, '7', 'SHUPLUY\r\n'),
(1536, 158, '8', 'YANAMA\r\n'),
(1537, 158, '5', 'QUILLO\r\n'),
(1538, 159, '2', 'ALFONSO UGARTE\r\n'),
(1539, 159, '3', 'CHINGALPO\r\n'),
(1540, 159, '4', 'HUAYLLABAMBA\r\n'),
(1541, 159, '5', 'QUICHES\r\n'),
(1542, 159, '6', 'SICSIBAMBA\r\n'),
(1543, 159, '7', 'ACOBAMBA\r\n'),
(1544, 159, '9', 'RAGASH\r\n'),
(1545, 159, '10', 'SAN JUAN\r\n'),
(1546, 159, '1', 'SIHUAS\r\n'),
(1547, 159, '8', 'CASHAPAMPA\r\n'),
(1548, 160, '1', 'RECUAY\r\n'),
(1549, 160, '2', 'COTAPARACO\r\n'),
(1550, 160, '3', 'HUAYLLAPAMPA\r\n'),
(1551, 160, '4', 'MARCA\r\n');
INSERT INTO `distrito` (`id`, `provincia_id`, `codigo`, `nombre`) VALUES
(1552, 160, '6', 'PARARIN\r\n'),
(1553, 160, '7', 'TAPACOCHA\r\n'),
(1554, 160, '8', 'TICAPAMPA\r\n'),
(1555, 160, '9', 'LLACLLIN\r\n'),
(1556, 160, '10', 'CATAC\r\n'),
(1557, 160, '5', 'PAMPAS CHICO\r\n'),
(1558, 161, '1', 'CABANA\r\n'),
(1559, 161, '2', 'BOLOGNESI\r\n'),
(1560, 161, '3', 'CONCHUCOS\r\n'),
(1561, 161, '4', 'HUACASCHUQUE\r\n'),
(1562, 161, '6', 'LACABAMBA\r\n'),
(1563, 161, '7', 'LLAPO\r\n'),
(1564, 161, '8', 'PALLASCA\r\n'),
(1565, 161, '9', 'PAMPAS\r\n'),
(1566, 161, '10', 'SANTA ROSA\r\n'),
(1567, 161, '11', 'TAUCA\r\n'),
(1568, 161, '5', 'HUANDOVAL\r\n'),
(1569, 162, '1', 'PISCOBAMBA\r\n'),
(1570, 162, '2', 'CASCA\r\n'),
(1571, 162, '3', 'LUCMA\r\n'),
(1572, 162, '5', 'LLAMA\r\n'),
(1573, 162, '6', 'LLUMPA\r\n'),
(1574, 162, '7', 'MUSGA\r\n'),
(1575, 162, '8', 'ELEAZAR GUZMAN BARRON\r\n'),
(1576, 162, '4', 'FIDEL OLIVAS ESCUDERO\r\n'),
(1577, 163, '1', 'RAMON CASTILLA\r\n'),
(1578, 163, '2', 'PEBAS\r\n'),
(1579, 163, '3', 'YAVARI\r\n'),
(1580, 163, '4', 'SAN PABLO\r\n'),
(1581, 164, '2', 'BALSAPUERTO\r\n'),
(1582, 164, '5', 'JEBEROS\r\n'),
(1583, 164, '6', 'LAGUNAS\r\n'),
(1584, 164, '10', 'SANTA CRUZ\r\n'),
(1585, 164, '11', 'TENIENTE CESAR LOPEZ ROJAS\r\n'),
(1586, 164, '1', 'YURIMAGUAS\r\n'),
(1587, 165, '2', 'ANDOAS\r\n'),
(1588, 165, '3', 'CAHUAPANAS\r\n'),
(1589, 165, '4', 'MANSERICHE\r\n'),
(1590, 165, '5', 'MORONA\r\n'),
(1591, 165, '6', 'PASTAZA\r\n'),
(1592, 165, '1', 'BARRANCA\r\n'),
(1593, 166, '1', 'CONTAMANA\r\n'),
(1594, 166, '3', 'PADRE MARQUEZ\r\n'),
(1595, 166, '4', 'PAMPA HERMOSA\r\n'),
(1596, 166, '6', 'INAHUAYA\r\n'),
(1597, 166, '5', 'SARAYACU\r\n'),
(1598, 166, '2', 'VARGAS GUERRA\r\n'),
(1599, 167, '1', 'REQUENA\r\n'),
(1600, 167, '2', 'ALTO TAPICHE\r\n'),
(1601, 167, '3', 'CAPELO\r\n'),
(1602, 167, '5', 'MAQUIA\r\n'),
(1603, 167, '6', 'PUINAHUA\r\n'),
(1604, 167, '7', 'SAQUENA\r\n'),
(1605, 167, '8', 'SOPLIN\r\n'),
(1606, 167, '9', 'TAPICHE\r\n'),
(1607, 167, '10', 'JENARO HERRERA\r\n'),
(1608, 167, '11', 'YAQUERANA\r\n'),
(1609, 167, '4', 'EMILIO SAN MARTIN\r\n'),
(1610, 168, '1', 'NAUTA\r\n'),
(1611, 168, '2', 'PARINARI\r\n'),
(1612, 168, '4', 'URARINAS\r\n'),
(1613, 168, '5', 'TROMPETEROS\r\n'),
(1614, 168, '3', 'TIGRE\r\n'),
(1615, 169, '1', 'IQUITOS\r\n'),
(1616, 169, '2', 'ALTO NANAY\r\n'),
(1617, 169, '3', 'FERNANDO LORES\r\n'),
(1618, 169, '4', 'LAS AMAZONAS\r\n'),
(1619, 169, '5', 'MAZAN\r\n'),
(1620, 169, '6', 'NAPO\r\n'),
(1621, 169, '8', 'TORRES CAUSANA\r\n'),
(1622, 169, '10', 'INDIANA\r\n'),
(1623, 169, '11', 'PUNCHANA\r\n'),
(1624, 169, '12', 'BELEN\r\n'),
(1625, 169, '13', 'SAN JUAN BAUTISTA\r\n'),
(1626, 169, '14', 'TNTE MANUEL CLAVERO\r\n'),
(1627, 169, '7', 'PUTUMAYO\r\n'),
(1628, 170, '1', 'ILO\r\n'),
(1629, 170, '2', 'EL ALGARROBAL\r\n'),
(1630, 170, '3', 'PACOCHA\r\n'),
(1631, 171, '1', 'OMATE\r\n'),
(1632, 171, '2', 'COALAQUE\r\n'),
(1633, 171, '3', 'CHOJATA\r\n'),
(1634, 171, '4', 'ICHUÑA\r\n'),
(1635, 171, '5', 'LA CAPILLA\r\n'),
(1636, 171, '6', 'LLOQUE\r\n'),
(1637, 171, '8', 'PUQUINA\r\n'),
(1638, 171, '9', 'QUINISTAQUILLAS\r\n'),
(1639, 171, '10', 'UBINAS\r\n'),
(1640, 171, '11', 'YUNGA\r\n'),
(1641, 171, '7', 'MATALAQUE\r\n'),
(1642, 172, '1', 'MOQUEGUA\r\n'),
(1643, 172, '2', 'CARUMAS\r\n'),
(1644, 172, '3', 'CUCHUMBAYA\r\n'),
(1645, 172, '4', 'SAN CRISTOBAL\r\n'),
(1646, 172, '5', 'TORATA\r\n'),
(1647, 172, '6', 'SAMEGUA\r\n'),
(1648, 173, '1', 'YUNGUYO\r\n'),
(1649, 173, '2', 'UNICACHI\r\n'),
(1650, 173, '3', 'ANAPIA\r\n'),
(1651, 173, '6', 'OLLARAYA\r\n'),
(1652, 173, '7', 'TINICACHI\r\n'),
(1653, 173, '4', 'COPANI\r\n'),
(1654, 173, '5', 'CUTURAPI\r\n'),
(1655, 174, '1', 'SANDIA\r\n'),
(1656, 174, '4', 'LIMBANI\r\n'),
(1657, 174, '5', 'PHARA\r\n'),
(1658, 174, '6', 'PATAMBUCO\r\n'),
(1659, 174, '7', 'QUIACA\r\n'),
(1660, 174, '8', 'SAN JUAN DEL ORO\r\n'),
(1661, 174, '10', 'YANAHUAYA\r\n'),
(1662, 174, '12', 'SAN PEDRO DE PUTINA PUNCO\r\n'),
(1663, 174, '3', 'CUYOCUYO\r\n'),
(1664, 174, '11', 'ALTO INAMBARI\r\n'),
(1665, 175, '1', 'LAMPA\r\n'),
(1666, 175, '2', 'CABANILLA\r\n'),
(1667, 175, '3', 'CALAPUJA\r\n'),
(1668, 175, '4', 'NICASIO\r\n'),
(1669, 175, '5', 'OCUVIRI\r\n'),
(1670, 175, '6', 'PALCA\r\n'),
(1671, 175, '7', 'PARATIA\r\n'),
(1672, 175, '9', 'SANTA LUCIA\r\n'),
(1673, 175, '10', 'VILAVILA\r\n'),
(1674, 175, '8', 'PUCARA\r\n'),
(1675, 176, '2', 'ACHAYA\r\n'),
(1676, 176, '3', 'ARAPA\r\n'),
(1677, 176, '4', 'ASILLO\r\n'),
(1678, 176, '6', 'CHUPA\r\n'),
(1679, 176, '7', 'JOSE DOMINGO CHOQUEHUANCA\r\n'),
(1680, 176, '8', 'MUÑANI\r\n'),
(1681, 176, '10', 'POTONI\r\n'),
(1682, 176, '12', 'SAMAN\r\n'),
(1683, 176, '13', 'SAN ANTON\r\n'),
(1684, 176, '15', 'SAN JUAN DE SALINAS\r\n'),
(1685, 176, '16', 'SANTIAGO DE PUPUJA\r\n'),
(1686, 176, '17', 'TIRAPATA\r\n'),
(1687, 176, '1', 'AZANGARO\r\n'),
(1688, 176, '5', 'CAMINACA\r\n'),
(1689, 176, '14', 'SAN JOSE\r\n'),
(1690, 177, '1', 'MOHO\r\n'),
(1691, 177, '3', 'TILALI\r\n'),
(1692, 177, '4', 'HUAYRAPATA\r\n'),
(1693, 177, '2', 'CONIMA\r\n'),
(1694, 178, '1', 'ILAVE\r\n'),
(1695, 178, '2', 'PILCUYO\r\n'),
(1696, 178, '3', 'SANTA ROSA\r\n'),
(1697, 178, '4', 'CAPASO\r\n'),
(1698, 178, '5', 'CONDURIRI\r\n'),
(1699, 179, '1', 'PUTINA\r\n'),
(1700, 179, '2', 'PEDRO VILCA APAZA\r\n'),
(1701, 179, '3', 'QUILCAPUNCU\r\n'),
(1702, 179, '4', 'ANANEA\r\n'),
(1703, 179, '5', 'SINA\r\n'),
(1704, 180, '1', 'JULIACA\r\n'),
(1705, 180, '3', 'CABANILLAS\r\n'),
(1706, 180, '4', 'CARACOTO\r\n'),
(1707, 180, '2', 'CABANA\r\n'),
(1708, 181, '1', 'AYAVIRI\r\n'),
(1709, 181, '2', 'ANTAUTA\r\n'),
(1710, 181, '3', 'CUPI\r\n'),
(1711, 181, '5', 'MACARI\r\n'),
(1712, 181, '6', 'NUÑOA\r\n'),
(1713, 181, '7', 'ORURILLO\r\n'),
(1714, 181, '8', 'SANTA ROSA\r\n'),
(1715, 181, '9', 'UMACHIRI\r\n'),
(1716, 181, '4', 'LLALLI\r\n'),
(1717, 182, '2', 'COJATA\r\n'),
(1718, 182, '4', 'INCHUPALLA\r\n'),
(1719, 182, '6', 'PUSI\r\n'),
(1720, 182, '7', 'ROSASPATA\r\n'),
(1721, 182, '8', 'TARACO\r\n'),
(1722, 182, '9', 'VILQUE CHICO\r\n'),
(1723, 182, '1', 'HUANCANE\r\n'),
(1724, 182, '11', 'HUATASANI\r\n'),
(1725, 183, '2', 'DESAGUADERO\r\n'),
(1726, 183, '3', 'HUACULLANI\r\n'),
(1727, 183, '6', 'PISACOMA\r\n'),
(1728, 183, '7', 'POMATA\r\n'),
(1729, 183, '10', 'ZEPITA\r\n'),
(1730, 183, '12', 'KELLUYO\r\n'),
(1731, 183, '1', 'JULI\r\n'),
(1732, 184, '1', 'MACUSANI\r\n'),
(1733, 184, '2', 'AJOYANI\r\n'),
(1734, 184, '3', 'AYAPATA\r\n'),
(1735, 184, '5', 'CORANI\r\n'),
(1736, 184, '6', 'CRUCERO\r\n'),
(1737, 184, '7', 'ITUATA\r\n'),
(1738, 184, '8', 'OLLACHEA\r\n'),
(1739, 184, '9', 'SAN GABAN\r\n'),
(1740, 184, '10', 'USICAYOS\r\n'),
(1741, 184, '4', 'COASA\r\n'),
(1742, 185, '1', 'PUNO\r\n'),
(1743, 185, '2', 'ACORA\r\n'),
(1744, 185, '4', 'CAPACHICA\r\n'),
(1745, 185, '5', 'COATA\r\n'),
(1746, 185, '6', 'CHUCUITO\r\n'),
(1747, 185, '7', 'HUATA\r\n'),
(1748, 185, '8', 'MAÑAZO\r\n'),
(1749, 185, '10', 'PICHACANI\r\n'),
(1750, 185, '11', 'SAN ANTONIO\r\n'),
(1751, 185, '13', 'VILQUE\r\n'),
(1752, 185, '14', 'PLATERIA\r\n'),
(1753, 185, '15', 'AMANTANI\r\n'),
(1754, 185, '9', 'PAUCARCOLLA\r\n'),
(1755, 185, '3', 'ATUNCOLLA\r\n'),
(1756, 185, '12', 'TIQUILLACA\r\n'),
(1757, 186, '1', 'BELLAVISTA\r\n'),
(1758, 186, '2', 'SAN RAFAEL\r\n'),
(1759, 186, '4', 'ALTO BIAVO\r\n'),
(1760, 186, '5', 'HUALLAGA\r\n'),
(1761, 186, '3', 'SAN PABLO\r\n'),
(1762, 186, '6', 'BAJO BIAVO\r\n'),
(1763, 187, '1', 'RIOJA\r\n'),
(1764, 187, '2', 'POSIC\r\n'),
(1765, 187, '3', 'YORONGOS\r\n'),
(1766, 187, '4', 'YURACYACU\r\n'),
(1767, 187, '6', 'ELIAS SOPLIN\r\n'),
(1768, 187, '7', 'SAN FERNANDO\r\n'),
(1769, 187, '8', 'PARDO MIGUEL\r\n'),
(1770, 187, '9', 'AWAJUN\r\n'),
(1771, 187, '5', 'NUEVA CAJAMARCA\r\n'),
(1772, 188, '1', 'LAMAS\r\n'),
(1773, 188, '3', 'BARRANQUITA\r\n'),
(1774, 188, '4', 'CAYNARACHI\r\n'),
(1775, 188, '5', 'CUÑUMBUQUI\r\n'),
(1776, 188, '6', 'PINTO RECODO\r\n'),
(1777, 188, '7', 'RUMISAPA\r\n'),
(1778, 188, '13', 'TABALOSOS\r\n'),
(1779, 188, '14', 'ZAPATERO\r\n'),
(1780, 188, '15', 'ALONSO DE ALVARADO\r\n'),
(1781, 188, '16', 'SAN ROQUE DE CUMBAZA\r\n'),
(1782, 188, '11', 'SHANAO\r\n'),
(1783, 189, '1', 'PICOTA\r\n'),
(1784, 189, '2', 'BUENOS AIRES\r\n'),
(1785, 189, '4', 'PILLUANA\r\n'),
(1786, 189, '5', 'PUCACACA\r\n'),
(1787, 189, '6', 'SAN CRISTOBAL\r\n'),
(1788, 189, '7', 'SAN HILARION\r\n'),
(1789, 189, '8', 'TINGO DE PONASA\r\n'),
(1790, 189, '9', 'TRES UNIDOS\r\n'),
(1791, 189, '10', 'SHAMBOYACU\r\n'),
(1792, 189, '3', 'CASPIZAPA\r\n'),
(1793, 190, '2', 'AGUA BLANCA\r\n'),
(1794, 190, '3', 'SHATOJA\r\n'),
(1795, 190, '4', 'SAN MARTIN\r\n'),
(1796, 190, '5', 'SANTA ROSA\r\n'),
(1797, 190, '1', 'SAN JOSE DE SISA\r\n'),
(1798, 191, '1', 'TOCACHE\r\n'),
(1799, 191, '2', 'NUEVO PROGRESO\r\n'),
(1800, 191, '4', 'SHUNTE\r\n'),
(1801, 191, '5', 'UCHIZA\r\n'),
(1802, 191, '3', 'POLVORA\r\n'),
(1803, 192, '1', 'TARAPOTO\r\n'),
(1804, 192, '2', 'ALBERTO LEVEAU\r\n'),
(1805, 192, '4', 'CACATACHI\r\n'),
(1806, 192, '6', 'CHAZUTA\r\n'),
(1807, 192, '8', 'EL PORVENIR\r\n'),
(1808, 192, '9', 'HUIMBAYOC\r\n'),
(1809, 192, '10', 'JUAN GUERRA\r\n'),
(1810, 192, '11', 'MORALES\r\n'),
(1811, 192, '12', 'PAPAPLAYA\r\n'),
(1812, 192, '16', 'SAN ANTONIO\r\n'),
(1813, 192, '20', 'SHAPAJA\r\n'),
(1814, 192, '21', 'LA BANDA DE SHILCAYO\r\n'),
(1815, 192, '7', 'CHIPURANA\r\n'),
(1816, 192, '19', 'SAUCE\r\n'),
(1817, 193, '1', 'JUANJUI\r\n'),
(1818, 193, '2', 'CAMPANILLA\r\n'),
(1819, 193, '4', 'PACHIZA\r\n'),
(1820, 193, '5', 'PAJARILLO\r\n'),
(1821, 193, '3', 'HUICUNGO\r\n'),
(1822, 194, '1', 'SAPOSOA\r\n'),
(1823, 194, '2', 'PISCOYACU\r\n'),
(1824, 194, '3', 'SACANCHE\r\n'),
(1825, 194, '4', 'TINGO DE SAPOSOA\r\n'),
(1826, 194, '5', 'ALTO SAPOSOA\r\n'),
(1827, 194, '6', 'EL ESLABON\r\n'),
(1828, 195, '1', 'MOYOBAMBA\r\n'),
(1829, 195, '2', 'CALZADA\r\n'),
(1830, 195, '3', 'HABANA\r\n'),
(1831, 195, '4', 'JEPELACIO\r\n'),
(1832, 195, '6', 'YANTALO\r\n'),
(1833, 195, '5', 'SORITOR\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ext_translations`
--

CREATE TABLE IF NOT EXISTS `ext_translations` (
`id` int(11) NOT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `object_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `field` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `foreign_key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fb_posts`
--

CREATE TABLE IF NOT EXISTS `fb_posts` (
  `id` int(11) NOT NULL,
  `imagen_publicacion` longtext COLLATE utf8_unicode_ci NOT NULL,
  `link_publicacion` longtext COLLATE utf8_unicode_ci NOT NULL,
  `veces_compartida_publicacion` int(11) NOT NULL,
  `me_gusta_publicacion` int(11) NOT NULL,
  `comentarios_publicacion` int(11) NOT NULL,
  `me_encanta_publicacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fb_posts`
--

INSERT INTO `fb_posts` (`id`, `imagen_publicacion`, `link_publicacion`, `veces_compartida_publicacion`, `me_gusta_publicacion`, `comentarios_publicacion`, `me_encanta_publicacion`) VALUES
(1, 'https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/13903224_1704419463157116_1718568067292912166_n.jpg?oh=f3c0214e6fb040c4cd4fca4ab46b77e6&oe=5882175D', 'https://www.facebook.com/lifeittvperu/photos/a.1580016908930706.1073741830.1579305129001884/1704419463157116/?type=3', 3, 5, 0, 1),
(2, 'https://scontent.xx.fbcdn.net/v/t1.0-9/p720x720/13895319_1703545959911133_6209473986969395568_n.jpg?oh=f64df71e8053c7734d35e202b0133548&oe=584C65CD', 'https://www.facebook.com/lifeittvperu/photos/a.1580016908930706.1073741830.1579305129001884/1703545959911133/?type=3', 3, 16, 0, 1),
(3, 'https://scontent.xx.fbcdn.net/v/t1.0-9/p720x720/13891912_1702809729984756_773518611347975524_n.jpg?oh=2cb0114af5bc64f835954c62e1667a84&oe=5848A85F', 'https://www.facebook.com/lifeittvperu/photos/a.1580016908930706.1073741830.1579305129001884/1702809729984756/?type=3', 0, 10, 1, 0),
(4, 'https://scontent.xx.fbcdn.net/v/t1.0-9/p720x720/13680533_1702548056677590_2146863917083813818_n.jpg?oh=82ff240b07335cba9e23655f58ab9e24&oe=587918E3', 'https://www.facebook.com/lifeittvperu/photos/a.1580016908930706.1073741830.1579305129001884/1702548056677590/?type=3', 0, 7, 5, 1),
(5, 'https://scontent.xx.fbcdn.net/v/t1.0-9/p720x720/13729006_1700586690207060_7246810208204008267_n.jpg?oh=16b92630eb90577c9f73ca7b64734cce&oe=587B070D', 'https://www.facebook.com/lifeittvperu/photos/a.1580016908930706.1073741830.1579305129001884/1700586690207060/?type=3', 48, 58, 58, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formulario_promocion`
--

CREATE TABLE IF NOT EXISTS `formulario_promocion` (
`id` int(11) NOT NULL,
  `promocion_id` int(11) DEFAULT NULL,
  `nombres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `formulario_promocion`
--

INSERT INTO `formulario_promocion` (`id`, `promocion_id`, `nombres`, `email`, `telefono`, `fecha`) VALUES
(1, 1, 'elio', 'elio@mail.com', '12313123', '0000-00-00 00:00:00'),
(2, 1, 'nombre', 'anthonyjts25@gmail.com', '12313123', '0000-00-00 00:00:00'),
(3, 2, 'adasdasd', 'asdasd', 'asdasd', '2016-09-09 16:23:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home`
--

CREATE TABLE IF NOT EXISTS `home` (
`id` int(11) NOT NULL,
  `imagen_video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contenido_video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_boton` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url_boton` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_seccion_compra` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mensaje_seccion_compra` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `procesos_compra` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `mensaje_proceso_compra` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `texto_boton_seccion_compra` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `url_boton_seccion_compra` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `catalogo_titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `catalogo_contenido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `catalogo_imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `catalogo_archivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `catalogo_boton` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home`
--

INSERT INTO `home` (`id`, `imagen_video`, `titulo_video`, `subtitulo_video`, `contenido_video`, `texto_boton`, `url_boton`, `titulo_seccion_compra`, `mensaje_seccion_compra`, `procesos_compra`, `mensaje_proceso_compra`, `texto_boton_seccion_compra`, `url_boton_seccion_compra`, `catalogo_titulo`, `catalogo_contenido`, `catalogo_imagen`, `catalogo_archivo`, `catalogo_boton`) VALUES
(1, 'uploads/home/video-img.jpg', 'Canal *Life it*', 'i Dream', 'Alivie dolores rápidamente de forma natural Masajes puntos de acupuntura en la sien permitiendo que te relajes y liberes todo el estrés.', 'Ir a canal Life it', 'http://www.youtube.com/watch?v=45_UgnJqLdw', 'Fácil y seguro', 'Ahorra tiempo, *Life it* te garantiza la mejor experiencia de compra', 'a:3:{i:0;a:2:{s:6:"imagen";s:23:"uploads/home/paso-1.png";s:9:"contenido";s:53:"Explicación clara del producto y todos los detalles.";}i:1;a:2:{s:6:"imagen";s:23:"uploads/home/paso-2.png";s:9:"contenido";s:53:"Compra segura gracias a nuestro sistema de seguridad.";}i:2;a:2:{s:6:"imagen";s:23:"uploads/home/paso-3.png";s:9:"contenido";s:55:"Envío rápido y devolución gratuita a nivel nacional.";}}', '¿Estás listo?', '¡Empieza ahora!', 'http://localhost:9001/categorias', 'Catálogo Octubre 2015', 'Aprovecha el fin de temporada. Descuentos de hasta 50%. Descarga el catálogo y conoce nuestras ofertas.', 'uploads/info/seccion-catalogo.jpg', 'uploads/home/campus1.pdf', 'Descargar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen_presentacion`
--

CREATE TABLE IF NOT EXISTS `imagen_presentacion` (
`id` int(11) NOT NULL,
  `imagenes_id` int(11) DEFAULT NULL,
  `url_video` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagen_todo` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `info`
--

CREATE TABLE IF NOT EXISTS `info` (
`id` int(11) NOT NULL,
  `correos_contacto` longtext COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` longtext COLLATE utf8_unicode_ci NOT NULL,
  `ga` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `contenido_terminos` longtext COLLATE utf8_unicode_ci NOT NULL,
  `mensaje_pagina_locales` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `imagen_pagina_locales` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valores_agregados_pagina_locales` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `mensaje_pagina_libro_reclamaciones` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `imagen_pagina_libro_reclamaciones` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo_principal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `televentas_lima` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `televentas_peru` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `horario_atencion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo_secundario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensaje_pagina_bolsa_trabajo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `imagen_pagina_bolsa_trabajo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitud` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `longitud` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `contenido_privacidad` longtext COLLATE utf8_unicode_ci NOT NULL,
  `url_ws_facebook` longtext COLLATE utf8_unicode_ci NOT NULL,
  `app_token_facebook` longtext COLLATE utf8_unicode_ci NOT NULL,
  `numero_publicaciones_facebook` int(11) NOT NULL,
  `fan_page_facebook` longtext COLLATE utf8_unicode_ci NOT NULL,
  `precio_mas_bajo` double NOT NULL,
  `precio_mas_alto` double NOT NULL,
  `facebook` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `banner_promocion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `precio_envio_lima` double NOT NULL,
  `precio_envio_provincias` double NOT NULL,
  `googleplus` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tiempo_envio_lima` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tiempo_envio_provincias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valorde_cambio` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `info`
--

INSERT INTO `info` (`id`, `correos_contacto`, `titulo`, `descripcion`, `ga`, `contenido_terminos`, `mensaje_pagina_locales`, `imagen_pagina_locales`, `valores_agregados_pagina_locales`, `mensaje_pagina_libro_reclamaciones`, `imagen_pagina_libro_reclamaciones`, `logo_principal`, `televentas_lima`, `televentas_peru`, `horario_atencion`, `logo_secundario`, `mensaje_pagina_bolsa_trabajo`, `imagen_pagina_bolsa_trabajo`, `latitud`, `longitud`, `contenido_privacidad`, `url_ws_facebook`, `app_token_facebook`, `numero_publicaciones_facebook`, `fan_page_facebook`, `precio_mas_bajo`, `precio_mas_alto`, `facebook`, `youtube`, `banner_promocion`, `precio_envio_lima`, `precio_envio_provincias`, `googleplus`, `tiempo_envio_lima`, `tiempo_envio_provincias`, `valorde_cambio`) VALUES
(1, 'jbravoaguinaga@gmail.com,juanb@staffcreativa.com', 'Lifeit', 'Lifeit', '00000000', '<p>El acceso y uso de este sitio web se rige por los &quot;T&eacute;rminos y Condiciones&quot; descritos a continuaci&oacute;n, as&iacute; como por la legislaci&oacute;n que se aplique en la Rep&uacute;blica de Per&uacute;. En consecuencia, todas las visitas y todos los contratos y transacciones que se realicen en este sitio, como asimismo sus efectos jur&iacute;dicos, quedar&aacute;n regidos por estas reglas y sometidas a esta legislaci&oacute;n.</p>\r\n\r\n<p><strong>Registro y datos de cuenta</strong></p>\r\n\r\n<p>Como usuario usted podr&aacute; registrarse y crear una cuenta en la cual podr&aacute; ver su historial de compras, sus productos deseados, realizar comentarios de sus productos, realizar comparaciones. Sin embargo, no es necesario su registro para realizar compras en nuestro sitio web.</p>\r\n\r\n<p>Si desea realizar alguna compra, tiene la opci&oacute;n de acceder como invitado y/o a trav&eacute;s de su cuenta de facebook. Sin embargo, si se le requerir&aacute; la informaci&oacute;n necesaria para poder realizar el despacho de su productos.</p>\r\n\r\n<p>El usuario asume total responsabilidad sobre la confidencialidad de la clave secreta de su cuenta. Tendr&aacute; la posibilidad de cambiar de clave accediendo al men&uacute; inferior &ldquo;Cambiar mi contrase&ntilde;a&rdquo;, as&iacute; como recuperar su clave a trav&eacute;s de una validaci&oacute;n de correo en caso no recuerde su clave de registro. La entrega a terceros de su clave personal no implica responsabilidad de Quality Products.</p>\r\n\r\n<p><strong>Derechos del usuario</strong></p>\r\n\r\n<p>Como usuario usted tendr&aacute; derechos de informaci&oacute;n, rectificaci&oacute;n y cancelaci&oacute;n de los datos personales conforme establecido por la ley sobre protecci&oacute;n de datos de car&aacute;cter personal, Ley N&deg; 29733. Adem&aacute;s, contar&aacute; con todos los derechos que son reconocidos por las leyes de protecci&oacute;n al consumidor, vigentes en el Per&uacute;.</p>\r\n\r\n<p><strong>Medios de Pago</strong></p>\r\n\r\n<p>Buscando su comodidad y seguridad le ofrecemos una variedad de medios de pago que se adaptan a sus necesidades:</p>\r\n\r\n<ul>\r\n	<li>Dep&oacute;sito bancario(*)</li>\r\n	<li>Tarjetas de cr&eacute;dito Visa</li>\r\n	<li>Tarjetas de cr&eacute;dito Master Card</li>\r\n	<li>PagoEfectivo (**)</li>\r\n	<li>Safety Pay (**)</li>\r\n</ul>\r\n\r\n<p>6. Revise la confirmaci&oacute;n de compra donde se indicar&aacute; el producto que usted est&aacute; comprando, costo de env&iacute;o y sus datos. Adicionalmente, se le enviar&aacute;n estos datos v&iacute;a email y tendr&aacute; la posibilidad de revisar la confirmaci&oacute;n de su compra desde su cuenta.<br />\r\ns&aacute;bado.</p>', 'Un nuevo concepto de retail://mejorar el estilo de vida de las personas.', 'uploads/local/b15-bg.jpg', 'a:4:{i:0;a:2:{s:6:"nombre";s:19:"Nuevas tecnologías";s:11:"descripcion";s:141:"Materiales utilizados en su fabricación y en su funcionalidad, desarrollada en torno a los hábitos de consumo y costumbres de las personas.";}i:1;a:2:{s:6:"nombre";s:7:"Diseño";s:11:"descripcion";s:158:"Cada producto crea una percepción de buen gusto, con buenos acabados, diseño ergonómico, líneas elegantes, formas acordes con el ritmo de la vida moderna.";}i:2;a:2:{s:6:"nombre";s:18:"Control de calidad";s:11:"descripcion";s:134:"Contamos con un cuidadoso y riguroso control de calidad en diferentes puntos del proceso de fabricación, transporte y almacenamiento.";}i:3;a:2:{s:6:"nombre";s:19:"Servicio al cliente";s:11:"descripcion";s:161:"Se demuestra en los productos a través de su uso en el tiempo, pudiendo las personas disfrutar de sus días con la confianza de que superarán sus expectativas.";}}', 'Ingresa a tu cuenta y conoce todos nuestros productos', 'uploads/banner-img.jpg', 'uploads/info/logo-561x-159.png', '399 - 1515', '0 800 20010', '<p>De Lunes a Viernes de 6:00 am a 1:00 am&nbsp;</p>\r\n\r\n<p>S&aacute;bados y Domingos de 6:00 am a 1:00 am</p>', 'uploads/info/logo.jpg', 'Ingresa a tu cuenta y conoce todos nuestros productos', 'uploads/banner-img.jpg', '-12.050000', '-77.050000,18', '<p>El acceso y uso de este sitio web se rige por los &quot;T&eacute;rminos y Condiciones&quot; descritos a continuaci&oacute;n, as&iacute; como por la legislaci&oacute;n que se aplique en la Rep&uacute;blica de Per&uacute;. En consecuencia, todas las visitas y todos los contratos y transacciones que se realicen en este sitio, como asimismo sus efectos jur&iacute;dicos, quedar&aacute;n regidos por estas reglas y sometidas a esta legislaci&oacute;n.</p>\r\n\r\n<p>Registro y datos de cuenta</p>\r\n\r\n<p>Como usuario usted podr&aacute; registrarse y crear una cuenta en la cual podr&aacute; ver su historial de compras, sus productos deseados, realizar comentarios de sus productos, realizar comparaciones. Sin embargo, no es necesario su registro para realizar compras en nuestro sitio web.</p>\r\n\r\n<p>Si desea realizar alguna compra, tiene la opci&oacute;n de acceder como invitado y/o a trav&eacute;s de su cuenta de facebook. Sin embargo, si se le requerir&aacute; la informaci&oacute;n necesaria para poder realizar el despacho de su productos.</p>\r\n\r\n<p>El usuario asume total responsabilidad sobre la confidencialidad de la clave secreta de su cuenta. Tendr&aacute; la posibilidad de cambiar de clave accediendo al men&uacute; inferior &ldquo;Cambiar mi contrase&ntilde;a&rdquo;, as&iacute; como recuperar su clave a trav&eacute;s de una validaci&oacute;n de correo en caso no recuerde su clave de registro. La entrega a terceros de su clave personal no implica responsabilidad de Quality Products.</p>\r\n\r\n<p>Derechos del usuario</p>\r\n\r\n<p>Como usuario usted tendr&aacute; derechos de informaci&oacute;n, rectificaci&oacute;n y cancelaci&oacute;n de los datos personales conforme establecido por la ley sobre protecci&oacute;n de datos de car&aacute;cter personal, Ley N&deg; 29733. Adem&aacute;s, contar&aacute; con todos los derechos que son reconocidos por las leyes de protecci&oacute;n al consumidor, vigentes en el Per&uacute;.</p>\r\n\r\n<p>Medios de Pago</p>\r\n\r\n<p>Buscando su comodidad y seguridad le ofrecemos una variedad de medios de pago que se adaptan a sus necesidades:</p>\r\n\r\n<ul>\r\n	<li>Dep&oacute;sito bancario(*)</li>\r\n	<li>Tarjetas de cr&eacute;dito Visa</li>\r\n	<li>Tarjetas de cr&eacute;dito Master Card</li>\r\n	<li>PagoEfectivo (**)</li>\r\n	<li>Safety Pay (**)</li>\r\n</ul>\r\n\r\n<p>6. Revise la confirmaci&oacute;n de compra donde se indicar&aacute; el producto que usted est&aacute; comprando, costo de env&iacute;o y sus datos. Adicionalmente, se le enviar&aacute;n estos datos v&iacute;a email y tendr&aacute; la posibilidad de revisar la confirmaci&oacute;n de su compra desde su cuenta.<br />\r\ns&aacute;bado.</p>', 'https://graph.facebook.com', '934278886718402|mV0zeulv33xkMrNu7MGbcMxxLQE', 5, 'lifeittvperu', 0, 500, 'http://facebook.com', 'http://youtube.com', 'uploads/info/banner-img.jpg', 50, 80, 'http://google.com', '3 a 5', '7 a 10', '3.36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `local`
--

CREATE TABLE IF NOT EXISTS `local` (
`id` int(11) NOT NULL,
  `lugar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `telefonos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` longtext COLLATE utf8_unicode_ci NOT NULL,
  `latitud` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `longitud` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `local`
--

INSERT INTO `local` (`id`, `lugar`, `direccion`, `telefonos`, `imagen`, `latitud`, `longitud`) VALUES
(1, 'C.C Jockey Plaza', 'Av. Petit Thouars 5356 Tda. 2063, Miraflores.', '555 5555,  998 170 746', 'uploads/info/seccion-catalogo.jpg', '-12.086676', '-77.058986'),
(2, 'La Rambla', 'C.C. Caminos del Inca 2do. piso.  Jirón Monterrey 123, Santiago de Surco', '444-3079, 998 170 746', 'uploads/local/pu-nn-ii-3s.jpg', '-12.076676', '-77.068986'),
(3, 'Plaza San Miguel', 'C.C. Caminos del Inca 2do. piso.  Jirón Monterrey 123, Santiago de Surco', '444-3079, 998 170 746', 'uploads/local/b20-img-1.jpg', '-12.076676', '-77.088986'),
(4, 'C.C. Plaza Norte', 'Av. Alfredo Mendiola No. 3698, Tienda  L430 Nuevo Boulevard, Independencia.', '998 170 746', 'uploads/local/img-home-campus-1.png', '-12.085676', '-77.058986'),
(5, 'C.C. Lima Plaza Sur', 'Calle Toulon con Antibes 197, La Molina', '656-9151', 'uploads/local/b10-img.png', '-12.066676', '-77.059986');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticia`
--

CREATE TABLE IF NOT EXISTS `noticia` (
`id` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contenido` longtext COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `noticia`
--

INSERT INTO `noticia` (`id`, `titulo`, `contenido`, `imagen`, `fecha`, `slug`) VALUES
(1, 'Este viernes todos en la Rambla San Borja', '<p>Te esperamos este viernes por la tarde en Mega Plaza con grandes sorpresas. &iexcl;No te lo puedes perder!</p>', 'uploads/noticia/noticia-1.jpg', '2016-08-14', 'este-viernes-todos-en-la-rambla-san-borja');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE IF NOT EXISTS `pedido` (
`id` int(11) NOT NULL,
  `distrito_facturacion_id` int(11) DEFAULT NULL,
  `nombres_completos_facturacion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `correo_facturacion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `telefono_facturacion` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_comprobante` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ruc` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dni` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `nombres_completos_envio` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correo_envio` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono_envio` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion_envio` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `distrito_envio_id` int(11) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `monto_final` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descuento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tiempo_envio` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `direccion_facturacion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`id`, `distrito_facturacion_id`, `nombres_completos_facturacion`, `correo_facturacion`, `telefono_facturacion`, `tipo_comprobante`, `ruc`, `dni`, `nombres_completos_envio`, `correo_envio`, `telefono_envio`, `direccion_envio`, `distrito_envio_id`, `fecha`, `monto_final`, `descuento`, `tiempo_envio`, `codigo`, `direccion_facturacion`, `estado`) VALUES
(1, NULL, 'Marcell Solis', 'e', 'l', 'b', NULL, 'l', 'Elio Marrcell', 'e', 'l', 'Av. Primavera 850 Chacarilla Santiago de Surco', NULL, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(2, NULL, 'Marcell Solis', 'elio.mstp@gmail.com', '3646956', 'boleta', NULL, '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', NULL, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(3, 1061, 'Marcell Solis', 'acongacardenas@gmail.com', 'asas', 'boleta', NULL, '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', NULL, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(4, 1061, 'Marcell Solis', 'acongacardenas@gmail.com', 'asas', 'boleta', NULL, '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', NULL, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(5, 7, 'Marcell G Solis', 'samelj_23_9@hotmail.com', '3646956', 'boleta', NULL, '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 7, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(6, 598, 'Marcell Solis', 'elio.mstp@gmail.com', '3646956', 'boleta', NULL, '77208443', 'Elio Marrcell', 'elio@gmail.com', '947331054', 'Av. Primavera 850 Chacarilla Santiago de Surco', 598, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(7, 598, 'Marcell Solis', 'elio.mstp@gmail.com', '3646956', 'boleta', NULL, '77208443', 'Elio Marrcell', 'elio@gmail.com', '947331054', 'Av. Primavera 850 Chacarilla Santiago de Surco', 598, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(8, 1019, 'Marcell Solis', 'a@a.com', '(+51) 971 019 565', 'boleta', NULL, '77208443', 'Elio Marrcell', 'elio@gmail.com', 'rtert', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1019, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(9, 1019, 'Marcell Solis', 'a@a.com', '(+51) 971 019 565', 'boleta', NULL, '77208443', 'Elio Marrcell', 'elio@gmail.com', 'rtert', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1019, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(10, 1019, 'Marcell Solis', 'a@a.com', '(+51) 971 019 565', 'boleta', NULL, '77208443', 'Elio Marrcell', 'elio@gmail.com', 'rtert', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1019, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(11, 909, 'Marcell G Solis', 'elio.mstp@gmail.com', '3646956', 'boleta', NULL, '77208443', 'Elio Marrcell', 'elio@gmail.com', '947331054', 'Av. Primavera 850 Chacarilla Santiago de Surco', 909, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(12, 1374, 'Marcell G Solis', 'elio.mstp@gmail.com', 'asas', 'boleta', NULL, '12345678', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1374, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(13, 1374, 'Marcell G Solis', 'elio.mstp@gmail.com', 'asas', 'boleta', NULL, '12345678', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1374, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(14, 1577, 'Marcell Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', NULL, '12345678', 'Elio Marrcell', 'elio@gmail.com', 'rtert', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1577, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(15, 1637, 'Marcell Solis', 'elio.mstp@gmail.com', 'asas', 'boleta', NULL, '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1637, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(16, 1637, 'Marcell Solis', 'elio.mstp@gmail.com', 'asas', 'boleta', NULL, '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1637, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(17, 1637, 'Marcell Solis', 'elio.mstp@gmail.com', 'asas', 'boleta', NULL, '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1637, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(18, 1637, 'Marcell Solis', 'elio.mstp@gmail.com', 'asas', 'boleta', NULL, '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1637, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(19, 1453, 'Marcell Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', NULL, '77208443', 'Elio Marrcell', 'a@a.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1453, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(20, 1453, 'Marcell Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', NULL, '77208443', 'Elio Marrcell', 'a@a.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1453, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(21, 1453, 'Marcell Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', NULL, '77208443', 'Elio Marrcell', 'a@a.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1453, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(22, 1453, 'Marcell Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', NULL, '77208443', 'Elio Marrcell', 'a@a.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1453, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(23, 1453, 'Marcell Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', NULL, '77208443', 'Elio Marrcell', 'a@a.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1453, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(24, 1453, 'Marcell Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', NULL, '77208443', 'Elio Marrcell', 'a@a.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1453, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(25, 1453, 'Marcell Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', NULL, '77208443', 'Elio Marrcell', 'a@a.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1453, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(26, 1453, 'Marcell Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', NULL, '77208443', 'Elio Marrcell', 'a@a.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1453, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(27, 1453, 'Marcell Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', NULL, '77208443', 'Elio Marrcell', 'a@a.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1453, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(28, 1453, 'Marcell Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', NULL, '77208443', 'Elio Marrcell', 'a@a.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1453, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(29, 1019, 'Marcell Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', NULL, '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1019, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(30, 781, 'Marcell Solis', 'acongacardenas@gmail.com', '3646956', 'ruc', '11077208443', '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 781, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(31, 781, 'Marcell G Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', '', '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 781, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(32, 781, 'Marcell G Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', '', '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 781, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(33, 781, 'Marcell G Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', '', '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 781, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(34, 781, 'Marcell G Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', '', '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 781, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(35, 781, 'Marcell G Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', '', '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 781, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(36, 781, 'Marcell G Solis', 'acongacardenas@gmail.com', '3646956', 'boleta', '', '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 781, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(37, 1245, 'Marcell Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'boleta', '', '77208443', 'Elio Marrcell', 'elio@gmail.com', 'rtert', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1245, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(38, 1245, 'Marcell Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'boleta', '', '77208443', 'Elio Marrcell', 'elio@gmail.com', 'rtert', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1245, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(39, 530, 'marcellllllllllllllllllll', 'a@a.com', '3646956', 'boleta', '', '77208443', 'Elio Marrcell', 'a@a.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 530, '0000-00-00 00:00:00', '', '', '', '', '', ''),
(40, NULL, 'faaaaaaaacccc', 'asdasdas', 'dasdasd', 'boleta', '', '77208443', 'asdasdasd', 'asdasdasd', 'adasdasd', 'Av. Primavera 850 Chacarilla Santiago de Surco', NULL, '2016-09-13 16:38:41', '', '', '', '', '', ''),
(41, 983, 'Marcell Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'boleta', '', '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 983, '2016-09-14 15:29:22', '', '', '', '', '', ''),
(42, 1374, 'Marcell Solis', 'elio.mstp@gmail.com', 'asas', 'boleta', '', '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1374, '2016-09-15 09:00:41', '', '', '', '', '', ''),
(43, 1450, 'Marcell Solis', 'acongacardenas@gmail.com', 'asas', 'boleta', '', '77208443', 'Elio Marrcell', 'elio@gmail.com', '12312312312', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1450, '2016-09-15 09:25:54', '', '', '', '', '', ''),
(44, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'boleta', 'Av. Primave', '', 'Elio Marrcell', 'elio@gmail.com', '947331054', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1090, '2016-09-22 10:50:38', '', '', '', '', '', ''),
(45, 1090, 'Marcell Solis', 'elio.mstp@gmail.com', '3646956', 'Av. Primavera 850 Ch', '', '77208443', 'Elio Marrcell', 'elio@gmail.com', '947331054', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1090, '2016-09-22 15:53:48', ' 1080', '', '', '', '', ''),
(46, 1090, 'Marcell Solis', 'elio.mstp@gmail.com', '3646956', 'Av. Primavera 850 Ch', '', '77208443', 'Elio Marrcell', 'elio@gmail.com', '947331054', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1090, '2016-09-22 15:55:54', ' 1080', '', '', '', '', ''),
(47, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'Av. Primavera 850 Ch', '', '77208443', 'Elio Marrcell', 'elio@gmail.com', '947331054', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1090, '2016-09-22 15:57:56', ' 1080', '', '', '', '', ''),
(48, 1090, 'Marcell Solis', 'elio.mstp@gmail.com', '3646956', 'factura', '11077208443', '7720', 'Marcell Solis', 'elio.mstp@gmail.com', '3646956', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1090, '2016-09-26 18:20:43', ' 1070', '', '', '', '', ''),
(49, 1065, 'Marcell G Solis', 'elio.mstp@gmail.com', '3646956', 'boleta', '', '77208443', 'Marcell G Solis', 'elio.mstp@gmail.com', '3646956', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1065, '2016-09-26 18:27:38', ' 250', '', '', '', '', ''),
(50, 1065, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'factura', '11077208443', '7720', 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1065, '2016-09-26 18:30:49', ' 200', '', '', '', '', ''),
(51, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '3646956', 'factura', NULL, '7720', 'Marcell G Solis', 'elio.mstp@gmail.com', '3646956', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1090, '2016-09-26 18:34:56', ' 150', '', '', '', '', ''),
(52, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'factura', '11077208443', '77208443', 'Marcell G Solis', 'Av. Primavera 850 Chacarilla Santiago de Surco', '(+51) 971 019 565', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1090, '2016-09-26 18:39:41', ' 200', '', '', '', '', ''),
(53, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'factura', '11077208443', '77208443', NULL, NULL, '', 'Av. Primavera 850 Chacarilla Santiago de Surco', 1090, '2016-09-26 19:02:29', ' 499', '', '', '', '', ''),
(54, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'boleta', '', '77208443', 'Elio Marrcell', 'elio@gmail.com', '947331054', 'Jr. Chimuc Capac #122 Dpto 201', 1090, '2016-09-26 19:05:37', ' 110', '', '', '', '', ''),
(55, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'boleta', '', '77208443', NULL, '', '', '3 a 5', 1090, '2016-09-26 19:31:19', ' 140', ' 10', NULL, '', '', ''),
(56, 1090, 'direccion', 'elio.mstp@gmail.com', '3646956', 'boleta', '', '77208443', NULL, '', '', '3 a 5', 1090, '2016-09-26 19:37:39', ' 190', ' 10', NULL, '', '', ''),
(57, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'boleta', NULL, '77208443', '', '', '', '', NULL, '2016-09-27 10:52:13', ' 190', ' 10', '3 a 5', '8df6u', 'Av. Primavera 850 Chacarilla Santiago de Surco', ''),
(58, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'factura', '11077208443', '77208443', '', '', '', '', NULL, '2016-09-27 10:53:18', ' 190', ' 10', '3 a 5', 'jeg1k', 'Av. Primavera 850 Chacarilla Santiago de Surco', ''),
(59, NULL, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'factura', '11077208443', '77208443', 'Elio Marrcell', 'elio@gmail.com', '947331054', 'Jr. Chimuc Capac #122 Dpto 201', 1099, '2016-09-27 10:54:04', ' 190', ' 10', '3 a 5', 'sejo4', 'Av. Primavera 850 Chacarilla Santiago de Surco', ''),
(60, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'factura', '11077208443', '77208443', 'Elio Marrcell', 'elio@gmail.com', '947331054', 'Jr. Chimuc Capac #122 Dpto 201', 1072, '2016-09-27 10:57:11', ' 190', ' 10', '3 a 5', 'jzecn', 'Av. Primavera 850 Chacarilla Santiago de Surco', ''),
(61, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'factura', '11077208443', '77208443', 'Elio Marrcell', 'elio@gmail.com', '947331054', 'Jr. Chimuc Capac #122 Dpto 201', 1072, '2016-09-27 10:58:26', ' 190', ' 10', '3 a 5', '355nwudk', 'Av. Primavera 850 Chacarilla Santiago de Surco', ''),
(62, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'factura', '11077208443', '77208443', 'Elio Marrcell', 'elio@gmail.com', '947331054', 'Jr. Chimuc Capac #122 Dpto 201', 1072, '2016-09-27 11:03:31', ' 190', ' 10', '3 a 5', '355nwudk', 'Av. Primavera 850 Chacarilla Santiago de Surco', 'P'),
(63, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'factura', '11077208443', '77208443', 'Elio Marrcell', 'elio@gmail.com', '947331054', 'Jr. Chimuc Capac #122 Dpto 201', 1072, '2016-09-27 11:03:49', ' 190', ' 10', '3 a 5', 'pz97ljue', 'Av. Primavera 850 Chacarilla Santiago de Surco', 'P'),
(64, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'factura', '11077208443', '77208443', 'Elio Marrcell', 'elio@gmail.com', '947331054', 'Jr. Chimuc Capac #122 Dpto 201', 1072, '2016-09-27 11:05:22', ' 190', ' 10', '3 a 5', 'h6vpzrpb', 'Av. Primavera 850 Chacarilla Santiago de Surco', 'P'),
(65, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'factura', '11077208443', '77208443', 'Elio Marrcell', 'elio@gmail.com', '947331054', 'Jr. Chimuc Capac #122 Dpto 201', 1072, '2016-09-27 11:09:34', ' 190', ' 10', '3 a 5', 'b1kaea7k', 'Av. Primavera 850 Chacarilla Santiago de Surco', 'P'),
(66, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'boleta', NULL, '77208443', '', '', '', '', NULL, '2016-09-27 11:24:21', ' 190', ' 10', '3 a 5', 'ocopa13x', 'Av. Primavera 850 Chacarilla Santiago de Surco', 'P'),
(67, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'boleta', NULL, '77208443', '', '', '', '', NULL, '2016-09-27 11:25:20', ' 190', ' 10', '3 a 5', '8vg83h8f', 'Av. Primavera 850 Chacarilla Santiago de Surco', 'P'),
(68, 1090, 'Marcell G', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'boleta', NULL, '77208443', '', '', '', '', NULL, '2016-09-27 11:37:05', ' 190', ' 10', '3 a 5', 'k0rb7ubt', 'Av. Primavera 850 Chacarilla Santiago de Surco', 'C'),
(69, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '3646956', 'boleta', NULL, '77208443', '', '', '', '', NULL, '2016-09-27 12:11:58', ' 600', ' 10', '3 a 5', 'b15hwy9u', 'Av. Primavera 850 Chacarilla Santiago de Surco', 'C'),
(70, 1099, 'Marcell G Solis', 'elio.mstp@gmail.com', '3646956', 'boleta', NULL, '77208443', '', '', '', '', NULL, '2016-09-27 13:08:00', ' 600', ' 0', '3 a 5', 'cpi65efd', 'Av. Primavera 850 Chacarilla Santiago de Surco', 'P'),
(71, 1099, 'Marcell G Solis', 'elio.mstp@gmail.com', '3646956', 'boleta', NULL, '77208443', '', '', '', '', NULL, '2016-09-27 13:10:08', ' 600', ' 0', '3 a 5', 'f8l2bpr4', 'Av. Primavera 850 Chacarilla Santiago de Surco', 'C'),
(72, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'boleta', NULL, '77208443', '', '', '', '', NULL, '2016-09-27 13:18:10', ' 499', ' 0', '3 a 5', '9qux0ujt', 'Av. Primavera 850 Chacarilla Santiago de Surco', 'C'),
(73, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '3646956', 'boleta', NULL, '77208443', '', '', '', '', NULL, '2016-09-27 13:29:58', ' 500', ' 0', '3 a 5', 'f82onygt', 'Av. Primavera 850 Chacarilla Santiago de Surco', 'C'),
(74, 1065, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'boleta', NULL, '77208443', '', '', '', '', NULL, '2016-09-27 15:02:22', ' 630', ' 20', '3 a 5', 'arr4q5he', 'Av. Primavera 850 Chacarilla Santiago de Surco', 'C'),
(75, 613, 'Marcell G Solis', 'elio.mstp@gmail.com', '3646956', 'boleta', NULL, '77208443', '', '', '', '', NULL, '2016-09-27 15:05:20', ' 200', ' 0', '7 a 10', '4xojl6q0', 'Av. Primavera 850 Chacarilla Santiago de Surco', 'C'),
(76, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'boleta', NULL, '77208443', '', '', '', '', NULL, '2016-09-27 15:08:17', ' 200', ' 0', '3 a 5', 'wzp5vvmt', 'Av. Primavera 850 Chacarilla Santiago de Surco', 'C'),
(77, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'boleta', NULL, '77208443', '', '', '', '', NULL, '2016-09-27 16:20:48', ' 135', ' 15', '3 a 5', 'blt1d2mp', 'Av. Primavera 850 Chacarilla Santiago de Surco', 'P'),
(78, 1090, 'Marcell G Solis', 'elio.mstp@gmail.com', '(+51) 971 019 565', 'boleta', NULL, '77208443', '', '', '', '', NULL, '2016-09-27 16:22:00', ' 135', ' 15', '3 a 5', 'zae00m3b', 'Av. Primavera 850 Chacarilla Santiago de Surco', 'P');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pregunta_frecuente`
--

CREATE TABLE IF NOT EXISTS `pregunta_frecuente` (
`id` int(11) NOT NULL,
  `pregunta` longtext COLLATE utf8_unicode_ci NOT NULL,
  `respuesta` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `pregunta_frecuente`
--

INSERT INTO `pregunta_frecuente` (`id`, `pregunta`, `respuesta`) VALUES
(1, '¿Como usuario usted podrá registrarse y crear una cuenta en la cual podrá ver su historial de compras, sus productos deseados?.', 'Realizar comentarios de sus productos, realizar comparaciones. Sin embargo, no es necesario su registro para realizar compras en nuestro sitio web.Si desea realizar alguna compra, tiene la opción de acceder como invitado y/o a través de su cuenta de facebook. Sin embargo, si se le requerirá la información necesaria para poder realizar el d espacho de su'),
(2, 'Por que ?', 'Aenean eu velit ut dolor condimentum aliquam. Maecenas vel fermentum neque, sit amet egestas elit. Fusce a rhoncus nibh. Duis bibendum, est eu ornare pellentesque, felis tellus imperdiet urna, vel egestas justo massa non nisi. Suspendisse id ligula nec tellus aliquet pharetra. Vivamus blandit blandit egestas. Donec mollis vitae velit id pretium. Vestibulum interdum ante eget purus vehicula bibendum at non libero. Vestibulum in augue congue, vehicula lacus vitae, ullamcorper nisi. Donec tristique placerat ligula at aliquet. Proin ut purus vitae libero elementum venenatis vel ac mauris.'),
(3, 'Probando pregunta frecuenta 3???', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla placerat nibh eu ligula feugiat malesuada ac id est. In ultrices lobortis tempus. Pellentesque quis ante non magna porttitor volutpat sit amet at urna. Nulla ut odio at arcu pellentesque condimentum vel eget neque. Aliquam non placerat urna. Donec hendrerit sagittis viverra. Proin commodo porttitor dui sit amet tincidunt. Suspendisse dapibus imperdiet eros. Nulla libero ligula, sodales a justo eget, ultrices rutrum sem. Aliquam ac tortor sed sapien egestas scelerisque. Maecenas porttitor porttitor diam, at aliquam lectus consectetur facilisis. Nullam ut finibus justo. Mauris sit amet facilisis tellus. Vestibulum sed turpis augue.</p>'),
(4, 'Pregunta 4', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla placerat nibh eu ligula feugiat malesuada ac id est. In ultrices lobortis tempus. Pellentesque quis ante non magna porttitor volutpat sit amet at urna. Nulla ut odio at arcu pellentesque condimentum vel eget neque. Aliquam non placerat urna. Donec hendrerit sagittis viverra. Proin commodo porttitor dui sit amet tincidunt. Suspendisse dapibus imperdiet eros. Nulla libero ligula, sodales a justo eget, ultrices rutrum sem. Aliquam ac tortor sed sapien egestas scelerisque. Maecenas porttitor porttitor diam, at aliquam lectus consectetur facilisis. Nullam ut finibus justo. Mauris sit amet facilisis tellus. Vestibulum sed turpis augue.</p>'),
(5, 'Pregunta 5', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla placerat nibh eu ligula feugiat malesuada ac id est. In ultrices lobortis tempus. Pellentesque quis ante non magna porttitor volutpat sit amet at urna. Nulla ut odio at arcu pellentesque condimentum vel eget neque. Aliquam non placerat urna. Donec hendrerit sagittis viverra. Proin commodo porttitor dui sit amet tincidunt. Suspendisse dapibus imperdiet eros. Nulla libero ligula, sodales a justo eget, ultrices rutrum sem. Aliquam ac tortor sed sapien egestas scelerisque. Maecenas porttitor porttitor diam, at aliquam lectus consectetur facilisis. Nullam ut finibus justo. Mauris sit amet facilisis tellus. Vestibulum sed turpis augue.</p>'),
(6, 'pregunta 6', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla placerat nibh eu ligula feugiat malesuada ac id est. In ultrices lobortis tempus. Pellentesque quis ante non magna porttitor volutpat sit amet at urna. Nulla ut odio at arcu pellentesque condimentum vel eget neque. Aliquam non placerat urna. Donec hendrerit sagittis viverra. Proin commodo porttitor dui sit amet tincidunt. Suspendisse dapibus imperdiet eros. Nulla libero ligula, sodales a justo eget, ultrices rutrum sem. Aliquam ac tortor sed sapien egestas scelerisque. Maecenas porttitor porttitor diam, at aliquam lectus consectetur facilisis. Nullam ut finibus justo. Mauris sit amet facilisis tellus. Vestibulum sed turpis augue.</p>'),
(7, 'Pregunta 7', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla placerat nibh eu ligula feugiat malesuada ac id est. In ultrices lobortis tempus. Pellentesque quis ante non magna porttitor volutpat sit amet at urna. Nulla ut odio at arcu pellentesque condimentum vel eget neque. Aliquam non placerat urna. Donec hendrerit sagittis viverra. Proin commodo porttitor dui sit amet tincidunt. Suspendisse dapibus imperdiet eros. Nulla libero ligula, sodales a justo eget, ultrices rutrum sem. Aliquam ac tortor sed sapien egestas scelerisque. Maecenas porttitor porttitor diam, at aliquam lectus consectetur facilisis. Nullam ut finibus justo. Mauris sit amet facilisis tellus. Vestibulum sed turpis augue.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla placerat nibh eu ligula feugiat malesuada ac id est. In ultrices lobortis tempus. Pellentesque quis ante non magna porttitor volutpat sit amet at urna. Nulla ut odio at arcu pellentesque condimentum vel eget neque. Aliquam non placerat urna. Donec hendrerit sagittis viverra. Proin commodo porttitor dui sit amet tincidunt. Suspendisse dapibus imperdiet eros. Nulla libero ligula, sodales a justo eget, ultrices rutrum sem. Aliquam ac tortor sed sapien egestas scelerisque. Maecenas porttitor porttitor diam, at aliquam lectus consectetur facilisis. Nullam ut finibus justo. Mauris sit amet facilisis tellus. Vestibulum sed turpis augue.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla placerat nibh eu ligula feugiat malesuada ac id est. In ultrices lobortis tempus. Pellentesque quis ante non magna porttitor volutpat sit amet at urna. Nulla ut odio at arcu pellentesque condimentum vel eget neque. Aliquam non placerat urna. Donec hendrerit sagittis viverra. Proin commodo porttitor dui sit amet tincidunt. Suspendisse dapibus imperdiet eros. Nulla libero ligula, sodales a justo eget, ultrices rutrum sem. Aliquam ac tortor sed sapien egestas scelerisque. Maecenas porttitor porttitor diam, at aliquam lectus consectetur facilisis. Nullam ut finibus justo. Mauris sit amet facilisis tellus. Vestibulum sed turpis augue.</p>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presentacion`
--

CREATE TABLE IF NOT EXISTS `presentacion` (
`id` int(11) NOT NULL,
  `presentacion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagenes` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `producto_id` int(11) DEFAULT NULL,
  `color` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `presentacion`
--

INSERT INTO `presentacion` (`id`, `presentacion`, `imagenes`, `producto_id`, `color`) VALUES
(1, 'Presentacion Azul', 'a:1:{i:0;a:2:{s:10:"imagenTodo";s:43:"uploads/presentacion/almohadas-azul-big.jpg";s:8:"urlVideo";N;}}', 2, 'rgb(61, 133, 198)'),
(2, 'Producto Marron', 'a:1:{i:0;a:2:{s:10:"imagenTodo";s:45:"uploads/presentacion/almohadas-marron-big.jpg";s:8:"urlVideo";N;}}', 2, 'rgb(180, 95, 6)'),
(3, 'Hp2 Azul', 'a:1:{i:0;a:2:{s:10:"imagenTodo";s:29:"uploads/producto/hp2-azul.png";s:8:"urlVideo";N;}}', 7, 'rgb(61, 133, 198)'),
(4, 'Hp2 Negra', 'a:1:{i:0;a:2:{s:10:"imagenTodo";s:24:"uploads/producto/hp2.png";s:8:"urlVideo";N;}}', 7, 'rgb(0, 0, 0)'),
(5, 'Dell Plateado', 'a:0:{}', 11, ''),
(6, 'DELL1 Rojo', 'a:0:{}', 11, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso_compra`
--

CREATE TABLE IF NOT EXISTS `proceso_compra` (
`id` int(11) NOT NULL,
  `proceso_id` int(11) DEFAULT NULL,
  `imagen` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `contenido` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE IF NOT EXISTS `producto` (
`id` int(11) NOT NULL,
  `subcategoria_id` int(11) DEFAULT NULL,
  `nombre_producto` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion_producto` longtext COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `precio` double NOT NULL,
  `precio_oferta` double DEFAULT NULL,
  `beneficio_producto` longtext COLLATE utf8_unicode_ci NOT NULL,
  `caracteristica_producto` longtext COLLATE utf8_unicode_ci NOT NULL,
  `info_producto` longtext COLLATE utf8_unicode_ci NOT NULL,
  `stock` double NOT NULL,
  `destacado` tinyint(1) DEFAULT NULL,
  `porcentaje_descuento` double DEFAULT NULL,
  `oferta` tinyint(1) DEFAULT NULL,
  `nuevo` tinyint(1) DEFAULT NULL,
  `en_stock` tinyint(1) DEFAULT NULL,
  `cupon_id` int(11) DEFAULT NULL,
  `aparece_en_home` tinyint(1) DEFAULT NULL,
  `ranking` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `subcategoria_id`, `nombre_producto`, `descripcion_producto`, `slug`, `imagen`, `precio`, `precio_oferta`, `beneficio_producto`, `caracteristica_producto`, `info_producto`, `stock`, `destacado`, `porcentaje_descuento`, `oferta`, `nuevo`, `en_stock`, `cupon_id`, `aparece_en_home`, `ranking`) VALUES
(1, 1, 'Cojin masajero', '<p>Prueba&nbsp;Prueba&nbsp;Prueba&nbsp;Prueba&nbsp;Prueba&nbsp;Prueba&nbsp;Prueba&nbsp;Prueba&nbsp;Prueba&nbsp;Prueba&nbsp;Prueba&nbsp;Prueba&nbsp;Prueba&nbsp;Prueba&nbsp;Prueba&nbsp;Prueba&nbsp;Prueba&nbsp;</p>', 'cojin-masajero', 'uploads/producto/24.jpg', 100, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.</p>', 12, 1, NULL, 0, 0, 1, 2, 0, NULL),
(2, 1, 'Green Tea pillow tradicional', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.</p>', 'green-tea-pillow-tradicional', 'uploads/producto/almohadas.jpg', 150, 148, '<ul>\r\n	<li>Proporciona la postura correcta al dormir, mantiene la columna totalmente alineada.</li>\r\n	<li>Disminuye la tensi&oacute;n de los m&uacute;sculos del cuello y la fatiga mental, lo cual conduce a un sue&ntilde;o profundo.</li>\r\n	<li>El dise&ntilde;o ventilado per mite que el aire fluya a trav&eacute;s de agujeros especiales que posee, lo que permite adaptarse r&aacute;pidamente a la temperatura de su cuerpo para una comodidad personalizada y el apoyo siempre justo.</li>\r\n	<li>La funda posee un tejido especial que expulsa la humedad de tu piel, te mantiene seco y c&oacute;modo durante toda la noche.</li>\r\n</ul>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum a adipisci dolorum. Obcaecati reprehenderit perspiciatis, similique animi! Modi doloremque doloribus, eligendi iste minima porro necessitatibus repellendus id optio a laboriosam.</p>\r\n\r\n<p><strong>Medidas:</strong>&nbsp;15 cm de alto x 23 cm de largo x 7 cm de ancho. 2</p>', '<ul>\r\n	<li>Proporciona la postura correcta al dormir, mantiene la columna totalmente alineada.</li>\r\n	<li>Disminuye la tensi&oacute;n de los m&uacute;sculos del cuello y la fatiga mental, lo cual conduce a un sue&ntilde;o profundo.</li>\r\n	<li>El dise&ntilde;o ventilado per mite que el aire fluya a trav&eacute;s de agujeros especiales que posee, lo que permite adaptarse r&aacute;pidamente a la temperatura de su cuerpo para una comodidad personalizada y el apoyo siempre justo.</li>\r\n	<li>La funda posee un tejido especial que expulsa la humedad de tu piel, te mantiene seco y c&oacute;modo durante toda la noche.</li>\r\n</ul>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum a adipisci dolorum. Obcaecati reprehenderit perspiciatis, similique animi! Modi doloremque doloribus, eligendi iste minima porro necessitatibus repellendus id optio a laboriosam.</p>\r\n\r\n<p><strong>Medidas:</strong>&nbsp;15 cm de alto x 23 cm de largo x 7 cm de ancho. 1</p>', '<ul>\r\n	<li>Proporciona la postura correcta al dormir, mantiene la columna totalmente alineada.</li>\r\n	<li>Disminuye la tensi&oacute;n de los m&uacute;sculos del cuello y la fatiga mental, lo cual conduce a un sue&ntilde;o profundo.</li>\r\n	<li>El dise&ntilde;o ventilado per mite que el aire fluya a trav&eacute;s de agujeros especiales que posee, lo que permite adaptarse r&aacute;pidamente a la temperatura de su cuerpo para una comodidad personalizada y el apoyo siempre justo.</li>\r\n	<li>La funda posee un tejido especial que expulsa la humedad de tu piel, te mantiene seco y c&oacute;modo durante toda la noche.</li>\r\n</ul>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum a adipisci dolorum. Obcaecati reprehenderit perspiciatis, similique animi! Modi doloremque doloribus, eligendi iste minima porro necessitatibus repellendus id optio a laboriosam.</p>\r\n\r\n<p><strong>Medidas:</strong>&nbsp;15 cm de alto x 23 cm de largo x 7 cm de ancho. 3</p>', 13, 0, 10, 1, 0, 1, 2, 0, NULL),
(3, 2, 'Cojin Marron', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.</p>', 'cojin-marron', 'uploads/producto/almohadas-marron-big.jpg', 150, 135, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.</p>', 15, 0, 5, 1, 0, 1, 2, 0, NULL),
(4, 1, 'Cojin Azul', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.</p>', 'cojin-azul', 'uploads/producto/almohadas-azul-1.jpg', 120, 100, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.</p>', 10, 0, NULL, 1, 0, 1, 2, 1, NULL),
(5, 3, 'Almohada con zoom', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.</p>', 'almohada-con-zoom', 'uploads/producto/almohadas.jpg', 150, 110, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.</p>', 11, 0, 15, 1, 0, 1, 2, 1, NULL),
(6, 5, 'Laptop HP 1', '<p>Aenean mattis lacus quam, eget vestibulum ex viverra a. Mauris eu ligula maximus metus varius euismod. Sed scelerisque risus ante, ac sollicitudin ipsum cursus vel. In fermentum diam sollicitudin arcu suscipit, in pulvinar massa lacinia. Donec commodo vestibulum nulla et dapibus. Quisque ultrices sem nec sapien aliquet hendrerit. Aenean feugiat vulputate ligula quis iaculis. Nullam vel dolor sed odio molestie elementum. Nunc iaculis, urna ut accumsan cursus, magna arcu sodales est, a commodo massa enim eu nulla. Aenean eget velit sollicitudin, finibus tellus non, tempor massa.</p>', 'laptop-hp-1', 'uploads/producto/hp255laptop.jpg', 150, 120, '<p>AMD Dual Core E2&ndash;3000 APU with Radeon&trade; HD 8280 Graphics (1.65 GHz, 1MB L2 cache, 2 cores)6 AMD Dual Core E2&ndash;2000 APU with Radeon&trade; HD 7340 Graphics (1.75 GHz, 1 MB L2 cache, 2 cores)6 AMD Dual Core E2&ndash;1800 APU with Radeon&trade; HD 7340 Graphics (1.7 GHz, 1 MB L2 cache, 2 cores)6 AMD Dual Core E1&ndash;2500 APU with Radeon&trade; HD 8240 Graphics (1.4 GHz, 1MB L2 cache, 2 cores)6 AMD Dual Core E1&ndash;1500 APU with Radeon&trade; HD 7310 Graphics (1.48 GHz, 1 MB L2 cache, 2 cores)6 AMD Dual Core E1&ndash;1200 APU with Radeon&trade; HD 7310 Graphics (1.4 GHz, 1 MB L2 cache, 2 cores)6 AMD Quad Core A4&ndash;5000 APU with Radeon&trade; HD 8330 Graphics (1.5 GHz, 2 MB L2 cache, 4 cores) 6 AMD Quad Core A6&ndash;5200 APU with Radeon&trade; HD 8400 Graphics (2.0 GHz, 2 MB L2 cache, 4 cores) 6</p>', '<p>AMD Dual Core E2&ndash;3000 APU with Radeon&trade; HD 8280 Graphics (1.65 GHz, 1MB L2 cache, 2 cores)6 AMD Dual Core E2&ndash;2000 APU with Radeon&trade; HD 7340 Graphics (1.75 GHz, 1 MB L2 cache, 2 cores)6 AMD Dual Core E2&ndash;1800 APU with Radeon&trade; HD 7340 Graphics (1.7 GHz, 1 MB L2 cache, 2 cores)6 AMD Dual Core E1&ndash;2500 APU with Radeon&trade; HD 8240 Graphics (1.4 GHz, 1MB L2 cache, 2 cores)6 AMD Dual Core E1&ndash;1500 APU with Radeon&trade; HD 7310 Graphics (1.48 GHz, 1 MB L2 cache, 2 cores)6 AMD Dual Core E1&ndash;1200 APU with Radeon&trade; HD 7310 Graphics (1.4 GHz, 1 MB L2 cache, 2 cores)6 AMD Quad Core A4&ndash;5000 APU with Radeon&trade; HD 8330 Graphics (1.5 GHz, 2 MB L2 cache, 4 cores) 6 AMD Quad Core A6&ndash;5200 APU with Radeon&trade; HD 8400 Graphics (2.0 GHz, 2 MB L2 cache, 4 cores) 6</p>', '<p>AMD Dual Core E2&ndash;3000 APU with Radeon&trade; HD 8280 Graphics (1.65 GHz, 1MB L2 cache, 2 cores)6 AMD Dual Core E2&ndash;2000 APU with Radeon&trade; HD 7340 Graphics (1.75 GHz, 1 MB L2 cache, 2 cores)6 AMD Dual Core E2&ndash;1800 APU with Radeon&trade; HD 7340 Graphics (1.7 GHz, 1 MB L2 cache, 2 cores)6 AMD Dual Core E1&ndash;2500 APU with Radeon&trade; HD 8240 Graphics (1.4 GHz, 1MB L2 cache, 2 cores)6 AMD Dual Core E1&ndash;1500 APU with Radeon&trade; HD 7310 Graphics (1.48 GHz, 1 MB L2 cache, 2 cores)6 AMD Dual Core E1&ndash;1200 APU with Radeon&trade; HD 7310 Graphics (1.4 GHz, 1 MB L2 cache, 2 cores)6 AMD Quad Core A4&ndash;5000 APU with Radeon&trade; HD 8330 Graphics (1.5 GHz, 2 MB L2 cache, 4 cores) 6 AMD Quad Core A6&ndash;5200 APU with Radeon&trade; HD 8400 Graphics (2.0 GHz, 2 MB L2 cache, 4 cores) 6</p>', 11, 0, NULL, 1, 0, NULL, 2, 0, NULL),
(7, 5, 'laptop hp2', '<p>Aenean mattis lacus quam, eget vestibulum ex viverra a. Mauris eu ligula maximus metus varius euismod. Sed scelerisque risus ante, ac sollicitudin ipsum cursus vel. In fermentum diam sollicitudin arcu suscipit, in pulvinar massa lacinia. Donec commodo vestibulum nulla et dapibus. Quisque ultrices sem nec sapien aliquet hendrerit. Aenean feugiat vulputate ligula quis iaculis. Nullam vel dolor sed odio molestie elementum. Nunc iaculis, urna ut accumsan cursus, magna arcu sodales est, a commodo massa enim eu nulla. Aenean eget velit sollicitudin, finibus tellus non, tempor massa.</p>', 'laptop-hp2', 'uploads/producto/hp2.png', 200, NULL, '<p>AMD Radeon&trade; HD 7310 (AMD Dual Core E1&ndash;1200 APU and E1&ndash;1500 APU) AMD Radeon&trade; HD 7340 (AMD Dual Core E2&ndash;1800 APU and E2&ndash;2000 APU) AMD Radeon&trade; HD 8240 (AMD Dual Core E1&ndash;2500 APU) AMD Radeon&trade; HD 8280 (AMD Dual Core E2&ndash;3000 APU) AMD Radeon&trade; HD 8330 (AMD Quad Core A4&ndash;5000 APU) AMD Radeon&trade; HD 8400 (AMD Quad Core A6&ndash;5200 APU) Audio/Visual High Definition Audio, stereo speaker, stereo headphone/line</p>', '<p>AMD Radeon&trade; HD 7310 (AMD Dual Core E1&ndash;1200 APU and E1&ndash;1500 APU) AMD Radeon&trade; HD 7340 (AMD Dual Core E2&ndash;1800 APU and E2&ndash;2000 APU) AMD Radeon&trade; HD 8240 (AMD Dual Core E1&ndash;2500 APU) AMD Radeon&trade; HD 8280 (AMD Dual Core E2&ndash;3000 APU) AMD Radeon&trade; HD 8330 (AMD Quad Core A4&ndash;5000 APU) AMD Radeon&trade; HD 8400 (AMD Quad Core A6&ndash;5200 APU) Audio/Visual High Definition Audio, stereo speaker, stereo headphone/line</p>', '<p>AMD Radeon&trade; HD 7310 (AMD Dual Core E1&ndash;1200 APU and E1&ndash;1500 APU) AMD Radeon&trade; HD 7340 (AMD Dual Core E2&ndash;1800 APU and E2&ndash;2000 APU) AMD Radeon&trade; HD 8240 (AMD Dual Core E1&ndash;2500 APU) AMD Radeon&trade; HD 8280 (AMD Dual Core E2&ndash;3000 APU) AMD Radeon&trade; HD 8330 (AMD Quad Core A4&ndash;5000 APU) AMD Radeon&trade; HD 8400 (AMD Quad Core A6&ndash;5200 APU) Audio/Visual High Definition Audio, stereo speaker, stereo headphone/line</p>', 5, 0, NULL, 0, 0, NULL, 3, 0, NULL),
(8, 5, 'Laptop HP 3', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin euismod quis ipsum vitae euismod. In sed nunc quis libero facilisis laoreet vel a tortor. Proin rutrum accumsan mauris eget ornare. Donec pharetra dapibus semper. Aliquam tempus et libero nec interdum. Donec eu rhoncus nisl. Aenean finibus malesuada risus eu semper. Donec sem lacus, posuere non erat ac, viverra fermentum tortor. Aliquam convallis, lacus a mattis mollis, sapien ligula egestas metus, vitae rutrum augue ipsum quis sapien. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ac pulvinar justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In hac habitasse platea dictumst.</p>', 'laptop-hp-3', 'uploads/producto/laptophp3.jpg', 180, 150, '<p>Qualcomm 802.11 b/g/n (1x1) wireless LAN module 1 Optional: Ralink 802.11 b/g/n (1x1) + Bluetooth&trade; 4.0 combo 1,3,13; 802.11b/g/n(1x1), Bluetooth&trade; 2.11,3,12 or Bluetooth&trade; 4.0+ High Speed (HS)1,3,13 Communications Realtek Ethernet (10/100/1</p>', '<p>Qualcomm 802.11 b/g/n (1x1) wireless LAN module 1 Optional: Ralink 802.11 b/g/n (1x1) + Bluetooth&trade; 4.0 combo 1,3,13; 802.11b/g/n(1x1), Bluetooth&trade; 2.11,3,12 or Bluetooth&trade; 4.0+ High Speed (HS)1,3,13 Communications Realtek Ethernet (10/100/1</p>', '<p>Qualcomm 802.11 b/g/n (1x1) wireless LAN module 1 Optional: Ralink 802.11 b/g/n (1x1) + Bluetooth&trade; 4.0 combo 1,3,13; 802.11b/g/n(1x1), Bluetooth&trade; 2.11,3,12 or Bluetooth&trade; 4.0+ High Speed (HS)1,3,13 Communications Realtek Ethernet (10/100/1</p>', 14, 0, NULL, 0, 1, NULL, 3, 0, NULL),
(9, 5, 'Laptop HP 4', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin euismod quis ipsum vitae euismod. In sed nunc quis libero facilisis laoreet vel a tortor. Proin rutrum accumsan mauris eget ornare. Donec pharetra dapibus semper. Aliquam tempus et libero nec interdum. Donec eu rhoncus nisl. Aenean finibus malesuada risus eu semper. Donec sem lacus, posuere non erat ac, viverra fermentum tortor. Aliquam convallis, lacus a mattis mollis, sapien ligula egestas metus, vitae rutrum augue ipsum quis sapien. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ac pulvinar justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In hac habitasse platea dictumst.</p>', 'laptop-hp-4', 'uploads/producto/hp4.jpg', 200, NULL, '<p>AMD Radeon&trade; HD 7310 (AMD Dual Core E1&ndash;1200 APU and E1&ndash;1500 APU) AMD Radeon&trade; HD 7340 (AMD Dual Core E2&ndash;1800 APU and E2&ndash;2000 APU) AMD Radeon&trade; HD 8240 (AMD Dual Core E1&ndash;2500 APU) AMD Radeon&trade; HD 8280 (AMD Dual Core E2&ndash;3000 APU) AMD Radeon&trade; HD 8330 (AMD Quad Core A4&ndash;5000 APU) AMD Radeon&trade; HD 8400 (AMD Quad Core A6&ndash;5200 APU)</p>', '<p>AMD Radeon&trade; HD 7310 (AMD Dual Core E1&ndash;1200 APU and E1&ndash;1500 APU) AMD Radeon&trade; HD 7340 (AMD Dual Core E2&ndash;1800 APU and E2&ndash;2000 APU) AMD Radeon&trade; HD 8240 (AMD Dual Core E1&ndash;2500 APU) AMD Radeon&trade; HD 8280 (AMD Dual Core E2&ndash;3000 APU) AMD Radeon&trade; HD 8330 (AMD Quad Core A4&ndash;5000 APU) AMD Radeon&trade; HD 8400 (AMD Quad Core A6&ndash;5200 APU)</p>', '<p>AMD Radeon&trade; HD 7310 (AMD Dual Core E1&ndash;1200 APU and E1&ndash;1500 APU) AMD Radeon&trade; HD 7340 (AMD Dual Core E2&ndash;1800 APU and E2&ndash;2000 APU) AMD Radeon&trade; HD 8240 (AMD Dual Core E1&ndash;2500 APU) AMD Radeon&trade; HD 8280 (AMD Dual Core E2&ndash;3000 APU) AMD Radeon&trade; HD 8330 (AMD Quad Core A4&ndash;5000 APU) AMD Radeon&trade; HD 8400 (AMD Quad Core A6&ndash;5200 APU)</p>', 10, 1, NULL, 0, 0, NULL, 3, 0, NULL),
(10, 5, 'Laptop HP 5', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin euismod quis ipsum vitae euismod. In sed nunc quis libero facilisis laoreet vel a tortor. Proin rutrum accumsan mauris eget ornare. Donec pharetra dapibus semper. Aliquam tempus et libero nec interdum. Donec eu rhoncus nisl. Aenean finibus malesuada risus eu semper. Donec sem lacus, posuere non erat ac, viverra fermentum tortor. Aliquam convallis, lacus a mattis mollis, sapien ligula egestas metus, vitae rutrum augue ipsum quis sapien. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ac pulvinar justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In hac habitasse platea dictumst.</p>', 'laptop-hp-5', 'uploads/producto/hp5.jpg', 250, NULL, '<p>AMD Radeon&trade; HD 7310 (AMD Dual Core E1&ndash;1200 APU and E1&ndash;1500 APU) AMD Radeon&trade; HD 7340 (AMD Dual Core E2&ndash;1800 APU and E2&ndash;2000 APU) AMD Radeon&trade; HD 8240 (AMD Dual Core E1&ndash;2500 APU) AMD Radeon&trade; HD 8280 (AMD Dual Core E2&ndash;3000 APU) AMD Radeon&trade; HD 8330 (AMD Quad Core A4&ndash;5000 APU) AMD Radeon&trade; HD 8400 (AMD Quad Core A6&ndash;5200 APU)</p>', '<p>AMD Radeon&trade; HD 7310 (AMD Dual Core E1&ndash;1200 APU and E1&ndash;1500 APU) AMD Radeon&trade; HD 7340 (AMD Dual Core E2&ndash;1800 APU and E2&ndash;2000 APU) AMD Radeon&trade; HD 8240 (AMD Dual Core E1&ndash;2500 APU) AMD Radeon&trade; HD 8280 (AMD Dual Core E2&ndash;3000 APU) AMD Radeon&trade; HD 8330 (AMD Quad Core A4&ndash;5000 APU) AMD Radeon&trade; HD 8400 (AMD Quad Core A6&ndash;5200 APU)</p>', '<p>AMD Radeon&trade; HD 7310 (AMD Dual Core E1&ndash;1200 APU and E1&ndash;1500 APU) AMD Radeon&trade; HD 7340 (AMD Dual Core E2&ndash;1800 APU and E2&ndash;2000 APU) AMD Radeon&trade; HD 8240 (AMD Dual Core E1&ndash;2500 APU) AMD Radeon&trade; HD 8280 (AMD Dual Core E2&ndash;3000 APU) AMD Radeon&trade; HD 8330 (AMD Quad Core A4&ndash;5000 APU) AMD Radeon&trade; HD 8400 (AMD Quad Core A6&ndash;5200 APU)</p>', 13, 0, 10, 0, 0, NULL, 3, 0, NULL),
(11, 6, 'Laptop DELL 1', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin euismod quis ipsum vitae euismod. In sed nunc quis libero facilisis laoreet vel a tortor. Proin rutrum accumsan mauris eget ornare. Donec pharetra dapibus semper. Aliquam tempus et libero nec interdum. Donec eu rhoncus nisl. Aenean finibus malesuada risus eu semper. Donec sem lacus, posuere non erat ac, viverra fermentum tortor. Aliquam convallis, lacus a mattis mollis, sapien ligula egestas metus, vitae rutrum augue ipsum quis sapien. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ac pulvinar justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In hac habitasse platea dictumst.</p>', 'laptop-dell-1', 'uploads/producto/dell1.jpg', 160, 110, '<p>Limited 1 year, 1 year limited warranty on primary battery. Optional HP Care Pack Services16,3 are extended service contracts which go beyond your standard warranties. For more details visit: http://www.hp.com/go/cpc.</p>', '<p>Limited 1 year, 1 year limited warranty on primary battery. Optional HP Care Pack Services16,3 are extended service contracts which go beyond your standard warranties. For more details visit: http://www.hp.com/go/cpc.</p>', '<p>Limited 1 year, 1 year limited warranty on primary battery. Optional HP Care Pack Services16,3 are extended service contracts which go beyond your standard warranties. For more details visit: http://www.hp.com/go/cpc.</p>', 9, 0, NULL, 1, 0, NULL, 2, 0, NULL),
(12, 7, 'CAMARA NIKON 1', '<p>Curabitur faucibus et ipsum a scelerisque. Integer vehicula, augue vitae ullamcorper rutrum, risus elit molestie magna, a euismod urna odio ultrices quam. Mauris accumsan nulla vitae diam blandit, malesuada finibus ligula maximus. Vivamus bibendum tortor eu neque auctor pretium. Quisque viverra quam eu fermentum placerat. Nulla quis dui tortor. Donec accumsan ligula quis ipsum aliquet tincidunt. Nullam lectus augue, dictum vitae sem sollicitudin, commodo faucibus purus. Vivamus eget ornare dolor. Phasellus faucibus felis massa, in malesuada augue egestas id.</p>', 'camara-nikon-1', 'uploads/producto/camara-1.png', 120, 100, '<p>Curabitur faucibus et ipsum a scelerisque. Integer vehicula, augue vitae ullamcorper rutrum, risus elit molestie magna, a euismod urna odio ultrices quam. Mauris accumsan nulla vitae diam blandit, malesuada finibus ligula maximus. Vivamus bibendum tortor eu neque auctor pretium. Quisque viverra quam eu fermentum placerat. Nulla quis dui tortor. Donec accumsan ligula quis ipsum aliquet tincidunt. Nullam lectus augue, dictum vitae sem sollicitudin, commodo faucibus purus. Vivamus eget ornare dolor. Phasellus faucibus felis massa, in malesuada augue egestas id.</p>', '<p>Curabitur faucibus et ipsum a scelerisque. Integer vehicula, augue vitae ullamcorper rutrum, risus elit molestie magna, a euismod urna odio ultrices quam. Mauris accumsan nulla vitae diam blandit, malesuada finibus ligula maximus. Vivamus bibendum tortor eu neque auctor pretium. Quisque viverra quam eu fermentum placerat. Nulla quis dui tortor. Donec accumsan ligula quis ipsum aliquet tincidunt. Nullam lectus augue, dictum vitae sem sollicitudin, commodo faucibus purus. Vivamus eget ornare dolor. Phasellus faucibus felis massa, in malesuada augue egestas id.</p>', '<p>Curabitur faucibus et ipsum a scelerisque. Integer vehicula, augue vitae ullamcorper rutrum, risus elit molestie magna, a euismod urna odio ultrices quam. Mauris accumsan nulla vitae diam blandit, malesuada finibus ligula maximus. Vivamus bibendum tortor eu neque auctor pretium. Quisque viverra quam eu fermentum placerat. Nulla quis dui tortor. Donec accumsan ligula quis ipsum aliquet tincidunt. Nullam lectus augue, dictum vitae sem sollicitudin, commodo faucibus purus. Vivamus eget ornare dolor. Phasellus faucibus felis massa, in malesuada augue egestas id.</p>', 13, 0, NULL, 1, 0, NULL, 2, 0, NULL),
(13, 5, 'LAPTOP HP - 6', '<p>LAPTOP ASUS - 6</p>', 'laptop-hp-6', 'uploads/producto/hp-6.jpg', 150, 120, '<p>AAAAAAAAAAAAAAA</p>', '<p>AAAAAAAAAAAAAAAA</p>', '<p>AAAAAAAAAAAAAAAAA</p>', 12, 0, NULL, 1, 0, NULL, 2, 0, NULL),
(14, 8, 'LAPTOP ASUS - 1', '<p>LAPTOP ASUS - 1</p>', 'laptop-asus-1', 'uploads/producto/asus-1.jpg', 500, 499, '<p>AAAAAAAAAAAAAAAAA</p>', '<p>AAAAAAAAAAAAA</p>', '<p>AAAAAAAAAAAAAAAA</p>', 11, 0, NULL, 0, 0, NULL, 2, 0, NULL),
(15, 8, 'LAPTOP ASUS - 2', '<p>adasdasaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</p>', 'laptop-asus-2', 'uploads/producto/asus-2.jpg', 500, NULL, '<p>SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS</p>', '<p>SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS</p>', '<p>SSSSSSSSSSSSSSSSSSSSSSSSSSSSSS</p>', 16, 0, NULL, 0, 0, NULL, 2, 0, '4'),
(16, 6, 'LAPTOP DELL - 2', '<p>LAPTOP DELL - 2</p>', 'laptop-dell-2', 'uploads/producto/dell2.jpg', 140, NULL, '<p>AAAAAAAAAAAAAAAAAAAAAA</p>', '<p>AAAAAAAAAAAAAAAAAAAAAAA</p>', '<p>AAAAAAAAAAAAAAAAAAAAAAAAAAAA</p>', 16, 0, NULL, 0, 0, NULL, 2, 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promocion`
--

CREATE TABLE IF NOT EXISTS `promocion` (
`id` int(11) NOT NULL,
  `nombres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` longtext COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `promocion`
--

INSERT INTO `promocion` (`id`, `nombres`, `descripcion`, `imagen`, `fecha_inicio`, `fecha_fin`) VALUES
(1, 'Dia de la madre', '<p>Aenean at convallis ligula. Suspendisse sem mauris, lacinia eget neque id, feugiat pulvinar quam. Vestibulum semper arcu a urna congue, vel commodo felis efficitur. Duis convallis hendrerit commodo. Duis velit sem, ultrices vel nulla ut, rutrum tincidunt</p>', 'uploads/promocion/b22.jpg', '2016-08-15', '2016-09-15'),
(2, 'Dia del padre', '<p>Aenean at convallis ligula. Suspendisse sem mauris, lacinia eget neque id, feugiat pulvinar quam. Vestibulum semper arcu a urna congue, vel commodo felis efficitur. Duis convallis hendrerit commodo. Duis velit sem, ultrices vel nulla ut, rutrum tincidunt</p>', 'uploads/promocion/b22-2.jpg', '2016-07-20', '2016-11-16'),
(3, 'Promoción día del niño', '<p>Promoci&oacute;n valida solo para ni&ntilde;os de verdad.</p>', 'uploads/b16-img.jpg', '2011-01-01', '2011-01-01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE IF NOT EXISTS `provincia` (
  `id` int(11) NOT NULL,
  `departamento_id` int(11) DEFAULT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `provincia`
--

INSERT INTO `provincia` (`id`, `departamento_id`, `codigo`, `nombre`) VALUES
(1, 1, '5', 'COTABAMBAS'),
(2, 1, '2', 'AYMARAES'),
(3, 1, '7', 'CHINCHEROS'),
(4, 1, '6', 'GRAU'),
(5, 1, '4', 'ANTABAMBA'),
(6, 1, '3', 'ANDAHUAYLAS'),
(7, 1, '1', 'ABANCAY'),
(8, 2, '7', 'ISLAY'),
(9, 2, '1', 'AREQUIPA'),
(10, 2, '8', 'LA UNION'),
(11, 2, '6', 'CONDESUYOS'),
(12, 2, '5', 'CASTILLA'),
(13, 2, '4', 'CARAVELI'),
(14, 2, '3', 'CAMANA'),
(15, 2, '2', 'CAYLLOMA'),
(16, 3, '13', 'SAN PABLO'),
(17, 3, '10', 'SAN MIGUEL'),
(18, 3, '4', 'CONTUMAZA'),
(19, 3, '1', 'CAJAMARCA'),
(20, 3, '12', 'SAN MARCOS'),
(21, 3, '11', 'SAN IGNACIO'),
(22, 3, '9', 'SANTA CRUZ'),
(23, 3, '8', 'JAEN'),
(24, 3, '7', 'HUALGAYOC'),
(25, 3, '6', 'CHOTA'),
(26, 3, '5', 'CUTERVO'),
(27, 3, '3', 'CELENDIN'),
(28, 3, '2', 'CAJABAMBA'),
(29, 4, '13', 'URUBAMBA'),
(30, 4, '12', 'QUISPICANCHI'),
(31, 4, '10', 'PARURO'),
(32, 4, '7', 'CHUMBIVILCAS'),
(33, 4, '2', 'ACOMAYO'),
(34, 4, '11', 'PAUCARTAMBO'),
(35, 4, '9', 'LA CONVENCION'),
(36, 4, '8', 'ESPINAR'),
(37, 4, '6', 'CANCHIS'),
(38, 4, '5', 'CANAS'),
(39, 4, '4', 'CALCA'),
(40, 4, '3', 'ANTA'),
(41, 4, '1', 'CUSCO'),
(42, 5, '4', 'CASTROVIRREYNA'),
(43, 5, '2', 'ACOBAMBA'),
(44, 5, '7', 'CHURCAMPA'),
(45, 5, '6', 'HUAYTARA'),
(46, 5, '5', 'TAYACAJA'),
(47, 5, '3', 'ANGARAES'),
(48, 5, '1', 'HUANCAVELICA'),
(49, 6, '10', 'LAURICOCHA'),
(50, 6, '4', 'HUAMALIES'),
(51, 6, '2', 'AMBO'),
(52, 6, '11', 'YAROWILCA'),
(53, 6, '9', 'HUACAYBAMBA'),
(54, 6, '8', 'PUERTO INCA'),
(55, 6, '7', 'PACHITEA'),
(56, 6, '6', 'LEONCIO PRADO'),
(57, 6, '5', 'MARAÑON'),
(58, 6, '3', 'DOS DE MAYO'),
(59, 6, '1', 'HUANUCO'),
(60, 7, '5', 'PALPA'),
(61, 7, '3', 'NAZCA'),
(62, 7, '1', 'ICA'),
(63, 7, '4', 'PISCO'),
(64, 7, '2', 'CHINCHA'),
(65, 8, '1', 'TAMBOPATA'),
(66, 8, '3', 'TAHUAMANU'),
(67, 8, '2', 'MANU'),
(68, 9, '3', 'OXAPAMPA'),
(69, 9, '1', 'PASCO'),
(70, 9, '2', 'DANIEL ALCIDES CARRION'),
(71, 10, '6', 'SULLANA'),
(72, 10, '3', 'HUANCABAMBA'),
(73, 10, '8', 'SECHURA'),
(74, 10, '7', 'TALARA'),
(75, 10, '5', 'PAITA'),
(76, 10, '4', 'MORROPON'),
(77, 10, '2', 'AYABACA'),
(78, 10, '1', 'PIURA'),
(79, 11, '3', 'JORGE BASADRE'),
(80, 11, '1', 'TACNA'),
(81, 11, '4', 'CANDARAVE'),
(82, 11, '2', 'TARATA'),
(83, 12, '1', 'TUMBES'),
(84, 12, '3', 'ZARUMILLA'),
(85, 12, '2', 'CONTRALMIRANTE VILLAR'),
(86, 13, '8', 'CHANCHAMAYO'),
(87, 13, '6', 'YAULI'),
(88, 13, '3', 'JAUJA'),
(89, 13, '1', 'HUANCAYO'),
(90, 13, '9', 'CHUPACA'),
(91, 13, '7', 'SATIPO'),
(92, 13, '5', 'TARMA'),
(93, 13, '4', 'JUNIN'),
(94, 13, '2', 'CONCEPCION'),
(95, 14, '11', 'GRAN CHIMU'),
(96, 14, '9', 'CHEPEN'),
(97, 14, '4', 'OTUZCO'),
(98, 14, '1', 'TRUJILLO'),
(99, 14, '12', 'VIRU'),
(100, 14, '10', 'JULCAN'),
(101, 14, '8', 'ASCOPE'),
(102, 14, '7', 'SANTIAGO DE CHUCO'),
(103, 14, '6', 'PATAZ'),
(104, 14, '5', 'PACASMAYO'),
(105, 14, '3', 'SANCHEZ CARRION'),
(106, 14, '2', 'BOLIVAR'),
(107, 15, '1', 'CHICLAYO'),
(108, 15, '3', 'LAMBAYEQUE'),
(109, 15, '2', 'FERREÑAFE'),
(110, 16, '8', 'HUARAL'),
(111, 16, '3', 'CANTA'),
(112, 16, '4', 'CAÑETE'),
(113, 16, '2', 'CAJATAMBO'),
(114, 16, '1', 'LIMA'),
(115, 16, '10', 'OYON'),
(116, 16, '9', 'BARRANCA'),
(117, 16, '7', 'YAUYOS'),
(118, 16, '6', 'HUAROCHIRI'),
(119, 16, '5', 'HUAURA'),
(120, 17, '1', 'CALLAO'),
(121, 18, '3', 'ATALAYA'),
(122, 18, '4', 'PURUS'),
(123, 18, '2', 'PADRE ABAD'),
(124, 18, '1', 'CORONEL PORTILLO'),
(125, 19, '11', 'SUCRE'),
(126, 19, '7', 'VICTOR FAJARDO'),
(127, 19, '3', 'HUANTA'),
(128, 19, '10', 'PAUCAR DEL SARA SARA'),
(129, 19, '9', 'VILCAS HUAMAN'),
(130, 19, '8', 'HUANCA SANCOS'),
(131, 19, '6', 'PARINACOCHAS'),
(132, 19, '5', 'LUCANAS'),
(133, 19, '4', 'LA MAR'),
(134, 19, '2', 'CANGALLO'),
(135, 19, '1', 'HUAMANGA'),
(136, 20, '5', 'RODRIGUEZ DE MENDOZA'),
(137, 20, '7', 'UTCUBAMBA'),
(138, 20, '6', 'CONDORCANQUI'),
(139, 20, '4', 'LUYA'),
(140, 20, '3', 'BONGARA'),
(141, 20, '2', 'BAGUA'),
(142, 20, '1', 'CHACHAPOYAS'),
(143, 21, '8', 'HUARI'),
(144, 21, '6', 'CORONGO'),
(145, 21, '3', 'BOLOGNESI'),
(146, 21, '18', 'ASUNCION'),
(147, 21, '16', 'ANTONIO RAIMONDI'),
(148, 21, '13', 'SANTA'),
(149, 21, '11', 'POMABAMBA'),
(150, 21, '7', 'HUAYLAS'),
(151, 21, '5', 'CASMA'),
(152, 21, '4', 'CARHUAZ'),
(153, 21, '2', 'AIJA'),
(154, 21, '1', 'HUARAZ'),
(155, 21, '20', 'OCROS'),
(156, 21, '19', 'HUARMEY'),
(157, 21, '17', 'CARLOS FERMIN FITZCARRALD'),
(158, 21, '15', 'YUNGAY'),
(159, 21, '14', 'SIHUAS'),
(160, 21, '12', 'RECUAY'),
(161, 21, '10', 'PALLASCA'),
(162, 21, '9', 'MARISCAL LUZURIAGA'),
(163, 22, '6', 'MARISCAL RAMON CASTILLA'),
(164, 22, '2', 'ALTO AMAZONAS'),
(165, 22, '7', 'DATEM DEL MARAÑON'),
(166, 22, '5', 'UCAYALI'),
(167, 22, '4', 'REQUENA'),
(168, 22, '3', 'LORETO'),
(169, 22, '1', 'MAYNAS'),
(170, 23, '3', 'ILO'),
(171, 23, '2', 'GENERAL SANCHEZ CERRO'),
(172, 23, '1', 'MARISCAL NIETO'),
(173, 24, '10', 'YUNGUYO'),
(174, 24, '8', 'SANDIA'),
(175, 24, '6', 'LAMPA'),
(176, 24, '2', 'AZANGARO'),
(177, 24, '13', 'MOHO'),
(178, 24, '12', 'EL COLLAO'),
(179, 24, '11', 'SAN ANTONIO DE PUTINA'),
(180, 24, '9', 'SAN ROMAN'),
(181, 24, '7', 'MELGAR'),
(182, 24, '5', 'HUANCANE'),
(183, 24, '4', 'CHUCUITO'),
(184, 24, '3', 'CARABAYA'),
(185, 24, '1', 'PUNO'),
(186, 25, '7', 'BELLAVISTA'),
(187, 25, '5', 'RIOJA'),
(188, 25, '3', 'LAMAS'),
(189, 25, '9', 'PICOTA'),
(190, 25, '10', 'EL DORADO'),
(191, 25, '8', 'TOCACHE'),
(192, 25, '6', 'SAN MARTIN'),
(193, 25, '4', 'MARISCAL CACERES'),
(194, 25, '2', 'HUALLAGA'),
(195, 25, '1', 'MOYOBAMBA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ranking`
--

CREATE TABLE IF NOT EXISTS `ranking` (
`id` int(11) NOT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `puntuacion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ranking`
--

INSERT INTO `ranking` (`id`, `producto_id`, `puntuacion`, `fecha`) VALUES
(1, 7, '1', '2016-09-23 10:27:03'),
(2, 7, '4', '2016-09-23 10:27:28'),
(3, 7, '4', '2016-09-23 10:32:10'),
(4, 7, '3', '2016-09-23 10:32:24'),
(5, 10, '4', '2016-09-23 10:32:44'),
(6, 6, '4', '2016-09-23 10:51:46'),
(7, 10, '5', '2016-09-23 10:59:04'),
(8, 7, '5', '2016-09-23 11:00:38'),
(9, 15, '4', '2016-09-26 15:38:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reclamo`
--

CREATE TABLE IF NOT EXISTS `reclamo` (
`id` int(11) NOT NULL,
  `nombres_reclamante` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_reclamo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mensaje` longtext COLLATE utf8_unicode_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `distrito_id` int(11) DEFAULT NULL,
  `tipo_documento_reclamante` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `numero_documento_reclamante` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos_reclamante` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `telefono_reclamante` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email_reclamante` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_documento_apoderado` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numero_documento_apoderado` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombres_apoderado` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos_apoderado` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `reclamo`
--

INSERT INTO `reclamo` (`id`, `nombres_reclamante`, `direccion`, `tipo_reclamo`, `mensaje`, `fecha`, `distrito_id`, `tipo_documento_reclamante`, `numero_documento_reclamante`, `apellidos_reclamante`, `telefono_reclamante`, `email_reclamante`, `tipo_documento_apoderado`, `numero_documento_apoderado`, `nombres_apoderado`, `apellidos_apoderado`) VALUES
(4, 'Juan', 'JLO 123', 'Reclamo', 'MENSAJE!!!!', '2016-09-08 11:56:39', 998, 'dni', '71056925', 'Bravo', '979814339', 'jbravoaguinaga@gmail.com', 'dni', '34r54354', 'Papá', 'Pa´pa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recuperar_contrasena_url`
--

CREATE TABLE IF NOT EXISTS `recuperar_contrasena_url` (
`id` int(11) NOT NULL,
  `url` longtext COLLATE utf8_unicode_ci NOT NULL,
  `codigo` longtext COLLATE utf8_unicode_ci NOT NULL,
  `fue_utilizada` tinyint(1) DEFAULT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `redirecciones`
--

CREATE TABLE IF NOT EXISTS `redirecciones` (
`id` int(11) NOT NULL,
  `desde` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `para` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategoria`
--

CREATE TABLE IF NOT EXISTS `subcategoria` (
`id` int(11) NOT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `nombre_sub` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion_sub` longtext COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `cupon_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `subcategoria`
--

INSERT INTO `subcategoria` (`id`, `categoria_id`, `nombre_sub`, `descripcion_sub`, `slug`, `cupon_id`) VALUES
(1, 1, 'sub1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.', 'sub1', 2),
(2, 1, 'sub 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.', 'sub-2', 2),
(3, 2, 'Subcategoria 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ex leo, fringilla eget nisi ac, facilisis iaculis dolor. Donec condimentum mi et libero facilisis lacinia. Suspendisse sed justo ultrices, sagittis elit eget, convallis risus. In hac habitasse platea dictumst. Sed urna tellus, molestie at sodales sit amet, euismod quis enim. Cras interdum dapibus ex non maximus. Curabitur auctor cursus mollis.', 'subcategoria-1', 2),
(4, 4, 'Más productos', 'Categoría más productos', 'mas-productos', 2),
(5, 5, 'HP', 'MARCA HP', 'hp', 4),
(6, 5, 'DELL', 'Laptops marca DELL', 'dell', 2),
(7, 6, 'NIKON', 'CAMARAS DE LA MARCA NIKON', 'nikon', 2),
(8, 5, 'ASUS', 'LAPTOP MARCA ASUS', 'asus', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `suscripcion`
--

CREATE TABLE IF NOT EXISTS `suscripcion` (
`id` int(11) NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `suscripcion`
--

INSERT INTO `suscripcion` (`id`, `email`, `fecha`) VALUES
(1, 'jbravoaguinaga@gmail.com', '2016-08-29 16:28:19'),
(2, 'sdsd', '2016-08-29 16:29:38'),
(3, 'dssdsd', '2016-08-29 16:29:54'),
(4, 'elio@gmail.com', '2016-08-29 16:30:44'),
(5, 'j', '2016-09-08 12:02:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `translate`
--

CREATE TABLE IF NOT EXISTS `translate` (
`id` int(11) NOT NULL,
  `texto` longtext COLLATE utf8_unicode_ci NOT NULL,
  `variable` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
`id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`) VALUES
(1, 'adminuser', 'adminuser', 'jbravoaguinaga@gmail.com', 'jbravoaguinaga@gmail.com', 1, 'pcmjyqycfpc04o4kk8w0o40c088ggo0', '$2y$13$pcmjyqycfpc04o4kk8w0ouKpdsjuKeVAfVr9idRo4z/pmngWOWS0i', '2016-09-27 15:34:10', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valor_agregado`
--

CREATE TABLE IF NOT EXISTS `valor_agregado` (
`id` int(11) NOT NULL,
  `valoresagregados_id` int(11) DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zero_seo`
--

CREATE TABLE IF NOT EXISTS `zero_seo` (
`id` int(11) NOT NULL,
  `path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `social_titulo` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `social_descripcion` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `social_imagen` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `zero_seo`
--

INSERT INTO `zero_seo` (`id`, `path`, `titulo`, `descripcion`, `social_titulo`, `social_descripcion`, `social_imagen`) VALUES
(1, '/', NULL, NULL, NULL, NULL, NULL),
(2, '/categorias/', NULL, NULL, NULL, NULL, NULL),
(3, '/categorias/masajes/sub1/cojin-masajero', NULL, NULL, NULL, NULL, NULL),
(4, '/categorias/fitness', NULL, NULL, NULL, NULL, NULL),
(5, '/categorias/masajes', NULL, NULL, NULL, NULL, NULL),
(6, '/categorias/almohadas', NULL, NULL, NULL, NULL, NULL),
(7, '/categorias/masajes/sub1/green-tea-pillow-tradicional', NULL, NULL, NULL, NULL, NULL),
(8, '/producto-detalle/', NULL, NULL, NULL, NULL, NULL),
(9, '/categorias/masajes/sub-2/cojin-marron', NULL, NULL, NULL, NULL, NULL),
(10, '/categorias/fitness/subcategoria-1/almohada-con-zoom', NULL, NULL, NULL, NULL, NULL),
(11, '/aaa/', NULL, NULL, NULL, NULL, NULL),
(12, '/categorias/fitness/subcategoria-1', NULL, NULL, NULL, NULL, NULL),
(13, '/carrito-compras/', NULL, NULL, NULL, NULL, NULL),
(14, '/categorias/masajes/sub1/cojin-azul', NULL, NULL, NULL, NULL, NULL),
(15, '/datos-facturacion/', NULL, NULL, NULL, NULL, NULL),
(16, '/login/', NULL, NULL, NULL, NULL, NULL),
(17, '/categorias/masajes/sub1', NULL, NULL, NULL, NULL, NULL),
(18, '/categorias/masajes/sub-2', NULL, NULL, NULL, NULL, NULL),
(19, '/revisar-y-pagar/', NULL, NULL, NULL, NULL, NULL),
(20, '/locales/', NULL, NULL, NULL, NULL, NULL),
(21, '/libro-reclamaciones/', NULL, NULL, NULL, NULL, NULL),
(22, '/contacto/', NULL, NULL, NULL, NULL, NULL),
(23, '/bolsa-trabajo/', NULL, NULL, NULL, NULL, NULL),
(24, '/preguntas-frecuentes/', NULL, NULL, NULL, NULL, NULL),
(25, '/terminos-condiciones/', NULL, NULL, NULL, NULL, NULL),
(26, '/privacidad-seguridad/', NULL, NULL, NULL, NULL, NULL),
(27, '/promociones/', NULL, NULL, NULL, NULL, NULL),
(28, '/categorias/masajes/cojin-azul', NULL, NULL, NULL, NULL, NULL),
(29, '/facturacion/', NULL, NULL, NULL, NULL, NULL),
(30, '/revisar-pagar/', NULL, NULL, NULL, NULL, NULL),
(31, '/paypal-form/', NULL, NULL, NULL, NULL, NULL),
(32, '/conocenos/', NULL, NULL, NULL, NULL, NULL),
(33, '/pedidos/31', NULL, NULL, NULL, NULL, NULL),
(34, '/pedidos', NULL, NULL, NULL, NULL, NULL),
(35, '/perfil', NULL, NULL, NULL, NULL, NULL),
(36, '/actualizar-cuenta/', NULL, NULL, NULL, NULL, NULL),
(37, '/gracias-contrasena', NULL, NULL, NULL, NULL, NULL),
(38, '/categorias', NULL, NULL, NULL, NULL, NULL),
(39, '/categorias/laptops', NULL, NULL, NULL, NULL, NULL),
(40, '/categorias/laptops/hp', NULL, NULL, NULL, NULL, NULL),
(41, '/categorias/laptops/hp/laptop-hp2', NULL, NULL, NULL, NULL, NULL),
(42, '/categorias/laptops/dell', NULL, NULL, NULL, NULL, NULL),
(43, '/categorias/laptops/dell/laptop-dell-1', NULL, NULL, NULL, NULL, NULL),
(44, '/categorias/laptops/hp/laptop-hp-5', NULL, NULL, NULL, NULL, NULL),
(45, '/categorias/laptops/hp/laptop-hp-1', NULL, NULL, NULL, NULL, NULL),
(46, '/categorias/laptops/hp/laptop-hp-4', NULL, NULL, NULL, NULL, NULL),
(47, '/olvidaste-contrasena/', NULL, NULL, NULL, NULL, NULL),
(48, '/error-contrasena/', NULL, NULL, NULL, NULL, NULL),
(49, '/gracias-por-comprar/', NULL, NULL, NULL, NULL, NULL),
(50, '/categorias/laptops/hp/laptop-hp-3', NULL, NULL, NULL, NULL, NULL),
(51, '/categorias/camaras', NULL, NULL, NULL, NULL, NULL),
(52, '/categorias/camaras/nikon/camara-nikon-1', NULL, NULL, NULL, NULL, NULL),
(53, '/categorias/laptops/hp/laptop-asus-6', NULL, NULL, NULL, NULL, NULL),
(54, '/categorias/laptops/asus/laptop-asus-1', NULL, NULL, NULL, NULL, NULL),
(55, '/categorias/laptops/dell/laptop-dell-2', NULL, NULL, NULL, NULL, NULL),
(56, '/categorias/data.twig', NULL, NULL, NULL, NULL, NULL),
(57, '/categorias/camaras/nikon', NULL, NULL, NULL, NULL, NULL),
(58, '/categorias/laptops/hp/laptop-hp-6', NULL, NULL, NULL, NULL, NULL),
(59, '/busquedas/', NULL, NULL, NULL, NULL, NULL),
(60, '/categorias/laptop-asus-1/laptop-asus-1/laptop-asus-1', NULL, NULL, NULL, NULL, NULL),
(61, '/categorias/laptops+hp', NULL, NULL, NULL, NULL, NULL),
(62, '/categorias/fitness+subcategoria-1', NULL, NULL, NULL, NULL, NULL),
(63, '/categorias/laptops/asus/laptop-asus-2', NULL, NULL, NULL, NULL, NULL),
(64, '/categorias/laptops/asus', NULL, NULL, NULL, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `banner`
--
ALTER TABLE `banner`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `bolsa_trabajo`
--
ALTER TABLE `bolsa_trabajo`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_DB09F3734E7121AF` (`provincia_id`);

--
-- Indices de la tabla `carrito`
--
ALTER TABLE `carrito`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_77E6BED5DE734E51` (`cliente_id`), ADD KEY `IDX_77E6BED57645698E` (`producto_id`), ADD KEY `IDX_77E6BED54854653A` (`pedido_id`), ADD KEY `IDX_77E6BED5D7AFF6F2` (`cupon_id`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_4E10122D989D9B62` (`slug`), ADD KEY `IDX_4E10122DD7AFF6F2` (`cupon_id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_50FE07D7E557397E` (`distrito_id`);

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_2741493C4E7121AF` (`provincia_id`);

--
-- Indices de la tabla `cupon`
--
ALTER TABLE `cupon`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `distrito`
--
ALTER TABLE `distrito`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_BE2719FD4E7121AF` (`provincia_id`);

--
-- Indices de la tabla `ext_translations`
--
ALTER TABLE `ext_translations`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `lookup_unique_idx` (`locale`,`object_class`,`field`,`foreign_key`), ADD KEY `translations_lookup_idx` (`locale`,`object_class`,`foreign_key`);

--
-- Indices de la tabla `fb_posts`
--
ALTER TABLE `fb_posts`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `formulario_promocion`
--
ALTER TABLE `formulario_promocion`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_9C9A3B03B1E453D4` (`promocion_id`);

--
-- Indices de la tabla `home`
--
ALTER TABLE `home`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `imagen_presentacion`
--
ALTER TABLE `imagen_presentacion`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_7E24A2D5AF194CD1` (`imagenes_id`);

--
-- Indices de la tabla `info`
--
ALTER TABLE `info`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `local`
--
ALTER TABLE `local`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticia`
--
ALTER TABLE `noticia`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_31205F96989D9B62` (`slug`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_C4EC16CE8A26512B` (`distrito_facturacion_id`), ADD KEY `IDX_C4EC16CE95EB7823` (`distrito_envio_id`);

--
-- Indices de la tabla `pregunta_frecuente`
--
ALTER TABLE `pregunta_frecuente`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `presentacion`
--
ALTER TABLE `presentacion`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_56A887B57645698E` (`producto_id`);

--
-- Indices de la tabla `proceso_compra`
--
ALTER TABLE `proceso_compra`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_7F398B3C640D1D53` (`proceso_id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_A7BB0615989D9B62` (`slug`), ADD KEY `IDX_A7BB061588D3B71A` (`subcategoria_id`), ADD KEY `IDX_A7BB0615D7AFF6F2` (`cupon_id`);

--
-- Indices de la tabla `promocion`
--
ALTER TABLE `promocion`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_D39AF2135A91C08D` (`departamento_id`);

--
-- Indices de la tabla `ranking`
--
ALTER TABLE `ranking`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_80B839D07645698E` (`producto_id`);

--
-- Indices de la tabla `reclamo`
--
ALTER TABLE `reclamo`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_558B0D87E557397E` (`distrito_id`);

--
-- Indices de la tabla `recuperar_contrasena_url`
--
ALTER TABLE `recuperar_contrasena_url`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `redirecciones`
--
ALTER TABLE `redirecciones`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_DA7FB914989D9B62` (`slug`), ADD KEY `IDX_DA7FB9143397707A` (`categoria_id`), ADD KEY `IDX_DA7FB914D7AFF6F2` (`cupon_id`);

--
-- Indices de la tabla `suscripcion`
--
ALTER TABLE `suscripcion`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `translate`
--
ALTER TABLE `translate`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_4A106377CC4D878D` (`variable`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_EF687F292FC23A8` (`username_canonical`), ADD UNIQUE KEY `UNIQ_EF687F2A0D96FBF` (`email_canonical`);

--
-- Indices de la tabla `valor_agregado`
--
ALTER TABLE `valor_agregado`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_D74020E4767D30CB` (`valoresagregados_id`);

--
-- Indices de la tabla `zero_seo`
--
ALTER TABLE `zero_seo`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `banner`
--
ALTER TABLE `banner`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `bolsa_trabajo`
--
ALTER TABLE `bolsa_trabajo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `carrito`
--
ALTER TABLE `carrito`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `cupon`
--
ALTER TABLE `cupon`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `ext_translations`
--
ALTER TABLE `ext_translations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `formulario_promocion`
--
ALTER TABLE `formulario_promocion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `home`
--
ALTER TABLE `home`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `imagen_presentacion`
--
ALTER TABLE `imagen_presentacion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `info`
--
ALTER TABLE `info`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `local`
--
ALTER TABLE `local`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `noticia`
--
ALTER TABLE `noticia`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT de la tabla `pregunta_frecuente`
--
ALTER TABLE `pregunta_frecuente`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `presentacion`
--
ALTER TABLE `presentacion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `proceso_compra`
--
ALTER TABLE `proceso_compra`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `promocion`
--
ALTER TABLE `promocion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `ranking`
--
ALTER TABLE `ranking`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `reclamo`
--
ALTER TABLE `reclamo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `recuperar_contrasena_url`
--
ALTER TABLE `recuperar_contrasena_url`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `redirecciones`
--
ALTER TABLE `redirecciones`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `suscripcion`
--
ALTER TABLE `suscripcion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `translate`
--
ALTER TABLE `translate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `valor_agregado`
--
ALTER TABLE `valor_agregado`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `zero_seo`
--
ALTER TABLE `zero_seo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=65;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `bolsa_trabajo`
--
ALTER TABLE `bolsa_trabajo`
ADD CONSTRAINT `FK_DB09F3734E7121AF` FOREIGN KEY (`provincia_id`) REFERENCES `provincia` (`id`);

--
-- Filtros para la tabla `carrito`
--
ALTER TABLE `carrito`
ADD CONSTRAINT `FK_77E6BED54854653A` FOREIGN KEY (`pedido_id`) REFERENCES `pedido` (`id`),
ADD CONSTRAINT `FK_77E6BED57645698E` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`),
ADD CONSTRAINT `FK_77E6BED5D7AFF6F2` FOREIGN KEY (`cupon_id`) REFERENCES `cupon` (`id`),
ADD CONSTRAINT `FK_77E6BED5DE734E51` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `categoria`
--
ALTER TABLE `categoria`
ADD CONSTRAINT `FK_4E10122DD7AFF6F2` FOREIGN KEY (`cupon_id`) REFERENCES `cupon` (`id`);

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
ADD CONSTRAINT `FK_50FE07D7E557397E` FOREIGN KEY (`distrito_id`) REFERENCES `distrito` (`id`);

--
-- Filtros para la tabla `contacto`
--
ALTER TABLE `contacto`
ADD CONSTRAINT `FK_2741493C4E7121AF` FOREIGN KEY (`provincia_id`) REFERENCES `provincia` (`id`);

--
-- Filtros para la tabla `distrito`
--
ALTER TABLE `distrito`
ADD CONSTRAINT `FK_BE2719FD4E7121AF` FOREIGN KEY (`provincia_id`) REFERENCES `provincia` (`id`);

--
-- Filtros para la tabla `formulario_promocion`
--
ALTER TABLE `formulario_promocion`
ADD CONSTRAINT `FK_9C9A3B03B1E453D4` FOREIGN KEY (`promocion_id`) REFERENCES `promocion` (`id`);

--
-- Filtros para la tabla `imagen_presentacion`
--
ALTER TABLE `imagen_presentacion`
ADD CONSTRAINT `FK_7E24A2D5AF194CD1` FOREIGN KEY (`imagenes_id`) REFERENCES `presentacion` (`id`);

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
ADD CONSTRAINT `FK_C4EC16CE8A26512B` FOREIGN KEY (`distrito_facturacion_id`) REFERENCES `distrito` (`id`),
ADD CONSTRAINT `FK_C4EC16CE95EB7823` FOREIGN KEY (`distrito_envio_id`) REFERENCES `distrito` (`id`);

--
-- Filtros para la tabla `presentacion`
--
ALTER TABLE `presentacion`
ADD CONSTRAINT `FK_56A887B57645698E` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`);

--
-- Filtros para la tabla `proceso_compra`
--
ALTER TABLE `proceso_compra`
ADD CONSTRAINT `FK_7F398B3C640D1D53` FOREIGN KEY (`proceso_id`) REFERENCES `home` (`id`);

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
ADD CONSTRAINT `FK_A7BB061588D3B71A` FOREIGN KEY (`subcategoria_id`) REFERENCES `subcategoria` (`id`),
ADD CONSTRAINT `FK_A7BB0615D7AFF6F2` FOREIGN KEY (`cupon_id`) REFERENCES `cupon` (`id`);

--
-- Filtros para la tabla `provincia`
--
ALTER TABLE `provincia`
ADD CONSTRAINT `FK_D39AF2135A91C08D` FOREIGN KEY (`departamento_id`) REFERENCES `departamento` (`id`);

--
-- Filtros para la tabla `ranking`
--
ALTER TABLE `ranking`
ADD CONSTRAINT `FK_80B839D07645698E` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`);

--
-- Filtros para la tabla `reclamo`
--
ALTER TABLE `reclamo`
ADD CONSTRAINT `FK_558B0D87E557397E` FOREIGN KEY (`distrito_id`) REFERENCES `distrito` (`id`);

--
-- Filtros para la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
ADD CONSTRAINT `FK_DA7FB9143397707A` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`),
ADD CONSTRAINT `FK_DA7FB914D7AFF6F2` FOREIGN KEY (`cupon_id`) REFERENCES `cupon` (`id`);

--
-- Filtros para la tabla `valor_agregado`
--
ALTER TABLE `valor_agregado`
ADD CONSTRAINT `FK_D74020E4767D30CB` FOREIGN KEY (`valoresagregados_id`) REFERENCES `info` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;