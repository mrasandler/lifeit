<?php
namespace AdminBundle\Twig;

use Core\ZeroBundle\Helper\Util;
use Symfony\Bridge\Doctrine\RegistryInterface;
use AdminBundle\Entity\Categoria;
use Symfony\Component\HttpFoundation\Session\Session;

class GlobalsExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    protected $doctrine;
    protected $util;
    protected $locals = array();
    protected $request;

    public function __construct(RegistryInterface $doctrine)
    {
        $this->doctrine = $doctrine;
        $this->util = new Util;
    }

    public function getGlobals()
    {
        // WebBundle:Home
        //$this->locals['infoHome'] = $this->doctrine->getRepository('AdminBundle:Home')->find(1);

        // WebBundle:Info
        $this->locals['info'] = $this->doctrine->getRepository('AdminBundle:Info')->find(1);

        $em = $this->doctrine->getManager();

        $request = $this->doctrine->getRepository('AdminBundle:Categoria');

        $query = $request->createQueryBuilder('c');
        $query
            ->select('c')
            ->where('c.slug != :slug')
            ->setParameter('slug', 'otros');

        $result = $query->getQuery();

        $this->locals['headers'] = $result->setMaxResults(4)->getResult();

        $this->locals['home'] = $this->doctrine->getRepository('AdminBundle:Home')->find(1);
        $querys = $request->createQueryBuilder('l');
        $querys->select('l')->where('l.id > :total')
        ->setParameter('total', 5)->orderBy('l.id','DESC');
        $resulta = $querys->getQuery();

        $this->locals['otros'] = $resulta->setMaxResults(5)->getResult();

        //sesion de clientes
        $session = new Session();

        $this->locals['sesion'] = $session->all();
        $clienteSesionActual = $session->get('idCliente');

        //Hay un usuario logueado, por ende lo que agregue al carrito va a BD como estado pendiente hasta que pague por ello.
        if($clienteSesionActual){
            $this->locals['cliente'] = $this->doctrine->getRepository('AdminBundle:Clientes')->findOneById($clienteSesionActual);

            //Se consulta a BD por el carrito del cliente logueado.
            $request = $em->getRepository('AdminBundle:Carrito');

            $query = $request->createQueryBuilder("C");
            $query
            ->select('P.id as presentacionid, P.imagenes as imagenProducto, P.presentacion as nombreProducto, C.cantidad as cantidadProducto, C.precioUnitario as precioProducto, C.precioReal as subTotalProducto')
            ->innerJoin('C.presentacion', 'P')
            ->innerJoin('C.cliente', 'Cl')
            ->where('Cl.id = :id AND C.estado != :estado')
            ->setParameter('id', $clienteSesionActual)
            ->setParameter('estado', 'C');
            $result = $query->getQuery();
            $carritoSesion = $result->getResult();
        }else{ //No hay un usuario logueado, por ende lo que agregue al carrito va a memoria caché hasta que inicie sesión y pague por ello.
            $carritoSesion = $session->get('carrito');
        }

        $this->locals['carrito'] = $carritoSesion;

        // retornando valores
        return $this->locals;
    }

    public function getName()
    {
        return 'AdminBundle:GlobalsExtension';
    }
}
