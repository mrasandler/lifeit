<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Core\ZeroBundle\Form\Type\CKEditorType;
use Core\ZeroBundle\Form\Type\FileBrowserType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class PromocionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombres', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa Nombre de la promocion'
                ),
                'label' => 'Nombre *',
            ))
            ->add('descripcion', CKEditorType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa descripcion de la promocion'
                ),
                'label' => 'Descripción *',
            ))
            ->add('imagen', FileBrowserType::class, array(
                      'label' => 'Imagen *',
                  ))
            ->add('fechaInicio', DateType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa la fecha de inicio de la promoción'
                ),
                'label' => 'Fecha de inicio *',
                'format' => 'dd/MM/yyyy',
            ))
            ->add('fechaFin', DateType::class, array(
              'attr' => array(
                'placeholder' => 'Ingresa la fecha de fin de la promoción'
              ),
              'label' => 'Fecha de fin *',
              'format' => 'dd/MM/yyyy',
            ))
        ;
    }
}
