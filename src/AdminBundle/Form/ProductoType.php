<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Core\ZeroBundle\Form\Type\CKEditorType;
use Core\ZeroBundle\Form\Type\FileBrowserType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Doctrine\ORM\EntityRepository;

class ProductoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('subcategoriaProd', 'entity', array(
              'class' => 'AdminBundle\Entity\Subcategoria',
              'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('c');
              },
              'group_by' => 'categoria.nombreCategoria',
              'property' => 'nombreSub',
              'label' => 'Subcategoría *',
            ))
            ->add('nombreProducto', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa nombre del producto'
                ),
                'label' => 'Nombre *',
            ))
            ->add('descripcionProducto', CKEditorType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa descripción del producto'
                ),
                'label' => 'Descripción *',
            ))
            ->add('precio', TextType::class, array(
                 'attr' => array(
                     'placeholder' => 'Ingresa precio normal del producto'
                 ),
                 'label' => 'Precio normal *',
            ))
            ->add('porcentajeDescuento', TextType::class, array(
                 'attr' => array(
                     'placeholder' => 'Ingresa porcentaje de descuento'
                 ),
                 'label' => 'Porcentaje descuento',
            ))
            ->add('precioOferta', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa precio en oferta del producto'
                 ),
                 'label' => 'Precio oferta',
            ))
            ->add('imagen', FileBrowserType::class, array(
              'label' => 'Imagen *',
            ))
            ->add('beneficioProducto', CKEditorType::class, array(
              'attr' => array(
               'placeholder' => 'Ingresa beneficios del producto'
              ),
              'label' => 'Beneficios *',
            ))
            ->add('caracteristicaProducto', CKEditorType::class, array(
              'attr' => array(
                'placeholder' => 'Ingresa características del producto'
              ),
              'label' => 'Características *',
            ))
            ->add('infoProducto', CKEditorType::class, array(
              'attr' => array(
                'placeholder' => 'Ingresa información de envío del producto'
              ),
              'label' => 'Información de envío *',
            ))
            ->add('destacado', null, array(
                'label' => '¿Es producto destacado?',
            ))
            ->add('oferta', null, array(
                'label' => '¿El producto está en oferta?',
            ))
            ->add('nuevo', null, array(
                'label' => '¿El producto es nuevo?',
            ))

            ->add('apareceEnHome', CheckboxType::class, array(
                'label' => '¿Aparece en el home?',
            ))

            ->add('cupon', 'entity', array(
                'class'    => 'AdminBundle\Entity\Cupon',
                'property' => 'codigo',
                'label'    => 'Cupón *'
            ))
            ->add('destacado', null, array(
                'label' => '¿Es producto destacado?',
            ))



        ;
    }
}
