<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Core\ZeroBundle\Form\Type\CKEditorType;
use Core\ZeroBundle\Form\Type\FileBrowserType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class CuponType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('codigo', TextType::class, array(
            'attr' => array(
                'placeholder' => 'Ingresa el código del cupón'
            ),
            'label' => 'Código *',
        ))
        ->add('descripcion', TextareaType::class, array(
            'attr' => array(
                'placeholder' => 'Ingresa la descripción del cupón'
            ),
            'label' => 'Descripción *',
        ))
        ->add('descuentoEnPorcentaje', CheckboxType::class, array(
          'label' => '¿Descuento en porcentaje?',
        ))
        ->add('descuento', NumberType::class, array(
            'attr' => array(
                'placeholder' => 'Ingresa la cantidad que descontará el cupón'
            ),
            'label' => 'Descuento (%) *',
        ))
        ->add('activo', CheckboxType::class, array(
          'label' => '¿Cupón activo?',
        ))
        ;
    }
}