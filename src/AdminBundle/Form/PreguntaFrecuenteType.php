<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Core\ZeroBundle\Form\Type\CKEditorType;
use Core\ZeroBundle\Form\Type\FileBrowserType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class PreguntaFrecuenteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pregunta', null, array(
                'attr' => array(
                    'placeholder' => 'Ingresa la pregunta'
                ),
                'label' => 'Pregunta *',
            ))
            ->add('respuesta', CKEditorType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa la respuesta a la pregunta'
                ),
                'label' => 'Respuesta *',
            ))
        ;
    }
}
