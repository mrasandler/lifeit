<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Core\ZeroBundle\Form\Type\CKEditorType;
use Core\ZeroBundle\Form\Type\FileBrowserType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class NoticiaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa título de la noticia'
                ),
                'label' => 'Título *',
            ))
            ->add('contenido', CKEditorType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa descripción de la noticia'
                ),
                'label' => 'Descripción *',
            ))
            ->add('imagen', FileBrowserType::class, array(
                'label' => 'Imagen *',
            ))
            ->add('fecha', 'date', array(
                'attr' => array(
                    'placeholder' => 'Ingresa la fecha de la noticia'
                ),
                'label' => 'Fecha *',
                'format' => 'dd/MM/yyyy',
            ))
        ;
    }
}
