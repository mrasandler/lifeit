<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Core\ZeroBundle\Form\Type\CKEditorType;
use Core\ZeroBundle\Form\Type\FileBrowserType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class InfoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('correosContacto')
            ->add('titulo', null, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el título general de la página web'
                ),
                'label' => 'Título *',
            ))
            ->add('descripcion', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa la descripción general de la página web'
                ),
                'label' => 'Descripción *',
            ))
            ->add('correosContacto', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa los correos de contacto separados por coma'
                ),
                'label' => 'Correos de contacto *',
            ))
            ->add('correopaypal', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el correopypal donde se realizaran los depositos'
                ),
                'label' => 'Correo paypal*',
            ))
            ->add('facebook', 'url', array(
                'attr' => array(
                    'placeholder' => 'Ingresa el enlace de Facebook de la empresa'
                ),
                'label' => 'Facebook *',
            ))
            ->add('youtube', 'url', array(
                'attr' => array(
                    'placeholder' => 'Ingresa el enlace de Youtube de la empresa'
                ),
                'label' => 'Youtube *',
            ))
             ->add('googleplus', 'url', array(
                'attr' => array(
                    'placeholder' => 'Ingresa el enlace de Google+ de la empresa'
                ),
                'label' => 'Google + *',
            ))

            ->add('bannerPromocion', FileBrowserType::class, array(
                'label' => 'Banner *',
            ))

            ->add('televentasLima', null, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el número de televentas Lima'
                ),
                'label' => 'Televentas Lima *',
            ))
            ->add('televentasPeru', null, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el número de televentas Perú'
                ),
                'label' => 'Televentas Perú *',
            ))
            ->add('horarioAtencion', CKEditorType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el horario de atención'
                ),
                'label' => 'Horario de atención *',
            ))
            ->add('ga', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el código Google Analytics asociado a la página web'
                ),
                'label' => 'GA *',
            ))
            ->add('contenidoTerminos', CKEditorType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el contenido de los términos y condiciones de uso de la página web'
                ),
                'label' => 'Términos y condiciones *',
            ))
            ->add('contenidoTerminosDelivery', CKEditorType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el contenido de los términos y condiciones del Delivery'
                ),
                'label' => 'Términos y condiciones Delivery *',
            ))
            ->add('contenidoPrivacidad', CKEditorType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el contenido de la privacidad y seguridad de la página'
                ),
                'label' => 'Privacidad y seguridad *',
            ))
            ->add('mensajePaginaLocales', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el mensaje del banner de la página'
                ),
                'label' => 'Mensaje *',
            ))
            ->add('imagenPaginaLocales', FileBrowserType::class, array(
                'label' => 'Imagen *',
            ))
            ->add('imagenProyecto', FileBrowserType::class, array(
                'label' => 'Imagen  Proyecto *',
            ))
            ->add('activarProyecto', null, array(
                'label' => '¿Quiere activar el Proyecto?',
            ))
            ->add('imagenLogoPrincipal', FileBrowserType::class, array(
                'label' => 'Imagen Logo Principal *',
            ))
            ->add('LogoSegundario', FileBrowserType::class, array(
                'label' => ' Logo Secundario *',
            ))
            ->add('mensajePaginaBolsaTrabajo', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el mensaje del banner de la página'
                ),
                'label' => 'Mensaje *',
            ))
            ->add('imagenPaginaBolsaTrabajo', FileBrowserType::class, array(
                'label' => 'Imagen *',
            ))
            ->add('valoresAgregadosPaginaLocales', CollectionType::class, array(
                'entry_type' => Type\ValorAgregadoType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'prototype' => true,
                'error_bubbling' => false,
                'prototype_name' => '__opt_prot__',
                'label' => 'Valores agregados *'
            ))
            ->add('mensajePaginaLibroReclamaciones', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el mensaje del banner de la página'
                ),
                'label' => 'Mensaje *',
            ))
            ->add('imagenPaginaLibroReclamaciones', FileBrowserType::class, array(
                'label' => 'Imagen *',
            ))
            ->add('latitud', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa latitud del local principal'
                ),
                'label' => 'Latitud *',
            ))
            ->add('longitud', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa longitud del local principal'
                ),
                'label' => 'Longitud *',
            ))
            ->add('urlWsFacebook', 'url', array(
                'attr' => array(
                    'placeholder' => 'Ingresa la URL del API',
                ),
                'label' => 'URL API Facebook *',
            ))
            ->add('appTokenFacebook', null, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el APP TOKEN'
                ),
                'label' => 'APP TOKEN *',
            ))
            ->add('numeroPublicacionesFacebook', IntegerType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el número de publicaciones que deseas que aparezcan'
                ),
                'label' => 'Número de publicaciones *',
            ))
            ->add('fanPageFacebook', null, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el nombre de la Fan page cuyos posts deseas que aparezcan'
                ),
                'label' => 'Fan page *',
            ))
            ->add('precioMasBajo', NumberType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el precio más bajo en el rango de precios al momento de filtrar un producto',
                ),
                'label' => 'Precio más bajo *',
            ))
            ->add('precioMasAlto', NumberType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el precio más alto en el rango de precios al momento de filtrar un producto',
                ),
                'label' => 'Precio más alto *',
            ))
            ->add('precioEnvioLima', NumberType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el monto para el envío de productos para Lima',
                ),
                'label' => 'Monto de envío Lima *',
            ))
            ->add('precioEnvioProvincias', NumberType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el monto para el envío de productos para provincias',
                ),
                'label' => 'Monto de envío provincias *',
            ))

             ->add('tiempoEnvioLima', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el tiempo de envío de productos para Lima',
                ),
                'label' => 'Tiempo de envío Lima *',
            ))

            ->add('tiempoEnvioProvincias', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el tiempo de envío de productos para provincias',
                ),
                'label' => 'Tiempo de envío provincias *',
            ))
            ->add('tituloFondoConocenos', TextType::class, array(
                'attr' => array(
                    'placeholder' => ''
                ),
                'label' => 'Titulo Imagen de Fondo *',
            ))
            ->add('subtituloFondoConocenos', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el subtitulo para la imagenes de fondo'
                ),
                'label' => 'Subtitulo imagen de fondo *',
            ))
            ->add('imagenFondoConocenos', FileBrowserType::class, array(
                'label' => 'Imagen *',
            ))
        ;
        ;
    }
}
