<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Core\ZeroBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Core\ZeroBundle\Form\Type\FileBrowserType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class HomeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
                ->add('activarSuscripcion', null, array(
                    'label' => '¿Activar Suscripcion?',
                ))
                ->add('activarCanal', null, array(
                    'label' => '¿Activar Canal ?',
                ))
              ->add('imagenVideo', FileBrowserType::class, array(
                      'label' => 'Imagen video *',
               ))
            ->add('tituloVideo', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa un título para el video',
                ),
                'label' => 'Título *',
            ))
            ->add('subtituloVideo', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa Subtítulo para el video',
                ),
                'label' => 'Subtítulo *',
            ))
            ->add('contenidoVideo', TextareaType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa un contenido para el video',
                ),
                'label' => 'Contenido *',
            ))
            ->add('textoBoton', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa un texto para el botón',
                ),
                'label' => 'Texto botón *',
            ))
             ->add('urlBoton', 'url' ,  array(
                'attr' => array(
                    'placeholder' => 'Ingresa url para el botón',
                ),
                'label' => 'Enlace video *',
            ))
            ->add('tituloSeccionCompra', null, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el título de la sección',
                ),
                'label' => 'Título *',
            ))
            ->add('mensajeSeccionCompra', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa breve descripción de la sección',
                ),
                'label' => 'Descripción *',
            ))
            ->add('procesosCompra', CollectionType::class, array(
                'entry_type' => Type\ProcesoCompraType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'prototype' => true,
                'error_bubbling' => false,
                'prototype_name' => '__opt_prot__',
                'label' => 'Procesos de compra *'
            ))
            ->add('mensajeProcesoCompra', null, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el mensaje adicional de la sección',
                ),
                'label' => 'Mensaje adicional *',
            ))
            ->add('textoBotonSeccionCompra', null, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el texto del botón de la sección',
                ),
                'label' => 'Texto botón *',
            ))
            ->add('urlBotonSeccionCompra', 'url', array(
                'attr' => array(
                    'placeholder' => 'Ingresa url para el botón de la sección',
                ),
                'label' => 'Enlace botón *',
            ))
            ->add('catalogoTitulo', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa un texto para el botón',
                ),
                'label' => 'Título *',
            ))
            ->add('catalogoContenido', TextareaType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa una breve descripción de la sección',
                ),
                'label' => 'Descripción *',
            ))
            ->add('catalogoImagen', FileBrowserType::class, array(
                      'label' => 'Imagen *',
               ))
            ->add('catalogoArchivo', FileBrowserType::class, array(
                      'label' => 'Archivo *',
               ))
            ->add('catalogoBoton', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el texto para el botón',
                ),
                'label' => 'Texto botón',
            ))

            ->add('imagenSuscribete', FileBrowserType::class, array(
                      'label' => 'Imagen Banner Suscribete *',
               ))
        ;
    }
}
