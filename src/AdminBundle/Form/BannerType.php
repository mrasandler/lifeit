<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Core\ZeroBundle\Form\Type\CKEditorType;
use Core\ZeroBundle\Form\Type\FileBrowserType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class BannerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
             ->add('titulo', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa título para el banner'
                ),
                'label' => 'Título *',
            ))
             ->add('contenido', TextareaType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa contenido para el banner'
                ),
                'label' => 'Contenido *',
            ))
             ->add('imagen', FileBrowserType::class, array(
                      'label' => 'Imagen superpuesta *',
               ))
             ->add('imagenFondo', FileBrowserType::class, array(
                      'label' => 'Imagen de fondo *',
               ))
             ->add('textoBoton1', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa texto para el primer botón'
                ),
                'label' => 'Texto botón 1',
            ))
            ->add('textoBoton2', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa título para el segundo botón'
                ),
                'label' => 'Texto botón 2',
            ))
            ->add('urlBoton1','url', array(
                'attr' => array(
                    'placeholder' => 'Ingresa url hacia donde llevará el botón 1'
                ),
                'label' => 'Enlace botón 1',
            ))
            ->add('urlBoton2','url', array(
                'attr' => array(
                    'placeholder' => 'Ingresa url hacia donde llevará el botón 2'
                ),
                'label' => 'Enlace botón 2',
            ))
        ;
    }
}
