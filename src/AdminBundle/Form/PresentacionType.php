<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Core\ZeroBundle\Form\Type\FileBrowserType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class PresentacionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder
      ->add('productoPre', 'entity', array(
          'class'    => 'AdminBundle\Entity\Producto',
          'property' => 'nombreProducto',
          'label'    => 'Producto *'
        ))
        ->add('productoTa', 'entity', array(
            'class'    => 'AdminBundle\Entity\Talla',
            'property' => 'talla',
            'label'    => 'Talla *'
         ))
         ->add('productoCo', 'entity', array(
              'class'    => 'AdminBundle\Entity\Color',
              'property' => 'color',
              'label'    => 'Color *'
          ))
         ->add('presentacion', TextType::class, array(
            'attr' => array(
                'placeholder' => 'Ingresa el nombre de la presentación'
            ),
            'label' => 'Nombre *',
          ))
        ->add('stock', TextType::class, array(
            'attr' => array(
                'placeholder' => 'Ingresa el número de ítems que tiene el producto en stock'
            ),
            'label' => 'Stock *',
          ))
        ->add('imagenes', CollectionType::class, array(
          'entry_type' => Type\ImagenPresentacionType::class,
          'allow_add' => true,
          'allow_delete' => true,
          'delete_empty' => true,
          'prototype' => true,
          'error_bubbling' => false,
          'prototype_name' => '__opt_prot__',
          'label' => 'Imágenes *'
        ))

      ;
    }
}
