<?php

namespace AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Core\ZeroBundle\Form\Type\CKEditorType;
use Core\ZeroBundle\Form\Type\FileBrowserType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class TallasProductoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('talla', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el valor de la Talla'
                ),
                'label' => 'Tallas *',
            ))
            ->add('stock', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el stock'
                ),
                'label' => 'Stock *',
            ))
        ;
    }
}