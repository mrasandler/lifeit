<?php

namespace AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Core\ZeroBundle\Form\Type\CKEditorType;
use Core\ZeroBundle\Form\Type\FileBrowserType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ImagenPresentacionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imagenTodo', FileBrowserType::class, array(
                'label' => 'Imagen *',
            ))
            ->add('urlVideo', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa url de video de la presentación'
                ),
                'label' => 'Enlace video',
            ))
        ;
    }
}
