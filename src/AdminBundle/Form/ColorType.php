<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Core\ZeroBundle\Form\Type\FileBrowserType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ColorType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder

         ->add('color', null, array(
            'attr' => array(
                'placeholder' => 'codigo del color'
            ),
            'label' => 'Codigo *',
          ))
          ->add('descripcion', TextType::class, array(
             'attr' => array(
                 'placeholder' => 'Nombre del Color'
             ),
             'label' => 'Descripcion *',
           ))
          ;
    }
}
