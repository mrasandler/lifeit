<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Core\ZeroBundle\Form\Type\CKEditorType;
use Core\ZeroBundle\Form\Type\FileBrowserType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class SubcategoriaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombreSub', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el nombre de la subcategoría'
                ),
                'label' => 'Nombre *',
            ))
            ->add('descripcionSub', TextareaType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa la descripción de la subcategoría'
                ),
                'label' => 'Descripción *',
            ))
            ->add('categoria', 'entity', array(
                'class'    => 'AdminBundle\Entity\Categoria',
                'property' => 'nombreCategoria',
                'label'    => 'Categoría *'
            ))
            ->add('cupon', 'entity', array(
                'class'    => 'AdminBundle\Entity\Cupon',
                'property' => 'codigo',
                'label'    => 'Cupón *'
            ))
        ;
    }
}
