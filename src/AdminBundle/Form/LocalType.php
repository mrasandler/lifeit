<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Core\ZeroBundle\Form\Type\CKEditorType;
use Core\ZeroBundle\Form\Type\FileBrowserType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class LocalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lugar', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el nombre del local'
                ),
                'label' => 'Nombre *',
            ))
            ->add('imagen', FileBrowserType::class, array(
                'label' => 'Imagen *',
            ))
            ->add('direccion', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa la dirección del local'
                ),
                'label' => 'Dirección *',
            ))
            ->add('telefonos', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa los teléfonos del local'
                ),
                'label' => 'Teléfonos *',
            ))
            ->add('latitud', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa la latitud del local'
                ),
                'label' => 'Latitud *',
            ))
            ->add('longitud', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa la longitud del local'
                ),
                'label' => 'Longitud *',
            ))
        ;
    }
}
