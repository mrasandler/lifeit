<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Core\ZeroBundle\Form\Type\CKEditorType;
use Core\ZeroBundle\Form\Type\FileBrowserType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class CategoriaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombreCategoria', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa el nombre de la categoría'
                ),
                'label' => 'Nombre *',
            ))
            ->add('descripcionCategoria', TextareaType::class, array(
                'attr' => array(
                    'placeholder' => 'Ingresa la descripción de la categoría'
                ),
                'label' => 'Descripción *',
            ))
            ->add('imagen', FileBrowserType::class, array(
              'label' => 'Imagen *',
            ))
            ->add('cupon', 'entity', array(
                'class'    => 'AdminBundle\Entity\Cupon',
                'property' => 'codigo',
                'label'    => 'Cupón *'
            ))
        ;
    }
}
