<?php

namespace AdminBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Banner{

      /**
       * @var integer
       *
       * @ORM\Column(name="id", type="integer")
       * @ORM\Id
       * @ORM\GeneratedValue(strategy="AUTO")
       */
      private $id;

      /**
       * @ORM\Column(type="string", length=255)
       * @Assert\NotBlank()
       */
      private $titulo;


      /**
       * @ORM\Column(type="string", length=255)
       * @Assert\NotBlank()
       */
      private $contenido;

      /**
       * @ORM\Column(type="string", length=255)
       * @Assert\NotBlank()
       */
      private $imagen;

      /**
       * @ORM\Column(type="string", length=255)
       * @Assert\NotBlank()
       */
      private $imagenFondo;

      /**
       * @ORM\Column(type="string", length=255, nullable=true)
       */
      private $textoBoton1;

      /**
       * @ORM\Column(type="string", length=255, nullable=true)
       */
      private $textoBoton2;

      /**
       * @ORM\Column(type="string", length=255, nullable=true)
       */
      private $urlBoton1;

      /**
       * @ORM\Column(type="string", length=255, nullable=true)
       */
      private $urlBoton2;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Banner
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set contenido
     *
     * @param string $contenido
     * @return Banner
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return string 
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     * @return Banner
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string 
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set imagenFondo
     *
     * @param string $imagenFondo
     * @return Banner
     */
    public function setImagenFondo($imagenFondo)
    {
        $this->imagenFondo = $imagenFondo;

        return $this;
    }

    /**
     * Get imagenFondo
     *
     * @return string 
     */
    public function getImagenFondo()
    {
        return $this->imagenFondo;
    }

    /**
     * Set textoBoton1
     *
     * @param string $textoBoton1
     * @return Banner
     */
    public function setTextoBoton1($textoBoton1)
    {
        $this->textoBoton1 = $textoBoton1;

        return $this;
    }

    /**
     * Get textoBoton1
     *
     * @return string 
     */
    public function getTextoBoton1()
    {
        return $this->textoBoton1;
    }

    /**
     * Set textoBoton2
     *
     * @param string $textoBoton2
     * @return Banner
     */
    public function setTextoBoton2($textoBoton2)
    {
        $this->textoBoton2 = $textoBoton2;

        return $this;
    }

    /**
     * Get textoBoton2
     *
     * @return string 
     */
    public function getTextoBoton2()
    {
        return $this->textoBoton2;
    }

    /**
     * Set urlBoton1
     *
     * @param string $urlBoton1
     * @return Banner
     */
    public function setUrlBoton1($urlBoton1)
    {
        $this->urlBoton1 = $urlBoton1;

        return $this;
    }

    /**
     * Get urlBoton1
     *
     * @return string 
     */
    public function getUrlBoton1()
    {
        return $this->urlBoton1;
    }

    /**
     * Set urlBoton2
     *
     * @param string $urlBoton2
     * @return Banner
     */
    public function setUrlBoton2($urlBoton2)
    {
        $this->urlBoton2 = $urlBoton2;

        return $this;
    }

    /**
     * Get urlBoton2
     *
     * @return string 
     */
    public function getUrlBoton2()
    {
        return $this->urlBoton2;
    }
}
