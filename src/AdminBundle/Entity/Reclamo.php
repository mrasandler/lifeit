<?php

namespace AdminBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Reclamo{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $tipoDocumentoReclamante;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $numeroDocumentoReclamante;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $nombresReclamante;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $apellidosReclamante;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $telefonoReclamante;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $emailReclamante;

    /**
    * @ORM\ManyToOne(targetEntity="Distrito", inversedBy="reclamos")
    * @ORM\JoinColumn(name="distrito_id", referencedColumnName="id")
    */
    private $distrito;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $direccion;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $tipoDocumentoApoderado;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $numeroDocumentoApoderado;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombresApoderado;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $apellidosApoderado;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank()
     */
    private $tipoReclamo;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $mensaje;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha = null;

    public function __construct(){
      $this->fecha = new \DateTime();
    }

    public function getFechaFormateada()
    {
        return $this->fecha->format('d-m-Y H:i:s');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipoDocumentoReclamante
     *
     * @param string $tipoDocumentoReclamante
     * @return Reclamo
     */
    public function setTipoDocumentoReclamante($tipoDocumentoReclamante)
    {
        $this->tipoDocumentoReclamante = $tipoDocumentoReclamante;

        return $this;
    }

    /**
     * Get tipoDocumentoReclamante
     *
     * @return string
     */
    public function getTipoDocumentoReclamante()
    {
        return $this->tipoDocumentoReclamante;
    }

    /**
     * Set numeroDocumentoReclamante
     *
     * @param string $numeroDocumentoReclamante
     * @return Reclamo
     */
    public function setNumeroDocumentoReclamante($numeroDocumentoReclamante)
    {
        $this->numeroDocumentoReclamante = $numeroDocumentoReclamante;

        return $this;
    }

    /**
     * Get numeroDocumentoReclamante
     *
     * @return string
     */
    public function getNumeroDocumentoReclamante()
    {
        return $this->numeroDocumentoReclamante;
    }

    /**
     * Set nombresReclamante
     *
     * @param string $nombresReclamante
     * @return Reclamo
     */
    public function setNombresReclamante($nombresReclamante)
    {
        $this->nombresReclamante = $nombresReclamante;

        return $this;
    }

    /**
     * Get nombresReclamante
     *
     * @return string
     */
    public function getNombresReclamante()
    {
        return $this->nombresReclamante;
    }

    /**
     * Set apellidosReclamante
     *
     * @param string $apellidosReclamante
     * @return Reclamo
     */
    public function setApellidosReclamante($apellidosReclamante)
    {
        $this->apellidosReclamante = $apellidosReclamante;

        return $this;
    }

    /**
     * Get apellidosReclamante
     *
     * @return string
     */
    public function getApellidosReclamante()
    {
        return $this->apellidosReclamante;
    }

    /**
     * Set telefonoReclamante
     *
     * @param string $telefonoReclamante
     * @return Reclamo
     */
    public function setTelefonoReclamante($telefonoReclamante)
    {
        $this->telefonoReclamante = $telefonoReclamante;

        return $this;
    }

    /**
     * Get telefonoReclamante
     *
     * @return string
     */
    public function getTelefonoReclamante()
    {
        return $this->telefonoReclamante;
    }

    /**
     * Set emailReclamante
     *
     * @param string $emailReclamante
     * @return Reclamo
     */
    public function setEmailReclamante($emailReclamante)
    {
        $this->emailReclamante = $emailReclamante;

        return $this;
    }

    /**
     * Get emailReclamante
     *
     * @return string
     */
    public function getEmailReclamante()
    {
        return $this->emailReclamante;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Reclamo
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set tipoDocumentoApoderado
     *
     * @param string $tipoDocumentoApoderado
     * @return Reclamo
     */
    public function setTipoDocumentoApoderado($tipoDocumentoApoderado)
    {
        $this->tipoDocumentoApoderado = $tipoDocumentoApoderado;

        return $this;
    }

    /**
     * Get tipoDocumentoApoderado
     *
     * @return string
     */
    public function getTipoDocumentoApoderado()
    {
        return $this->tipoDocumentoApoderado;
    }

    /**
     * Set numeroDocumentoApoderado
     *
     * @param string $numeroDocumentoApoderado
     * @return Reclamo
     */
    public function setNumeroDocumentoApoderado($numeroDocumentoApoderado)
    {
        $this->numeroDocumentoApoderado = $numeroDocumentoApoderado;

        return $this;
    }

    /**
     * Get numeroDocumentoApoderado
     *
     * @return string
     */
    public function getNumeroDocumentoApoderado()
    {
        return $this->numeroDocumentoApoderado;
    }

    /**
     * Set nombresApoderado
     *
     * @param string $nombresApoderado
     * @return Reclamo
     */
    public function setNombresApoderado($nombresApoderado)
    {
        $this->nombresApoderado = $nombresApoderado;

        return $this;
    }

    /**
     * Get nombresApoderado
     *
     * @return string
     */
    public function getNombresApoderado()
    {
        return $this->nombresApoderado;
    }

    /**
     * Set apellidosApoderado
     *
     * @param string $apellidosApoderado
     * @return Reclamo
     */
    public function setApellidosApoderado($apellidosApoderado)
    {
        $this->apellidosApoderado = $apellidosApoderado;

        return $this;
    }

    /**
     * Get apellidosApoderado
     *
     * @return string
     */
    public function getApellidosApoderado()
    {
        return $this->apellidosApoderado;
    }

    /**
     * Set tipoReclamo
     *
     * @param string $tipoReclamo
     * @return Reclamo
     */
    public function setTipoReclamo($tipoReclamo)
    {
        $this->tipoReclamo = $tipoReclamo;

        return $this;
    }

    /**
     * Get tipoReclamo
     *
     * @return string
     */
    public function getTipoReclamo()
    {
        return $this->tipoReclamo;
    }

    /**
     * Set mensaje
     *
     * @param string $mensaje
     * @return Reclamo
     */
    public function setMensaje($mensaje)
    {
        $this->mensaje = $mensaje;

        return $this;
    }

    /**
     * Get mensaje
     *
     * @return string
     */
    public function getMensaje()
    {
        return $this->mensaje;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Reclamo
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set distrito
     *
     * @param \AdminBundle\Entity\Distrito $distrito
     * @return Reclamo
     */
    public function setDistrito(\AdminBundle\Entity\Distrito $distrito = null)
    {
        $this->distrito = $distrito;

        return $this;
    }

    /**
     * Get distrito
     *
     * @return \AdminBundle\Entity\Distrito
     */
    public function getDistrito()
    {
        return $this->distrito;
    }
}
