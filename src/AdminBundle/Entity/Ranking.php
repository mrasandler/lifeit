<?php

namespace AdminBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Ranking{

   /**
    * @var integer
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $id;

   /**
    * @ORM\ManyToOne(targetEntity="Producto", inversedBy="ranking")
    * @ORM\JoinColumn(name="producto_id", referencedColumnName="id" , onDelete="CASCADE")
    * @Assert\NotBlank()
    */
    private $producto;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
     private $puntuacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
     private $fecha = null;

     public function __construct(){
        $this->fecha = new \DateTime();
     }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set puntuacion
     *
     * @param string $puntuacion
     * @return Ranking
     */
    public function setPuntuacion($puntuacion)
    {
        $this->puntuacion = $puntuacion;

        return $this;
    }

    /**
     * Get puntuacion
     *
     * @return string
     */
    public function getPuntuacion()
    {
        return $this->puntuacion;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Ranking
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set producto
     *
     * @param \AdminBundle\Entity\Producto $producto
     * @return Ranking
     */
    public function setProducto(\AdminBundle\Entity\Producto $producto = null)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \AdminBundle\Entity\Producto
     */
    public function getProducto()
    {
        return $this->producto;
    }
}
