<?php

namespace AdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 */
class Pedido
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Carrito", mappedBy="pedido")
     */
    protected $carrito;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $nombresCompletosFacturacion;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $correoFacturacion;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $direccionFacturacion;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank()
     */
    private $telefonoFacturacion;

    /**
    * @ORM\ManyToOne(targetEntity="Distrito", inversedBy="pedidosFacturacion")
    * @ORM\JoinColumn(name="distrito_facturacion_id", referencedColumnName="id")
    * @Assert\NotBlank()
    */
    private $distritoFacturacion;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank()
     */
    private $tipoComprobante;

    /**
     * @ORM\Column(type="string", length=11, nullable=true)
     */
    private $ruc;

    /**
     * @ORM\Column(type="string", length=8)
     * @Assert\NotBlank()
     */
    private $dni;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $nombresCompletosEnvio;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $correoEnvio;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $telefonoEnvio;

    /**
    * @ORM\ManyToOne(targetEntity="Distrito", inversedBy="pedidosEnvio")
    * @ORM\JoinColumn(name="distrito_envio_id", referencedColumnName="id", nullable=true)
    */
    private $distritoEnvio;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $direccionEnvio;

     /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha = null;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $montoFinal;    ////monto total de la compra total

     /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $descuento;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $codigo;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $estado;

     /**
     * @ORM\Column(type="string" , nullable=true)
     */
    private $tiempoEnvio;


    public function __construct(){
        $this->fecha = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombresCompletosFacturacion
     *
     * @param string $nombresCompletosFacturacion
     * @return Pedido
     */
    public function setNombresCompletosFacturacion($nombresCompletosFacturacion)
    {
        $this->nombresCompletosFacturacion = $nombresCompletosFacturacion;

        return $this;
    }

    /**
     * Get nombresCompletosFacturacion
     *
     * @return string
     */
    public function getNombresCompletosFacturacion()
    {
        return $this->nombresCompletosFacturacion;
    }

    /**
     * Set correoFacturacion
     *
     * @param string $correoFacturacion
     * @return Pedido
     */
    public function setCorreoFacturacion($correoFacturacion)
    {
        $this->correoFacturacion = $correoFacturacion;

        return $this;
    }

    /**
     * Get correoFacturacion
     *
     * @return string
     */
    public function getCorreoFacturacion()
    {
        return $this->correoFacturacion;
    }

    /**
     * Set direccionFacturacion
     *
     * @param string $direccionFacturacion
     * @return Pedido
     */
    public function setDireccionFacturacion($direccionFacturacion)
    {
        $this->direccionFacturacion = $direccionFacturacion;

        return $this;
    }

    /**
     * Get direccionFacturacion
     *
     * @return string
     */
    public function getDireccionFacturacion()
    {
        return $this->direccionFacturacion;
    }

    /**
     * Set telefonoFacturacion
     *
     * @param string $telefonoFacturacion
     * @return Pedido
     */
    public function setTelefonoFacturacion($telefonoFacturacion)
    {
        $this->telefonoFacturacion = $telefonoFacturacion;

        return $this;
    }

    /**
     * Get telefonoFacturacion
     *
     * @return string
     */
    public function getTelefonoFacturacion()
    {
        return $this->telefonoFacturacion;
    }

    /**
     * Set tipoComprobante
     *
     * @param string $tipoComprobante
     * @return Pedido
     */
    public function setTipoComprobante($tipoComprobante)
    {
        $this->tipoComprobante = $tipoComprobante;

        return $this;
    }

    /**
     * Get tipoComprobante
     *
     * @return string
     */
    public function getTipoComprobante()
    {
        return $this->tipoComprobante;
    }

    /**
     * Set ruc
     *
     * @param string $ruc
     * @return Pedido
     */
    public function setRuc($ruc)
    {
        $this->ruc = $ruc;

        return $this;
    }

    /**
     * Get ruc
     *
     * @return string
     */
    public function getRuc()
    {
        return $this->ruc;
    }

    /**
     * Set dni
     *
     * @param string $dni
     * @return Pedido
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set nombresCompletosEnvio
     *
     * @param string $nombresCompletosEnvio
     * @return Pedido
     */
    public function setNombresCompletosEnvio($nombresCompletosEnvio)
    {
        $this->nombresCompletosEnvio = $nombresCompletosEnvio;

        return $this;
    }

    /**
     * Get nombresCompletosEnvio
     *
     * @return string
     */
    public function getNombresCompletosEnvio()
    {
        return $this->nombresCompletosEnvio;
    }

    /**
     * Set correoEnvio
     *
     * @param string $correoEnvio
     * @return Pedido
     */
    public function setCorreoEnvio($correoEnvio)
    {
        $this->correoEnvio = $correoEnvio;

        return $this;
    }

    /**
     * Get correoEnvio
     *
     * @return string
     */
    public function getCorreoEnvio()
    {
        return $this->correoEnvio;
    }

    /**
     * Set telefonoEnvio
     *
     * @param string $telefonoEnvio
     * @return Pedido
     */
    public function setTelefonoEnvio($telefonoEnvio)
    {
        $this->telefonoEnvio = $telefonoEnvio;

        return $this;
    }

    /**
     * Get telefonoEnvio
     *
     * @return string
     */
    public function getTelefonoEnvio()
    {
        return $this->telefonoEnvio;
    }

    /**
     * Set direccionEnvio
     *
     * @param string $direccionEnvio
     * @return Pedido
     */
    public function setDireccionEnvio($direccionEnvio)
    {
        $this->direccionEnvio = $direccionEnvio;

        return $this;
    }

    /**
     * Get direccionEnvio
     *
     * @return string
     */
    public function getDireccionEnvio()
    {
        return $this->direccionEnvio;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Pedido
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set montoFinal
     *
     * @param string $montoFinal
     * @return Pedido
     */
    public function setMontoFinal($montoFinal)
    {
        $this->montoFinal = $montoFinal;

        return $this;
    }

    /**
     * Get montoFinal
     *
     * @return string
     */
    public function getMontoFinal()
    {
        return $this->montoFinal;
    }

    /**
     * Set descuento
     *
     * @param string $descuento
     * @return Pedido
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Get descuento
     *
     * @return string
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Pedido
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Pedido
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set tiempoEnvio
     *
     * @param string $tiempoEnvio
     * @return Pedido
     */
    public function setTiempoEnvio($tiempoEnvio)
    {
        $this->tiempoEnvio = $tiempoEnvio;

        return $this;
    }

    /**
     * Get tiempoEnvio
     *
     * @return string
     */
    public function getTiempoEnvio()
    {
        return $this->tiempoEnvio;
    }

    /**
     * Add carrito
     *
     * @param \AdminBundle\Entity\Carrito $carrito
     * @return Pedido
     */
    public function addCarrito(\AdminBundle\Entity\Carrito $carrito)
    {
        $this->carrito[] = $carrito;

        return $this;
    }

    /**
     * Remove carrito
     *
     * @param \AdminBundle\Entity\Carrito $carrito
     */
    public function removeCarrito(\AdminBundle\Entity\Carrito $carrito)
    {
        $this->carrito->removeElement($carrito);
    }

    /**
     * Get carrito
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCarrito()
    {
        return $this->carrito;
    }

    /**
     * Set distritoFacturacion
     *
     * @param \AdminBundle\Entity\Distrito $distritoFacturacion
     * @return Pedido
     */
    public function setDistritoFacturacion(\AdminBundle\Entity\Distrito $distritoFacturacion = null)
    {
        $this->distritoFacturacion = $distritoFacturacion;

        return $this;
    }

    /**
     * Get distritoFacturacion
     *
     * @return \AdminBundle\Entity\Distrito
     */
    public function getDistritoFacturacion()
    {
        return $this->distritoFacturacion;
    }

    /**
     * Set distritoEnvio
     *
     * @param \AdminBundle\Entity\Distrito $distritoEnvio
     * @return Pedido
     */
    public function setDistritoEnvio(\AdminBundle\Entity\Distrito $distritoEnvio = null)
    {
        $this->distritoEnvio = $distritoEnvio;

        return $this;
    }

    /**
     * Get distritoEnvio
     *
     * @return \AdminBundle\Entity\Distrito
     */
    public function getDistritoEnvio()
    {
        return $this->distritoEnvio;
    }
}
