<?php

namespace AdminBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Cupon{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=20)
   * @Assert\NotBlank()
   */
  private $codigo;

  /**
   * @ORM\Column(type="string", length=100)
   * @Assert\NotBlank()
   */
  private $descripcion;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $descuentoEnPorcentaje;

  /**
   * @ORM\Column(type="float", scale=2)
   * @Assert\NotBlank()
   */
  private $descuento;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $activo;

  /**
   * @ORM\OneToMany(targetEntity="Categoria", mappedBy="cupon")
   */
  protected $categorias;

  /**
   * @ORM\OneToMany(targetEntity="Subcategoria", mappedBy="cupon")
   */
  protected $subcategorias;

  /**
   * @ORM\OneToMany(targetEntity="Producto", mappedBy="cupon")
   */
  protected $productos;

    /**
     * @ORM\OneToMany(targetEntity="Carrito", mappedBy="cupon")
     */
    protected $carrito;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->categorias = new \Doctrine\Common\Collections\ArrayCollection();
    $this->subcategorias = new \Doctrine\Common\Collections\ArrayCollection();
    $this->productos = new \Doctrine\Common\Collections\ArrayCollection();
    $this->carrito = new \Doctrine\Common\Collections\ArrayCollection();
  }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Cupon
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Cupon
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set descuentoEnPorcentaje
     *
     * @param boolean $descuentoEnPorcentaje
     * @return Cupon
     */
    public function setDescuentoEnPorcentaje($descuentoEnPorcentaje)
    {
        $this->descuentoEnPorcentaje = $descuentoEnPorcentaje;

        return $this;
    }

    /**
     * Get descuentoEnPorcentaje
     *
     * @return boolean
     */
    public function getDescuentoEnPorcentaje()
    {
        return $this->descuentoEnPorcentaje;
    }

    /**
     * Set descuento
     *
     * @param float $descuento
     * @return Cupon
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Get descuento
     *
     * @return float
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Cupon
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Add categorias
     *
     * @param \AdminBundle\Entity\Categoria $categorias
     * @return Cupon
     */
    public function addCategoria(\AdminBundle\Entity\Categoria $categorias)
    {
        $this->categorias[] = $categorias;

        return $this;
    }

    /**
     * Remove categorias
     *
     * @param \AdminBundle\Entity\Categoria $categorias
     */
    public function removeCategoria(\AdminBundle\Entity\Categoria $categorias)
    {
        $this->categorias->removeElement($categorias);
    }

    /**
     * Get categorias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategorias()
    {
        return $this->categorias;
    }

    /**
     * Add subcategorias
     *
     * @param \AdminBundle\Entity\Subcategoria $subcategorias
     * @return Cupon
     */
    public function addSubcategoria(\AdminBundle\Entity\Subcategoria $subcategorias)
    {
        $this->subcategorias[] = $subcategorias;

        return $this;
    }

    /**
     * Remove subcategorias
     *
     * @param \AdminBundle\Entity\Subcategoria $subcategorias
     */
    public function removeSubcategoria(\AdminBundle\Entity\Subcategoria $subcategorias)
    {
        $this->subcategorias->removeElement($subcategorias);
    }

    /**
     * Get subcategorias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubcategorias()
    {
        return $this->subcategorias;
    }

    /**
     * Add productos
     *
     * @param \AdminBundle\Entity\Producto $productos
     * @return Cupon
     */
    public function addProducto(\AdminBundle\Entity\Producto $productos)
    {
        $this->productos[] = $productos;

        return $this;
    }

    /**
     * Remove productos
     *
     * @param \AdminBundle\Entity\Producto $productos
     */
    public function removeProducto(\AdminBundle\Entity\Producto $productos)
    {
        $this->productos->removeElement($productos);
    }

    /**
     * Get productos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductos()
    {
        return $this->productos;
    }

    /**
     * Add carrito
     *
     * @param \AdminBundle\Entity\Carrito $carrito
     * @return Cupon
     */
    public function addCarrito(\AdminBundle\Entity\Carrito $carrito)
    {
        $this->carrito[] = $carrito;

        return $this;
    }

    /**
     * Remove carrito
     *
     * @param \AdminBundle\Entity\Carrito $carrito
     */
    public function removeCarrito(\AdminBundle\Entity\Carrito $carrito)
    {
        $this->carrito->removeElement($carrito);
    }

    /**
     * Get carrito
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCarrito()
    {
        return $this->carrito;
    }
}
