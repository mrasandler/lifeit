<?php

namespace AdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 */
class ValorAgregado
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Info", inversedBy="valoresAgregadosPaginaLocales")
     * @ORM\JoinColumn(name="valoresagregados_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $paginaLocales;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $descripcion;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return ValorAgregado
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return ValorAgregado
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set paginaLocales
     *
     * @param \AdminBundle\Entity\Info $paginaLocales
     * @return ValorAgregado
     */
    public function setPaginaLocales(\AdminBundle\Entity\Info $paginaLocales = null)
    {
        $this->paginaLocales = $paginaLocales;

        return $this;
    }

    /**
     * Get paginaLocales
     *
     * @return \AdminBundle\Entity\Info 
     */
    public function getPaginaLocales()
    {
        return $this->paginaLocales;
    }
}
