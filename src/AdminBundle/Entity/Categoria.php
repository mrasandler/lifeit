<?php

namespace AdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 */
class Categoria
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nombreCategoria;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $descripcionCategoria;

    /**
     * @Gedmo\Slug(fields={"nombreCategoria"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="Subcategoria", mappedBy="categoria")
     */
    protected $subcategoria;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $imagen;

    /**
     * @ORM\ManyToOne(targetEntity="Cupon", inversedBy="categorias")
     * @ORM\JoinColumn(name="cupon_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $cupon;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subcategoria = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreCategoria
     *
     * @param string $nombreCategoria
     * @return Categoria
     */
    public function setNombreCategoria($nombreCategoria)
    {
        $this->nombreCategoria = $nombreCategoria;

        return $this;
    }

    /**
     * Get nombreCategoria
     *
     * @return string
     */
    public function getNombreCategoria()
    {
        return $this->nombreCategoria;
    }

    /**
     * Set descripcionCategoria
     *
     * @param string $descripcionCategoria
     * @return Categoria
     */
    public function setDescripcionCategoria($descripcionCategoria)
    {
        $this->descripcionCategoria = $descripcionCategoria;

        return $this;
    }

    /**
     * Get descripcionCategoria
     *
     * @return string
     */
    public function getDescripcionCategoria()
    {
        return $this->descripcionCategoria;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Categoria
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     * @return Categoria
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Add subcategoria
     *
     * @param \AdminBundle\Entity\Subcategoria $subcategoria
     * @return Categoria
     */
    public function addSubcategorium(\AdminBundle\Entity\Subcategoria $subcategoria)
    {
        $this->subcategoria[] = $subcategoria;

        return $this;
    }

    /**
     * Remove subcategoria
     *
     * @param \AdminBundle\Entity\Subcategoria $subcategoria
     */
    public function removeSubcategorium(\AdminBundle\Entity\Subcategoria $subcategoria)
    {
        $this->subcategoria->removeElement($subcategoria);
    }

    /**
     * Get subcategoria
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubcategoria()
    {
        return $this->subcategoria;
    }

    /**
     * Set cupon
     *
     * @param \AdminBundle\Entity\Cupon $cupon
     * @return Categoria
     */
    public function setCupon(\AdminBundle\Entity\Cupon $cupon = null)
    {
        $this->cupon = $cupon;

        return $this;
    }

    /**
     * Get cupon
     *
     * @return \AdminBundle\Entity\Cupon 
     */
    public function getCupon()
    {
        return $this->cupon;
    }
}
