<?php

namespace AdminBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Distrito{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=255)
   * @Assert\NotBlank()
   */
  private $codigo;

  /**
   * @ORM\Column(type="string", length=100)
   * @Assert\NotBlank()
   */
  private $nombre;

  /**
   * @ORM\ManyToOne(targetEntity="Provincia", inversedBy="distritos")
   * @ORM\JoinColumn(name="provincia_id", referencedColumnName="id")
   * @Assert\NotBlank()
   */
  private $provincia;

  /**
   * @ORM\OneToMany(targetEntity="Reclamo", mappedBy="distrito")
   */
  protected $reclamos;

  /**
   * @ORM\OneToMany(targetEntity="Pedido", mappedBy="distritoFacturacion")
   */
  protected $pedidosFacturacion;

  /**
   * @ORM\OneToMany(targetEntity="Pedido", mappedBy="distritoEnvio")
   */
  protected $pedidosEnvio;

  /**
   * @ORM\OneToMany(targetEntity="Clientes", mappedBy="distritoCliente")
   */
  protected $clientes;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reclamos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pedidosFacturacion = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pedidosEnvio = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Distrito
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Distrito
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Distrito
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set provincia
     *
     * @param \AdminBundle\Entity\Provincia $provincia
     * @return Distrito
     */
    public function setProvincia(\AdminBundle\Entity\Provincia $provincia = null)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia
     *
     * @return \AdminBundle\Entity\Provincia 
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Add reclamos
     *
     * @param \AdminBundle\Entity\Reclamo $reclamos
     * @return Distrito
     */
    public function addReclamo(\AdminBundle\Entity\Reclamo $reclamos)
    {
        $this->reclamos[] = $reclamos;

        return $this;
    }

    /**
     * Remove reclamos
     *
     * @param \AdminBundle\Entity\Reclamo $reclamos
     */
    public function removeReclamo(\AdminBundle\Entity\Reclamo $reclamos)
    {
        $this->reclamos->removeElement($reclamos);
    }

    /**
     * Get reclamos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReclamos()
    {
        return $this->reclamos;
    }

    /**
     * Add pedidosFacturacion
     *
     * @param \AdminBundle\Entity\Pedido $pedidosFacturacion
     * @return Distrito
     */
    public function addPedidosFacturacion(\AdminBundle\Entity\Pedido $pedidosFacturacion)
    {
        $this->pedidosFacturacion[] = $pedidosFacturacion;

        return $this;
    }

    /**
     * Remove pedidosFacturacion
     *
     * @param \AdminBundle\Entity\Pedido $pedidosFacturacion
     */
    public function removePedidosFacturacion(\AdminBundle\Entity\Pedido $pedidosFacturacion)
    {
        $this->pedidosFacturacion->removeElement($pedidosFacturacion);
    }

    /**
     * Get pedidosFacturacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPedidosFacturacion()
    {
        return $this->pedidosFacturacion;
    }

    /**
     * Add pedidosEnvio
     *
     * @param \AdminBundle\Entity\Pedido $pedidosEnvio
     * @return Distrito
     */
    public function addPedidosEnvio(\AdminBundle\Entity\Pedido $pedidosEnvio)
    {
        $this->pedidosEnvio[] = $pedidosEnvio;

        return $this;
    }

    /**
     * Remove pedidosEnvio
     *
     * @param \AdminBundle\Entity\Pedido $pedidosEnvio
     */
    public function removePedidosEnvio(\AdminBundle\Entity\Pedido $pedidosEnvio)
    {
        $this->pedidosEnvio->removeElement($pedidosEnvio);
    }

    /**
     * Get pedidosEnvio
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPedidosEnvio()
    {
        return $this->pedidosEnvio;
    }

    /**
     * Add clientes
     *
     * @param \AdminBundle\Entity\Clientes $clientes
     * @return Distrito
     */
    public function addCliente(\AdminBundle\Entity\Clientes $clientes)
    {
        $this->clientes[] = $clientes;

        return $this;
    }

    /**
     * Remove clientes
     *
     * @param \AdminBundle\Entity\Clientes $clientes
     */
    public function removeCliente(\AdminBundle\Entity\Clientes $clientes)
    {
        $this->clientes->removeElement($clientes);
    }

    /**
     * Get clientes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientes()
    {
        return $this->clientes;
    }
}
