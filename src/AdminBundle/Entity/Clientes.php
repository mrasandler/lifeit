<?php

namespace AdminBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Clientes{

      /**
       * @var integer
       *
       * @ORM\Column(name="id", type="integer")
       * @ORM\Id
       * @ORM\GeneratedValue(strategy="AUTO")
       */
      private $id;

      /**
       * @ORM\Column(type="string", length=255)
       * @Assert\NotBlank()
       */
      private $nombres;

      /**
       * @ORM\Column(type="string", length=255)
       * @Assert\NotBlank()
       */
      private $email;

      /**
       * @ORM\Column(type="string", length=255)
       * @Assert\NotBlank()
       */
      private $telefono;

      /**
       * @ORM\Column(type="string", length=255)
       * @Assert\NotBlank()
       */
      private $dni;

      /**
       * @ORM\Column(type="string")
       * @Assert\NotBlank()
       */
      private $fechaNacimiento;

      /**
       * @ORM\Column(type="string", length=255)
       * @Assert\NotBlank()
       */
      private $password;

      /**
       * @ORM\Column(type="string", length=255, nullable=true)
       */
      private $direccion;

      /**
       * @ORM\OneToMany(targetEntity="Carrito", mappedBy="cliente")
       */
      protected $carritoCliente;

      /**
       * @ORM\ManyToOne(targetEntity="Distrito", inversedBy="clientes")
       * @ORM\JoinColumn(name="distrito_id", referencedColumnName="id")
       * @Assert\NotBlank()
       */
      private $distritoCliente;

      /**
       * @var \DateTime
       *
       * @ORM\Column(name="fecha", type="datetime")
       */
      private $fecha = null;


       public function getFechaFormateada()
      {
          return $this->fecha->format('d-m-Y');
      }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fecha = new \DateTime();
        $this->carritoCliente = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombres
     *
     * @param string $nombres
     * @return Clientes
     */
    public function setNombres($nombres)
    {
        $this->nombres = $nombres;

        return $this;
    }

    /**
     * Get nombres
     *
     * @return string
     */
    public function getNombres()
    {
        return $this->nombres;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Clientes
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Clientes
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set dni
     *
     * @param string $dni
     * @return Clientes
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set fechaNacimiento
     *
     * @param string $fechaNacimiento
     * @return Clientes
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    /**
     * Get fechaNacimiento
     *
     * @return string
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Clientes
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Clientes
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Add carritoCliente
     *
     * @param \AdminBundle\Entity\Carrito $carritoCliente
     * @return Clientes
     */
    public function addCarritoCliente(\AdminBundle\Entity\Carrito $carritoCliente)
    {
        $this->carritoCliente[] = $carritoCliente;

        return $this;
    }

    /**
     * Remove carritoCliente
     *
     * @param \AdminBundle\Entity\Carrito $carritoCliente
     */
    public function removeCarritoCliente(\AdminBundle\Entity\Carrito $carritoCliente)
    {
        $this->carritoCliente->removeElement($carritoCliente);
    }

    /**
     * Get carritoCliente
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCarritoCliente()
    {
        return $this->carritoCliente;
    }

    /**
     * Set distritoCliente
     *
     * @param \AdminBundle\Entity\Distrito $distritoCliente
     * @return Clientes
     */
    public function setDistritoCliente(\AdminBundle\Entity\Distrito $distritoCliente = null)
    {
        $this->distritoCliente = $distritoCliente;

        return $this;
    }

    /**
     * Get distritoCliente
     *
     * @return \AdminBundle\Entity\Distrito
     */
    public function getDistritoCliente()
    {
        return $this->distritoCliente;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Clientes
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }
}
