<?php

namespace AdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 */
class Presentacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $presentacion;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     */
    private $stock;
    /**
     * @ORM\Column(type="array")
     */
    protected $imagenes;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="presentacion")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\NotBlank()
     */
    private $productoPre;

    /**
     * @ORM\ManyToOne(targetEntity="Talla", inversedBy="presentaciontalla")
     * @ORM\JoinColumn(name="talla_id", referencedColumnName="id")
     *
     */
    private $productoTa;

    /**
     * @ORM\ManyToOne(targetEntity="Color", inversedBy="presentacionColor")
     * @ORM\JoinColumn(name="color_id", referencedColumnName="id")
     *@Assert\NotBlank()
     */
    private $productoCo;

    /**
     * @ORM\OneToMany(targetEntity="Carrito", mappedBy="presentacion")
     */
    protected $carritoPresentacion;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set presentacion
     *
     * @param string $presentacion
     *
     * @return Presentacion
     */
    public function setPresentacion($presentacion)
    {
        $this->presentacion = $presentacion;

        return $this;
    }

    /**
     * Get presentacion
     *
     * @return string
     */
    public function getPresentacion()
    {
        return $this->presentacion;
    }

    /**
     * Set stock
     *
     * @param float $stock
     *
     * @return Presentacion
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return float
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set imagenes
     *
     * @param array $imagenes
     *
     * @return Presentacion
     */
    public function setImagenes($imagenes)
    {
        $this->imagenes = $imagenes;

        return $this;
    }

    /**
     * Get imagenes
     *
     * @return array
     */
    public function getImagenes()
    {
        return $this->imagenes;
    }

    /**
     * Set productoPre
     *
     * @param \AdminBundle\Entity\Producto $productoPre
     *
     * @return Presentacion
     */
    public function setProductoPre(\AdminBundle\Entity\Producto $productoPre = null)
    {
        $this->productoPre = $productoPre;

        return $this;
    }

    /**
     * Get productoPre
     *
     * @return \AdminBundle\Entity\Producto
     */
    public function getProductoPre()
    {
        return $this->productoPre;
    }

    /**
     * Set productoTa
     *
     * @param \AdminBundle\Entity\Talla $productoTa
     *
     * @return Presentacion
     */
    public function setProductoTa(\AdminBundle\Entity\Talla $productoTa = null)
    {
        $this->productoTa = $productoTa;

        return $this;
    }

    /**
     * Get productoTa
     *
     * @return \AdminBundle\Entity\Talla
     */
    public function getProductoTa()
    {
        return $this->productoTa;
    }

    /**
     * Set productoCo
     *
     * @param \AdminBundle\Entity\Color $productoCo
     *
     * @return Presentacion
     */
    public function setProductoCo(\AdminBundle\Entity\Color $productoCo = null)
    {
        $this->productoCo = $productoCo;

        return $this;
    }

    /**
     * Get productoCo
     *
     * @return \AdminBundle\Entity\Color
     */
    public function getProductoCo()
    {
        return $this->productoCo;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->carritoPresentacion = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add carritoPresentacion
     *
     * @param \AdminBundle\Entity\Carrito $carritoPresentacion
     *
     * @return Presentacion
     */
    public function addCarritoPresentacion(\AdminBundle\Entity\Carrito $carritoPresentacion)
    {
        $this->carritoPresentacion[] = $carritoPresentacion;

        return $this;
    }

    /**
     * Remove carritoPresentacion
     *
     * @param \AdminBundle\Entity\Carrito $carritoPresentacion
     */
    public function removeCarritoPresentacion(\AdminBundle\Entity\Carrito $carritoPresentacion)
    {
        $this->carritoPresentacion->removeElement($carritoPresentacion);
    }

    /**
     * Get carritoPresentacion
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCarritoPresentacion()
    {
        return $this->carritoPresentacion;
    }
}
