<?php

namespace AdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 */
class Home
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $imagenVideo;

     /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $tituloVideo;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $subtituloVideo;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $contenidoVideo;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $textoBoton;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $urlBoton;

    /**
     *
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $tituloSeccionCompra;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $mensajeSeccionCompra;

    /**
     * @ORM\Column(type="array")
     */
    protected $procesosCompra;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $mensajeProcesoCompra;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $textoBotonSeccionCompra;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $urlBotonSeccionCompra;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $catalogoTitulo;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $catalogoContenido;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $catalogoImagen;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $catalogoArchivo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $catalogoBoton;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imagenSuscribete;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $activarSuscripcion;


    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $activarCanal;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imagenVideo
     *
     * @param string $imagenVideo
     * @return Home
     */
    public function setImagenVideo($imagenVideo)
    {
        $this->imagenVideo = $imagenVideo;

        return $this;
    }

    /**
     * Get imagenVideo
     *
     * @return string
     */
    public function getImagenVideo()
    {
        return $this->imagenVideo;
    }

    /**
     * Set tituloVideo
     *
     * @param string $tituloVideo
     * @return Home
     */
    public function setTituloVideo($tituloVideo)
    {
        $this->tituloVideo = $tituloVideo;

        return $this;
    }

    /**
     * Get tituloVideo
     *
     * @return string
     */
    public function getTituloVideo()
    {
        return $this->tituloVideo;
    }

    /**
     * Set subtituloVideo
     *
     * @param string $subtituloVideo
     * @return Home
     */
    public function setSubtituloVideo($subtituloVideo)
    {
        $this->subtituloVideo = $subtituloVideo;

        return $this;
    }

    /**
     * Get subtituloVideo
     *
     * @return string
     */
    public function getSubtituloVideo()
    {
        return $this->subtituloVideo;
    }

    /**
     * Set contenidoVideo
     *
     * @param string $contenidoVideo
     * @return Home
     */
    public function setContenidoVideo($contenidoVideo)
    {
        $this->contenidoVideo = $contenidoVideo;

        return $this;
    }

    /**
     * Get contenidoVideo
     *
     * @return string
     */
    public function getContenidoVideo()
    {
        return $this->contenidoVideo;
    }

    /**
     * Set textoBoton
     *
     * @param string $textoBoton
     * @return Home
     */
    public function setTextoBoton($textoBoton)
    {
        $this->textoBoton = $textoBoton;

        return $this;
    }

    /**
     * Get textoBoton
     *
     * @return string
     */
    public function getTextoBoton()
    {
        return $this->textoBoton;
    }

    /**
     * Set urlBoton
     *
     * @param string $urlBoton
     * @return Home
     */
    public function setUrlBoton($urlBoton)
    {
        $this->urlBoton = $urlBoton;

        return $this;
    }

    /**
     * Get urlBoton
     *
     * @return string
     */
    public function getUrlBoton()
    {
        return $this->urlBoton;
    }

    /**
     * Set tituloSeccionCompra
     *
     * @param string $tituloSeccionCompra
     * @return Home
     */
    public function setTituloSeccionCompra($tituloSeccionCompra)
    {
        $this->tituloSeccionCompra = $tituloSeccionCompra;

        return $this;
    }

    /**
     * Get tituloSeccionCompra
     *
     * @return string
     */
    public function getTituloSeccionCompra()
    {
        return $this->tituloSeccionCompra;
    }

    /**
     * Set mensajeSeccionCompra
     *
     * @param string $mensajeSeccionCompra
     * @return Home
     */
    public function setMensajeSeccionCompra($mensajeSeccionCompra)
    {
        $this->mensajeSeccionCompra = $mensajeSeccionCompra;

        return $this;
    }

    /**
     * Get mensajeSeccionCompra
     *
     * @return string
     */
    public function getMensajeSeccionCompra()
    {
        return $this->mensajeSeccionCompra;
    }

    /**
     * Set procesosCompra
     *
     * @param array $procesosCompra
     * @return Home
     */
    public function setProcesosCompra($procesosCompra)
    {
        $this->procesosCompra = $procesosCompra;

        return $this;
    }

    /**
     * Get procesosCompra
     *
     * @return array
     */
    public function getProcesosCompra()
    {
        return $this->procesosCompra;
    }

    /**
     * Set mensajeProcesoCompra
     *
     * @param string $mensajeProcesoCompra
     * @return Home
     */
    public function setMensajeProcesoCompra($mensajeProcesoCompra)
    {
        $this->mensajeProcesoCompra = $mensajeProcesoCompra;

        return $this;
    }

    /**
     * Get mensajeProcesoCompra
     *
     * @return string
     */
    public function getMensajeProcesoCompra()
    {
        return $this->mensajeProcesoCompra;
    }

    /**
     * Set textoBotonSeccionCompra
     *
     * @param string $textoBotonSeccionCompra
     * @return Home
     */
    public function setTextoBotonSeccionCompra($textoBotonSeccionCompra)
    {
        $this->textoBotonSeccionCompra = $textoBotonSeccionCompra;

        return $this;
    }

    /**
     * Get textoBotonSeccionCompra
     *
     * @return string
     */
    public function getTextoBotonSeccionCompra()
    {
        return $this->textoBotonSeccionCompra;
    }

    /**
     * Set urlBotonSeccionCompra
     *
     * @param string $urlBotonSeccionCompra
     * @return Home
     */
    public function setUrlBotonSeccionCompra($urlBotonSeccionCompra)
    {
        $this->urlBotonSeccionCompra = $urlBotonSeccionCompra;

        return $this;
    }

    /**
     * Get urlBotonSeccionCompra
     *
     * @return string
     */
    public function getUrlBotonSeccionCompra()
    {
        return $this->urlBotonSeccionCompra;
    }

    /**
     * Set catalogoTitulo
     *
     * @param string $catalogoTitulo
     * @return Home
     */
    public function setCatalogoTitulo($catalogoTitulo)
    {
        $this->catalogoTitulo = $catalogoTitulo;

        return $this;
    }

    /**
     * Get catalogoTitulo
     *
     * @return string
     */
    public function getCatalogoTitulo()
    {
        return $this->catalogoTitulo;
    }

    /**
     * Set catalogoContenido
     *
     * @param string $catalogoContenido
     * @return Home
     */
    public function setCatalogoContenido($catalogoContenido)
    {
        $this->catalogoContenido = $catalogoContenido;

        return $this;
    }

    /**
     * Get catalogoContenido
     *
     * @return string
     */
    public function getCatalogoContenido()
    {
        return $this->catalogoContenido;
    }

    /**
     * Set catalogoImagen
     *
     * @param string $catalogoImagen
     * @return Home
     */
    public function setCatalogoImagen($catalogoImagen)
    {
        $this->catalogoImagen = $catalogoImagen;

        return $this;
    }

    /**
     * Get catalogoImagen
     *
     * @return string
     */
    public function getCatalogoImagen()
    {
        return $this->catalogoImagen;
    }

    /**
     * Set catalogoArchivo
     *
     * @param string $catalogoArchivo
     * @return Home
     */
    public function setCatalogoArchivo($catalogoArchivo)
    {
        $this->catalogoArchivo = $catalogoArchivo;

        return $this;
    }

    /**
     * Get catalogoArchivo
     *
     * @return string
     */
    public function getCatalogoArchivo()
    {
        return $this->catalogoArchivo;
    }

    /**
     * Set catalogoBoton
     *
     * @param string $catalogoBoton
     * @return Home
     */
    public function setCatalogoBoton($catalogoBoton)
    {
        $this->catalogoBoton = $catalogoBoton;

        return $this;
    }

    /**
     * Get catalogoBoton
     *
     * @return string
     */
    public function getCatalogoBoton()
    {
        return $this->catalogoBoton;
    }

    /**
     * Set imagenSuscribete
     *
     * @param string $imagenSuscribete
     *
     * @return Home
     */
    public function setImagenSuscribete($imagenSuscribete)
    {
        $this->imagenSuscribete = $imagenSuscribete;

        return $this;
    }

    /**
     * Get imagenSuscribete
     *
     * @return string
     */
    public function getImagenSuscribete()
    {
        return $this->imagenSuscribete;
    }

    /**
     * Set activarSuscripcion
     *
     * @param boolean $activarSuscripcion
     *
     * @return Home
     */
    public function setActivarSuscripcion($activarSuscripcion)
    {
        $this->activarSuscripcion = $activarSuscripcion;

        return $this;
    }

    /**
     * Get activarSuscripcion
     *
     * @return boolean
     */
    public function getActivarSuscripcion()
    {
        return $this->activarSuscripcion;
    }

    /**
     * Set activarCanal
     *
     * @param boolean $activarCanal
     *
     * @return Home
     */
    public function setActivarCanal($activarCanal)
    {
        $this->activarCanal = $activarCanal;

        return $this;
    }

    /**
     * Get activarCanal
     *
     * @return boolean
     */
    public function getActivarCanal()
    {
        return $this->activarCanal;
    }
}
