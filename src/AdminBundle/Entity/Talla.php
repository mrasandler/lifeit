<?php

namespace AdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 */
class Talla
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank()
     */
    private $talla;
    /**
     * @ORM\Column(type="string", length=20)
     *
     */
    private $descripcion;
    /**
     * @ORM\OneToMany(targetEntity="Presentacion", mappedBy="productoTa")
     *
     *
     */
    private $presentaciontalla;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->presentaciontalla = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set talla
     *
     * @param string $talla
     *
     * @return Talla
     */
    public function setTalla($talla)
    {
        $this->talla = $talla;

        return $this;
    }

    /**
     * Get talla
     *
     * @return string
     */
    public function getTalla()
    {
        return $this->talla;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Talla
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Add presentaciontalla
     *
     * @param \AdminBundle\Entity\Presentacion $presentaciontalla
     *
     * @return Talla
     */
    public function addPresentaciontalla(\AdminBundle\Entity\Presentacion $presentaciontalla)
    {
        $this->presentaciontalla[] = $presentaciontalla;

        return $this;
    }

    /**
     * Remove presentaciontalla
     *
     * @param \AdminBundle\Entity\Presentacion $presentaciontalla
     */
    public function removePresentaciontalla(\AdminBundle\Entity\Presentacion $presentaciontalla)
    {
        $this->presentaciontalla->removeElement($presentaciontalla);
    }

    /**
     * Get presentaciontalla
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPresentaciontalla()
    {
        return $this->presentaciontalla;
    }
}
