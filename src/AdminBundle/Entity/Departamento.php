<?php

namespace AdminBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Departamento{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=255)
   * @Assert\NotBlank()
   */
  private $codigo;

  /**
   * @ORM\Column(type="string", length=100)
   * @Assert\NotBlank()
   */
  private $nombre;

  /**
   * @ORM\OneToMany(targetEntity="Provincia", mappedBy="departamento")
   */
  protected $provincias;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->provincias = new \Doctrine\Common\Collections\ArrayCollection();
  }

    /**
     * Set id
     *
     * @param integer $id
     * @return Departamento
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Departamento
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Departamento
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add provincias
     *
     * @param \AdminBundle\Entity\Provincia $provincias
     * @return Departamento
     */
    public function addProvincia(\AdminBundle\Entity\Provincia $provincias)
    {
        $this->provincias[] = $provincias;

        return $this;
    }

    /**
     * Remove provincias
     *
     * @param \AdminBundle\Entity\Provincia $provincias
     */
    public function removeProvincia(\AdminBundle\Entity\Provincia $provincias)
    {
        $this->provincias->removeElement($provincias);
    }

    /**
     * Get provincias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProvincias()
    {
        return $this->provincias;
    }
}
