<?php

namespace AdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 */
class Info
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $correosContacto;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $correopaypal;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $titulo;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $ga;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $contenidoTerminos;
    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $contenidoTerminosDelivery;
    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $contenidoPrivacidad;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $mensajePaginaLocales;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $imagenPaginaLocales;
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $imagenLogoPrincipal;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $LogoSegundario;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $imagenProyecto;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $activarProyecto;

    /**
     * @ORM\Column(type="string",nullable=true, length=255)
     *
     */
    private $pdfDocumento;
    /**
     * @ORM\Column(type="array")
     */
    protected $valoresAgregadosPaginaLocales;

     /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $imagenFondoConocenos;

     /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $tituloFondoConocenos;

     /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $subtituloFondoConocenos;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $mensajePaginaLibroReclamaciones;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $imagenPaginaLibroReclamaciones;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $mensajePaginaBolsaTrabajo;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $imagenPaginaBolsaTrabajo;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $televentasLima;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $televentasPeru;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $horarioAtencion;

    /**
    * @ORM\Column(type="string", length=100)
    * @Assert\NotBlank()
    */
    private $latitud;

    /**
    * @ORM\Column(type="string", length=100)
    * @Assert\NotBlank()
    */
    private $longitud;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $urlWsFacebook;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $appTokenFacebook;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $numeroPublicacionesFacebook;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $fanPageFacebook;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     */
    private $precioMasBajo;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     */
    private $precioMasAlto;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $facebook;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $youtube;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $googleplus;

     /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $bannerPromocion;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     */
    private $precioEnvioLima;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     */
    private $precioEnvioProvincias;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $tiempoEnvioLima;

     /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $tiempoEnvioProvincias;

     /**
     * @ORM\Column(type="string")
     */
    private $valordeCambio;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set correosContacto
     *
     * @param string $correosContacto
     * @return Info
     */
    public function setCorreosContacto($correosContacto)
    {
        $this->correosContacto = $correosContacto;

        return $this;
    }

    /**
     * Get correosContacto
     *
     * @return string
     */
    public function getCorreosContacto()
    {
        return $this->correosContacto;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Info
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Info
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set ga
     *
     * @param string $ga
     * @return Info
     */
    public function setGa($ga)
    {
        $this->ga = $ga;

        return $this;
    }

    /**
     * Get ga
     *
     * @return string
     */
    public function getGa()
    {
        return $this->ga;
    }

    /**
     * Set contenidoTerminos
     *
     * @param string $contenidoTerminos
     * @return Info
     */
    public function setContenidoTerminos($contenidoTerminos)
    {
        $this->contenidoTerminos = $contenidoTerminos;

        return $this;
    }

    /**
     * Get contenidoTerminos
     *
     * @return string
     */
    public function getContenidoTerminos()
    {
        return $this->contenidoTerminos;
    }

    /**
     * Set contenidoPrivacidad
     *
     * @param string $contenidoPrivacidad
     * @return Info
     */
    public function setContenidoPrivacidad($contenidoPrivacidad)
    {
        $this->contenidoPrivacidad = $contenidoPrivacidad;

        return $this;
    }

    /**
     * Get contenidoPrivacidad
     *
     * @return string
     */
    public function getContenidoPrivacidad()
    {
        return $this->contenidoPrivacidad;
    }

    /**
     * Set mensajePaginaLocales
     *
     * @param string $mensajePaginaLocales
     * @return Info
     */
    public function setMensajePaginaLocales($mensajePaginaLocales)
    {
        $this->mensajePaginaLocales = $mensajePaginaLocales;

        return $this;
    }

    /**
     * Get mensajePaginaLocales
     *
     * @return string
     */
    public function getMensajePaginaLocales()
    {
        return $this->mensajePaginaLocales;
    }

    /**
     * Set imagenPaginaLocales
     *
     * @param string $imagenPaginaLocales
     * @return Info
     */
    public function setImagenPaginaLocales($imagenPaginaLocales)
    {
        $this->imagenPaginaLocales = $imagenPaginaLocales;

        return $this;
    }

    /**
     * Get imagenPaginaLocales
     *
     * @return string
     */
    public function getImagenPaginaLocales()
    {
        return $this->imagenPaginaLocales;
    }

    /**
     * Set valoresAgregadosPaginaLocales
     *
     * @param array $valoresAgregadosPaginaLocales
     * @return Info
     */
    public function setValoresAgregadosPaginaLocales($valoresAgregadosPaginaLocales)
    {
        $this->valoresAgregadosPaginaLocales = $valoresAgregadosPaginaLocales;

        return $this;
    }

    /**
     * Get valoresAgregadosPaginaLocales
     *
     * @return array
     */
    public function getValoresAgregadosPaginaLocales()
    {
        return $this->valoresAgregadosPaginaLocales;
    }

    /**
     * Set imagenFondoConocenos
     *
     * @param string $imagenFondoConocenos
     * @return Info
     */
    public function setImagenFondoConocenos($imagenFondoConocenos)
    {
        $this->imagenFondoConocenos = $imagenFondoConocenos;

        return $this;
    }

    /**
     * Get imagenFondoConocenos
     *
     * @return string
     */
    public function getImagenFondoConocenos()
    {
        return $this->imagenFondoConocenos;
    }

    /**
     * Set tituloFondoConocenos
     *
     * @param string $tituloFondoConocenos
     * @return Info
     */
    public function setTituloFondoConocenos($tituloFondoConocenos)
    {
        $this->tituloFondoConocenos = $tituloFondoConocenos;

        return $this;
    }

    /**
     * Get tituloFondoConocenos
     *
     * @return string
     */
    public function getTituloFondoConocenos()
    {
        return $this->tituloFondoConocenos;
    }

    /**
     * Set subtituloFondoConocenos
     *
     * @param string $subtituloFondoConocenos
     * @return Info
     */
    public function setSubtituloFondoConocenos($subtituloFondoConocenos)
    {
        $this->subtituloFondoConocenos = $subtituloFondoConocenos;

        return $this;
    }

    /**
     * Get subtituloFondoConocenos
     *
     * @return string
     */
    public function getSubtituloFondoConocenos()
    {
        return $this->subtituloFondoConocenos;
    }

    /**
     * Set mensajePaginaLibroReclamaciones
     *
     * @param string $mensajePaginaLibroReclamaciones
     * @return Info
     */
    public function setMensajePaginaLibroReclamaciones($mensajePaginaLibroReclamaciones)
    {
        $this->mensajePaginaLibroReclamaciones = $mensajePaginaLibroReclamaciones;

        return $this;
    }

    /**
     * Get mensajePaginaLibroReclamaciones
     *
     * @return string
     */
    public function getMensajePaginaLibroReclamaciones()
    {
        return $this->mensajePaginaLibroReclamaciones;
    }

    /**
     * Set imagenPaginaLibroReclamaciones
     *
     * @param string $imagenPaginaLibroReclamaciones
     * @return Info
     */
    public function setImagenPaginaLibroReclamaciones($imagenPaginaLibroReclamaciones)
    {
        $this->imagenPaginaLibroReclamaciones = $imagenPaginaLibroReclamaciones;

        return $this;
    }

    /**
     * Get imagenPaginaLibroReclamaciones
     *
     * @return string
     */
    public function getImagenPaginaLibroReclamaciones()
    {
        return $this->imagenPaginaLibroReclamaciones;
    }

    /**
     * Set mensajePaginaBolsaTrabajo
     *
     * @param string $mensajePaginaBolsaTrabajo
     * @return Info
     */
    public function setMensajePaginaBolsaTrabajo($mensajePaginaBolsaTrabajo)
    {
        $this->mensajePaginaBolsaTrabajo = $mensajePaginaBolsaTrabajo;

        return $this;
    }

    /**
     * Get mensajePaginaBolsaTrabajo
     *
     * @return string
     */
    public function getMensajePaginaBolsaTrabajo()
    {
        return $this->mensajePaginaBolsaTrabajo;
    }

    /**
     * Set imagenPaginaBolsaTrabajo
     *
     * @param string $imagenPaginaBolsaTrabajo
     * @return Info
     */
    public function setImagenPaginaBolsaTrabajo($imagenPaginaBolsaTrabajo)
    {
        $this->imagenPaginaBolsaTrabajo = $imagenPaginaBolsaTrabajo;

        return $this;
    }

    /**
     * Get imagenPaginaBolsaTrabajo
     *
     * @return string
     */
    public function getImagenPaginaBolsaTrabajo()
    {
        return $this->imagenPaginaBolsaTrabajo;
    }

    /**
     * Set televentasLima
     *
     * @param string $televentasLima
     * @return Info
     */
    public function setTeleventasLima($televentasLima)
    {
        $this->televentasLima = $televentasLima;

        return $this;
    }

    /**
     * Get televentasLima
     *
     * @return string
     */
    public function getTeleventasLima()
    {
        return $this->televentasLima;
    }

    /**
     * Set televentasPeru
     *
     * @param string $televentasPeru
     * @return Info
     */
    public function setTeleventasPeru($televentasPeru)
    {
        $this->televentasPeru = $televentasPeru;

        return $this;
    }

    /**
     * Get televentasPeru
     *
     * @return string
     */
    public function getTeleventasPeru()
    {
        return $this->televentasPeru;
    }

    /**
     * Set horarioAtencion
     *
     * @param string $horarioAtencion
     * @return Info
     */
    public function setHorarioAtencion($horarioAtencion)
    {
        $this->horarioAtencion = $horarioAtencion;

        return $this;
    }

    /**
     * Get horarioAtencion
     *
     * @return string
     */
    public function getHorarioAtencion()
    {
        return $this->horarioAtencion;
    }

    /**
     * Set latitud
     *
     * @param string $latitud
     * @return Info
     */
    public function setLatitud($latitud)
    {
        $this->latitud = $latitud;

        return $this;
    }

    /**
     * Get latitud
     *
     * @return string
     */
    public function getLatitud()
    {
        return $this->latitud;
    }

    /**
     * Set longitud
     *
     * @param string $longitud
     * @return Info
     */
    public function setLongitud($longitud)
    {
        $this->longitud = $longitud;

        return $this;
    }

    /**
     * Get longitud
     *
     * @return string
     */
    public function getLongitud()
    {
        return $this->longitud;
    }

    /**
     * Set urlWsFacebook
     *
     * @param string $urlWsFacebook
     * @return Info
     */
    public function setUrlWsFacebook($urlWsFacebook)
    {
        $this->urlWsFacebook = $urlWsFacebook;

        return $this;
    }

    /**
     * Get urlWsFacebook
     *
     * @return string
     */
    public function getUrlWsFacebook()
    {
        return $this->urlWsFacebook;
    }

    /**
     * Set appTokenFacebook
     *
     * @param string $appTokenFacebook
     * @return Info
     */
    public function setAppTokenFacebook($appTokenFacebook)
    {
        $this->appTokenFacebook = $appTokenFacebook;

        return $this;
    }

    /**
     * Get appTokenFacebook
     *
     * @return string
     */
    public function getAppTokenFacebook()
    {
        return $this->appTokenFacebook;
    }

    /**
     * Set numeroPublicacionesFacebook
     *
     * @param integer $numeroPublicacionesFacebook
     * @return Info
     */
    public function setNumeroPublicacionesFacebook($numeroPublicacionesFacebook)
    {
        $this->numeroPublicacionesFacebook = $numeroPublicacionesFacebook;

        return $this;
    }

    /**
     * Get numeroPublicacionesFacebook
     *
     * @return integer
     */
    public function getNumeroPublicacionesFacebook()
    {
        return $this->numeroPublicacionesFacebook;
    }

    /**
     * Set fanPageFacebook
     *
     * @param string $fanPageFacebook
     * @return Info
     */
    public function setFanPageFacebook($fanPageFacebook)
    {
        $this->fanPageFacebook = $fanPageFacebook;

        return $this;
    }

    /**
     * Get fanPageFacebook
     *
     * @return string
     */
    public function getFanPageFacebook()
    {
        return $this->fanPageFacebook;
    }

    /**
     * Set precioMasBajo
     *
     * @param float $precioMasBajo
     * @return Info
     */
    public function setPrecioMasBajo($precioMasBajo)
    {
        $this->precioMasBajo = $precioMasBajo;

        return $this;
    }

    /**
     * Get precioMasBajo
     *
     * @return float
     */
    public function getPrecioMasBajo()
    {
        return $this->precioMasBajo;
    }

    /**
     * Set precioMasAlto
     *
     * @param float $precioMasAlto
     * @return Info
     */
    public function setPrecioMasAlto($precioMasAlto)
    {
        $this->precioMasAlto = $precioMasAlto;

        return $this;
    }

    /**
     * Get precioMasAlto
     *
     * @return float
     */
    public function getPrecioMasAlto()
    {
        return $this->precioMasAlto;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     * @return Info
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set youtube
     *
     * @param string $youtube
     * @return Info
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;

        return $this;
    }

    /**
     * Get youtube
     *
     * @return string
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * Set googleplus
     *
     * @param string $googleplus
     * @return Info
     */
    public function setGoogleplus($googleplus)
    {
        $this->googleplus = $googleplus;

        return $this;
    }

    /**
     * Get googleplus
     *
     * @return string
     */
    public function getGoogleplus()
    {
        return $this->googleplus;
    }

    /**
     * Set bannerPromocion
     *
     * @param string $bannerPromocion
     * @return Info
     */
    public function setBannerPromocion($bannerPromocion)
    {
        $this->bannerPromocion = $bannerPromocion;

        return $this;
    }

    /**
     * Get bannerPromocion
     *
     * @return string
     */
    public function getBannerPromocion()
    {
        return $this->bannerPromocion;
    }

    /**
     * Set precioEnvioLima
     *
     * @param float $precioEnvioLima
     * @return Info
     */
    public function setPrecioEnvioLima($precioEnvioLima)
    {
        $this->precioEnvioLima = $precioEnvioLima;

        return $this;
    }

    /**
     * Get precioEnvioLima
     *
     * @return float
     */
    public function getPrecioEnvioLima()
    {
        return $this->precioEnvioLima;
    }

    /**
     * Set precioEnvioProvincias
     *
     * @param float $precioEnvioProvincias
     * @return Info
     */
    public function setPrecioEnvioProvincias($precioEnvioProvincias)
    {
        $this->precioEnvioProvincias = $precioEnvioProvincias;

        return $this;
    }

    /**
     * Get precioEnvioProvincias
     *
     * @return float
     */
    public function getPrecioEnvioProvincias()
    {
        return $this->precioEnvioProvincias;
    }

    /**
     * Set tiempoEnvioLima
     *
     * @param string $tiempoEnvioLima
     * @return Info
     */
    public function setTiempoEnvioLima($tiempoEnvioLima)
    {
        $this->tiempoEnvioLima = $tiempoEnvioLima;

        return $this;
    }

    /**
     * Get tiempoEnvioLima
     *
     * @return string
     */
    public function getTiempoEnvioLima()
    {
        return $this->tiempoEnvioLima;
    }

    /**
     * Set tiempoEnvioProvincias
     *
     * @param string $tiempoEnvioProvincias
     * @return Info
     */
    public function setTiempoEnvioProvincias($tiempoEnvioProvincias)
    {
        $this->tiempoEnvioProvincias = $tiempoEnvioProvincias;

        return $this;
    }

    /**
     * Get tiempoEnvioProvincias
     *
     * @return string
     */
    public function getTiempoEnvioProvincias()
    {
        return $this->tiempoEnvioProvincias;
    }

    /**
     * Set valordeCambio
     *
     * @param string $valordeCambio
     * @return Info
     */
    public function setValordeCambio($valordeCambio)
    {
        $this->valordeCambio = $valordeCambio;

        return $this;
    }

    /**
     * Get valordeCambio
     *
     * @return string
     */
    public function getValordeCambio()
    {
        return $this->valordeCambio;
    }

    /**
     * Set contenidoTerminosDelivery
     *
     * @param string $contenidoTerminosDelivery
     *
     * @return Info
     */
    public function setContenidoTerminosDelivery($contenidoTerminosDelivery)
    {
        $this->contenidoTerminosDelivery = $contenidoTerminosDelivery;

        return $this;
    }

    /**
     * Get contenidoTerminosDelivery
     *
     * @return string
     */
    public function getContenidoTerminosDelivery()
    {
        return $this->contenidoTerminosDelivery;
    }

    /**
     * Set imagenLogoPrincipal
     *
     * @param string $imagenLogoPrincipal
     *
     * @return Info
     */
    public function setImagenLogoPrincipal($imagenLogoPrincipal)
    {
        $this->imagenLogoPrincipal = $imagenLogoPrincipal;

        return $this;
    }

    /**
     * Get imagenLogoPrincipal
     *
     * @return string
     */
    public function getImagenLogoPrincipal()
    {
        return $this->imagenLogoPrincipal;
    }

    /**
     * Set pdfDocumento
     *
     * @param string $pdfDocumento
     *
     * @return Info
     */
    public function setPdfDocumento($pdfDocumento)
    {
        $this->pdfDocumento = $pdfDocumento;

        return $this;
    }

    /**
     * Get pdfDocumento
     *
     * @return string
     */
    public function getPdfDocumento()
    {
        return $this->pdfDocumento;
    }

    /**
     * Set imagenProyecto
     *
     * @param string $imagenProyecto
     *
     * @return Info
     */
    public function setImagenProyecto($imagenProyecto)
    {
        $this->imagenProyecto = $imagenProyecto;

        return $this;
    }

    /**
     * Get imagenProyecto
     *
     * @return string
     */
    public function getImagenProyecto()
    {
        return $this->imagenProyecto;
    }

    /**
     * Set logoSegundario
     *
     * @param string $logoSegundario
     *
     * @return Info
     */
    public function setLogoSegundario($logoSegundario)
    {
        $this->LogoSegundario = $logoSegundario;

        return $this;
    }

    /**
     * Get logoSegundario
     *
     * @return string
     */
    public function getLogoSegundario()
    {
        return $this->LogoSegundario;
    }

    /**
     * Set correopaypal
     *
     * @param string $correopaypal
     *
     * @return Info
     */
    public function setCorreopaypal($correopaypal)
    {
        $this->correopaypal = $correopaypal;

        return $this;
    }

    /**
     * Get correopaypal
     *
     * @return string
     */
    public function getCorreopaypal()
    {
        return $this->correopaypal;
    }

    /**
     * Set activarProyecto
     *
     * @param boolean $activarProyecto
     *
     * @return Info
     */
    public function setActivarProyecto($activarProyecto)
    {
        $this->activarProyecto = $activarProyecto;

        return $this;
    }

    /**
     * Get activarProyecto
     *
     * @return boolean
     */
    public function getActivarProyecto()
    {
        return $this->activarProyecto;
    }
}
