<?php

namespace AdminBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Provincia{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=255)
   * @Assert\NotBlank()
   */
  private $codigo;

  /**
   * @ORM\Column(type="string", length=100)
   * @Assert\NotBlank()
   */
  private $nombre;

  /**
   * @ORM\ManyToOne(targetEntity="Departamento", inversedBy="provincias")
   * @ORM\JoinColumn(name="departamento_id", referencedColumnName="id")
   * @Assert\NotBlank()
   */
  private $departamento;

  /**
   * @ORM\OneToMany(targetEntity="Distrito", mappedBy="provincia")
   */
  protected $distritos;

  /**
   * @ORM\OneToMany(targetEntity="Contacto", mappedBy="provincia")
   */
  protected $contactos;

  /**
   * @ORM\OneToMany(targetEntity="BolsaTrabajo", mappedBy="provincia")
   */
  protected $bolsasTrabajo;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->distritos = new \Doctrine\Common\Collections\ArrayCollection();
    $this->contactos = new \Doctrine\Common\Collections\ArrayCollection();
    $this->bolsasTrabajo = new \Doctrine\Common\Collections\ArrayCollection();
  }

    /**
     * Set id
     *
     * @param integer $id
     * @return Provincia
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Provincia
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Provincia
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set departamento
     *
     * @param \AdminBundle\Entity\Departamento $departamento
     * @return Provincia
     */
    public function setDepartamento(\AdminBundle\Entity\Departamento $departamento = null)
    {
        $this->departamento = $departamento;

        return $this;
    }

    /**
     * Get departamento
     *
     * @return \AdminBundle\Entity\Departamento 
     */
    public function getDepartamento()
    {
        return $this->departamento;
    }

    /**
     * Add distritos
     *
     * @param \AdminBundle\Entity\Distrito $distritos
     * @return Provincia
     */
    public function addDistrito(\AdminBundle\Entity\Distrito $distritos)
    {
        $this->distritos[] = $distritos;

        return $this;
    }

    /**
     * Remove distritos
     *
     * @param \AdminBundle\Entity\Distrito $distritos
     */
    public function removeDistrito(\AdminBundle\Entity\Distrito $distritos)
    {
        $this->distritos->removeElement($distritos);
    }

    /**
     * Get distritos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDistritos()
    {
        return $this->distritos;
    }

    /**
     * Add contactos
     *
     * @param \AdminBundle\Entity\Contacto $contactos
     * @return Provincia
     */
    public function addContacto(\AdminBundle\Entity\Contacto $contactos)
    {
        $this->contactos[] = $contactos;

        return $this;
    }

    /**
     * Remove contactos
     *
     * @param \AdminBundle\Entity\Contacto $contactos
     */
    public function removeContacto(\AdminBundle\Entity\Contacto $contactos)
    {
        $this->contactos->removeElement($contactos);
    }

    /**
     * Get contactos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContactos()
    {
        return $this->contactos;
    }

    /**
     * Add bolsasTrabajo
     *
     * @param \AdminBundle\Entity\BolsaTrabajo $bolsasTrabajo
     * @return Provincia
     */
    public function addBolsasTrabajo(\AdminBundle\Entity\BolsaTrabajo $bolsasTrabajo)
    {
        $this->bolsasTrabajo[] = $bolsasTrabajo;

        return $this;
    }

    /**
     * Remove bolsasTrabajo
     *
     * @param \AdminBundle\Entity\BolsaTrabajo $bolsasTrabajo
     */
    public function removeBolsasTrabajo(\AdminBundle\Entity\BolsaTrabajo $bolsasTrabajo)
    {
        $this->bolsasTrabajo->removeElement($bolsasTrabajo);
    }

    /**
     * Get bolsasTrabajo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBolsasTrabajo()
    {
        return $this->bolsasTrabajo;
    }
}
