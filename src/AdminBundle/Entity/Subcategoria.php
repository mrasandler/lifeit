<?php

namespace AdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 */
class Subcategoria
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nombreSub;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $descripcionSub;

    /**
     * @ORM\ManyToOne(targetEntity="Categoria", inversedBy="subcategoria")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $categoria;

    /**
     * @Gedmo\Slug(fields={"nombreSub"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="Producto", mappedBy="subcategoriaProd")
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="Cupon", inversedBy="subcategorias")
     * @ORM\JoinColumn(name="cupon_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $cupon;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->producto = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreSub
     *
     * @param string $nombreSub
     * @return Subcategoria
     */
    public function setNombreSub($nombreSub)
    {
        $this->nombreSub = $nombreSub;

        return $this;
    }

    /**
     * Get nombreSub
     *
     * @return string 
     */
    public function getNombreSub()
    {
        return $this->nombreSub;
    }

    /**
     * Set descripcionSub
     *
     * @param string $descripcionSub
     * @return Subcategoria
     */
    public function setDescripcionSub($descripcionSub)
    {
        $this->descripcionSub = $descripcionSub;

        return $this;
    }

    /**
     * Get descripcionSub
     *
     * @return string 
     */
    public function getDescripcionSub()
    {
        return $this->descripcionSub;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Subcategoria
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set categoria
     *
     * @param \AdminBundle\Entity\Categoria $categoria
     * @return Subcategoria
     */
    public function setCategoria(\AdminBundle\Entity\Categoria $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \AdminBundle\Entity\Categoria 
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Add producto
     *
     * @param \AdminBundle\Entity\Producto $producto
     * @return Subcategoria
     */
    public function addProducto(\AdminBundle\Entity\Producto $producto)
    {
        $this->producto[] = $producto;

        return $this;
    }

    /**
     * Remove producto
     *
     * @param \AdminBundle\Entity\Producto $producto
     */
    public function removeProducto(\AdminBundle\Entity\Producto $producto)
    {
        $this->producto->removeElement($producto);
    }

    /**
     * Get producto
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set cupon
     *
     * @param \AdminBundle\Entity\Cupon $cupon
     * @return Subcategoria
     */
    public function setCupon(\AdminBundle\Entity\Cupon $cupon = null)
    {
        $this->cupon = $cupon;

        return $this;
    }

    /**
     * Get cupon
     *
     * @return \AdminBundle\Entity\Cupon 
     */
    public function getCupon()
    {
        return $this->cupon;
    }
}
