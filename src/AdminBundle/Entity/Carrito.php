<?php

namespace AdminBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Carrito{

      /**
       * @var integer
       *
       * @ORM\Column(name="id", type="integer")
       * @ORM\Id
       * @ORM\GeneratedValue(strategy="AUTO")
       */
      private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Clientes", inversedBy="carritoCliente")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $cliente;

     /**
     * @ORM\ManyToOne(targetEntity="Presentacion", inversedBy="carritoPresentacion")
     * @ORM\JoinColumn(name="presentacion_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $presentacion;   

     /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha = null;

    /**
     * @ORM\Column(type="float", scale=2)
     * @Assert\NotBlank()
     */
    private $precioUnitario;    ////Esto vuela porque se puede obtener desde la presentacion

    /**
     * @ORM\Column(type="float", scale=2)
     * @Assert\NotBlank()
     */
    private $precioReal;   ////precio * cantidad

    /**
     * @ORM\Column(type="float", scale=2, nullable=true)
     */
    private $precioRealConCupon; ////precio con descuento

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
     private $estado;     ////se calcula en el controlador

     /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
     private $cantidad;         ////se obtiene de la vista detalleProducto

     /**
      * @ORM\ManyToOne(targetEntity="Pedido", inversedBy="carrito")
      * @ORM\JoinColumn(name="pedido_id", referencedColumnName="id")
      * @Assert\NotBlank()
      */
      private $pedido;    ////Pedido  ????

      /**
       * @ORM\ManyToOne(targetEntity="Cupon", inversedBy="carrito")
       * @ORM\JoinColumn(name="cupon_id", referencedColumnName="id")
       * @Assert\NotBlank()
       */
      private $cupon;      //////Se obtiene del producto


    public function __construct()
    {
        $this->fecha = new \DateTime();
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Carrito
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set precioUnitario
     *
     * @param float $precioUnitario
     *
     * @return Carrito
     */
    public function setPrecioUnitario($precioUnitario)
    {
        $this->precioUnitario = $precioUnitario;

        return $this;
    }

    /**
     * Get precioUnitario
     *
     * @return float
     */
    public function getPrecioUnitario()
    {
        return $this->precioUnitario;
    }

    /**
     * Set precioReal
     *
     * @param float $precioReal
     *
     * @return Carrito
     */
    public function setPrecioReal($precioReal)
    {
        $this->precioReal = $precioReal;

        return $this;
    }

    /**
     * Get precioReal
     *
     * @return float
     */
    public function getPrecioReal()
    {
        return $this->precioReal;
    }

    /**
     * Set precioRealConCupon
     *
     * @param float $precioRealConCupon
     *
     * @return Carrito
     */
    public function setPrecioRealConCupon($precioRealConCupon)
    {
        $this->precioRealConCupon = $precioRealConCupon;

        return $this;
    }

    /**
     * Get precioRealConCupon
     *
     * @return float
     */
    public function getPrecioRealConCupon()
    {
        return $this->precioRealConCupon;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return Carrito
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     *
     * @return Carrito
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set cliente
     *
     * @param \AdminBundle\Entity\Clientes $cliente
     *
     * @return Carrito
     */
    public function setCliente(\AdminBundle\Entity\Clientes $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \AdminBundle\Entity\Clientes
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * Set presentacion
     *
     * @param \AdminBundle\Entity\Presentacion $presentacion
     *
     * @return Carrito
     */
    public function setPresentacion(\AdminBundle\Entity\Presentacion $presentacion = null)
    {
        $this->presentacion = $presentacion;

        return $this;
    }

    /**
     * Get presentacion
     *
     * @return \AdminBundle\Entity\Presentacion
     */
    public function getPresentacion()
    {
        return $this->presentacion;
    }

    /**
     * Set pedido
     *
     * @param \AdminBundle\Entity\Pedido $pedido
     *
     * @return Carrito
     */
    public function setPedido(\AdminBundle\Entity\Pedido $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \AdminBundle\Entity\Pedido
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    /**
     * Set cupon
     *
     * @param \AdminBundle\Entity\Cupon $cupon
     *
     * @return Carrito
     */
    public function setCupon(\AdminBundle\Entity\Cupon $cupon = null)
    {
        $this->cupon = $cupon;

        return $this;
    }

    /**
     * Get cupon
     *
     * @return \AdminBundle\Entity\Cupon
     */
    public function getCupon()
    {
        return $this->cupon;
    }
}
