<?php

namespace AdminBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Promocion{

      /**
       * @var integer
       *
       * @ORM\Column(name="id", type="integer")
       * @ORM\Id
       * @ORM\GeneratedValue(strategy="AUTO")
       */
      private $id;

      /**
       * @ORM\Column(type="string", length=255)
       * @Assert\NotBlank()
       */
      private $nombres;

      /**
       * @ORM\Column(type="text")
       * @Assert\NotBlank()
       */
      private $descripcion;

      /**
       * @ORM\Column(type="string", length=255)
       * @Assert\NotBlank()
       */
      private $imagen;

      /**
       * @ORM\Column(type="date")
       * @Assert\NotBlank()
       */
      private $fechaInicio;

      /**
       * @ORM\Column(type="date")
       * @Assert\NotBlank()
       */
      private $fechaFin;

    /**
     * @ORM\OneToMany(targetEntity="FormularioPromocion", mappedBy="promocion")
     */
      protected $formulario;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->formulario = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombres
     *
     * @param string $nombres
     * @return Promocion
     */
    public function setNombres($nombres)
    {
        $this->nombres = $nombres;

        return $this;
    }

    /**
     * Get nombres
     *
     * @return string 
     */
    public function getNombres()
    {
        return $this->nombres;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Promocion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     * @return Promocion
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string 
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return Promocion
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return Promocion
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Add formulario
     *
     * @param \AdminBundle\Entity\FormularioPromocion $formulario
     * @return Promocion
     */
    public function addFormulario(\AdminBundle\Entity\FormularioPromocion $formulario)
    {
        $this->formulario[] = $formulario;

        return $this;
    }

    /**
     * Remove formulario
     *
     * @param \AdminBundle\Entity\FormularioPromocion $formulario
     */
    public function removeFormulario(\AdminBundle\Entity\FormularioPromocion $formulario)
    {
        $this->formulario->removeElement($formulario);
    }

    /**
     * Get formulario
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFormulario()
    {
        return $this->formulario;
    }
}
