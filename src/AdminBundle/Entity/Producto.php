<?php

namespace AdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 */
class Producto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $nombreProducto;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $descripcionProducto;

    /**
     * @ORM\ManyToOne(targetEntity="Subcategoria", inversedBy="producto")
     * @ORM\JoinColumn(name="subcategoria_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $subcategoriaProd;

    /**
     * @Gedmo\Slug(fields={"nombreProducto"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $imagen;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $porcentajeDescuento;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $precioOferta;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     */
    private $precio;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $beneficioProducto;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $caracteristicaProducto;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $infoProducto;



    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $destacado;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $oferta;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $nuevo;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $apareceEnHome;

    /**
     * @ORM\ManyToOne(targetEntity="Cupon", inversedBy="productos")
     * @ORM\JoinColumn(name="cupon_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $cupon;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $ranking;

    /**
     * @ORM\OneToMany(targetEntity="Presentacion", mappedBy="productoPre")
     */
    protected $presentacion;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->carritoProducto = new \Doctrine\Common\Collections\ArrayCollection();
        $this->presentacion = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreProducto
     *
     * @param string $nombreProducto
     * @return Producto
     */
    public function setNombreProducto($nombreProducto)
    {
        $this->nombreProducto = $nombreProducto;

        return $this;
    }

    /**
     * Get nombreProducto
     *
     * @return string
     */
    public function getNombreProducto()
    {
        return $this->nombreProducto;
    }

    /**
     * Set descripcionProducto
     *
     * @param string $descripcionProducto
     * @return Producto
     */
    public function setDescripcionProducto($descripcionProducto)
    {
        $this->descripcionProducto = $descripcionProducto;

        return $this;
    }

    /**
     * Get descripcionProducto
     *
     * @return string
     */
    public function getDescripcionProducto()
    {
        return $this->descripcionProducto;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Producto
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     * @return Producto
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set porcentajeDescuento
     *
     * @param float $porcentajeDescuento
     * @return Producto
     */
    public function setPorcentajeDescuento($porcentajeDescuento)
    {
        $this->porcentajeDescuento = $porcentajeDescuento;

        return $this;
    }

    /**
     * Get porcentajeDescuento
     *
     * @return float
     */
    public function getPorcentajeDescuento()
    {
        return $this->porcentajeDescuento;
    }

    /**
     * Set precioOferta
     *
     * @param float $precioOferta
     * @return Producto
     */
    public function setPrecioOferta($precioOferta)
    {
        $this->precioOferta = $precioOferta;

        return $this;
    }

    /**
     * Get precioOferta
     *
     * @return float
     */
    public function getPrecioOferta()
    {
        return $this->precioOferta;
    }


    /**
     * Set precio
     *
     * @param float $precio
     * @return Producto
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set enStock
     *
     * @param boolean $enStock
     * @return Producto
     */
    public function setEnStock($enStock)
    {
        $this->enStock = $enStock;

        return $this;
    }

    /**
     * Get enStock
     *
     * @return boolean
     */
    public function getEnStock()
    {
        return $this->enStock;
    }

    /**
     * Set beneficioProducto
     *
     * @param string $beneficioProducto
     * @return Producto
     */
    public function setBeneficioProducto($beneficioProducto)
    {
        $this->beneficioProducto = $beneficioProducto;

        return $this;
    }

    /**
     * Get beneficioProducto
     *
     * @return string
     */
    public function getBeneficioProducto()
    {
        return $this->beneficioProducto;
    }

    /**
     * Set caracteristicaProducto
     *
     * @param string $caracteristicaProducto
     * @return Producto
     */
    public function setCaracteristicaProducto($caracteristicaProducto)
    {
        $this->caracteristicaProducto = $caracteristicaProducto;

        return $this;
    }

    /**
     * Get caracteristicaProducto
     *
     * @return string
     */
    public function getCaracteristicaProducto()
    {
        return $this->caracteristicaProducto;
    }

    /**
     * Set infoProducto
     *
     * @param string $infoProducto
     * @return Producto
     */
    public function setInfoProducto($infoProducto)
    {
        $this->infoProducto = $infoProducto;

        return $this;
    }

    /**
     * Get infoProducto
     *
     * @return string
     */
    public function getInfoProducto()
    {
        return $this->infoProducto;
    }

    /**
     * Set destacado
     *
     * @param boolean $destacado
     * @return Producto
     */
    public function setDestacado($destacado)
    {
        $this->destacado = $destacado;

        return $this;
    }

    /**
     * Get destacado
     *
     * @return boolean
     */
    public function getDestacado()
    {
        return $this->destacado;
    }

    /**
     * Set oferta
     *
     * @param boolean $oferta
     * @return Producto
     */
    public function setOferta($oferta)
    {
        $this->oferta = $oferta;

        return $this;
    }

    /**
     * Get oferta
     *
     * @return boolean
     */
    public function getOferta()
    {
        return $this->oferta;
    }

    /**
     * Set nuevo
     *
     * @param boolean $nuevo
     * @return Producto
     */
    public function setNuevo($nuevo)
    {
        $this->nuevo = $nuevo;

        return $this;
    }

    /**
     * Get nuevo
     *
     * @return boolean
     */
    public function getNuevo()
    {
        return $this->nuevo;
    }

    /**
     * Set apareceEnHome
     *
     * @param boolean $apareceEnHome
     * @return Producto
     */
    public function setApareceEnHome($apareceEnHome)
    {
        $this->apareceEnHome = $apareceEnHome;

        return $this;
    }

    /**
     * Get apareceEnHome
     *
     * @return boolean
     */
    public function getApareceEnHome()
    {
        return $this->apareceEnHome;
    }

    /**
     * Set ranking
     *
     * @param string $ranking
     * @return Producto
     */
    public function setRanking($ranking)
    {
        $this->ranking = $ranking;

        return $this;
    }

    /**
     * Get ranking
     *
     * @return string
     */
    public function getRanking()
    {
        return $this->ranking;
    }

    /**
     * Set subcategoriaProd
     *
     * @param \AdminBundle\Entity\Subcategoria $subcategoriaProd
     * @return Producto
     */
    public function setSubcategoriaProd(\AdminBundle\Entity\Subcategoria $subcategoriaProd = null)
    {
        $this->subcategoriaProd = $subcategoriaProd;

        return $this;
    }

    /**
     * Get subcategoriaProd
     *
     * @return \AdminBundle\Entity\Subcategoria
     */
    public function getSubcategoriaProd()
    {
        return $this->subcategoriaProd;
    }

    /**
     * Add carritoProducto
     *
     * @param \AdminBundle\Entity\Carrito $carritoProducto
     * @return Producto
     */
    public function addCarritoProducto(\AdminBundle\Entity\Carrito $carritoProducto)
    {
        $this->carritoProducto[] = $carritoProducto;

        return $this;
    }

    /**
     * Remove carritoProducto
     *
     * @param \AdminBundle\Entity\Carrito $carritoProducto
     */
    public function removeCarritoProducto(\AdminBundle\Entity\Carrito $carritoProducto)
    {
        $this->carritoProducto->removeElement($carritoProducto);
    }

    /**
     * Get carritoProducto
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCarritoProducto()
    {
        return $this->carritoProducto;
    }

    /**
     * Set cupon
     *
     * @param \AdminBundle\Entity\Cupon $cupon
     * @return Producto
     */
    public function setCupon(\AdminBundle\Entity\Cupon $cupon = null)
    {
        $this->cupon = $cupon;

        return $this;
    }

    /**
     * Get cupon
     *
     * @return \AdminBundle\Entity\Cupon
     */
    public function getCupon()
    {
        return $this->cupon;
    }

    /**
     * Add presentacion
     *
     * @param \AdminBundle\Entity\Presentacion $presentacion
     * @return Producto
     */
    public function addPresentacion(\AdminBundle\Entity\Presentacion $presentacion)
    {
        $this->presentacion[] = $presentacion;

        return $this;
    }

    /**
     * Remove presentacion
     *
     * @param \AdminBundle\Entity\Presentacion $presentacion
     */
    public function removePresentacion(\AdminBundle\Entity\Presentacion $presentacion)
    {
        $this->presentacion->removeElement($presentacion);
    }

    /**
     * Get presentacion
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPresentacion()
    {
        return $this->presentacion;
    }
}
