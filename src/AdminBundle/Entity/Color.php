<?php

namespace AdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 */
class Color
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank()
     */
    private $color;
    /**
     * @ORM\Column(type="string", length=20)
     *
     */
    private $descripcion;
    /**
     * @ORM\OneToMany(targetEntity="Presentacion", mappedBy="productoCo")
     *
     *
     */
    private $presentacionColor;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->presentacionColor = new \Doctrine\Common\Collections\ArrayCollection();
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Color
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Color
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Add presentacionColor
     *
     * @param \AdminBundle\Entity\Presentacion $presentacionColor
     *
     * @return Color
     */
    public function addPresentacionColor(\AdminBundle\Entity\Presentacion $presentacionColor)
    {
        $this->presentacionColor[] = $presentacionColor;

        return $this;
    }

    /**
     * Remove presentacionColor
     *
     * @param \AdminBundle\Entity\Presentacion $presentacionColor
     */
    public function removePresentacionColor(\AdminBundle\Entity\Presentacion $presentacionColor)
    {
        $this->presentacionColor->removeElement($presentacionColor);
    }

    /**
     * Get presentacionColor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPresentacionColor()
    {
        return $this->presentacionColor;
    }
}
