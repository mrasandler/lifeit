<?php

namespace AdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 */
class ProyectoTv
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $de;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $email;

    /**
     * @ORM\Column(type="string",nullable=true, length=255)
     *
     */
    private $pdfDocumento;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank()
     */
    private $fechacimiento;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha = null;

    public function __construct(){
        $this->fecha = new \DateTime(date("d-m-Y"));
    }

    public function getFechaFormateada()
    {
        return $this->fecha->format('d-m-Y');
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set de
     *
     * @param string $de
     *
     * @return ProyectoTv
     */
    public function setDe($de)
    {
        $this->de = $de;

        return $this;
    }

    /**
     * Get de
     *
     * @return string
     */
    public function getDe()
    {
        return $this->de;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return ProyectoTv
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ProyectoTv
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set pdfDocumento
     *
     * @param string $pdfDocumento
     *
     * @return ProyectoTv
     */
    public function setPdfDocumento($pdfDocumento)
    {
        $this->pdfDocumento = $pdfDocumento;

        return $this;
    }

    /**
     * Get pdfDocumento
     *
     * @return string
     */
    public function getPdfDocumento()
    {
        return $this->pdfDocumento;
    }

    /**
     * Set fechacimiento
     *
     * @param string $fechacimiento
     *
     * @return ProyectoTv
     */
    public function setFechacimiento($fechacimiento)
    {
        $this->fechacimiento = $fechacimiento;

        return $this;
    }

    /**
     * Get fechacimiento
     *
     * @return string
     */
    public function getFechacimiento()
    {
        return $this->fechacimiento;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return ProyectoTv
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }
}
