<?php

namespace AdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class FbPosts
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Assert\NotBlank()
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $imagenPublicacion;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $linkPublicacion;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $vecesCompartidaPublicacion;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $meGustaPublicacion;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $comentariosPublicacion;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $meEncantaPublicacion;

    /**
     * Set id
     *
     * @param integer $id
     * @return FbPosts
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imagenPublicacion
     *
     * @param string $imagenPublicacion
     * @return FbPosts
     */
    public function setImagenPublicacion($imagenPublicacion)
    {
        $this->imagenPublicacion = $imagenPublicacion;

        return $this;
    }

    /**
     * Get imagenPublicacion
     *
     * @return string 
     */
    public function getImagenPublicacion()
    {
        return $this->imagenPublicacion;
    }

    /**
     * Set linkPublicacion
     *
     * @param string $linkPublicacion
     * @return FbPosts
     */
    public function setLinkPublicacion($linkPublicacion)
    {
        $this->linkPublicacion = $linkPublicacion;

        return $this;
    }

    /**
     * Get linkPublicacion
     *
     * @return string 
     */
    public function getLinkPublicacion()
    {
        return $this->linkPublicacion;
    }

    /**
     * Set vecesCompartidaPublicacion
     *
     * @param integer $vecesCompartidaPublicacion
     * @return FbPosts
     */
    public function setVecesCompartidaPublicacion($vecesCompartidaPublicacion)
    {
        $this->vecesCompartidaPublicacion = $vecesCompartidaPublicacion;

        return $this;
    }

    /**
     * Get vecesCompartidaPublicacion
     *
     * @return integer 
     */
    public function getVecesCompartidaPublicacion()
    {
        return $this->vecesCompartidaPublicacion;
    }

    /**
     * Set meGustaPublicacion
     *
     * @param integer $meGustaPublicacion
     * @return FbPosts
     */
    public function setMeGustaPublicacion($meGustaPublicacion)
    {
        $this->meGustaPublicacion = $meGustaPublicacion;

        return $this;
    }

    /**
     * Get meGustaPublicacion
     *
     * @return integer 
     */
    public function getMeGustaPublicacion()
    {
        return $this->meGustaPublicacion;
    }

    /**
     * Set comentariosPublicacion
     *
     * @param integer $comentariosPublicacion
     * @return FbPosts
     */
    public function setComentariosPublicacion($comentariosPublicacion)
    {
        $this->comentariosPublicacion = $comentariosPublicacion;

        return $this;
    }

    /**
     * Get comentariosPublicacion
     *
     * @return integer 
     */
    public function getComentariosPublicacion()
    {
        return $this->comentariosPublicacion;
    }

    /**
     * Set meEncantaPublicacion
     *
     * @param integer $meEncantaPublicacion
     * @return FbPosts
     */
    public function setMeEncantaPublicacion($meEncantaPublicacion)
    {
        $this->meEncantaPublicacion = $meEncantaPublicacion;

        return $this;
    }

    /**
     * Get meEncantaPublicacion
     *
     * @return integer 
     */
    public function getMeEncantaPublicacion()
    {
        return $this->meEncantaPublicacion;
    }
}
