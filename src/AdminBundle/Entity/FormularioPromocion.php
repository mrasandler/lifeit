<?php

namespace AdminBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class FormularioPromocion{

      /**
       * @var integer
       *
       * @ORM\Column(name="id", type="integer")
       * @ORM\Id
       * @ORM\GeneratedValue(strategy="AUTO")
       */
      private $id;

     /**
       * @ORM\ManyToOne(targetEntity="Promocion", inversedBy="formulario")
       * @ORM\JoinColumn(name="promocion_id", referencedColumnName="id")
       * @Assert\NotBlank()
       */
      private $promocion;

      /**
       * @ORM\Column(type="string", length=255)
       * @Assert\NotBlank()
       */
      private $nombres;

      /**
       * @ORM\Column(type="string", length=255)
       * @Assert\NotBlank()
       */
      private $email;

      /**
       * @ORM\Column(type="string", length=255)
       * @Assert\NotBlank()
       */
      private $telefono;

      /**
       * @var \DateTime
       *
       * @ORM\Column(name="fecha", type="datetime")
       */
      private $fecha = null;

      public function __construct(){
        $this->fecha = new \DateTime();
      }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombres
     *
     * @param string $nombres
     * @return FormularioPromocion
     */
    public function setNombres($nombres)
    {
        $this->nombres = $nombres;

        return $this;
    }

    /**
     * Get nombres
     *
     * @return string 
     */
    public function getNombres()
    {
        return $this->nombres;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return FormularioPromocion
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return FormularioPromocion
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return FormularioPromocion
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set promocion
     *
     * @param \AdminBundle\Entity\Promocion $promocion
     * @return FormularioPromocion
     */
    public function setPromocion(\AdminBundle\Entity\Promocion $promocion = null)
    {
        $this->promocion = $promocion;

        return $this;
    }

    /**
     * Get promocion
     *
     * @return \AdminBundle\Entity\Promocion 
     */
    public function getPromocion()
    {
        return $this->promocion;
    }
}
