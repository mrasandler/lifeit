<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AdminBundle\Entity\Suscripcion;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @Route("/Suscripcion")
 */
class SuscripcionController extends Controller
{
    /**
     * Lists all Suscripcion entities.
     *
     * @Route("/", name="zerobundle_admin_suscripcion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:Suscripcion')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * @Route("/exportar-suscripcion/", name="exportar-suscripcion")
     */
    public function exportarSuscripcionAction(Request $request)
    {
      // ask the service for a Excel5
      $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

      $em = $this->getDoctrine()->getManager();

      $entities = $em->getRepository('AdminBundle:Suscripcion')->findAll();

      $phpExcelObject->getProperties()->setCreator("Life It")
      ->setLastModifiedBy("Life It")
      ->setTitle("Lista de suscripciones")
      ->setSubject("Lista de suscripciones")
      ->setDescription("Listado de suscripciones de Life It")
      ->setKeywords("Suscripción Life It")
      ->setCategory("Listado de Suscripciones");
      $phpExcelObject->setActiveSheetIndex(0)
      ->setCellValue('A1', 'Email')
      ->setCellValue('B1', 'Fecha');

      foreach ($entities as $key => $value) {
      $phpExcelObject->setActiveSheetIndex(0)
      ->setCellValue('A'.($key+2), $value->getEmail())
      ->setCellValue('B'.($key+2), $value->getFechaFormateada());
      }

      //->setCellValue('B2', 'world!');
      $phpExcelObject->getActiveSheet()->setTitle('Personas que se han suscrito');
      // Set active sheet index to the first sheet, so Excel opens this as the first sheet
      $phpExcelObject->setActiveSheetIndex(0);

      // create the writer
      $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
      // create the response
      $response = $this->get('phpexcel')->createStreamedResponse($writer);
      // adding headers
      $dispositionHeader = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      'suscripciones-'.date('Y-m-d--H-i-s').'.xls'
      );
      $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
      $response->headers->set('Pragma', 'public');
      $response->headers->set('Cache-Control', 'maxage=1');
      $response->headers->set('Content-Disposition', $dispositionHeader);

      return $response;
    }
}