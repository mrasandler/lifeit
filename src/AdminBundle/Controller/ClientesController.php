<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AdminBundle\Entity\Clientes;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @Route("/Clientes")
 */
class ClientesController extends Controller
{
    /**
     * Lists all Clientes entities.
     *
     * @Route("/", name="zerobundle_admin_clientes")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:Clientes')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * @Route("/exportar-clientes/", name="exportar-clientes")
     */
    public function exportarClientesAction(Request $request)
    {
      // ask the service for a Excel5
      $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

      $em = $this->getDoctrine()->getManager();

      $entities = $em->getRepository('AdminBundle:Clientes')->findAll();

      $phpExcelObject->getProperties()->setCreator("Life It")
      ->setLastModifiedBy("Life It")
      ->setTitle("Lista de clientes")
      ->setSubject("Lista de clientes")
      ->setDescription("Listado de clientes de Life It")
      ->setKeywords("Suscripción Life It")
      ->setCategory("Listado de Clientes");
      $phpExcelObject->setActiveSheetIndex(0)
      ->setCellValue('A1', 'Nombres y apellidos')
      ->setCellValue('B1', 'Email')
      ->setCellValue('C1', 'Teléfono')
      ->setCellValue('D1', 'DNI')
      ->setCellValue('E1', 'Fecha de Nacimiento')
      ->setCellValue('F1', 'Dirección')
      ->setCellValue('G1', 'Fecha de registro');

      foreach ($entities as $key => $value) {
      $phpExcelObject->setActiveSheetIndex(0)
      ->setCellValue('A'.($key+2), $value->getNombres())
      ->setCellValue('B'.($key+2), $value->getEmail())
      ->setCellValue('C'.($key+2), $value->getTelefono())
      ->setCellValue('D'.($key+2), $value->getDni())
      ->setCellValue('E'.($key+2), $value->getFechaNacimiento())
      ->setCellValue('F'.($key+2), $value->getDireccion())
      ->setCellValue('G'.($key+2), $value->getFechaFormateada());
      }

      //->setCellValue('B2', 'world!');
      $phpExcelObject->getActiveSheet()->setTitle('Personas que se han registrado');
      // Set active sheet index to the first sheet, so Excel opens this as the first sheet
      $phpExcelObject->setActiveSheetIndex(0);

      // create the writer
      $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
      // create the response
      $response = $this->get('phpexcel')->createStreamedResponse($writer);
      // adding headers
      $dispositionHeader = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      'clienteses-'.date('Y-m-d--H-i-s').'.xls'
      );
      $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
      $response->headers->set('Pragma', 'public');
      $response->headers->set('Cache-Control', 'maxage=1');
      $response->headers->set('Content-Disposition', $dispositionHeader);

      return $response;
    }
}