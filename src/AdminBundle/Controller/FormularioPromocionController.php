<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AdminBundle\Entity\FormularioPromocion;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @Route("/FormularioPromocion")
 */
class FormularioPromocionController extends Controller
{
    /**
     * Lists all FormularioPromocion entities.
     *
     * @Route("/", name="zerobundle_admin_formulariopromocion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:FormularioPromocion')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * @Route("/exportar-formulariopromocion/", name="exportar-formulariopromocion")
     */
    public function exportarFormularioPromocionAction(Request $request)
    {
      // ask the service for a Excel5
      $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

      $em = $this->getDoctrine()->getManager();

      $entities = $em->getRepository('AdminBundle:FormularioPromocion')->findAll();

      $phpExcelObject->getProperties()->setCreator("Life It")
        ->setLastModifiedBy("Life It")
        ->setTitle("Lista de suscritos a promociones")
        ->setSubject("Lista de suscritos a promociones")
        ->setDescription("Listado de suscritos a promociones de Life It")
        ->setKeywords("suscritos a promociones Life It")
        ->setCategory("Listado de suscritos a promociones");

      $phpExcelObject->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Promoción')
        ->setCellValue('B1', 'Cliente')
        ->setCellValue('C1', 'Correo')
        ->setCellValue('D1', 'Teléfono')
        ->setCellValue('E1', 'Fecha registro');

      foreach ($entities as $key => $value) {
        $phpExcelObject->setActiveSheetIndex(0)
          ->setCellValue('A'.($key+2), $value->getPromocion()->getNombres())
          ->setCellValue('B'.($key+2), $value->getNombres())
          ->setCellValue('C'.($key+2), $value->getEmail())
          ->setCellValue('D'.($key+2), $value->getTelefono())
          ->setCellValue('E'.($key+2), $value->getFecha());
      }

      //->setCellValue('B2', 'world!');
      $phpExcelObject->getActiveSheet()->setTitle('Personas que se han suscrito a promociones');
      // Set active sheet index to the first sheet, so Excel opens this as the first sheet
      $phpExcelObject->setActiveSheetIndex(0);

      // create the writer
      $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
      // create the response
      $response = $this->get('phpexcel')->createStreamedResponse($writer);
      // adding headers
      $dispositionHeader = $response->headers->makeDisposition(
        ResponseHeaderBag::DISPOSITION_ATTACHMENT,
        'suscripciones-promociones-'.date('Y-m-d--H-i-s').'.xls'
      );

      $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
      $response->headers->set('Pragma', 'public');
      $response->headers->set('Cache-Control', 'maxage=1');
      $response->headers->set('Content-Disposition', $dispositionHeader);

      return $response;
    }
}
