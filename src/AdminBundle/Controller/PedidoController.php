<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AdminBundle\Entity\Pedido;
use AdminBundle\Form\PedidoType;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @Route("/Pedido")
 */
class PedidoController extends Controller
{
    /**
     * Lists all Pedido entities.
     *
     * @Route("/", name="zerobundle_admin_pedido")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:Pedido')->findBy(array('estado' => 'C'));

        return array(
            'entities' => $entities,
        );
    }

    /**
     * @Route("/exportar-pedido/", name="exportar-pedido")
     */
    public function exportarPedidoAction(Request $request)
    {
      // ask the service for a Excel5
      $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

      $em = $this->getDoctrine()->getManager();

      $entities = $em->getRepository('AdminBundle:Pedido')->findAll();

      $phpExcelObject->getProperties()->setCreator("Life It")
        ->setLastModifiedBy("Life It")
        ->setTitle("Inscritos a bolsa de trabajo")
        ->setSubject("Inscritos a bolsa de trabajo")
        ->setDescription("Inscritos a bolsa de trabajo")
        ->setKeywords("Bolsa de trabajo Life It")
        ->setCategory("Inscritos a bolsa de trabajos");

      $phpExcelObject->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Nombres y apellidos')
        ->setCellValue('B1', 'Documento')
        ->setCellValue('C1', 'Provincia')
        ->setCellValue('D1', 'Email')
        ->setCellValue('E1', 'Teléfono')
        ->setCellValue('F1', 'Fecha inscripción');

      foreach ($entities as $key => $value) {
        $phpExcelObject->setActiveSheetIndex(0)
          ->setCellValue('A'.($key+2), $value->getNombres())
          ->setCellValue('B'.($key+2), $value->getDocumento())
          ->setCellValue('C'.($key+2), $value->getProvincia())
          ->setCellValue('D'.($key+2), $value->getEmail())
          ->setCellValue('E'.($key+2), $value->getTelefono())
          ->setCellValue('F'.($key+2), $value->getFechaFormateada());
      }

      //->setCellValue('B2', 'world!');
      $phpExcelObject->getActiveSheet()->setTitle('Inscritos a la bolsa de trabajo');
      // Set active sheet index to the first sheet, so Excel opens this as the first sheet
      $phpExcelObject->setActiveSheetIndex(0);

      // create the writer
      $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
      // create the response
      $response = $this->get('phpexcel')->createStreamedResponse($writer);
      // adding headers
      $dispositionHeader = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      'inscritos-bolsa-trabajo-'.date('Y-m-d--H-i-s').'.xls'
      );
      $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
      $response->headers->set('Pragma', 'public');
      $response->headers->set('Cache-Control', 'maxage=1');
      $response->headers->set('Content-Disposition', $dispositionHeader);

      return $response;
    }

    /**
     * Displays a form to view an existing Pedido entity.
     *
     * @Route("/{id}/view", name="zerobundle_admin_pedido_view")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function formAction(Request $request, Pedido $entity)
    {
      $editForm = $this->createForm('AdminBundle\Form\PedidoType', $entity, array(
        'action' => $this->generateUrl('zerobundle_admin_pedido_view', array('id' => $entity->getId())),
      ));

      $editForm->add('submit', SubmitType::class, array('label' => 'Regresar'));
      $editForm->handleRequest($request);

      if ($editForm->isSubmitted() && $editForm->isValid()) {
        $return = ($request->query->get('ajax') == 'true') ? array('ajax'=>'true'):array();

        if ($editForm->get('submit')->isClicked())
        {
          $url = 'zerobundle_admin_pedido';
        }

        return $this->redirectToRoute($url, $return);
      }

      return array(
          'entity' => $entity,
          'form' => $editForm->createView(),
      );
    }
}
