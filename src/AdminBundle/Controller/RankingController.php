<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AdminBundle\Entity\Ranking;
use AdminBundle\Form\RankingType;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @Route("/Ranking")
 */
class RankingController extends Controller
{
    /**
     * Lists all Ranking entities.
     *
     * @Route("/", name="zerobundle_admin_ranking")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:Ranking')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * @Route("/exportar-ranking/", name="exportar-ranking")
     */
    public function exportarRankingAction(Request $request)
    {
      // ask the service for a Excel5
      $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

      $em = $this->getDoctrine()->getManager();

      $entities = $em->getRepository('AdminBundle:Ranking')->findAll();

      $phpExcelObject->getProperties()->setCreator("Life It")
        ->setLastModifiedBy("Life It")
        ->setTitle("Inscritos a bolsa de trabajo")
        ->setSubject("Inscritos a bolsa de trabajo")
        ->setDescription("Inscritos a bolsa de trabajo")
        ->setKeywords("Bolsa de trabajo Life It")
        ->setCategory("Inscritos a bolsa de trabajos");

      $phpExcelObject->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Nombres y apellidos')
        ->setCellValue('B1', 'Documento')
        ->setCellValue('C1', 'Provincia')
        ->setCellValue('D1', 'Email')
        ->setCellValue('E1', 'Teléfono')
        ->setCellValue('F1', 'Fecha inscripción');

      foreach ($entities as $key => $value) {
        $phpExcelObject->setActiveSheetIndex(0)
          ->setCellValue('A'.($key+2), $value->getNombres())
          ->setCellValue('B'.($key+2), $value->getDocumento())
          ->setCellValue('C'.($key+2), $value->getProvincia())
          ->setCellValue('D'.($key+2), $value->getEmail())
          ->setCellValue('E'.($key+2), $value->getTelefono())
          ->setCellValue('F'.($key+2), $value->getFechaFormateada());
      }

      //->setCellValue('B2', 'world!');
      $phpExcelObject->getActiveSheet()->setTitle('Inscritos a la bolsa de trabajo');
      // Set active sheet index to the first sheet, so Excel opens this as the first sheet
      $phpExcelObject->setActiveSheetIndex(0);

      // create the writer
      $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
      // create the response
      $response = $this->get('phpexcel')->createStreamedResponse($writer);
      // adding headers
      $dispositionHeader = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      'inscritos-bolsa-trabajo-'.date('Y-m-d--H-i-s').'.xls'
      );
      $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
      $response->headers->set('Pragma', 'public');
      $response->headers->set('Cache-Control', 'maxage=1');
      $response->headers->set('Content-Disposition', $dispositionHeader);

      return $response;
    }
}
