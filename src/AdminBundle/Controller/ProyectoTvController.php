<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AdminBundle\Entity\ProyectoTv;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @Route("/ProyectoTv")
 */
class ProyectoTvController extends Controller
{
    /**
     * Lists all NovedadesContacto entities.
     *
     * @Route("/", name="zerobundle_admin_proyectotv")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:ProyectoTv')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * @Route("/exportar-proyectotv/", name="exportar-proyectotv")
     */
    public function exportarTalleresContactoAction(Request $request){
        // ask the service for a Excel5
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:ProyectoTv')->findAll();

        $phpExcelObject->getProperties()->setCreator("Life It")
           ->setLastModifiedBy("Life It")
           ->setTitle("Lista de personas que nos han contactado")
           ->setSubject("Lista de personas que nos han contactado")
           ->setDescription("Listado de personas que nos han contactado desde Proyectos Tv")
           ->setKeywords("Contactos Proyectos Tv ")
           ->setCategory("Listado de personas que nos han contactado");
        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Fecha')
            ->setCellValue('B1', 'De')
            ->setCellValue('C1', 'E-mail')
            ->setCellValue('D1', 'Telefono')
            ->setCellValue('E1', 'Fecha Nacimiento');

        foreach ($entities as $key => $value) {
            $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A'.($key+2), $value->getFechaFormateada())
                    ->setCellValue('B'.($key+2), $value->getDe())
                    ->setCellValue('C'.($key+2), $value->getEmail())
                    ->setCellValue('D'.($key+2), $value->getTelefono())
                    ->setCellValue('E'.($key+2), $value->getFechacimiento());

        }

           //->setCellValue('B2', 'world!');
        $phpExcelObject->getActiveSheet()->setTitle('Contactos Proyectos Tv');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcelObject->setActiveSheetIndex(0);

        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'Proyectos Tv'.date('d-m-Y--H-i-s').'.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }
}
