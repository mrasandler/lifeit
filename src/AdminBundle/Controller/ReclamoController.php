<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AdminBundle\Entity\Reclamo;
use AdminBundle\Form\ReclamoType;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @Route("/Reclamo")
 */
class ReclamoController extends Controller
{
    /**
     * Lists all Reclamo entities.
     *
     * @Route("/", name="zerobundle_admin_reclamo")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:Reclamo')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * @Route("/exportar-reclamo/", name="exportar-reclamo")
     */
    public function exportarReclamoAction(Request $request)
    {
      // ask the service for a Excel5
      $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

      $em = $this->getDoctrine()->getManager();

      $entities = $em->getRepository('AdminBundle:Reclamo')->findAll();

      $phpExcelObject->getProperties()->setCreator("Life It")
        ->setLastModifiedBy("Life It")
        ->setTitle("Lista de reclamos")
        ->setSubject("Lista de reclamos")
        ->setDescription("Listado de reclamos a Life It")
        ->setKeywords("Reclamos Life It")
        ->setCategory("Listado de Reclamos");

      $phpExcelObject->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Reclamante')
        ->setCellValue('B1', 'Tipo de documento')
        ->setCellValue('C1', 'Número de documento')
        ->setCellValue('D1', 'Teléfono')
        ->setCellValue('E1', 'Correo')
        ->setCellValue('F1', 'Provincia')
        ->setCellValue('G1', 'Distrito')
        ->setCellValue('H1', 'Dirección')
        ->setCellValue('I1', 'Apoderado')
        ->setCellValue('J1', 'Tipo de documento')
        ->setCellValue('K1', 'Número de documento')
        ->setCellValue('L1', 'Tipo de reclamo')
        ->setCellValue('M1', 'Mensaje')
        ->setCellValue('N1', 'Fecha de reclamo');

      foreach ($entities as $key => $value) {

        if ($value->getDistrito()) {

          $provincia = $value->getDistrito()->getProvincia()->getNombre();
          $distrito  = $value->getDistrito()->getNombre();

        }else{

          $provincia = "--";
          $distrito = '--';

        }

        $phpExcelObject->setActiveSheetIndex(0)
          ->setCellValue('A'.($key+2), $value->getNombresReclamante()." ".$value->getApellidosReclamante())
          ->setCellValue('B'.($key+2), $value->getTipoDocumentoReclamante())
          ->setCellValue('C'.($key+2), $value->getNumeroDocumentoReclamante())
          ->setCellValue('D'.($key+2), $value->getTelefonoReclamante())
          ->setCellValue('E'.($key+2), $value->getEmailReclamante())
          ->setCellValue('F'.($key+2), $provincia)
          ->setCellValue('G'.($key+2), $distrito)
          ->setCellValue('H'.($key+2), $value->getDireccion())
          ->setCellValue('I'.($key+2), $value->getNombresApoderado())
          ->setCellValue('J'.($key+2), $value->getTipoDocumentoApoderado())
          ->setCellValue('K'.($key+2), $value->getNumeroDocumentoApoderado())
          ->setCellValue('L'.($key+2), $value->getTipoReclamo())
          ->setCellValue('M'.($key+2), $value->getMensaje())
          ->setCellValue('N'.($key+2), $value->getFecha());
      }

      //->setCellValue('B2', 'world!');
      $phpExcelObject->getActiveSheet()->setTitle('Personas que han hecho reclamos');
      // Set active sheet index to the first sheet, so Excel opens this as the first sheet
      $phpExcelObject->setActiveSheetIndex(0);

      // create the writer
      $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
      // create the response
      $response = $this->get('phpexcel')->createStreamedResponse($writer);
      // adding headers
      $dispositionHeader = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
        'reclamos-'.date('Y-m-d--H-i-s').'.xls'
      );
      $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
      $response->headers->set('Pragma', 'public');
      $response->headers->set('Cache-Control', 'maxage=1');
      $response->headers->set('Content-Disposition', $dispositionHeader);

      return $response;
    }
}
