<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AdminBundle\Entity\Contacto;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @Route("/Contacto")
 */
class ContactoController extends Controller
{
    /**
     * Lists all Contacto entities.
     *
     * @Route("/", name="zerobundle_admin_contacto")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:Contacto')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * @Route("/exportar-contacto/", name="exportar-contacto")
     */
    public function exportarContactoAction(Request $request)
    {
      // ask the service for a Excel5
      $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

      $em = $this->getDoctrine()->getManager();

      $entities = $em->getRepository('AdminBundle:Contacto')->findAll();

      $phpExcelObject->getProperties()->setCreator("Life It")
        ->setLastModifiedBy("Life It")
        ->setTitle("Lista de contactos")
        ->setSubject("Lista de contactos")
        ->setDescription("Listado de contactos de Life It")
        ->setKeywords("contacto Life It")
        ->setCategory("Listado de Contactos");

      $phpExcelObject->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Nombres y apellidos')
        ->setCellValue('B1', 'Email')
        ->setCellValue('C1', 'Teléfono')
        ->setCellValue('D1', 'Provincia')
        ->setCellValue('E1', 'Mensaje')
        ->setCellValue('F1', 'Fecha mensaje');

      foreach ($entities as $key => $value) {
        $phpExcelObject->setActiveSheetIndex(0)
          ->setCellValue('A'.($key+2), $value->getNombres())
          ->setCellValue('B'.($key+2), $value->getEmail())
          ->setCellValue('C'.($key+2), $value->getTelefono())
          ->setCellValue('D'.($key+2), $value->getProvincia())
          ->setCellValue('E'.($key+2), $value->getMensaje())
          ->setCellValue('F'.($key+2), $value->getFechaFormateada());
      }

      //->setCellValue('B2', 'world!');
      $phpExcelObject->getActiveSheet()->setTitle('Personas que nos han contactado');
      // Set active sheet index to the first sheet, so Excel opens this as the first sheet
      $phpExcelObject->setActiveSheetIndex(0);

      // create the writer
      $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
      // create the response
      $response = $this->get('phpexcel')->createStreamedResponse($writer);
      // adding headers
      $dispositionHeader = $response->headers->makeDisposition(
        ResponseHeaderBag::DISPOSITION_ATTACHMENT,
        'contactos-'.date('Y-m-d--H-i-s').'.xls'
      );

      $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
      $response->headers->set('Pragma', 'public');
      $response->headers->set('Cache-Control', 'maxage=1');
      $response->headers->set('Content-Disposition', $dispositionHeader);

      return $response;
    }
}
