<?php

namespace WebBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Clientes;
use AdminBundle\Entity\Departamento;
use AdminBundle\Entity\Provincia;
use AdminBundle\Entity\Distrito;
use AdminBundle\Entity\FbPosts;
use AdminBundle\Entity\Contacto;
use AdminBundle\Entity\BolsaTrabajo;
use AdminBundle\Entity\Reclamo;
use AdminBundle\Entity\Suscripcion;
use AdminBundle\Entity\Home;
use AdminBundle\Entity\Carrito;
use AdminBundle\Entity\FormularioPromocion;
use AdminBundle\Entity\Noticia;
use AdminBundle\Entity\Categoria;
use AdminBundle\Entity\Pedido;
use AdminBundle\Entity\Presentacion;
use AdminBundle\Entity\ProyectoTv;
use AdminBundle\Entity\Ranking;
use AdminBundle\Entity\Info;
use AdminBundle\Entity\RecuperarContrasenaUrl;
use Guzzle\Http\Client;
use Guzzle\Http\EntityBody;
use Guzzle\Http\Message\Request as wsrq;
use Guzzle\Http\Message\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\VarDumper\VarDumper;

class DefaultController extends Controller
{
    protected $locals = array();
    protected $sessionVal = array();

    /**
     * @Route("/", name="home")
     * @Template()
     */
    public function homeAction()
    {

      $em = $this->getDoctrine();
      $this->locals['banners'] = $em->getRepository('AdminBundle:Banner')->findAll();
      $this->locals['fbposts'] = $em->getRepository('AdminBundle:FbPosts')->findAll();
      $this->locals['home'] = $em->getRepository('AdminBundle:Home')->find(1);
      $this->locals['noticias'] = $em->getRepository('AdminBundle:Noticia')->findAll();
      $this->locals['productosHome'] = $em->getRepository('AdminBundle:Producto')->findBy(
        array('apareceEnHome' => 1)//,
        //array(),
        //0,2
      );

      // $session = $this->get('session');
      // $session->remove('carrito');
      // $session->remove('idCupon');
        // dump($this->sessionVal);
        // exit();
      return $this->locals;
    }

    /**
     * @Route("/login/", name="login")
     * @Method({"GET"})
     * @Template()
     */
    public function loginAction(){
      $session = $this->get('session');
      $idCliente = $session->get('idCliente');

      //Si aún no está logueado
      if($idCliente){
        return $this->redirect("/"); //Debe redirigir al PERFIL del usuario, pero aun no se tiene ese template
      }
    }



    private function agregarEditarItem($session, $presentacionid, $precioProducto, $cantidadProducto, $subTotalProducto){
            dump("agregareditaritem");

      if (is_numeric($presentacionid)){
        $em = $this->getDoctrine();
        $em2 = $em->getManager();

        //Buscamos el producto cuyo id de producto vamos a agregar.
        $presentacion = $em->getRepository('AdminBundle:Presentacion')->findOneById($presentacionid);

        //Obtenemos su stock, imagen, nombre.
        $stock = $presentacion->getStock();
        $imagen = $presentacion->getImagenes();
        $nombre = $presentacion->getProductoPre()->getNombreProducto();

        //Id cliente de la sesion y cupon de la sesion
        $clienteSesionActual = $session->get('idCliente');
        $cupon = $session->get('idCupon');

        //Si existe el producto y si el stock de éste es mayor a 0 se ejecuta lo siguiente:
        if ($presentacion && $stock > 0){
          //Hay un usuario logueado, por ende lo que agregue al carrito va a BD como estado pendiente hasta que pague por ello.
          if($clienteSesionActual){

            $request = $em2->getRepository('AdminBundle:Carrito');

            $query = $request->createQueryBuilder("C");
            $query
            ->select('C')
            ->where('C.cliente = :clienteid AND C.presentacion = :presentacionid AND C.estado != :estado')
            ->setParameter('clienteid', $clienteSesionActual)
            ->setParameter('presentacionid' , $presentacionid)
            ->setParameter('estado' , 'C');

            $result = $query->getQuery();

            $carritoProductosActual = $result->getOneOrNullResult();

            if($carritoProductosActual){
              $cantidad = $carritoProductosActual->getCantidad();
              $cantidadProducto = $cantidadProducto + $cantidad;

                      if ($cantidadProducto <= $stock ) {
                          $carritoProductosActual->setCantidad($cantidadProducto);
                          $precio = $carritoProductosActual->getPrecioReal();
                          $precioReal = $precioProducto * $cantidadProducto;
                          $carritoProductosActual->setPrecioReal($precioReal);
                          $em2->flush();

                      }
                      ///ojala que funcione
            }else{

              $objCarrito = new Carrito();
              $instanceOfCliente = $em->getRepository('AdminBundle:Clientes')->findOneById($clienteSesionActual);
              $objCarrito->setCliente($instanceOfCliente);
              $instanceOfPresentacion = $em->getRepository('AdminBundle:Presentacion')->findOneById($presentacionid);
              $objCarrito->setPresentacion($instanceOfPresentacion);
              $precioReal = $precioProducto * $cantidadProducto;
              $objCarrito->setPrecioReal($precioReal);
              $objCarrito->setPrecioUnitario($precioProducto);
              $objCarrito->setEstado('P');
              $objCarrito->setCantidad($cantidadProducto);

              $manager = $em->getManager();
              $manager->persist($objCarrito);
              $manager->flush();

            }
          }

          else{
                // dump("no usuario logeado");
                // exit();
               //No hay un usuario logueado, por ende lo que agregue al carrito va a memoria caché hasta que inicie sesión y pague por ello.
            //Obtenemos el carrito
            $this->sessionVal = $session->get('carrito');

            //Evaluamos la cantidad que se quiere comprar del producto con su stock actual.
            if($cantidadProducto > $stock){
              $cantidadProducto = $stock;
            }

            //Bandera
            $sw = false;

            //Recorremos el array de productos del carrito que está en caché
            for($i = 0; $i < count($this->sessionVal); $i++){
              if($this->sessionVal[$i]["presentacionid"] == $presentacionid){ //Si existe el ID en el arreglo de productos, se actualiza la cantidad comprada.
                $cantidadAux = $this->sessionVal[$i]['cantidadProducto'];
                $cantidadAux = $cantidadAux + $cantidadProducto;
                $this->sessionVal[$i]['cantidadProducto'] = $cantidadAux;
                $this->sessionVal[$i]['subTotalProducto'] = $cantidadAux * $precioProducto;
                $this->sessionVal[$i]['subTotalConCupon'] = $subTotalProducto = 0 ? $cantidadAux * $precioProducto : $subTotalProducto;
                $sw = true;

                break;
              }
            }

            if(!$sw){ //Si no existe el ID en el arreglo de productos, se añade el producto que se desea comprar.
              if(count($this->sessionVal) == 0){
                $this->sessionVal = array();
              }

              if(!$cupon){
                array_push($this->sessionVal, array(
                  'imagenProducto' => $imagen,
                  'nombreProducto' => $nombre,
                  'presentacionid' => $presentacionid,
                  'cantidadProducto' => $cantidadProducto,
                  'precioProducto' => $precioProducto,
                  'subTotalProducto' => $subTotalProducto,
                  'subTotalConCupon' => $subTotalProducto,
                  'idCupon' => NULL,
                  'stockProducto' => $stock
                ));
              }else{
                if ( $cupon[0]->getId() == $presentacion->getCupon()->getId()) {
                     array_push($this->sessionVal, array(
                      'imagenProducto' => $imagen,
                      'nombreProducto' => $nombre,
                      'presentacionid' => $presentacionid,
                      'cantidadProducto' => $cantidadProducto,
                      'precioProducto' => $precioProducto,
                      'subTotalProducto' => $subTotalProducto,
                      'subTotalConCupon' => $subTotalProducto,
                      'idCupon' => $cupon[0]->getId(),
                      'stockProducto' => $stock
                    ));
                }else{
                  array_push($this->sessionVal, array(
                  'imagenProducto' => $imagen,
                  'nombreProducto' => $nombre,
                  'presentacionid' => $presentacionid,
                  'cantidadProducto' => $cantidadProducto,
                  'precioProducto' => $precioProducto,
                  'subTotalProducto' => $subTotalProducto,
                  'subTotalConCupon' => $subTotalProducto,
                  'idCupon' => NULL,
                  'stockProducto' => $stock,

                ));
                }

              }
            }

            $session->set('carrito', $this->sessionVal);
          }
        }
      }

      return $this->sessionVal;

    }

    /**
     * @Route("/agregar-producto/", name="agregar-producto")
     */
    public function carritoaddAction(Request $request){
            dump("entra 1");

        $em = $this->getDoctrine();

        //Accedemos a la sesion actual.
        $session = $this->get('session');
        dump($session);
        //Obtenemos un hipotético idCupon que está en el pedido de la sesión actual.
        $cupon = $session->get('idCupon');

        //Obtenemos el carrito actual
        $carrito1 = $session->get('carrito');
        $carrito = (array) $carrito1;

        //Try catch
        try {


          //Obtenemos idProducto, precioProducto, cantidadProducto del producto que vamos a agregar al carrito.
          $presentacionid = $request->request->get('presentacionId');
          $ids_carrito = array_column($carrito, 'presentacionid');
          dump($ids_carrito);

        //$idProducto = $request->request->get('producto');
          $precioProducto = $request->request->get('precio');
          $cantidad_seleccionada = $request->request->get('cantidad');

          $cantidadProducto = (intval($cantidad_seleccionada) < 1) ? 1 : intval($cantidad_seleccionada);

          //Buscamos en la entidad Producto al producto que queremos agregar al carrito.
          $presentacion = $em->getRepository('AdminBundle:Presentacion')->findOneById($presentacionid);

          $stock_presentacion = (int)$presentacion->getStock();
          $id_presentacion = $presentacion->getId();

          //////Si el carrito esta vacio
          if ( $carrito == null  ) {
                dump("entra si es q el carrito esta vacio osea null");

              if($cupon){
                //Buscar si alguno de los productos agregados antes al pedido actual son de la misma categoria o subcategoria que éste nuevo que se quiere agregar. Si eso fuera cierto entonces ya no se aplica descuento porque ya se hizo antes ello. Si no fuera cierto entonces se busca para el producto que queremos agregar si éste, su categoria o subcategoria están asociados al cupón.
                $nuevoSubTotalProducto = $cantidadProducto * $precioProducto;
              }else{
                //Si no existe un cupón asociado al pedido actual simplemente el subTotal del producto que queremos agregar será la cantidad de éste por su precio unitario.
                $nuevoSubTotalProducto = $cantidadProducto * $precioProducto;
              }
              // $nuevoSubTotalProducto = $cantidadProducto * $precioProducto;
              $carritoProductosActual = $this->agregarEditarItem($session, $presentacionid, $precioProducto, $cantidadProducto, $nuevoSubTotalProducto);

              $rpta = array("state" => true, "data" => "correcto");

          }else {

              dump("entra aca");

                                //busca que exista la presentacion seleccionada
                            if (!in_array( $presentacionid , $ids_carrito)) {
                                dump("no existe aun ");

                                if($cupon){
                                  //Buscar si alguno de los productos agregados antes al pedido actual son de la misma categoria o subcategoria que éste nuevo que se quiere agregar. Si eso fuera cierto entonces ya no se aplica descuento porque ya se hizo antes ello. Si no fuera cierto entonces se busca para el producto que queremos agregar si éste, su categoria o subcategoria están asociados al cupón.
                                  $nuevoSubTotalProducto = $cantidadProducto * $precioProducto;
                                }else{
                                  //Si no existe un cupón asociado al pedido actual simplemente el subTotal del producto que queremos agregar será la cantidad de éste por su precio unitario.
                                  $nuevoSubTotalProducto = $cantidadProducto * $precioProducto;
                                }
                                // $nuevoSubTotalProducto = $cantidadProducto * $precioProducto;
                                $carritoProductosActual = $this->agregarEditarItem($session, $presentacionid, $precioProducto, $cantidadProducto, $nuevoSubTotalProducto);

                                $rpta = array("state" => true, "data" => "correcto");
                            }else {  /////Si no encuentra la presentacion entonces agrega la seleccionada
                                dump("si existe");

                                foreach ($carrito as $key => $value){

                                        $array_values =array_values($value);
                                         $presentacionid_carrito =  $array_values[2];

                                    if ( $presentacionid_carrito === $presentacionid  ) {

                                            dump("hay un match ");

                                          $cantidad_carrito = array_values($value);
                                          $cantidad_total= $array_values[3] + $cantidad_seleccionada;

                                                  if ($cantidad_total > $stock_presentacion) {
                                                      dump("no se puede agregar mas productos");
                                                         exit();
                                                      $rpta = array("state" => false, "data" => "No se puede agregar mas llego al tope del stock");

                                                  }else {
                                                        dump("si se puede agregar mas productos");

                                                      if($cupon){
                                                        //Buscar si alguno de los productos agregados antes al pedido actual son de la misma categoria o subcategoria que éste nuevo que se quiere agregar. Si eso fuera cierto entonces ya no se aplica descuento porque ya se hizo antes ello. Si no fuera cierto entonces se busca para el producto que queremos agregar si éste, su categoria o subcategoria están asociados al cupón.
                                                        $nuevoSubTotalProducto = $cantidadProducto * $precioProducto;
                                                      }else{
                                                        //Si no existe un cupón asociado al pedido actual simplemente el subTotal del producto que queremos agregar será la cantidad de éste por su precio unitario.
                                                        $nuevoSubTotalProducto = $cantidadProducto * $precioProducto;
                                                      }
                                                      // $nuevoSubTotalProducto = $cantidadProducto * $precioProducto;
                                                      $carritoProductosActual = $this->agregarEditarItem($session, $presentacionid, $precioProducto, $cantidadProducto, $nuevoSubTotalProducto);

                                                      $rpta = array("state" => true, "data" => "correcto");
                                                  }

                                                  break;

                                    }

                                }
                                dump("salio del for");
                            }

                //   //Cuando existe un cupón asociado al pedido actual se hace lo siguiente:
                $rpta = array("state" => true, "data" => "correcto"); //borrar esto

          }

            // dump("entra 3");
            // exit();
        }catch (\Exception $e) {
          $this->get('session')->getFlashBag()->add('error', $e->getMessage());
          $this->get('logger')->error($e->getMessage());
          $rpta = array("state" => false, "data" => "Incorrectooooooooo");
        }

            // dump("salio de todo");
            // exit();

        return new JsonResponse($rpta);
  }





    private function buscarProducto($producto, $carrito, $cupon){
      $em = $this->getDoctrine();
      $contador = 0;
      $subTotalProducto = 0;

      //1. Iteramos los productos que tenemos actualmente en el carrito
      for ($i = 0; $i < count($carrito); $i++) {
        //IdProducto del producto perteneciente al carrito
        $idProducto = $carrito[$i]["idProducto"];

        //Buscamos el producto cuyo idProducto fue encontrado en el listado de productos del carrito
        $productoEncontrado = $em->getRepository('AdminBundle:Producto')->findOneById($idProducto);

        //Si la subcategoria del producto que esta en el carrito en la posicion "i" NO es igual al del producto agregado en este momento al pedido actual se pasa a evaluar la categoria
        if($productoEncontrado->getSubcategoriaProd()->getId() == $producto->getSubcategoriaProd()->getId()){
          $contador++;
        }else{ //Si la categoria del producto que esta en el carrito en la posicion "i" NO es igual al del producto agregado en este momento al pedido actual entonces se retorna false.
          if($productoEncontrado->getSubcategoriaProd()->getCategoria()->getId() == $producto->getSubcategoriaProd()->getCategoria()->getId()){
            $contador++;
          }
        }
      }

      //2. Si el contador es igual a cero; es decir, la categoria o subcategoria del producto que quiero agregar actualmente no está asociado a ninguna categoria o subcategoria de productos agregados anteriormente, lo que se procede hacer es buscar para el producto que queremos agregar si éste, su categoria o subcategoria están asociados al cupón.
      if($contador == 0){
        //Si el cupon del producto que quiero agregar es igual al del cupon asociado al pedido
        if($producto->getCupon()->getId() == $cupon[0]->getId()){
          $subTotalProducto = ($cantidadProducto * $precioProducto) - $cupon[0]->getDescuento();
        }else{
          //Si el cupon de la subcategoria del producto que quiero agregar es igual al del cupon asociado al pedido
          if($producto->getSubcategoriaProd()->getCupon()->getId() == $cupon[0]->getId()){
            $subTotalProducto = ($cantidadProducto * $precioProducto);
          }else{
            //Si el cupon de la categoria del producto que quiero agregar es igual al del cupon asociado al pedido
            if($producto->getSubcategoriaProd()->getCategoria()->getCupon()->getId() == $cupon[0]->getId()){
              $subTotalProducto = ($cantidadProducto * $precioProducto);
            }else{
            //Si ningun cupon está asociado al producto que quiero agregar, entonces el subtotal es el precio calculado como se hace normalmente: precio * cantidad
              $subTotalProducto = $cantidadProducto * $precioProducto;
            }
          }
        }
      }

      return $subTotalProducto;
    }

    /**
     * @Route("/conocenos/", name="conocenos")
     * @Template()
     */
    public function conocenosAction(){
      $em = $this->getDoctrine();

      $this->locals['conocenos'] = $em->getRepository('AdminBundle:Info')->findAll();

      return $this->locals;
    }

    /**
     * @Route("/revisar-y-pagar/", name="facturacion")
     * @Method({"POST"})
     */
    public function facturacionpostAction(Request $request){
        $em = $this->getDoctrine();
        $session = $this->get('session');
        $cliente = $session->get('idCliente');
        $tipo = $request->request->get('tipo');
        $cuponID = $request->request->get('cupon');
        $totalCarrito = $request->request->get('total_carrito');
        $descuentoCarrito = $request->request->get('descuento_carrito');

        if ($tipo == 'boleta') {
            $ruc = null;
        }else{
            $ruc = $request->request->get('ruc');
        }

         $this->locals['facturacion'] = [
              $request->request->get('nombres'),
              $request->request->get('email'),
              $request->request->get('telefono'),
              $request->request->get('departamento'),
              $request->request->get('provincia'),
              $request->request->get('distrito'),
              $request->request->get('tipo'),
              $request->request->get('direccion'),
              $ruc,
              $request->request->get('dni'),
         ];

          $misma_direccion = $request->request->get('misma_direccion');

          if ($misma_direccion == 'on') {
              $nombre = null;
              $email = null;
              $telefono = null;
              $departamento = null;
              $provincia = null;
              $distrito = null;
              $direccion = null;
          }else{
              $nombre = $request->request->get('nombres2');
              $email = $request->request->get('email2');
              $telefono = $request->request->get('telefono2');
              $departamento = $request->request->get('departamento2');
              $provincia = $request->request->get('provincia2');
              $distrito = $request->request->get('distrito2');
              $direccion = $request->request->get('direccion2');

          }

          $this->locals['envio'] = [
              $nombre,
              $email,
              $telefono,
              $departamento,
              $provincia,
              $distrito,
              $direccion,
              $cuponID,
              $totalCarrito,
              $descuentoCarrito
          ];
              // dump($this->locals['envio']);
              // exit();
          //exit();
        $session = $this->get('session');
        $idCliente = $session->get('idCliente');

        $this->locals['carrito_facturacion'] = $em->getRepository('AdminBundle:Carrito')->findBy(array('cliente' => $idCliente , 'estado' => 'P'));
        // dump($this->locals['carrito_facturacion']);
        // exit();
        return $this->render('WebBundle:Default:listapedidos.html.twig', array('carrito_facturacion' => $this->locals['carrito_facturacion'] , 'envio' =>  $this->locals['envio'] , 'facturacion' =>  $this->locals['facturacion']));
    }

    /**
     * @Route("/eliminar-producto/", name="eliminar-producto")
     */
    public function carritoremoveAction(Request $request){

      $prodId = $request->request->get("prodId");
      $sw = $request->request->get("sw");
      $conCupon = $request->request->get("conCupon");
      $session = $this->get('session');
      $idCliente = $session->get('idCliente');
      $cupon = $session->get('idCupon');

      if ($prodId) {
        if(!$idCliente){ //No está en una sesión, por ende la eliminación es de lo que está en caché
          $arrayProductos = $session->get('carrito');

          $em = $this->getDoctrine();
          $producto = $em->getRepository('AdminBundle:Presentacion')->findOneById($prodId);

          //Si existe un cupón
          if ($cupon) {
            if ($producto->getProductoPre()->getCupon()->getId() == $cupon[0]->getId()) {
              $session->remove('idCupon');
            }
          }

          for ($i = 0; $i < count($arrayProductos); $i++) {
            if($arrayProductos[$i]["idCupon"] == $producto->getProductoPre()->getCupon()->getId() && $conCupon != 0){

            if ($arrayProductos[$i]['presentacionid'] == $producto->getId()) {
                $conCupon = $conCupon - 1;
                    if ($conCupon == 0) {
                       $session->remove('idCupon');
                    }
                unset($arrayProductos[$i]);
                $this->sessionVal = array_values($arrayProductos);
                $session->set('carrito', $this->sessionVal);
              }
            }else{
              if ($arrayProductos[$i]['presentacionid'] == $producto->getId()) {
                unset($arrayProductos[$i]);
                $this->sessionVal = array_values($arrayProductos);
                $session->set('carrito', $this->sessionVal);
              }

            }
          }
        }
        else{ //Estoy en una sesión de usuario, por lo tanto lo que se elimine se hace de BD

          $em = $this->getDoctrine()->getManager();
          $request = $em->getRepository('AdminBundle:Carrito');

          $query = $request->createQueryBuilder("C");
          $query
          ->select('C')
          ->innerJoin('C.presentacion', 'P')
          ->innerJoin('C.cliente', 'Cl')
          ->where('Cl.id = :idCliente and P.id = :idProducto and C.estado = :estado')
          ->setParameter('idCliente', $idCliente)
          ->setParameter('idProducto', $prodId)
          ->setParameter('estado', 'P');

          $result = $query->getQuery();
          $detalleId = $result->getResult();
          $productoCarritoID = $detalleId[0]->getId();

          $itemCarrito = $em->getRepository('AdminBundle:Carrito')->findOneById($productoCarritoID);
          $em->remove($itemCarrito);
          $em->flush();
        }

        //Vemos si habia algun cupón en caché. Si param es igual a 1 entonces se elimina de caché, sino se mantiene.
        // if($sw == 1){
        //   $session->remove('idCupon');
        // }
      }
      // return new JsonResponse(array("state" => true, "data" => $cupon[0]->getId()));

       return new JsonResponse(array("state" => true, "data" => $prodId));
    }

    /**
     * @Route("/fill-ubigeo/", name="fill-ubigeo")
     */
    /*public function fillUbigeoAction(Request $request){
      $em = $this->getDoctrine();
      //set_time_limit(120);

      $jsonData = file_get_contents("../ubigeo.json");
      $phpArray = json_decode($jsonData, true);

      foreach ($phpArray as $key => $value) {
        $pk = $value["pk"];
        $model = $value["model"];
        $nombre = $value["fields"]["nombre"];
        $codigo = $value["fields"]["codigo"];

        if($model === "ubigeo.departamento"){
          $entity = new Departamento();
        }

        if($model === "ubigeo.provincia"){
          $entity = new Provincia();
        }

        if($model === "ubigeo.distrito"){
          $entity = new Distrito();
        }

        $entity->setId($pk);
        $entity->setCodigo($codigo);
        $entity->setNombre($nombre);

        if($model === "ubigeo.provincia"){
          $dpto = $value["fields"]["departamento"];
          $instanceOfDpto = $em->getRepository('AdminBundle:Departamento')->findOneById($dpto);
          $entity->setDepartamento($instanceOfDpto);
        }

        if($model === "ubigeo.distrito"){
          $provincia = $value["fields"]["provincia"];
          $instanceOfProv = $em->getRepository('AdminBundle:Provincia')->findOneById($provincia);
          $entity->setProvincia($instanceOfProv);
        }

        //insercion en BD
        $manager = $em->getManager();
        $manager->persist($entity);
        $manager->flush();
      }

      return new JsonResponse(array("state" => true, "data" => "Correcto"));
    }*/

    /**
     * @Route("/carrito-compras/", name="carrito-compras")
     * @Method({"GET"})
     * @Template()
     */
    public function carritocomprasAction(Request $request){

      $em = $this->getDoctrine();
      $em2 = $em->getManager();

      $session = $this->get('session');

      $clienteSesionActual = $session->get('idCliente');

      //Si está logueado, debe listar los productos del carrito que ya están en BD como estado pendiente + alerta inicial de productos que hata tenido antes en el carrito y que aun no ha comprado

      if($clienteSesionActual){

        //Se consulta a BD por el carrito del cliente logueado.
        $request = $em2->getRepository('AdminBundle:Carrito');

        $query = $request->createQueryBuilder("C");
        $query
        ->select('Pre.id as presentacionid, Pre.imagenes as imagenProducto, Pre.presentacion as nombreProducto , Pre.stock as stockProducto, Cu.id as idCupon , C.cantidad as cantidadProducto, C.precioUnitario as precioProducto, C.precioReal as subTotalProducto')
        ->innerJoin('C.presentacion', 'Pre')
        ->innerJoin('Pre.productoPre', 'Pro')
        ->innerJoin('Pro.cupon', 'Cu')
        ->innerJoin('C.cliente', 'Cl')
        ->where('Cl.id = :id AND C.estado != :estado')
        ->setParameter('id', $clienteSesionActual)
        ->setParameter('estado' , 'C');

        $result = $query->getQuery();

        $carritoProductosActual = $result->getResult();

      }else{ //Si no está logueado, debe listar los productos almacenados en caché.
        $carritoProductosActual = $session->get('carrito');
      }

      $this->locals['carritoProductosActual'] = $carritoProductosActual;

      return $this->locals;
    }

    /**
     * @Route("/revisar-y-pagar/", name="revisar-y-pagar")
     * @Method({"GET"})
     * @Template()
     */
    public function listapedidosAction(Request $request){

      $em = $this->getDoctrine();
      $em2 = $em->getManager();

      $session = $this->get('session');
      $clienteSesionActual = $session->get('idCliente');

      if ($clienteSesionActual || !$clienteSesionActual) {
          return $this->redirect('/datos-facturacion');
      }
      //Si está logueado, debe listar los productos del carrito que ya están en BD como estado pendiente.

    }

    /**
     * @Route("/login/user/", name="login-user")
     */
    public function userloginAction(Request $request){
            // dump("sdfsdf");
            // exit;
        $nombre = $request->request->get('usuario');
        $password = $request->request->get('password');
            // "nombre" ---> es lo mismo que "email"
        $em = $this->getDoctrine();
        $em2 = $em->getManager();

        $request = $em->getRepository('AdminBundle:Clientes');

        $query = $request->createQueryBuilder('c');
        $query
            ->select('c.id, c.nombres, c.email , c.password, c.dni')
            ->where('c.email = :email')
            ->setParameter('email', $nombre);

        $result = $query->getQuery();
        // dump($this->get('session'));
        // exit();
        $clienteConsultado = $result->getResult();


        if ($clienteConsultado) {
            if (password_verify($password, $clienteConsultado[0]['password'])) {
              $valor = true;

              $session = $this->get('session');
              $carritoOverSession = $session->get('carrito');

               $cupon = $session->get('idCupon');
              //$session = new Session();
              //$session->start();

              //Se asignan los datos de la sesión del usuario en caché.
              $session->set('idCliente', $clienteConsultado[0]['id']);
              $session->set('nombreCliente', $clienteConsultado[0]['nombres']);
              $session->set('correoCliente', $clienteConsultado[0]['email']);
              $session->set('dniCliente', $clienteConsultado[0]['dni']);

              //Registro de productos de carrito de compra (si es que existen) en base de datos para el usuario que se loguea.
              if($carritoOverSession){
                $totalProductos = count($carritoOverSession);
                $idClienteSesion = $session->get('idCliente');

                for ($i = 0; $i < $totalProductos; $i++) {
                    //Si agrega un producto cuando no haya iniciado sesion y ya lo tiene de antes en su sesion iniciada, entonces se le añadirá la nueva cantidad a la cantidad que tiene actualmente para ese producto
                    $presentacionid = $carritoOverSession[$i]["presentacionid"];
                    $stock = (int)$carritoOverSession[$i]["stockProducto"];
                    // dump($stock);
                    // exit();
                    $request = $em2->getRepository('AdminBundle:Carrito');

                    $query = $request->createQueryBuilder("C");
                    $query
                    ->select('C.id as itemCarritoId, C.cantidad as cantidadProducto, P.stock as stock')
                    ->innerJoin('C.presentacion', 'P')
                    ->innerJoin('C.cliente', 'Cl')
                    ->where('Cl.id = :idCliente and C.estado = :estado and P.id = :presentacionid')
                    ->setParameter('idCliente', $idClienteSesion)
                    ->setParameter('estado', 'P')
                    ->setParameter('presentacionid', $presentacionid);
                    $result = $query->getQuery();
                    $productoCarrito = $result->getOneOrNullResult();

                    $existe = count($productoCarrito);

                    $cantidadProducto = $carritoOverSession[$i]["cantidadProducto"];
                    $precioProducto = $carritoOverSession[$i]["precioProducto"];
                     $stockpresentacion = (int)$productoCarrito["stock"];
                    //  dump($stockpresentacion);
                    //  exit();

                    if($existe > 0){ //Si el producto ya existe previamente solamente se actualiza la cantidad que se desea comprar.
                      $productoCantidad = $productoCarrito["cantidadProducto"];
                      $itemCarritoId = $productoCarrito["itemCarritoId"];
                       $stockpresentacion = (int)$productoCarrito["stock"];

                      $cantidadProducto = (intval($cantidadProducto)<1) ? 1 : intval($cantidadProducto);
                      $cantidadProducto = $cantidadProducto + $productoCantidad;

                      if ($cantidadProducto <= $stock ) {
                          $subTotalProducto = $cantidadProducto * $precioProducto;

                          $itemCarrito = $em->getRepository('AdminBundle:Carrito')->findOneById($itemCarritoId);

                          $itemCarrito->setCantidad($cantidadProducto);
                          $itemCarrito->setPrecioReal($subTotalProducto);

                          $em2->flush();
                      }

                    }else{ //Si el producto aun no existe entonces se inserta en la BD.
                      $objCarrito = new Carrito();

                      $subTotalProducto = $carritoOverSession[$i]["subTotalProducto"];

                      $objCarrito->setCantidad($cantidadProducto);
                      $objCarrito->setPrecioReal($subTotalProducto);
                      $objCarrito->setEstado('P');

                      $instanceOfCliente = $em->getRepository('AdminBundle:Clientes')->findOneById($idClienteSesion);
                      $objCarrito->setCliente($instanceOfCliente);

                      $instanceOfProducto = $em->getRepository('AdminBundle:Presentacion')->findOneById($presentacionid);
                      $objCarrito->setPresentacion($instanceOfProducto);
                      $objCarrito->setPrecioUnitario($precioProducto);

                      $manager = $em->getManager();
                      $manager->persist($objCarrito);
                      $manager->flush();
                    }
                }
                //Se borra de caché el carrito guardado si es que existe alguno. Esto se hace debido a que una vez que el usuario se loguea, los productos que haya puesto en el carrito van a base de datos como estado pendiente.
                $session->remove('carrito');
              }
            }else{
              $valor = false;
            }
        }else{
          $valor = false;
        }

        return new JsonResponse($valor);
    }

    /**
     * @Route("/cerrar-sesion/", name="cerrar-sesion")
     */
    public function logoutAction(Request $request){
      $session = $this->get('session');
      $session->remove('idCliente');
      $session->remove('nombreCliente');
      $session->remove('correoCliente');
      $session->remove('dniCliente');
      $session->remove('idCupon');

      return new JsonResponse(array("state" => true, "data" => "/")); //Debe redirigir al HOME
    }

    /**
     * @Route("/login/", name="login_post")
     * @Method({"POST"})
     */
    public function loginpostAction(Request $request){
      $em = $this->getDoctrine();
      $nombre = $request->request->get("nombres");
      $fecha = $request->request->get("fecha_nacimiento");

      try {
          $entity = new Clientes();
          $entity->setNombres($request->request->get("nombres"));
          $entity->setTelefono($request->request->get("telefono"));
          $entity->setEmail($request->request->get("email"));
          $entity->setDni($request->request->get("dni"));
          $entity->setFechaNacimiento($request->request->get("fecha_nacimiento"));
          $entity->setDireccion($request->request->get("adasdasd"));
          $password = $request->request->get('password');
          $confirm_password = $request->request->get('password_confirm');

          if($password != $confirm_password){
              $error = "Error de contraseña";

              return $this->render('WebBundle:login.html.twig', array('error'=>$error));
          }

          $options = [
            'cost' => 11,
        ];
          //cost para incrementar el tamaño de la password
          //$hashed = password_hash($password, PASSWORD_BCRYPT, $options);
          $hashed = password_hash($password, PASSWORD_DEFAULT);
          $entity->setPassword($hashed);
          $manager = $em->getManager();
          $manager->persist($entity);
          $manager->flush();
          $this->addFlash('success', 'Se ha registrado correctamente');
      } catch (\Exception $e) {
          $this->get('session')->getFlashBag()->add('error', $e->getMessage());
          $this->get('logger')->error($e->getMessage());
      }

      return $this->redirect('/login/');
    }

    /**
     * @Route("/promociones/", name="promociones")
     * @Method({"GET"})
     * @Template()
     */
    public function promocionesAction(){
      $em = $this->getDoctrine();
      $this->locals['promociones'] = $em->getRepository('AdminBundle:Promocion')->findAll();

      return $this->locals;
    }

    /**
     * @Route("/promociones/", name="promociones_post")
     * @Method({"POST"})
     */
    public function promocionespostAction(Request $request){
      $em = $this->getDoctrine();
      $idpromo = $request->request->get('idpromo');
      $promocion = $em->getRepository('AdminBundle:Promocion')->findOneById($idpromo);
      try {
          $entity = new FormularioPromocion();
          $entity->setPromocion($promocion);
          $entity->setNombres($request->request->get("nombres"));
          $entity->setEmail($request->request->get("email"));
          $entity->setTelefono($request->request->get("telefono"));

          $manager = $em->getManager();
          $manager->persist($entity);
          $manager->flush();
          $this->addFlash('success', 'Se ha registrado correctamente');
      } catch (\Exception $e) {
          $this->get('session')->getFlashBag()->add('error', $e->getMessage());
          $this->get('logger')->error($e->getMessage());
      }

      return $this->render('WebBundle:Default:gracias.html.twig');
    }

    /**
     * @Route("/locales/", name="locales")
     * @Template()
     */
    public function localesAction(){
          $em = $this->getDoctrine();
          $this->locals['locales'] = $em->getRepository('AdminBundle:Local')->findAll();

          return $this->locals;
    }

    /**
     * @Route("/categorias/{slug2}/{slug}/{slug3}", name="productos_slug")
     * @Template()
     */
    public function presentacionAction(Request $request){
        try {

            $em = $this->getDoctrine();


            $presentacionid = $request->request->get('colorID');
            // dump($presentacionid);
            // exit();
            parse_str($presentacionid, $string);

            $repository = $em->getRepository('AdminBundle:Presentacion');

            $presentacion = $repository->findOneById($presentacionid);


             $this->locals['presentacion'] = $presentacion;

            return $this->locals;

        } catch (\Exception $e) {
            $response = array('state' => false, 'data' => $e->getMessage());
        }

            return new JsonResponse($response);

    }

    /**
     * @Route("/buscar-stock/", name="buscar-stock")
     * @Template()
     */
    public function pollFunction(Request $request){

            $presentacionid = $request->request->get("presentacionId");

            $em = $this->getDoctrine();

            try {

                $repository = $this->getDoctrine()->getRepository('AdminBundle:Presentacion');

                $presentacion = $repository->findOneById($presentacionid);

                if ($presentacion) {

                    $response = array('state' => true, 'data' => (int)$presentacion->getStock());

                }

            } catch (Exception $e) {

                $response = array('state' => false, 'data' => $e->getMessage());
            }

            return new JsonResponse($response);

    }




    /**
     * @Route("/categorias/{slug2}/{slug}/{slug3}", name="productos_slug")
     * @Template()
     */
    public function productosAction($slug = null,$slug2 = null, $slug3 = null ){
        // dump("entro");
        // exit();
      $em = $this->getDoctrine();

      //$this->locals['categorias'] = $em->getRepository('AdminBundle:Categoria')->findAll();

      $request = $em->getRepository('AdminBundle:Categoria');
      $query = $request->createQueryBuilder('c');
      $query
          ->select('c')
          ->where('c.slug != :slug')
          ->setParameter('slug', 'otros');
      $result = $query->getQuery();
      $this->locals['categorias'] = $result->getResult();


      $this->locals['subcategorias'] = $em->getRepository('AdminBundle:Subcategoria')->findAll();
      $this->locals['productos'] = $em->getRepository('AdminBundle:Producto')->findBySubcategoriaProd($slug, array() ,3 , 3);

      if($slug3 != null){

      $em = $this->getDoctrine();

      $producto1 = $em->getRepository('AdminBundle:Producto')->findOneBySlug($slug3);
      $this->locals['detalle'] = $producto1;

      $presentaciones = $em->getRepository('AdminBundle:Presentacion')->findBy(
          array('productoPre' => $producto1->getId())
      );

                    ///Busca solamente uno xD
      $presentacion = $em->getRepository('AdminBundle:Presentacion')->findOneBy( array('productoPre' => $producto1->getId()) );

      $this->locals['presentaciones'] = $presentaciones;
      $this->locals['presentacion'] = $presentacion;
      $request = $em->getRepository('AdminBundle:Producto');

      //obtengo id de la subcategoria asignada al producto
      $querys = $request->createQueryBuilder('s');
      $querys
          ->select('s')
          ->where('s.slug = :slug')
          ->setParameter('slug', $slug3);

      $id = $querys->getQuery();
      $re = $id->getResult();

      //obtengo el id del producto

      $prod_id = $request->createQueryBuilder('m');
      $prod_id
          ->select('m.id')
          ->where('m.slug = :slug')
          ->setParameter('slug', $slug3);

      $prod = $prod_id->getQuery();
      $res = $prod->getResult();

      $query = $request->createQueryBuilder('p');
      $query
          ->select('p')
          ->where('p.slug != :slug AND p.subcategoriaProd = :id')
          ->setParameter('slug', $slug3)
          ->setParameter('id', $re[0]->getSubcategoriaProd()->getId())
          ->setMaxResults(3);

      $result = $query->getQuery();
      $this->locals['relacionados'] = $result->getResult();


      //obtengo puntuacion del producto
      $ranking = $em->getRepository('AdminBundle:Ranking');

      $query_ranking = $ranking->createQueryBuilder('R');
      $query_ranking
          ->select('R')
          ->where('R.producto = :id')
          ->setParameter('id', $re);

      $result_rank = $query_ranking->getQuery();
      $this->locals['puntuacion'] = $result_rank->getResult();

       return $this->render('WebBundle:Default:productosdetalle.html.twig', $this->locals);
      }else{

        // este slug es para las subcategorias

        if ($slug != null) {
        //   $request = $em->getRepository('AdminBundle:Producto');
          //
        //   $query = $request->createQueryBuilder('P');
        //   $query
        //       ->select('P')
        //       ->innerJoin('P.subcategoriaProd' , 'S')
        //       ->where('S.slug = :slug')
        //       ->setParameter('slug', $slug)
        //       ->setMaxResults(6)
        //       ->orderBy('P.id', 'ASC');
          //
        //   $result = $query->getQuery();
        $request = $em->getRepository('AdminBundle:Producto');

        $query = $request->createQueryBuilder('P');
        $query
            ->select('P')
            ->innerJoin('P.subcategoriaProd', 'S')
            ->where('S.slug = :slug')
            ->setParameter('slug', $slug)
            ->orderBy('P.id', 'asc');

        $result = $query->getQuery();

        $products  = $result->getResult();


        $array_productos = array();
        $i = 1;

        foreach ($products as $key => $product) {

              $presentations = $product->getPresentacion();

              if ($presentations) {
                      $cantidad = 0;
                  foreach ($presentations as $key1 => $presentation)
                  {
                      if ($presentation->getStock() > 0) {
                          $cantidad++;
                      }

                  }

                  if ($cantidad > 0) {
                      array_push($array_productos, $product);
                  }
              }
        }
          $this->locals['apendice'] =  array_slice($array_productos, 0 , 3);

          //dump( $this->locals['apendice'][0]->getProducto() );
          //exit();

        }elseif ($slug2 != null && $slug == null) {

//////////////////////////////////////////////// Aca empieza esta  shit !!!
          $request = $em->getRepository('AdminBundle:Producto');

          $query = $request->createQueryBuilder('P');
          $query
              ->select('P')
              ->innerJoin('P.subcategoriaProd', 'S')
              ->innerJoin('S.categoria' , 'C')
              ->where('C.slug = :slug')
              ->setParameter('slug', $slug2)
              ->orderBy('P.id', 'asc');

          $result = $query->getQuery();

          $products  = $result->getResult();


          $array_productos = array();
          $i = 1;

          foreach ($products as $key => $product) {

                $presentations = $product->getPresentacion();

                if ($presentations) {
                        $cantidad = 0;
                    foreach ($presentations as $key1 => $presentation)
                    {
                        if ($presentation->getStock() > 0) {
                            $cantidad++;
                        }

                    }

                    if ($cantidad > 0) {
                        array_push($array_productos, $product);
                    }
                }
          }
            //   dump(count($products));
        //   dump(count($array_productos));
        //   dump(count(array_slice($array_productos, 0 , 3)));
        //   exit();
/////////////////////////////////////////////////////////////// aca termina una super shit
          $this->locals['categories'] = array_slice($array_productos, 0 , 3);

          $session = $this->get('session');
          $session_busqueda = $session->get('rBusqueda');

          if ($session_busqueda || $session_busqueda == null) {
            $session->remove('rBusqueda');
          }

        }

        return $this->locals;
    }
  }

    // *
    //  * @Route("/libro-reclamaciones/", name="libro-reclamaciones")
    //  * @Method({"GET"})
    //  * @Template()

    // public function libroreclamacionesAction(){
    // }

    // /**
    //  * @Route("/libro-reclamaciones/", name="libro-reclamaciones-post")
    //  * @Method({"POST"})
    //  */
    // public function libroreclamacionespostAction(Request $request){
    //   $em = $this->getDoctrine();
    //   $reclamo = $request->request->get("reclamo1");
    //   $reclamo2 = $request->request->get("reclamo2");

    //   $tipo = "Queja";
    //   $distrito = $request->request->get("distrito");
    //   $instanceOfDistrito = $em->getRepository('AdminBundle:Distrito')->findOneById($distrito);

    //   if($reclamo == "on"){
    //     $tipo = "Reclamo";
    //   }

    //   try {
    //       $entity = new Reclamo();
    //       $entity->setTipoDocumentoReclamante($request->request->get("tipodoc"));
    //       $entity->setNumeroDocumentoReclamante($request->request->get("numdoc"));
    //       $entity->setNombresReclamante($request->request->get("nombre"));
    //       $entity->setApellidosReclamante($request->request->get("apellido"));
    //       $entity->setTelefonoReclamante($request->request->get("telefono"));
    //       $entity->setEmailReclamante($request->request->get("email"));
    //       $entity->setDistrito($instanceOfDistrito);
    //       $entity->setDireccion($request->request->get("direccion"));
    //       $entity->setTipoDocumentoApoderado($request->request->get("tipodocApoderado"));
    //       $entity->setNumeroDocumentoApoderado($request->request->get("documentoApoderado"));
    //       $entity->setNombresApoderado($request->request->get("nombreApoderado"));
    //       $entity->setApellidosApoderado($request->request->get("apellidoApoderado"));
    //       $entity->setTipoReclamo($tipo);
    //       $entity->setMensaje($request->request->get("mensaje"));

    //       $manager = $em->getManager();
    //       $manager->persist($entity);
    //       $manager->flush();
    //   } catch (\Exception $e) {
    //       $this->get('session')->getFlashBag()->add('error', $e->getMessage());
    //       $this->get('logger')->error($e->getMessage());
    //   }

    //   return $this->render('WebBundle:Default:gracias.html.twig');
    // }

    /**
     * @Route("/contacto/", name="contacto")
     * @Method({"GET"})
     * @Template()
     */
    public function contactoAction(){
    }

    /**
     * @Route("/contacto/", name="contacto_post")
     * @Method({"POST"})
     */
    public function contactopostAction(Request $request){
      $em = $this->getDoctrine();

      $this->locals['detalle'] = $em->getRepository('AdminBundle:Info')->find(1);

      $correosContacto = explode(",", $this->locals['detalle']->getCorreosContacto());

      try {
          $entity = new Contacto();
          $entity->setNombres($request->request->get("nombres"));
          $entity->setEmail($request->request->get("email"));
          $entity->setTelefono($request->request->get("telefono"));

          $instanceOfProv = $em->getRepository('AdminBundle:Provincia')->findOneById($request->request->get("provincia"));
          $entity->setProvincia($instanceOfProv);
          $entity->setMensaje($request->request->get("mensaje"));
          $fecha = new \DateTime();

          $manager = $em->getManager();
          $manager->persist($entity);
          $manager->flush();
      } catch (\Exception $e) {
          $this->get('session')->getFlashBag()->add('error', $e->getMessage());
          $this->get('logger')->error($e->getMessage());
      }

      $message = \Swift_Message::newInstance()
         ->setSubject('Mensaje de contacto')
         ->setFrom(array('noreply-lifeit@staffcreativa.com' => 'Staff Creativa'))
         ->setTo($correosContacto)
         ->setBody(
             $this->renderView('WebBundle:Emails:FormContacto.html.twig',
                 array(
                     'nombres' => $request->request->get('nombres'),
                     'email' => $request->request->get('email'),
                     'telefono' => $request->request->get('telefono'),
                     'provincia' => $instanceOfProv->getNombre(),
                     'mensaje' => $request->request->get('mensaje'),
                     'fecha' => date_format($fecha, 'd-m-Y H:i:s'),
                     'correos' => $correosContacto,
                 )
             ),
             'text/html'
         );

        $this->get('mailer')->send($message);

        return $this->render('WebBundle:Default:gracias.html.twig');
    }

    /**
     * @Route("/bolsa-trabajo/", name="bolsa-trabajo")
     * @Method({"GET"})
     * @Template()
     */
    public function bolsatrabajoAction(){

    }

    /**
     * @Route("/bolsa-trabajo/", name="bolsa-trabajo-post")
     * @Method({"POST"})
     */
    public function bolsatrabajopostAction(Request $request){
      $em = $this->getDoctrine();

      try {
        $entity = new BolsaTrabajo();
        $entity->setNombres($request->request->get("nombres"));
        $entity->setDocumento($request->request->get("documento"));

        $instanceOfProv = $em->getRepository('AdminBundle:Provincia')->findOneById($request->request->get("provincia"));
        $entity->setProvincia($instanceOfProv);
        $entity->setEmail($request->request->get("email"));
        $entity->setTelefono($request->request->get("telefono"));

        $file = $request->files->get('cv');
        $ext = $file->guessExtension();
        $file_name = time().".".$ext;
        $file->move("uploads" , $file_name);
        $ruta = "uploads"."/".$file_name;

        $entity->setCv($file_name);
        $entity->setRutaCv($ruta);

        $query = $em->getRepository('AdminBundle:BolsaTrabajo');
        $query = $query->createQueryBuilder('b')
        ->where('b.email = :email or b.documento = :documento')
        ->setParameter('email', $entity->getEmail())
        ->setParameter('documento', $entity->getDocumento())
        ->getQuery();
        $total_filas = count($query->getResult());

        if($total_filas == 0){ //Aun no esta registrado en la bolsa de trabajo
          $manager = $em->getManager();
          $manager->persist($entity);
          $manager->flush();

          //$this->addFlash('success', 'Se ha registrado correctamente');
          $renderPage = "WebBundle:Default:gracias.html.twig";
        }else{
          $this->addFlash('warning', 'Ya se ha registrado anteriormente');
          $renderPage = "WebBundle:Default:bolsatrabajo.html.twig";
        }
      } catch (\Exception $e) {
        $this->get('session')->getFlashBag()->add('error', $e->getMessage());
        $this->get('logger')->error($e->getMessage());
        $this->addFlash('error', $e->getMessage());
      }
      //return $this->redirectToRoute('bolsa-trabajo');
      return $this->render($renderPage);
    }

    /**
     * @Route("/preguntas-frecuentes/", name="preguntas-frecuentes")
     * @Template()
     */
    public function preguntasfrecuentesAction(){
      $em = $this->getDoctrine();
      $this->locals['preguntas'] = $em->getRepository('AdminBundle:PreguntaFrecuente')->findAll();

      return $this->locals;
    }

    /**
     * @Route("/terminos-condiciones/", name="terminos-condiciones")
     *
     * @Template()
     */
    public function terminoscondicionesAction(){
      $em = $this->getDoctrine();
      $this->locals['info'] = $em->getRepository('AdminBundle:Info')->find(1);

      return $this->locals;
    }

    /**
     * @Route("/terminos-condiciones-delivery/", name="condiciones-delivery")
     * @Template()
     */
    public function terminoscondicionesDeliveryAction(){

      $em = $this->getDoctrine();
      $this->locals['info'] = $em->getRepository('AdminBundle:Info')->find(1);

      return $this->locals;
    }

    /**
     * @Route("/privacidad-seguridad/", name="privacidad-seguridad")
     * @Template()
     */
    public function privacidadseguridadAction(){
    }

    /**
     * @Route("/datos-facturacion/", name="datos-facturacion")
     * @Template()
     */
    public function datosfacturacionAction(){

      $session = $this->get('session');
      $idCliente = $session->get('idCliente');


      if ($session->get('idCliente') ) {

          //Si aún no está logueado
          if(!$idCliente){
            return $this->redirect("/login/");
          }
          return $this->locals;
      }else{
         return $this->redirect('/login');
      }

      $em = $this->getDoctrine();

      //Si aún no está logueado
      if(!$idCliente){
        return $this->redirect("/login/");
      } else{ //Si está logueado
        $elementosCarrito = count($em->getRepository('AdminBundle:Carrito')->findBy(
          array("cliente" => $idCliente, "estado" => "P") //where
        ));

        if($elementosCarrito == 0){
          return $this->redirect("/carrito-compras/");
        }
      }
      return $this->locals;
    }

    /**
     * @Route("/suscribete/", name="suscribete")
     */
    public function suscribeteAction(Request $request){
      $em = $this->getDoctrine();

      try {
           $entity = new Suscripcion();

           $entity->setEmail($request->request->get('txtemail'));

            $query = $em->getRepository('AdminBundle:Suscripcion');
            $query = $query->createQueryBuilder('s')
                    ->where('s.email = :email')
                    ->setParameter('email', $entity->getEmail())
                    ->getQuery();
            $total_filas = count($query->getResult());

            if($total_filas == 0){ //Aun no esta suscrito
                $manager = $em->getManager();
                $manager->persist($entity);
                $manager->flush();
                $result = array('state'=>true, 'data'=>'Se ha suscrito correctamente');
            }else{
                $result = array('state'=>true, 'data'=>'Ya se ha suscrito anteriormente');
            }
       } catch (\Exception $e) {
           $result = array('state'=>false, 'data'=>'Error durante la suscripción, intente de nuevo');
       }

       return new JsonResponse($result);
    }

    /**
     * @Route("/parse-fb-api/", name="parsefbapi")
     */
    public function parsefbapiAction(Request $request){
        //Conexion con Doctrine ORM

        $em = $this->getDoctrine();
        $em2 = $this->getDoctrine()->getManager();

        //Entidad INFO
        $infoAux = $this->getDoctrine()->getRepository('AdminBundle:Info')->find(1);

        try {
            //Entidad FBPOSTS
            $fbPosts = $this->getDoctrine()->getRepository('AdminBundle:FbPosts')->findAll();
            $totalFbPosts = count($fbPosts);

            $appToken = $infoAux->getAppTokenFacebook(); //Cuando entre a producción debe cambiarse por el de Real Plaza
            $fields = "reactions.type(LOVE).summary(true),message,full_picture,link,shares,likes.summary(true),comments.summary(true)";
            $totalPublicaciones = $infoAux->getNumeroPublicacionesFacebook();
            $fanPage = $infoAux->getFanPageFacebook();
            //$fbId = $infoAux->getIdUsuarioFacebook(); //De Real Plaza
            $WsUrl = $infoAux->getUrlWsFacebook();
            $client = new Client($WsUrl);
            $requestWs = $client->createRequest();

            //$this->doctrine->resetManager();
            $requestWs->setPath("/".$fanPage."/posts");
            //$requestWs->setPath("/".$fbId."/photos/uploaded");

            $requestWs->getQuery()
                ->set('fields', $fields)
                ->set('limit', $totalPublicaciones)
                ->set('access_token', $appToken);

            $response = $requestWs->send();
            $body = $response->getBody(true);
            $bodyArray = json_decode($body);

            $datos = $bodyArray->data;
            //$arregloPublicaciones = array();

            if($totalFbPosts > 0){
                $em2->createQuery('DELETE FROM AdminBundle:FbPosts')->execute();
            }

            for ($i = 0; $i < count($datos); $i++) {
                $objFb = new FbPosts();

                //$textoPublicacion = $datos[$i]->message;
                $imagenPublicacion = "";
                $linkPublicacion = "";
                $vecesCompartidaPublicacion = 0;
                $meGustaPublicacion = 0;
                $comentariosPublicacion = 0;
                $meEncantaPublicacion = 0;

                if (isset($datos[$i]->full_picture)) {
                    $imagenPublicacion = $datos[$i]->full_picture;
                    $linkPublicacion = $datos[$i]->link;

                    if(preg_match("/.gif/", $imagenPublicacion, $matches) && preg_match("/.gif/", $linkPublicacion, $matches)){
                        $imagenPublicacion = $datos[$i]->link;
                    }
                }

                if (isset($datos[$i]->link)) {
                    $linkPublicacion = $datos[$i]->link;
                }

                if (isset($datos[$i]->shares)) {
                    $vecesCompartidaPublicacion = $datos[$i]->shares->count;
                }

                if (isset($datos[$i]->likes)) {
                    $meGustaPublicacion = $datos[$i]->likes->summary->total_count;
                }

                if (isset($datos[$i]->comments)) {
                    $comentariosPublicacion = $datos[$i]->comments->summary->total_count;
                }

                if (isset($datos[$i]->reactions)) {
                    $meEncantaPublicacion = $datos[$i]->reactions->summary->total_count;
                }

                $objFb->setId($i+1);
                $objFb->setImagenPublicacion($imagenPublicacion);
                $objFb->setLinkPublicacion($linkPublicacion);
                $objFb->setVecesCompartidaPublicacion($vecesCompartidaPublicacion);
                $objFb->setMeGustaPublicacion($meGustaPublicacion);
                $objFb->setComentariosPublicacion($comentariosPublicacion);
                $objFb->setMeEncantaPublicacion($meEncantaPublicacion);

                $manager = $em->getManager();
                $manager->persist($objFb);
                $manager->flush();
            }

            //$this->locals['fbposts'] = $arregloPublicaciones;

            //$result = array('state' => true, 'data' => $arregloPublicaciones, 'message' => 'Lectura correcta de WebService');
            $result = array('state' => true, 'data' => 'Lectura correcta de WebService');
        } catch (\Exception $e) {
            $result = array('state' => false, 'data' => $e->getMessage());
        }

       return new JsonResponse($result);
    }

    /**
     * @Route("/cargar-departamento/", name="cargardepartamento")
     * @Method("GET")
     * @Template()
     */
    public function cargardepartamentoAction(Request $request){
      $em = $this->getDoctrine();

      $provincias = $em->getRepository('AdminBundle:Departamento')->findBy(
        array(), //where
        array('nombre' => 'ASC') //order by
      );
      $array = array();

      foreach ($provincias as $key => $value) {
        $subarray = array("id" => $value->getId(), "nombre" => $value->getNombre());
        array_push($array, $subarray);
      }

      $result = array('state'=>true, 'data'=> $array);

      return new JsonResponse($result);
    }

    /**
     * @Route("/cargar-provincia/", name="cargarprovincia")
     * @Method("POST")
     * @Template()
     */
    public function cargarprovinciaAction(Request $request){
      $em = $this->getDoctrine();
      $depId = $request->request->get('param');

      if ($depId) {
         $provincias = $em->getRepository('AdminBundle:Provincia')->findByDepartamento($depId , array('nombre' => 'ASC'));
      }else{
         $provincias = $em->getRepository('AdminBundle:Provincia')->findBy(array(), array('nombre' => 'ASC'));
      }
      $array = array();

      foreach ($provincias as $key => $value) {
        $subarray = array("id" => $value->getId(), "nombre" => $value->getNombre());
        array_push($array, $subarray);
      }

      $result = array('state'=>true, 'data'=> $array);

      return new JsonResponse($result);
    }

    private function calculoDescuento($existeCupon, $itemArray){
      $idCupon = $existeCupon["id"];
      $tipoDescuento = $existeCupon["descuentoEnPorcentaje"];
      $descuento = $existeCupon["descuento"];

      $subTotalProducto = $itemArray["subTotalProducto"];

      if($tipoDescuento){ //Si es true es descuento porcentual
        $subTotalProducto = $subTotalProducto - (($subTotalProducto * $descuento) / 100);
      }else{ //En soles
        $subTotalProducto = $subTotalProducto - $descuento;
      }

      $itemArray["subTotalProducto"] = $subTotalProducto;
    }

    /**
     * @Route("/agregar-cupon/", name="agregarcupon")
     * @Method("POST")
     */
    public function agregarcuponAction(Request $request){
      $numCupon = $request->request->get('numCupon');

      $idProducto = json_decode(stripcslashes($request->request->get('idProducto')));
      // $productos = explode(",", $idProducto[1]);
      $session = $this->get('session');
      $clienteSesionActual = $session->get('idCliente');

      $em = $this->getDoctrine();
      $em2 = $em->getManager();
      ///////
      $result = array('state'=>true, 'data'=> 'Incorrecto');

      if($clienteSesionActual){ //Si existe una sesión
        $instanceOfCupon = $em->getRepository('AdminBundle:Cupon')->findBy(
          array("codigo" => $numCupon, "activo" => 1) //where
        );

        $contador = 0;

        if(!$instanceOfCupon){
          $result = array('state' => false, 'data' => $idProducto);
        }else{
          $this->sessionVal = $session->get('idCupon');
          $array = array();

          // se evalua si hay algun cupon activo
            foreach ($idProducto as $idProd) {

                $request = $em2->getRepository('AdminBundle:Producto');

                $query = $request->createQueryBuilder("P");
                $query
                ->select('C.id, C.descuentoEnPorcentaje, C.descuento')
                ->innerJoin('P.cupon', 'C')
                ->where('P.id = :id AND C.activo = :estado AND C.codigo = :cupon')
                ->setParameter('cupon', $numCupon)
                ->setParameter('estado', 1)
                ->setParameter('id', $idProd);

                $result = $query->getQuery();
                $existeCupon = $result->getOneOrNullResult();

                if ($existeCupon) {
                    $contador++;
                }else{
                  $request = $em2->getRepository('AdminBundle:Producto');
                  $query = $request->createQueryBuilder("P");
                  $query
                  ->select('C.id, C.descuentoEnPorcentaje, C.descuento')
                  ->innerJoin('P.subcategoriaProd', 'S')
                  ->innerJoin('S.cupon', 'C')
                  ->where('P.id = :id AND C.activo = :estado AND C.codigo = :cupon')
                  ->setParameter('cupon', $numCupon)
                  ->setParameter('estado', 1)
                  ->setParameter('id', $idProd);

                  $result = $query->getQuery();
                  $existeCupon = $result->getOneOrNullResult();

                    if ($existeCupon) {
                        $contador++;
                    } else {
                      $request = $em2->getRepository('AdminBundle:Producto');
                      $query = $request->createQueryBuilder("P");
                      $query
                      ->select('C.id, C.descuentoEnPorcentaje, C.descuento')
                      ->innerJoin('P.subcategoriaProd', 'S')
                      ->innerJoin('S.categoria', 'Ct')
                      ->innerJoin('Ct.cupon', 'C')
                      ->where('P.id = :id AND C.activo = :estado AND C.codigo = :cupon')
                      ->setParameter('cupon', $numCupon)
                      ->setParameter('estado', 1)
                      ->setParameter('id', $idProd);

                      $result = $query->getQuery();
                      $existeCupon = $result->getOneOrNullResult();
                      if ($existeCupon) {
                        $contador++;
                      }
                    }
                 //fin de este else
                }
            }
        }

          //compruebo que hay un producto relacionado con el cupon
          if ($contador >= 1) {
            foreach ($instanceOfCupon as $key => $value) {
              $subarray = array(
                "id" =>$value->getId(),
                "codigo" => $value->getCodigo(),
                "descuento" => $value->getDescuento(),
                "tipo" => $value->getDescuentoEnPorcentaje());
                array_push($array, $subarray);
            }

            $carrito = $session->get('carrito');
            $session->set('idCupon', $instanceOfCupon);
            $cupon = $session->get('idCupon');

            $result = array('state' => true, 'data' => $array);
        }


      }else{ //Está todo en caché
        //1. Se evalúa si existe el cupón.
        $instanceOfCupon = $em->getRepository('AdminBundle:Cupon')->findBy(
          array("codigo" => $numCupon, "activo" => 1) //where
        );

        if(!$instanceOfCupon){
          $result = array('state' => false, 'data' => 'No existe el cupón ingresado');
        }else{
          //2. Se evalua si el cupon existe en el carrito
          $this->sessionVal = $session->get('carrito');
          $array = array();
          $contador = 0;

          for ($i = 0; $i < count($this->sessionVal); $i++) {
            $idProducto = $this->sessionVal[$i]['idProducto'];

            $request = $em2->getRepository('AdminBundle:Producto');

            $query = $request->createQueryBuilder("P");
            $query
            ->select('C.id, C.descuentoEnPorcentaje, C.descuento')
            ->innerJoin('P.cupon', 'C')
            ->where('P.id = :id AND C.activo = :estado AND C.codigo = :cupon')
            ->setParameter('cupon', $numCupon)
            ->setParameter('estado', 1)
            ->setParameter('id', $idProducto);

            $result = $query->getQuery();
            $existeCupon = $result->getOneOrNullResult();

            if ($existeCupon) {
                $contador++;
            }else{
                $request = $em2->getRepository('AdminBundle:Producto');
                $query = $request->createQueryBuilder("P");
                $query
                ->select('C.id, C.descuentoEnPorcentaje, C.descuento')
                ->innerJoin('P.subcategoriaProd', 'S')
                ->innerJoin('S.cupon', 'C')
                ->where('P.id = :id AND C.activo = :estado AND C.codigo = :cupon')
                ->setParameter('cupon', $numCupon)
                ->setParameter('estado', 1)
                ->setParameter('id', $idProducto);

                $result = $query->getQuery();
                $existeCupon = $result->getOneOrNullResult();
                if ($existeCupon) {
                  $contador++;
                }else{
                  $request = $em2->getRepository('AdminBundle:Producto');
                  $query = $request->createQueryBuilder("P");
                  $query
                  ->select('C.id, C.descuentoEnPorcentaje, C.descuento')
                  ->innerJoin('P.subcategoriaProd', 'S')
                  ->innerJoin('S.categoria', 'Ct')
                  ->innerJoin('Ct.cupon', 'C')
                  ->where('P.id = :id AND C.activo = :estado AND C.codigo = :cupon')
                  ->setParameter('cupon', $numCupon)
                  ->setParameter('estado', 1)
                  ->setParameter('id', $idProducto);

                  $result = $query->getQuery();
                  $existeCupon = $result->getOneOrNullResult();
                  if ($existeCupon) {
                    $contador++;
                  }
                }
            }
          }

          if ($contador >= 1) {
            foreach ($instanceOfCupon as $key => $value) {
              $subarray = array("id" =>$value->getId(),"codigo" => $value->getCodigo(), "descuento" => $value->getDescuento(), "tipo" => $value->getDescuentoEnPorcentaje());
                array_push($array, $subarray);
            }
            $carrito = $session->get('carrito');
            $session->set('idCupon', $instanceOfCupon);
            $cupon = $session->get('idCupon');

            for ($i = 0; $i < count($carrito); $i++) {
                $idProducto = $carrito[$i]['idProducto'];
                $producto = $em->getRepository('AdminBundle:Producto')->findOneById($idProducto);
                $stock = $producto->getStock();
                //$stock = $producto->getEnStock();
                $imagen = $producto->getImagen();
                $nombre = $producto->getNombreProducto();
                $clienteSesionActual = $session->get('idCliente');
                $cupon = $session->get('idCupon');

              if ($producto->getCupon()->getId() == $cupon[0]->getId()) {
               if ($cupon) {
                  if ($carrito[$i]['idCupon'] == null) {
                      $carrito[$i]['idCupon'] = $array[0]['id'];
                  }
                }
            }

            $session->set('carrito',$carrito);
          }
          $result = array('state' => true, 'data' => $array);
        }else{
          $result = array('state' => false, 'data' => "NO APLICA A NINGUN PRODUCTO");
        }
      }
    }

    return new JsonResponse($result);
  }

    /**
     * @Route("/actualizar-carrito/", name="actualizarcarrito")
     * @Method("POST")
     */
    public function actualizarcarritoAction(Request $request)
    {
        // dump($request);
        // exit();
       $id         = json_decode(stripcslashes($request->request->get('id')));
       $precios    = json_decode(stripcslashes($request->request->get('precio')));
       $pedidos    = json_decode(stripcslashes($request->request->get('pedido')));
       $cantidadId = count($id);

       $result = array('state'=>true, 'data'=> 'Incorrecto');

       $em = $this->getDoctrine();
       $em2 = $em->getManager();
       $session = $this->get('session');
       $clienteSesionActual = $session->get('idCliente');

          if ($clienteSesionActual)  {
             for ($i=0 ; $i < $cantidadId ; $i++) {
               $request = $em2->getRepository('AdminBundle:Carrito');

              $query = $request->createQueryBuilder("C");
              $query
                  ->select('C')
                  ->where('C.cliente = :clienteid AND C.presentacion = :productoid')
                  ->setParameter('clienteid', $clienteSesionActual)
                  ->setParameter('productoid' , $id[$i]);
                  $result = $query->getQuery();

                  $carritoProductosActual = $result->getResult();

                  if ($carritoProductosActual) {
                    foreach ($carritoProductosActual as $carrito) {
                      $precioRealNuevo = $pedidos[$i] * $precios[$i];
                      $carrito->setCantidad($pedidos[$i]);
                      $carrito->setPrecioReal($precioRealNuevo);
                      $em2->flush();
                    }
                  }

                 $result = array('state'=>true, 'data'=> $carritoProductosActual);
          }

        }else {

               $this->sessionVal = $session->get('carrito');

                 for ($i = 0; $i < count($id); $i++) {
                   $precioRealNuevo = $pedidos[$i] * $precios[$i];

                    if($this->sessionVal[$i]["idProducto"] == $id[$i]){ //Si existe el ID en el arreglo de productos, se actualiza la cantidad comprada.
                      $this->sessionVal[$i]['cantidadProducto'] = $pedidos[$i];
                      $this->sessionVal[$i]['subTotalProducto'] = $precioRealNuevo;
                      $result = array('state'=>true, 'data'=> $id[$i]);
                      //break;
                    }
                  }
              $session->set('carrito', $this->sessionVal);
          }

        return new JsonResponse($result);
    }

    /**
     * @Route("/cargar-distrito/", name="cargardistrito")
     * @Method("POST")
     * @Template()
     */
    public function cargardistritoAction(Request $request)
    {
      $em = $this->getDoctrine();

      $provId = $request->request->get('param');

      if($provId){ //Si no es nulo
        $distritos = $em->getRepository('AdminBundle:Distrito')->findByProvincia($provId, array('nombre' => 'ASC')); //order by
      }else{
        $distritos = $em->getRepository('AdminBundle:Distrito')->findAll();
      }

      $array = array();

      foreach ($distritos as $key => $value) {
        $subarray = array("id" => $value->getId(), "nombre" => $value->getNombre());
        array_push($array, $subarray);
      }

      $result = array('state'=>true, 'data'=> $array);

      return new JsonResponse($result);
    }

    /**
     * @Route("/paypal-form/", name="paypal")
     * @Method("GET")
     * @Template()
     */
    public function paypalAction(Request $request){
      $txn_id = $request->request->get('txn_id');
      $payment_status = $request->request->get('payment_status');
    //   dump($txn_id);
    //   dump($payment_status);
      exit();
    }

    /**
     * @Route("/paypal-form/", name="paypal-form")
     * @Method("POST")
     */
    public function paypalpostAction(Request $request){

      $envio = $request->request->get('option_name1');
      $codigo = $request->request->get('option_name2');
      // return new JsonResponse(array("state" => 'ok', "data" => $codigo));

      $ma = $this->getDoctrine()->getManager();

      $pedido  = $ma->getRepository('AdminBundle:Pedido')->findOneByCodigo($codigo);
      $carrito = $ma->getRepository('AdminBundle:Carrito')->findByPedido($pedido->getId());

      if($pedido){

        $pedido->setEstado("C");
        $ma->flush();

        foreach ($carrito as $car) {
            $car->setEstado("C");
            $ma->flush();

            $presentacionid = $car->getPresentacion()->getId();
            $cantidad = $car->getCantidad();

            $presentacion = $ma->getRepository('AdminBundle:Presentacion')->findOneById($presentacionid);
            $stock = $presentacion->getStock();
            $stock_actual = $stock - $cantidad;

            $presentacion->setStock($stock_actual);
            $ma->flush();
        }

      }

     $session = $this->get('session');
     $session->remove('idCupon');
     return $this->render('WebBundle:Default:graciasporcomprar.html.twig');
    }

    /**
     * @Route("/perfil", name="perfil")
     * @Method("GET")
     * @Template()
     */
    public function micuentaAction(){

      $session = $this->get('session');
      $idCliente = $session->get('idCliente');

      if (!$idCliente) {
        return $this->redirect("/");
      }

      $em = $this->getDoctrine();

      $this->locals['perfil'] = $em->getRepository('AdminBundle:Clientes')->findOneById($idCliente);

      return $this->locals;


    }

    /**
     * @Route("/pedidos", name="pedidos")
     * @Method("GET")
     * @Template()
     */
    public function mispedidos(){
      $session = $this->get('session');
      $idCliente = $session->get('idCliente');

      if (!$idCliente) {
        return $this->redirect("/");
      }

      $em = $this->getDoctrine();

      $this->locals['carrito_pedido'] = $em->getRepository('AdminBundle:Carrito')->findBy(array('cliente' => $idCliente , 'estado' => 'C'));

      return $this->locals;

    }

    /**
     * @Route("/informacion-personal/", name="informacion-personal")
     */
    public function informacionPersonalAction(Request $request){
      $session = $this->get('session');
      $idCliente = $session->get('idCliente');

      //Si aún no está logueado
      if(!$idCliente){
         $result = array('state' => true, 'data' => 'Usuario no identificado');
         return new JsonResponse($result);
      }else{
          $nombre    = $request->request->get('nombre');
          $dni       = $request->request->get('dni');
          $distritos = $request->request->get('distrito');
          $direccion = $request->request->get('direccion');
          $fecha     = $request->request->get('fecha');

          $ma = $this->getDoctrine()->getManager();
          $cliente = $ma->getRepository('AdminBundle:Clientes')->findOneById($idCliente);
          $distrito = $ma->getRepository('AdminBundle:Distrito')->findOneById($distritos);

          $cliente->setNombres($nombre);
          $cliente->setDni($dni);
          $cliente->setDistritoCliente($distrito);
          $cliente->setFechaNacimiento($fecha);
          $cliente->setDireccion($direccion);

          $ma->flush();

          $response = $fecha;
          return new JsonResponse($response);
      }
    }

    /**
     * @Route("/actualizar-cuenta/", name="actualizar-cuenta")
     */
    public function actualizarCuentaAction(Request $request){
      $session = $this->get('session');
      $idCliente = $session->get('idCliente');

      //Si aún no está logueado
      if(!$idCliente){
         $response = "Usuario no identificado";
         return $response;
      }else{
          $correo   = $request->request->get('correo');
            if ($correo != "" || $correo != null) {
              $ma = $this->getDoctrine()->getManager();
              $cliente = $ma->getRepository('AdminBundle:Clientes')->findOneById($idCliente);

              $cliente->setEmail($correo);
              $ma->flush();

              $response = "bien";
              return new JsonResponse($response);
          }else{
              $response = "error";
              return new JsonResponse($response);
          }

          //return $this->render("WebBundle:Default:carritocompras.html.twig");
      }
    }

    /**
     * @Route("/cambiar-contrasena/", name="cambiar-contrasena")
     */
    public function cambiarContrasena(Request $request){
      $session = $this->get('session');
      $idCliente = $session->get('idCliente');
      //Si aún no está logueado
      if(!$idCliente){
        $response = "error0";
        return $response;
      }else{
        $actual = $request->request->get('actual');
        $nueva = $request->request->get('nueva');
        $confirmar = $request->request->get('confirmar');

        $ma = $this->getDoctrine()->getManager();
        $cliente = $ma->getRepository('AdminBundle:Clientes')->findOneById($idCliente);

        if (password_verify($actual, $cliente->getPassword())) {
            if ($nueva !== $actual) {
                if ($nueva == $confirmar) {
                      $hashed = password_hash($nueva , PASSWORD_DEFAULT);
                      $cliente->setPassword($hashed);
                      $ma->flush();

                      $response = "OK";
                      return new JsonResponse($response);
                }else{
                      $response = "error2";
                      return new JsonResponse($response);
                }
            }else{
                $response = "error3";
                return new JsonResponse($response);
            }
        }else{
          $response = "error1";
          return new JsonResponse($response);
        }
      }
    }

    /**
     * @Route("/olvidaste-contrasena/", name="olvidaste-contrasena-form")
     * @Method("POST")
     * @Template()
     */
    public function olvidastecontrasenaformAction(Request $request){
      $em = $this->getDoctrine();

      $correo = $request->request->get('correo');
      $cliente = $em->getRepository('AdminBundle:Clientes')->findOneByEmail($correo);

      if($cliente){ //Existe
        $url = $request->getSchemeAndHttpHost()."/reestablecer-contrasena/".base64_encode($correo);

        $message = \Swift_Message::newInstance()
        ->setSubject('Recuperar contraseña')
        ->setFrom(array('noreply-lifeit@staffcreativa.com' => 'Staff Creativa'))
        ->setTo($correo)
        ->setBody(
           $this->renderView('WebBundle:Emails:FormRecuperarContrasena.html.twig',
               array(
               'nombres' => $cliente->getNombres(),
               'email' =>  $correo,
               'url' => $url,
               )
           ),
           'text/html'
        );

        $this->get('mailer')->send($message);

        $em = $this->getDoctrine();

        $objU = new RecuperarContrasenaUrl();
        $objU->setUrl($url);
        $objU->setCodigo(base64_encode($correo));
        $objU->setFueUtilizada(false);

        $manager = $em->getManager();
        $manager->persist($objU);
        $manager->flush();

        return $this->render('WebBundle:Default:graciascontrasena.html.twig');
      }else{ // No existe
        return $this->render('WebBundle:Default:errorcontrasena.html.twig');
      }
    }

    /**
     * @Route("/olvidaste-contrasena/", name="olvidaste-contrasena")
     * @Method("GET")
     * @Template()
     */
    public function olvidastecontrasenaAction(Request $request){
    }

    /**
     * @Route("/eliminar-cupon/", name="eliminar-cupon")
     */
    public function eliminarCuponAction(Request $request){
      $cupon = $request->request->get("cupon");
      $session = $this->get('session');
      $cuponSession = $session->get('idCupon');

      $session->remove('idCupon');

      return new JsonResponse(array("state" => true, "data" => "Correcto"));
    }

    /**
     * @Route("/maqueta", name="maqueta")
     * @Template()
     */
    public function maquetaAction(){
    }

    /**
    * @Route("/reestablecer-contrasena/{code}", name="reestablecer-contrasena")
    * @Method("GET")
    * @Template()
    */
    public function reestablecercontrasenaAction($code)
    {
      try {
        $em = $this->getDoctrine()->getManager();

        $url = $em->getRepository("AdminBundle:RecuperarContrasenaUrl")->findOneByCodigo($code);

        if($url){
          $current = new \DateTime();
          $delta = 3600; // 1 hora de tiempo
          $diferencia = abs(date_timestamp_get($current) - date_timestamp_get($url->getFechaCreacion()));

          if($diferencia < $delta){ //Si la Url aun no muere su tiempo de vida
            $correo = base64_decode($code);
            $cliente = $em->getRepository("AdminBundle:Clientes")->findByEmail($correo);

            if($cliente){ //El cliente existe
              $this->locals["code"] = $code;

              return $this->locals;
            }else{ //El cliente no existe
              return $this->redirect("/");
            }
          }else{ //Si ya pasó su tiempo de vida entonces se elimina de la BD
            $em->remove($url);
            $em->flush();

            return $this->redirect("/");
          }
        }else{
          return $this->redirect("/");
        }
      } catch (Exception $e) {
        $this->get('session')->getFlashBag()->add('error', $e->getMessage());
        $this->get('logger')->error($e->getMessage());
      }
    }

    /**
    * @Route("/reestablecer-contrasena-form/", name="reestablecer-contrasena-form")
    * @Method("POST")
    */
    public function reestablecercontrasenaformAction(Request $request)
    {
      try {
        $em = $this->getDoctrine()->getManager();

        $pass = $request->request->get("pass");
        $reppass = $request->request->get("reppass");
        $code = $request->request->get("code");

        $correo = base64_decode($code);
        $cliente = $em->getRepository("AdminBundle:Clientes")->findOneByEmail($correo);

        if($cliente){ //El cliente existe
          $nuevopass = password_hash($pass, PASSWORD_DEFAULT);
          $cliente->setPassword($nuevopass);
          $em->flush();

          $url = $em->getRepository("AdminBundle:RecuperarContrasenaUrl")->findOneByCodigo($code);
          $em->remove($url);
          $em->flush();

          return new JsonResponse(array("state" => true, "data" => "OK"));
        }else{ //El cliente no existe
          return $this->redirect("/");
        }
      } catch (Exception $e) {
        $this->get('session')->getFlashBag()->add('error', $e->getMessage());
        $this->get('logger')->error($e->getMessage());
        return new JsonResponse(array("state" => false, "data" => $e->getMessage()));
      }
    }

    /**
     * @Route("/calcular-precios/", name="calcular-precios")
     */
    public function calcularPreciosAction(Request $request)
    {
      $precios = json_decode(stripcslashes($request->request->get('precio')));
      $pedidos = json_decode(stripcslashes($request->request->get('pedido')));
      $descuento = $request->request->get('descuento');
      $tipo = $request->request->get('tipo');
      $cantidadProductos = count($precios);
      $session = $this->get('session');
      $cupon = $session->get('idCupon');

      if ($cupon) {
        $totalFinal = 0;
        $descuentaso = $cantidadProductos * $descuento;
        for($i = 0 ; $i < $cantidadProductos ; $i++){
            $total = $precios[$i] * $pedidos[$i];
            $totalFinal = $totalFinal + $total;
        }

         return new JsonResponse(array("state" => true,
          "data" => $totalFinal,
          "descuento"=> $descuentaso));
      }else{
         $totalFinal = 0;
         for($i = 0 ; $i < $cantidadProductos ; $i++){
          $total = $precios[$i] * $pedidos[$i];
          $totalFinal = $totalFinal + $total;
         }

          return new JsonResponse(array("state" => true,
            "data" => $totalFinal));
      }
    }

    /**
     * @Route("/datos-carrito/", name="datos-carrito")
     */
    public function enviarDatosCarrito(Request $request){

      $totalCarrito = $request->request->get("total");
      $descuentoCarrito = $request->request->get("descuento");
      $carritoPedido = array('total' => $totalCarrito, 'descuento' => $descuentoCarrito);

      $session = $this->get('session');

      $session->set('datosCarrito', $carritoPedido);

      $result = true;

      return new JsonResponse($result);
    }

    /**
     * @Route("/generar-puntuacion/", name="generar-puntuacion")
     */
    public function generarPuntuacion(Request $request){

      $valor = $request->request->get("valor");
      $idProducto = $request->request->get("producto");

      $em = $this->getDoctrine();
      $producto = $em->getRepository('AdminBundle:Producto')->findOneById($idProducto);

      $ranking = new Ranking();
      $ranking->setPuntuacion($valor);
      $ranking->setProducto($producto);

      $manager = $em->getManager();
      $manager->persist($ranking);
      $manager->flush();

      $request = $em->getRepository('AdminBundle:Ranking');

      $query = $request->createQueryBuilder('R');
      $query
           ->select("avg(R.puntuacion) as ranking, count(R.puntuacion) as cantidad")
           ->innerJoin('R.producto', 'P')
           ->where('R.producto = :idProducto')
           ->setParameter('idProducto', $idProducto);

      $resultado = $query->getQuery();
      $resultados = $resultado->getResult();

      $puntuacion = $resultados[0]['ranking'];

      $ema = $this->getDoctrine()->getManager();
      $product = $ema->getRepository('AdminBundle:Producto')->find($idProducto);

      if ($product) {
         $product->setRanking($puntuacion);
         $ema->flush();
      }

      $result = true;

      return new JsonResponse($result);

    }

    /**
     * @Route("/resultados/", name="resultados")
     */
    public function resultadoAction(){
      $app_id = 'cc1db8bb3e5143a28da46f28f1428ebe';
      $oxr_url = "https://openexchangerates.org/api/latest.json?app_id=" . $app_id;

      // Open CURL session:
      $ch = curl_init($oxr_url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      // Get the data:
      $json = curl_exec($ch);
      curl_close($ch);

      // Decode JSON response:
      $oxr_latest = json_decode($json);
      $aproximado = round($oxr_latest->rates->PEN , 2 );
      // You can now access the rates inside the parsed object, like so:
      // printf(
      //     "1 %s IGUAL %s NUEVOS SOLES at %s",
      //     $oxr_latest->base,
      //     $aproximado = round($oxr_latest->rates->PEN , 2 ),
      //     date('H:i jS F, Y', $oxr_latest->timestamp)
      // );

      $em = $this->getDoctrine()->getManager();

      $info = $em->getRepository('AdminBundle:Info')->findOneById(1);
      $info->setValordeCambio($aproximado);
      $em->flush();

      return new JsonResponse("OK");

    }

     /**
     * @Route("/cargar-productos/", name="cargar-productos")
     * @Template()
     */
     public function dataAction(Request $request){

        $em = $this->getDoctrine();
        $em2 = $em->getManager();

        $current = $request->request->get("actual");

        $slug_categoria = $request->request->get("slug_cat");
        $slug_subcategoria = $request->request->get("slug_subcat");

        //para traer mas productos

        if ($slug_categoria && ($slug_subcategoria == "" || $slug_subcategoria == null )) {

                    $request = $em2->getRepository("AdminBundle:Producto");
                    $query = $request->createQueryBuilder("P");
                            $query->select('P')
                                        ->innerJoin('P.subcategoriaProd', 'S')
                                        ->innerJoin('S.categoria' , 'C')
                                        ->where('C.slug = :slug')
                                        ->setParameter('slug', $slug_categoria)
                                        ->orderBy('P.id');
                                    ;
                     $result = $query->getQuery();
                     $products  = $result->getResult();

                     $array_productos = array();
                     $i = 1;

                     foreach ($products as $key => $product) {

                           $presentations = $product->getPresentacion();

                           if ($presentations) {
                                    $cantidad = 0;
                               foreach ($presentations as $key1 => $presentation)
                               {
                                   if ($presentation->getStock() > 0) {
                                       $cantidad++;
                                   }

                               }

                               if ($cantidad > 0) {
                                   array_push($array_productos, $product);
                               }
                           }
                     }
                    //  dump($array_productos);
                    //  dump(count($array_productos));
                    //  dump(count(array_slice($array_productos, $current , 3)));
                    //  exit();

              //para saber cuantos productos existen
            //    $query2 = $request->createQueryBuilder("P");
            //    $query2->select('P')
            //           ->innerJoin('P.subcategoriaProd', 'S')
            //           ->innerJoin('S.categoria' , 'C')
            //           ->where('C.slug = :slug')
            //           ->setParameter('slug', $slug_categoria)
            //           ->groupBy('P.id');
               //
            //    $result2 = $query2->getQuery();

            //  $totalProductos = count($result2->getResult()) - $current;
            //  dump($current);
            //  dump(count($result2->getResult()));
            //  dump($totalProductos);
            //  exit();

        } else {

            $request = $em2->getRepository("AdminBundle:Producto");
                         $query = $request->createQueryBuilder("P");
                         $query->select('P')

                              ->innerJoin('P.subcategoriaProd', 'S')
                              ->innerJoin('S.categoria' , 'C')
                              ->where(' C.slug = :slug and S.slug = :slug2')
                              ->setParameter('slug', $slug_categoria)
                              ->setParameter('slug2', $slug_subcategoria)
                              ->groupBy('P.id');



                          $result = $query->getQuery();

                          //////////////////////////////////////////////////////////////// $products  = $result->getResult();
                          $products  = $result->getResult();

                                        $array_productos = array();
                                        $i = 1;
                                        foreach ($products as $key => $product) {

                                              $presentations = $product->getPresentacion();

                                              if ($presentations) {
                                                      $cantidad = 0;
                                                  foreach ($presentations as $key1 => $presentation)
                                                  {
                                                      if ($presentation->getStock() > 0) {
                                                          $cantidad++;
                                                      }

                                                  }

                                                  if ($cantidad > 0) {
                                                      array_push($array_productos, $product);
                                                  }
                                              }
                                        }


              //para saber cuantos productos existen
            // $query2 = $request->createQueryBuilder("P");
            // $query2->select('P')
            //          ->innerJoin('P.subcategoriaProd', 'S')
            //          ->innerJoin('S.categoria' , 'C')
            //          ->where(' C.slug = :slug and S.slug = :slug2')
            //          ->setParameter('slug', $slug_categoria)
            //          ->setParameter('slug2', $slug_subcategoria)
            //          ->groupBy('P.id');
            //
            //   $result2 = $query2->getQuery();
            //   $totalProductos = count($result2->getResult()) - $current;
        }

        $array = array();

        // $array["total"] = $totalProductos;
        // dump($current);
        // dump(count($array_productos));
        // dump( count(array_slice($array_productos, $current , 3)));
        // exit();
        $this->locals['listado'] = array_slice($array_productos, $current , 3);
        $this->locals['colecciones'] = array_slice($array_productos, $current , 3);

        return $this->locals;

    }

    /**
     * @Route("/busquedas/", name="busquedas")
     * @Method("POST")
     */
    public function busquedasAction(Request $request){


          $em = $this->getDoctrine();

          $valor = $request->request->get('buscador');
          $valor_final = strtolower($valor);
          $request = $em->getRepository('AdminBundle:Producto');

          $query = $request->createQueryBuilder('P');
          $query
            ->select('P, S , C')
            ->innerJoin('P.subcategoriaProd' , 'S')
            ->innerJoin('S.categoria' , 'C')
            ->where('LOWER(P.nombreProducto) LIKE :data')
            ->setParameter('data', '%'.$valor_final.'%')
            ->orderBy('P.id', 'ASC')
            ->setMaxResults(6);


          $result = $query->getQuery();
          $this->locals['productosEncontrados'] = $result->getResult();

          $session = $this->get('session');

          if ( count($this->locals['productosEncontrados']) == 0 ) {

            $session->set('rBusqueda', null);
          } else {

            $session->set('rBusqueda' ,  $this->locals['productosEncontrados']);

          }
           return $this->redirect('/categorias?q='.$valor_final);
        // dump($valor_final , $this->locals['productosEncontrados']);
        // dump($this->locals['productosEncontrados'][0]->getSubcategoriaProd()-> getSlug());
        // dump(count($this->locals['productosEncontrados']));
        // exit();
    }

    /**
     * @Route("/cargar-productos-busqueda/", name="cargar-productos-busqueda")
     * @Template()
     */
     public function datasaAction(Request $request){

        $em = $this->getDoctrine();
        $em2 = $em->getManager();

        $current = $request->request->get("actual");
        $current = $request->request->get("actual");
        $parameter = $request->request->get("parameter");
        $final_parameter = strtolower($parameter);

            //para traer mas productos

            $request = $em->getRepository("AdminBundle:Producto");
            $query = $request->createQueryBuilder("P");
            $query  ->select('P')
                    ->where('LOWER(P.nombreProducto) LIKE :data')
                    ->setParameter('data', '%'.$final_parameter.'%')
                    ->orderBy('P.id');

              $result = $query->getQuery();

              $products  = $result->getResult();

              $array_productos = array();
              $i = 1;

              foreach ($products as $key => $product) {

                    $presentations = $product->getPresentacion();

                    if ($presentations) {
                             $cantidad = 0;
                        foreach ($presentations as $key1 => $presentation)
                        {
                            if ($presentation->getStock() > 0) {
                                $cantidad++;
                            }

                        }

                        if ($cantidad > 0) {
                            array_push($array_productos, $product);
                        }
                    }
              }
            // $request = $em->getRepository("AdminBundle:Presentacion");
            // $query = $request->createQueryBuilder("Pre");
            // $query->select('Pro.id as idProducto, Pro.nombreProducto , Pro.descripcionProducto ,  Pro.slug , Pro.imagen , Pro.porcentajeDescuento , Pro.precioOferta ,  Pro.precio , Pro.oferta , Pro.nuevo , Pro.apareceEnHome , Pro.ranking ,  S.slug as slugSudCategoria , C.slug as slugCategoria , Cu.id as idCupon')
            //      ->innerJoin('Pre.productoPre', 'Pro')
            //      ->innerJoin('Pro.cupon', 'Cu')
            //      ->innerJoin('Pro.subcategoriaProd', 'S')
            //      ->innerJoin('S.categoria' , 'C')
            //      ->where('Pre.stock > :cantidad and LOWER(Pro.nombreProducto) LIKE :data')
            //      ->setParameter('cantidad',0)
            //      ->setParameter('data', '%'.$final_parameter.'%')
            //      ->orderBy('Pro.id')
            //      ->setFirstResult($current)
            //      ->setMaxResults(3);
            //   $result = $query->getQuery();
            /////////////////////////////////////

              //para saber cuantos productos existen
            //   $request = $em2->getRepository("AdminBundle:Presentacion");
            //   $query2 = $request->createQueryBuilder("Pre");
            //   $query2->select('Pro.id as idProducto')
            //          ->innerJoin('Pre.productoPre', 'Pro')
            //          ->where('Pre.stock > :cantidad and LOWER(Pro.nombreProducto) like :data')
            //          ->setParameter('cantidad',0)
            //          ->setParameter('data', '%'.$final_parameter.'%')
            //          ->groupBy('Pro.id');
              //
            //   $result2 = $query2->getQuery();
            //   $totalProductos = count($result2->getResult()) - $current;


        $array = array();

        // $array["total"] = $totalProductos;
        $this->locals['parametros'] = $parameter.$current;
        $this->locals['listado'] =  array_slice($array_productos, $current , 3);
        $this->locals['colecciones'] = array_slice($array_productos, $current , 3);

        return $this->locals;

    }

    /**
     * @Route("/registrar-pedido/", name="registrar-pedido")
     */
    public function registraPedido(Request $request){


        //// En este metodo se tendria que realizar la verificacion del stock antes de pasar a paypal
      $formulario = $request->request->get('formulario');
      $codigo = $request->request->get("codigo");

      parse_str($formulario, $array);
      $arrayF = explode("," , $array['factura']);
      $arrayE = explode("," , $array['envio']);


       $em = $this->getDoctrine();

       if ($arrayE[5]) {
         $distrito_f = $em->getRepository('AdminBundle:Distrito')->findOneById($arrayF[5]);
         $distrito_e = $em->getRepository('AdminBundle:Distrito')->findOneById($arrayE[5]);
       }else{
         $distrito_f = $em->getRepository('AdminBundle:Distrito')->findOneById($arrayF[5]);
         $distrito_e = null;
       }

       if ($arrayF[6] == 'factura') {
          $ruc = $arrayF[8];
       } else {
          $ruc = null;
       }

       //para registrar nuevo pedido como pendiente

        try {
             $entity = new Pedido();
             $entity->setNombresCompletosFacturacion($arrayF[0]);
             $entity->setCorreoFacturacion($arrayF[1]);
             $entity->setTelefonoFacturacion($arrayF[2]);
             $entity->setDistritoFacturacion($distrito_f);
             $entity->setTipoComprobante($arrayF[6]);
             $entity->setDireccionFacturacion($arrayF[7]);
             $entity->setRuc($ruc);
             $entity->setDni($arrayF[9]);

             $entity->setNombresCompletosEnvio($arrayE[0]);
             $entity->setCorreoEnvio($arrayE[1]);
             $entity->setTelefonoEnvio($arrayE[2]);
             $entity->setDistritoEnvio($distrito_e);
             $entity->setDireccionEnvio($arrayE[6]);

             $entity->setMontoFinal($arrayE[8]);
             $entity->setDescuento($arrayE[9]);
             $entity->setTiempoEnvio($array['fecha']);
             $entity->setCodigo($codigo);
             $entity->setEstado("P");

             $manager = $em->getManager();
             $manager->persist($entity);
             // dump($entity->getId());
             // exit();
             $manager->flush();

      $session = $this->get('session');
      $idCliente = $session->get('idCliente');

      $ma = $this->getDoctrine()->getManager();

      $cupon = $ma->getRepository('AdminBundle:Cupon')->findOneById($arrayE[7]);

      $carrito = $ma->getRepository('AdminBundle:Carrito')->findBy(array('cliente' => $idCliente , 'estado' => 'P'));

      $pedido = $ma->getRepository('AdminBundle:Pedido')->findOneById($entity->getId());

        foreach ($carrito as $car) {
                $car->setCupon($cupon);
                $car->setPedido($pedido);
                $car->setEstado("P");
                $ma->flush();
        }

      //     $this->addFlash('success', 'Se ha registrado correctamente');
      } catch (\Exception $e) {
          $this->get('session')->getFlashBag()->add('error', $e->getMessage());
          $this->get('logger')->error($e->getMessage());
      }

     return new JsonResponse(array('state'=> true , 'data' => $array['fecha'] ));
    }
    /**
     * @Route("/registrar-proyectotv/", name="proyectotv")
     *@Method({"POST"})
     */

    public function proyectoTvContacto(Request $request)
    {
        $em = $this->getDoctrine();
        $this->locals['detalle'] = $em->getRepository('AdminBundle:Info')->find(1);
        $correosContacto = explode(",", $this->locals['detalle']->getCorreosContacto());
      try {

          $fecha = new \DateTime($request->request->get('fecha_nacimiento'));
          $formateada = $fecha->format('d-m-Y');

          $obj = new ProyectoTv();
          $obj->setDe($request->request->get('de'));
          $obj->setEmail($request->request->get('email'));
          $obj->setTelefono($request->request->get('telefono'));
          $obj->setFechacimiento($fecha);
          $file = $request->files->get('archivo');
          $ext = $file->guessExtension();
          $file_name = time().".".$ext;
          $file->move("uploads" , $file_name);
          $obj->setPdfDocumento("uploads/".$file_name);

          $manager = $em->getManager();
          $manager->persist($obj);
          $manager->flush();

          $message = \Swift_Message::newInstance()
                   ->setSubject("Proyecto Tv")
                   ->setFrom(array('noreply-lifeit@screativa.com' => 'Proyecto Tv'))
                   ->setTo($correosContacto)
                   ->setBody(
                       $this->renderView('WebBundle:Emails:FormProyectoContacto.html.twig',
                           array(
                               'de'               => $request->request->get('de'),
                               'email'            => $request->request->get('email'),
                               'telefono'         => $request->request->get('telefono'),
                               'fecha_nacimiento' => $formateada
                           )
                       ),
                       'text/html'
                   );

           $this->get('mailer')->send($message);

         return $this->render('WebBundle:Default:gracias.html.twig');

        } catch (Exception $e) {
          throw new Exception("Error Processing Request", $e->getMessage());

        }

    }

    /**
     * @Route("/gracias", name="gracias-form")
     * @Template()
     */
    public function graciasAction(){

    }

}
