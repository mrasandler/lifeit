<?php

namespace WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class FormController extends Controller
{
    protected $locals = array();

    public function return_list($list_email)
    {
        $list = explode(',',$list_email);
        return array_map('trim', $list);
    }

    public function envioEmail()
    {
        $twig = $this->container->get('twig')->getGlobals();

        // if ( $this->locals['email_type'] == 'contacto') {
            $lista_destinatario = $this->return_list($twig['info']->getCorreosContacto());
        // }elseif ( $this->locals['email_type'] == 'suscripcion') {
        //     $lista_destinatario = $this->return_list($twig['info']->getFormSuscripcion());
        // }else{
        //     return;
        // }

        $message = \Swift_Message::newInstance()
            ->setSubject( '[U. Autonoma] '.$this->locals['email_titulo'] )
            ->setFrom( 'webmaster@staff.digital' )
            ->setReplyTo( $this->locals['detalle']->getEmail() )
            ->setTo( $lista_destinatario )
            ->setBcc( 'webmaster@staff.digital' )
            ->setBody(
                $this->renderView(
                    $this->locals['email_template'],
                    $this->locals
                ),
                'text/html'
            )
        ;
        $this->get('mailer')->send($message);
    }

    /**
     * @Route("/pea-gestion-publica/", name="pea-gestion-publica")
     * @Template()
     */
    public function peagestionAction(Request $request)
    {
        $entity = new FormPEAGestion();

        $em = $this->getDoctrine();
        $form = $this->createFormBuilder($entity)
            ->setMethod('POST')
            ->setAction($this->generateUrl('pea-gestion-publica'))
            ->add('nombres')
            ->add('dni')
            ->add('telefono')
            ->add('email', TextType::class)
            ->add('empresa')
            ->add('cargo')
            ->add('suscripcion')
            ->add('terminos')
            ->getForm();

        if ($request->isMethod('POST')) {

            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                $this->locals['detalle'] = $entity;

                $this->locals['email_titulo'] = 'PEA Gestión Publica';
                $this->locals['email_type'] = 'pea';
                $this->locals['email_template'] = 'WebBundle:Emails:FormPEA.html.twig';

                $this->envioEmail();

                return $this->redirect( $this->generateUrl( 'mensaje-enviado' ) );
            }
        }
        $this->locals['form'] = $form->createView();
        return $this->locals;
    }

    /**
     * @Route("/evaluacion-preferente/", name="evaluacion-preferente")
     * @Template()
     */
    public function evaluacionAction(Request $request)
    {
        $entity = new FormEvaluacion();

        $distritos = array(
            '' => 'Distrito',
            'Lima' => 'Lima',
            'Ica' => 'Ica');

        $carreras = array(
            '' => 'Seleccione:',
            'opcion 1' => 'opcion 1',
            'opcion 2' => 'opcion 2');

        $em = $this->getDoctrine();
        $form = $this->createFormBuilder($entity)
            ->setMethod('POST')
            ->setAction($this->generateUrl('evaluacion-preferente'))
            ->add('carrera', ChoiceType::class, array(
                    'choices' => $carreras,
                    'required' => true
                ))
            ->add('nombres')
            ->add('dni')
            ->add('telefono')
            ->add('email', TextType::class)
            ->add('distrito', ChoiceType::class, array(
                    'choices' => $distritos,
                    'required' => true
                ))
            ->add('suscripcion')
            ->add('terminos')
            ->getForm();

        if ($request->isMethod('POST')) {

            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                $this->locals['detalle'] = $entity;

                $this->locals['email_titulo'] = 'Evaluación preferente';
                $this->locals['email_type'] = 'pea';
                $this->locals['email_template'] = 'WebBundle:Emails:FormEvaluacion.html.twig';

                $this->envioEmail();

                return $this->redirect( $this->generateUrl( 'mensaje-enviado' ) );
            }
        }
        $this->locals['form'] = $form->createView();
        return $this->locals;
    }

    /**
     * @Route("/charlas-interactivas/", name="charlas-interactivas")
     * @Template()
     */
    public function charlasAction(Request $request)
    {
        $entity = new FormCharlas();

        $charlas = array(
            '' => 'Distrito',
            'Lima' => 'Lima',
            'Ica' => 'Ica');

        $distritos = array(
            '' => 'Distrito',
            'Lima' => 'Lima',
            'Ica' => 'Ica');

        $em = $this->getDoctrine();
        $form = $this->createFormBuilder($entity)
            ->setMethod('POST')
            ->setAction($this->generateUrl('charlas-interactivas'))
            ->add('charla', ChoiceType::class, array(
                    'choices' => $charlas,
                    'required' => true
                ))
            ->add('nombres')
            ->add('dni')
            ->add('telefono')
            ->add('email', TextType::class)
            ->add('distrito', ChoiceType::class, array(
                    'choices' => $distritos,
                    'required' => true
                ))
            ->add('suscripcion')
            ->add('terminos')
            ->getForm();

        if ($request->isMethod('POST')) {

            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                $this->locals['detalle'] = $entity;

                $this->locals['email_titulo'] = 'Charlas interactivas';
                $this->locals['email_type'] = 'pea';
                $this->locals['email_template'] = 'WebBundle:Emails:FormCharlas.html.twig';

                $this->envioEmail();

                return $this->redirect( $this->generateUrl( 'mensaje-enviado' ) );
            }
        }
        $this->locals['form'] = $form->createView();
        return $this->locals;
    }

    /**
     * @Route("/form-admision/", name="form-admision")
     * @Template()
     */
    public function admisionAction(Request $request)
    {
        $em = $this->getDoctrine();
        // $this->locals['detalle'] = $em->getRepository('AdminBundle:Contacto')->find(1);
        return $this->locals;
    }

    /**
     * @Route("/mensaje-enviado/", name="mensaje-enviado")
     * @Template()
     */
    public function graciasAction(Request $request)
    {
        $em = $this->getDoctrine();
        // $this->locals['detalle'] = $em->getRepository('AdminBundle:Contacto')->find(1);
        return $this->locals;
    }
}
