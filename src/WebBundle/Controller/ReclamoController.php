<?php

namespace WebBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Departamento;
use AdminBundle\Entity\Provincia;
use AdminBundle\Entity\Distrito;
use AdminBundle\Entity\Reclamo;
use AdminBundle\Entity\Home;
use Guzzle\Http\Client;
use Guzzle\Http\EntityBody;
use Guzzle\Http\Message\Request as wsrq;
use Guzzle\Http\Message\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\VarDumper\VarDumper;

class ReclamoController extends Controller
{
    protected $locals = array();
    protected $sessionVal = array();

    /**
     * @Route("/libro-reclamaciones/", name="libro-reclamaciones")
     * @Template()
     */
    public function libroreclamacionesAction(){

    }

    /**
     * @Route("/enviar-reclamaciones/", name="enviar-reclamaciones")
     */
    public function enviarReclamo(Request $request){
        $em = $this->getDoctrine()->getManager();

        $formulario = $request->request->get('form');
        parse_str($formulario, $reclamo);

        // dump($reclamo);
        // exit();

        //Si no se marco checkbox de menor de edad su valor sera off
        $comprobar = null;

        if (isset($reclamo['menor'])) {
           $comprobar = $reclamo['menor'];
        }


        if($comprobar){

          $reclamos = new Reclamo();
          $reclamos->setTipoDocumentoApoderado($reclamo["tipodocApoderado"]);
          $reclamos->setNumeroDocumentoApoderado($reclamo["documentoApoderado"]);
          $reclamos->setNombresApoderado($reclamo["nombreApoderado"]);
          $reclamos->setApellidosApoderado($reclamo["apellidoApoderado"]);
          $reclamos->setTipoReclamo($reclamo['reclamo1']);
          $reclamos->setMensaje($reclamo["mensaje"]);

          $em->persist($reclamos);
          $em->flush();

          $rpta = array('state' => true, 'message' => 'Menor de edad');

        }else{

          $distrito = $em->getRepository('AdminBundle:Distrito')->findOneBy(array('id' => $reclamo['distrito']));

          $reclamos = new Reclamo();
          $reclamos->setTipoDocumentoReclamante($reclamo["tipodoc"]);
          $reclamos->setNumeroDocumentoReclamante($reclamo["numdoc"]);
          $reclamos->setNombresReclamante($reclamo["nombre"]);
          $reclamos->setApellidosReclamante($reclamo["apellido"]);
          $reclamos->setTelefonoReclamante($reclamo["telefono"]);
          $reclamos->setEmailReclamante($reclamo["email"]);
          $reclamos->setDistrito($distrito);
          $reclamos->setDireccion($reclamo["direccion"]);
          $reclamos->setTipoReclamo($reclamo['reclamo1']);
          $reclamos->setMensaje($reclamo["mensaje"]);

          $em->persist($reclamos);
          $em->flush();

          $rpta = array('state' => true, 'message' => 'Mayor de edad');
        }

        return new JsonResponse($rpta);

    }

}
